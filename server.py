import math
import tempfile
import os
import json
from fastapi import FastAPI, File, Request
from starlette.responses import StreamingResponse, FileResponse
import argparse
import sys
from typing import Dict, Any

import shutil
import uvicorn
import numpy as np
import base64   
import cv2
import subprocess

from test import call_parser
import multiprocessing as mp

# TODO: Currentlt, use single GPU. For multi gpu, change the CUDA_VISIBLE_DEVICES
# os.environ["CUDA_VISIBLE_DEVICES"] = "0"
app = FastAPI()

@app.get("/")
def read_root():
    return {"message": "LGAP Server Running"}

@app.post("/test")
async def test(request: Request):
    data_as_json = await request.json()
    lgs = list(data_as_json.values())
    molecules_ids = list(data_as_json.keys())
    # im_b64 = data_as_json["images"]
    # lgs = data_as_json["lgs"].split("<end lg>")
    # lgs = list(filter(('').__ne__, lgs))

    # molecules_ids_line_break = data_as_json["molecules_ids"]
    # molecules_ids = molecules_ids_line_break.split("\n")
    # molecules_ids = list(filter(('').__ne__, molecules_ids))
        
    # model_name = "chem_icdar2024_augmented_pubchem"
    model_name = "chem_icdar2024_augmented_pubchem_comp_graph_no_uniform_sampling"
    # model_directory = "outputs/weights_pipeline_chem/"
    model_directory = "outputs/weights_pipeline_chem/chem_icdar2024_augmented_pubchem_comp_graph"
    # model_directory = "outputs/run_chem_augmented_pubchem_comp_graph"
    output_path = "PIPELINE"
    if os.path.exists(output_path):
        shutil.rmtree(output_path, ignore_errors=True) 

    # epoch = 27
    epoch = 29
    number_of_processes = math.ceil(mp.cpu_count()*0.75)         
    
    list_args = [
        # "CUDA_VISIBLE_DEVICES=1",
        # "./run-parser-server",
        "test.py",        
        "--config",
        "config/defaultINFTY.yaml",
        "-m",
        model_name,
        "-md",
        model_directory,
        "-o",
        output_path,
        "-r",
        "config/run.yaml",
        "-op",
        "1",
        "-le",
        str(epoch),
        "-lo",
        "1",
        "-w",
        str(number_of_processes),
        #"0",
        # "-l",
        # lg_folder,        
        # "-i",
        # molecules_id_file,
        "-rec",
        "0",
        "-bs",
        "128",
        # "-p", "0"
    ]
    
    sys.argv = list_args
       
    print("---Running parser....")    

    predicted_lg_files = call_parser(molecules_ids,lgs,True)
    print("---Parser done")    

    return predicted_lg_files

# Create argparse function to accept port for the server
def parse_args():
    parser = argparse.ArgumentParser(description="Run the server")
    parser.add_argument("--port", type=int, default=8007, help="Port to run the server on")
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    uvicorn.run(app, host="0.0.0.0", port=args.port)
