#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 2020

@author: as1211
Modified by: Abhisek Dey
"""
import logging
import os
import sys
import json
import torch
import argparse
import operator
import numpy as np
import torch.distributed as dist
import torch.multiprocessing as mp
mp.set_sharing_strategy('file_system')
from torch.distributed.algorithms.join import Join, _JoinConfig
from functools import reduce
from torch.utils.data import DataLoader
from src.utils.utils import (get_config, save_config, update_config,
                            get_checkpoint, get_ccs)
from src.utils.log import Logger
from src.models.model import Tester, separate_outputs
from src.datasets.dataset import MathSymbolDataset , worker_init_fn, collate_fn
from src.datasets.dataloader import partition_dataset, CustomDataLoader 
from src.utils.postprocessing import postprocessGenORLg, convert_stk_predictions_to_lg
from train import set_seed
from tqdm import tqdm
import time
import csv
from protables.ProTables import write_pro_tsv, read_pro_tsv,\
                            merge_pro_tables, filter_pr_table, obj_keep_types, merge_pro_tables_torchrun

from io import StringIO
from qdgga_settings import LABELS_TRANS_CSV, MATHML_MAP_DIR
from train import set_default
# from src.utils.msp_settings import PARSE_DOT_OUT_DIR

from lgeval.src.lg import Lg
from lgeval.src.lg2txt import get_MML
from lgeval.src.lg2dot import lgDag

# Python GraphiViz 
from graphviz import Source

'''
    Convert a label graph string to a .dot string.
'''


def get_dot(lg):
    lg.hideUnlabeledEdges()
    (_, _, _, _, correctSegs, segRelDiffs) = lg.compare(lg)

    return lgDag(lg, lg, False, correctSegs, segRelDiffs)


def flattenString(inString):
    # DEBUG: tabs must be replaced by spaces for TSV files!
    # Use '\\n' and '\\t' in strings.
    return ("\"" + inString + "\""). \
        replace('\\', '\\\\'). \
        replace('\n', '\\n'). \
        replace('\t', '\\t')


def getBB(bbList):
    (minX, minY, maxX, maxY) = bbList[0]

    for bb in bbList[1:]:
        (mX, mY, MX, MY) = bb
        minX = min(minX, mX)
        minY = min(minY, mY)
        maxX = max(maxX, MX)
        maxY = max(maxY, MY)

    return (minX, minY, maxX, maxY)


class Parser(object):
    '''QG-GGA Parser Class'''

    # def __init__(self, default_config=None, run_config=None,
    #              InputData=None, model_name=None, model_dir=None,
    #              last_epoch=None):
    def __init__(self, opts):
        self.opts = opts
        self.PHASE = "test"
        self.model_name = opts.model_name
        self.InputData = opts.data.format

        # Load options
        # default_opts = get_config(default_config)
        # run_opts = get_config(run_config)[model_name][self.PHASE]
        # self.opts = update_config(default_opts, run_opts)

        # Load saved model
        self.saved_model_dir = os.path.join(opts.model_dir, self.model_name)
        # if last_epoch:
        #     self.opts.init.last_epoch = last_epoch
        # else:
        #     self.opts.init.last_epoch = "latest"

        self.opts.init.update(get_checkpoint(self.saved_model_dir, \
                self.opts.init.last_epoch, self.PHASE))
        self.opts.data.last_epoch = self.opts.init.last_epoch

    @staticmethod
    def write_dot_pdf(dotString, dot_dir, fileName, pdfName):
        # Write a dot file to disk
        dotObj = Source(dotString)
        dotPdfName = os.path.join(dot_dir, pdfName, fileName + ".pdf")

        # Use pipe() command to avoid source files created w. render()
        if not os.path.exists(os.path.join(dot_dir, pdfName)):
            os.makedirs(os.path.join(dot_dir, pdfName))

        with open(dotPdfName, 'wb') as dotPdf:
            try:
                piped = dotObj.pipe(format="pdf")
                dotPdf.write(piped)
            except Exception as x:
                logging.warning(f"Failed to generate pdf from {fileName}, for pdf {pdfName}")

    @staticmethod
    def update_pro_table(lgString, dataset, indexTuple, instances,
                         includeLg=False, includeMml=False):
        # Update PRO table in data loader with parse results.
        # ( d, p, r ) = ( dataset.currDoc, dataset.currPage, dataset.currRegion )
        # AKS: Use indexTuple passed from the calling function
        (d, p, r) = indexTuple

        # RZ: In a hurry - parsing string rather than using .lg fields and 
        #     functions, which are likely faster.
        regionObjects = dataset.proTableList[d][p][r][1]

        # Sort if instances are unordered since they come from lg file
        if len(regionObjects[0]) >= 7:
            regionObjects.sort(key=lambda x: x[6])

        newObjects = []
        newRelations = []
        for row in csv.reader(StringIO(lgString)):
            if len(row) > 0 and row[0] == 'O':
                (objId, objLabel) = row[1:3]
                objLabel = objLabel.strip()
                primitiveList = row[4:]
                primitiveInts = map(lambda x: instances.index(int(x)), primitiveList)
                bboxList = [regionObjects[i][0:4] for i in primitiveInts]
                (minX, minY, maxX, maxY) = getBB(bboxList)
                newObjects.append(
                    (minX, minY, maxX, maxY, objLabel, 'o'))

            elif len(row) > 0 and row[0] == 'R':
                (parentId, childId, relationship) = row[1:4]
                (parentId, childId, relationship) = \
                    (parentId.strip(), childId.strip(), relationship.strip())
                # Increment symbol references by 1 for TSV 1-based indexing
                parentIndex = int(parentId[3:]) + 1
                childIndex = int(childId[3:]) + 1
                newRelations.append(
                    (parentIndex, childIndex, 0, 0, relationship, 'R'))

        annotations = []
        if includeMml:
            # Hide warnings for MML translation to avoid msgs for valid symbols
            mmlString = get_MML(lgString, MATHML_MAP_DIR, LABELS_TRANS_CSV,
                                warnings=False)
            annotations.append(
                (0, 0, 0, 0, flattenString(mmlString), 'MML'))

        if includeLg:
            # Label graphs can be seen as graphs in the HTML; omit by default.
            annotations.append(
                (0, 0, 0, 0, flattenString(lgString), 'LG'))

        # Empty the list, and add outputs
        regionObjects.clear()
        regionObjects.extend(newObjects)
        regionObjects.extend(newRelations)
        regionObjects.extend(annotations)

    @staticmethod
    def write_tsv_output( loader, docIndex, tsv_output_dir, finalFormula, 
                    sortObjs=True, suffix='-out', lg=False, model_name="", last_epoch=""):
        # RZ: DEBUG -- this is used to write a single file (!)
        #              using write_pro_tsv, designed for lists.
        symbolMap = {",": "COMMA", "\"": "DQUOTE", "fraction(-)": "-"}

        if not lg:
            file_name = loader.dataset.data_files[docIndex]
            pageFiles = []
        else:
            file_name = model_name + "_" + str(last_epoch)
            # Since each lg formula is in a separate page
            pageFiles = loader.dataset.data_files.tolist()
            tsv_output_dir = os.path.join(tsv_output_dir, model_name)

        docProPairs = [ ( file_name, loader.dataset.proTableList[ docIndex ] ) ]
        docPageSizePairs = []
        if not lg:
            docPageSizePairs = loader.dataset.docs_page_sizes[docIndex:docIndex+1]
        #DEBUG
        #print("write_tsv_output Page sizes:\n" + str(docPageSizePairs))
        if loader.dataset.tsv:
            data_dir = loader.dataset.tsv_dir
        else:
            data_dir = loader.dataset.lg_dir

        # AD Mod: Set the indexes according to the dataloader
        start = loader.dataset.start_idx_triples
        stop = loader.dataset.end_idx_triples

        write_pro_tsv( tsv_output_dir, docProPairs, symbolMap, 
                docPageSizePairs, suffix, sortObjs, page_files=pageFiles, 
                page_dir=data_dir, lg=lg, start=start, stop=stop)

    def run_test(self, rank, world_size, dataset, loader,
                 dot2pdf=False, given_sym=False, given_rel=False,
                 seed=0, ddp=False):
        print(f"  Running parser on gpu {rank}...")
        set_seed(seed)

        if ddp:
            rank = int(os.environ['LOCAL_RANK'])
            world_size = dist.get_world_size()

        self.tester = Tester(self.opts.init, self.opts.net, rank, world_size)
        logger = Logger( self.opts.run_dir, rank, self.opts.init.last_epoch,
                        self.model_name, mode="test")

        ##################################
        # Parse formulas
        ##################################
        if rank == 0:
            pbar = tqdm(total=dataset.tot, desc="  Processing")
        formulasParsed = 0
        prev_doc = None
        prevDoc = None

        iteration = 0
        merge_dict = {}
        new_merge_dict = {}
        merged_files = loader.dataset.data_files

        model_dir = os.path.join(self.opts.run_dir, self.model_name)
        if not os.path.exists(model_dir):
            os.makedirs(model_dir, exist_ok=True)
        f = open(os.path.join(model_dir, f"merge_dict_test_{rank}.json"), "w")
        all_files = list(map(lambda x:x.replace(".lg", ""),
                             loader.dataset.data_files))
        while len(merged_files):
            json_string = json.dumps(merge_dict, default=set_default)
            f.write("-------------------------------------------------------------------------\n")
            f.write(f"Iteration = {iteration}")
            f.write("\n" + json_string + "\n")
            print("-------------------------------------------------------------------------\n")
            print(f"rank = {rank}, Iteration = {iteration}")
            print("----------------------")

            merged_files = []
            regionTriples = []
            # import pprint
            # pprint.pprint(merge_dict)
            # print(f"rank = {rank}, Sym Contexts = {partition.symContexts}")
            # print(f"rank = {rank}, Length Partition = {len(partition)}")
            # print(f"rank = {rank}, RegionTriples= {partition.regionTriples}")
            # import pdb; pdb.set_trace()
            
            # print(f'Iteration {iteration} Rank {rank} region Triples: \n {loader.dataset.regionTriples}')
            new_merge_dict = dict.fromkeys(all_files, False)
            for data in logger(loader):
                # RZ: Access invokes pre-processing
                # !! QD-GGA parser execution
                try:
                    if not iteration:
                        for fileName in data["file_name"]:
                            if fileName not in merge_dict:
                                merge_dict[fileName] = data['merge_dict'][fileName]
                    
                    # print(f"rank={rank}, filename = {data['file_name']}")
                    # print(f"rank={rank}, merge_idx = {data['merge_idx']}")
                    (symProb, segProb, relProb) = self.tester.testStkPairChild3(
                                                    data["images"], data["pos_enc"],
                                                    data["num_primitives"], data["num_edges"],
                                                    data["num_features"], data["graph_rel_edges"],
                                                    data["graph_seg_edges"], data["graph_rel_seg_edges"],
                                                    data["graph_los_edges"], 
                                                    data["image_shape"], data["file_name"])

                    num_primitives = data["num_primitives"]
                    pair_stk_lengths = [len(edges) for edges in data["graph_rel_edges"]]
                    pair_stk_lengths.insert(0, 0)

                    # Separate the outputs from the batch output
                    symProb, symbols = separate_outputs([symProb, data["symbols"].numpy()],
                                                        lengths=num_primitives)
                    if relProb.size != 0:
                        relProb = separate_outputs(relProb, lengths=pair_stk_lengths)

                    if not loader.dataset.node_seg:
                        if not loader.dataset.prune_seg:
                            seg_lengths = pair_stk_lengths
                        else:
                            seg_lengths = [len(edges) for edges in data["graph_seg_edges"]]
                            seg_lengths.insert(0, 0)
                    # If segments is equal to no. of primitives
                    else:
                        seg_lengths = num_primitives

                    segs = separate_outputs(data["segments"].numpy(), 
                                            lengths=seg_lengths)
                    segProb = separate_outputs(segProb, lengths=seg_lengths)

                    symbols_, segs_, rels_ = ([symbol.argmax(1) for symbol in symProb],
                                              [seg.argmax(1) for seg in segProb if segProb],
                                              [rel.argmax(1) for rel in relProb if relProb])

                    for i, fileName in enumerate(data["file_name"]):
                        merge_edges = get_ccs(data["graph_seg_edges"][i][np.where(segs_[i] == 0)].tolist())
                        if len(merge_edges) and merge_edges not in merge_dict[fileName] \
                        and iteration < 6:
                            merge_dict[fileName].append(merge_edges)
                            new_merge_dict[fileName] = True
                            # new_merge_idx[fileName] = new_merge_idx.get(fileName, default_idx) + 1


                            regionTriple = loader.dataset.file2triples[fileName + ".lg"]

                            idx = np.where(np.all(loader.dataset.regionTriples[:, :3] ==\
                                     regionTriple[:3], axis=1))[0]
                            new_merge_idx = loader.dataset.regionTriples[idx, 3].max() + 1
                            
                            # regionTriples.append((*regionTriple[:3], regionTriple[3] + 1))
                            # import pdb; pdb.set_trace()
                            regionTriples.append([*regionTriple[:3], new_merge_idx])

                            # new_merge_idx[fileName] = new_merge_idx.get(fileName, default_idx) + 1
                            # print("Appended")
                            # print(f"rank = {rank}, regionTriples = {regionTriples}")
                            # merge_indices.append(new_merge_idx[fileName])
                            # if world_size >= 2:
                                # partition_idx = np.where(np.all(dataset.regionTriples ==
                                                                # regionTriple, axis=1))[0][0]
                                # partition_indices.append(partition_idx)


                    lg_dir = None
                    if self.opts.out.lg_output:
                        lg_dir = os.path.join(self.opts.run_dir, self.opts.model_name, 
                                           self.opts.model_name  + "_" + str(self.opts.init.last_epoch))
                        os.makedirs(lg_dir, exist_ok=True)

                    # unmerged_files.extend(list(map(operator.itemgetter(0), filter(lambda item: not item[1],
                    #                                                          new_merge_dict.items()))))
                    merged_files.extend(list(map(operator.itemgetter(0), filter(operator.itemgetter(1), 
                                                                           new_merge_dict.items()))))
                        
                    if dataset.lg_mode == "contours":
                        prim_annotations = data["contours"]
                    else:
                        # TODO: No support for this currently since we are
                        # working from contours
                        prim_annotations = separate_outputs(data["prim_bbox"].numpy(), 
                                                            stk_lengths)

                    for i, fileName in enumerate(data["file_name"]):
                        if not new_merge_dict[fileName]:
                            pdfName = data["pdf_name"][i]
                            curr_doc = pdfName
                            # init prev_doc

                            if prev_doc is None:
                                prev_doc = curr_doc

                            if self.opts.init.given_seg:
                            # if self.opts.init.symbols_only or given_sym:
                                segs_ = segs.copy()

                            # Convert results to Object-Relation .lg string.
                            # RZ: dataset.expr_graph created during data loading/preprocessing.
                            rel_edges = data["graph_rel_edges"][i].numpy()
                            seg_edges = data["graph_seg_edges"][i].numpy()
                            instances = data["instances"][i].tolist()
                            lgString = postprocessGenORLg(
                                fileName, data['expr_graph'][i], rel_edges, relProb[i], 
                                seg_edges, instances, symProb[i], dataset.symbols, 
                                dataset.rels, segments=segs_[i], removeNoRel=True,
                                oneRelLimit=dataset.oneRelLimit, given_sym=given_sym, node_segment=dataset.node_seg,
                                given_rel=given_rel, to_lg=self.opts.out.lg_output, lg_dir=lg_dir,
                                dataset=dataset.dataset, extract_mst=dataset.extract_mst,
                                annotations=prim_annotations[i]
                            )

                            # Clean up, increment counts
                            torch.cuda.empty_cache()
                            formulasParsed += 1
                            if rank == 0:
                                pbar.update(1)
                            finalFormula = (formulasParsed == len(loader.dataset.regionTriples))

                            # Create PRO Table output

                            lg = Lg( StringIO(lgString) )
                            indexTuple = data["indexTuple"][i]
                            Parser.update_pro_table(lgString, loader.dataset, indexTuple, 
                                                    instances, includeLg=loader.dataset.lg)
                            currDoc, _, _ = indexTuple
                            if prevDoc is None:
                                prevDoc = currDoc

                            # If final formula reached, write out current document. Otherwise,
                            # write out previous document.
                            # docIndex = currDoc - 1
                            # AKS: Previous logic for doc index was wrong if there are
                            # files with no formulas
                            docIndex = prevDoc
                            if finalFormula:
                                docIndex = currDoc


                            # Create dot string --> PDF for label graph *if asked*
                            if dot2pdf:
                                Parser.write_dot_pdf(get_dot(lg), self.opts.out.dot_dir, fileName, pdfName)

                except Exception as e:
                    import traceback
                    traceback.print_exc()
                    print(f"\tWARNING: Some error (CUDA) in File: >> {data['file_name']}\n")
                    print(e)
                    # print(data)


            loader.dataset.regionTriples = np.array(regionTriples)
            loader.dataset.end = len(regionTriples)
            # partition.regionTriples = regionTriples
            # print(f"rank = {rank}, regionTriples = {partition.regionTriples}")
            # partition.merge_indices = merge_indices
            # partition.symContexts = np.array([False] * len(merged_files))
            # if world_size >= 2:
                # partition.index = np.array(range(len(regionTriples)), dtype=int)
                # partition.index = np.array(partition_indices, dtype=int)

                # print(f"rank = {rank}, new index = {partition.index}")
            # print(f"rank = {rank}, length partition = {len(partition)}")

            np.save(os.path.join(self.opts.run_dir, self.model_name,
                              f"regionTriples_{iteration}_{rank}.npy"),
                    loader.dataset.regionTriples)

            iteration += 1
            loader.dataset.merge_dict = merge_dict
            loader.dataset.curr_epoch += 1
        f.close()

        # AD MOD: If in a multi-GPU setting, check if there are any formulas allocated to the GPU first
        if prev_doc is not None:
            print("  * Generating TSV for " + prev_doc)
            # Write on different files by each process
            if world_size >= 2:
                suffix = '-out-' + str(rank)
            else:
                suffix = '-out'

            prev_doc = curr_doc
            prevDoc = currDoc

            # Do NOT sort objects, to preserve object order for relations.
            Parser.write_tsv_output( loader, docIndex, self.opts.run_dir, finalFormula,
                                    sortObjs=False, suffix=suffix, lg=dataset.lg,
                                    model_name=self.model_name,
                                    last_epoch=self.opts.init.last_epoch)

    def parse(self):
        ##################################
        # Config + Data Preparation
        ##################################
        # Update directories, create config file for run
        # if tsv_output_dir:
            # self.opts.tsv_output_dir = tsv_output_dir
        # else:
        # self.opts.tsv_output_dir = self.opts.run_dir
        # self.opts.dot_dir = dot_dir

        print(f"  Output TSV directory: {os.path.abspath(self.opts.run_dir)}")

        # AKS: Modify for new structure
        # if img_list: self.opts.data.Img_list = img_list
        # pimg_dir = self.opts.data.data_dir
        # if not ptsv_dir and lg_dir:
            # self.opts.data.lg_dir = lg_dir

        if not os.path.isdir(self.opts.run_dir):
            os.makedirs(self.opts.run_dir)
        save_config(self.opts,
                    os.path.join(self.opts.run_dir, "00_" + self.PHASE + "_options.yaml"))

        files = []
        if os.path.exists(self.opts.data.Img_list):
            for filename in open(self.opts.data.Img_list):
                if self.opts.data.dataset == "infty" or self.opts.data.dataset == "chem":
                    files.append(os.path.join(filename.strip() + '.lg'))
                elif self.opts.data.dataset == "crohme":
                    files.append(os.path.join(filename.strip() + '.inkml'))

        # Generate input data, and logger.
        self.opts.data.Img_list = files

        if self.opts.init.num_gpu == "multi":
            n_gpu = torch.cuda.device_count()
        elif self.opts.init.num_gpu == "single":
            n_gpu = 1

        # If single GPU, don't use DDP
        if n_gpu == 1:
            print('Running WITHOUT DPP!!!!')
            ddp = False
            rank = 0
            num_gpus = 1
        else:
            print('Running WITH DDP!!!')
            dist.init_process_group(backend='nccl')
            torch.cuda.set_device(int(os.environ['LOCAL_RANK']))
            ddp = True
            rank = dist.get_rank()
            num_gpus = dist.get_world_size()


        # Generate input data, and logger.
        dataset = MathSymbolDataset(**self.opts.data, rank=rank,
                                    num_gpus=num_gpus, model_dir=os.path.join(self.opts.run_dir, 
                                                                              self.model_name)
                                    )
        print(f"  Number of Original files for [GPU: {rank}] in job = {dataset.tot}")

        # AKS: If empty formula regions
        if len(dataset) == 0:
            print(">> Warning: Skipping parser; no formula regions defined.")
            sys.exit()
        
        loader = DataLoader(dataset, batch_size=1, num_workers=self.opts.init.num_workers, 
                            worker_init_fn=worker_init_fn, collate_fn=collate_fn)   

        self.run_test(rank=rank, world_size=num_gpus, dataset=dataset, loader=loader,
                      dot2pdf=self.opts.out.slt_pdf, given_sym=self.opts.init.given_sym, 
                      given_rel=self.opts.init.given_rel,
                      seed=self.opts.init.seed, ddp=ddp)

        # Synchronize The processes
        if n_gpu > 1:
            print(f'PROCESS FOR GPU {rank} finished!!')
            dist.barrier()
        # If more than 1 GPU, combine the TSV files by each GPU (only on Process 0)
        if n_gpu > 1 and rank == 0:
            symbol_map = {",": "COMMA", "\"": "DQUOTE", "fraction(-)": "-"}

            # Reinitialize the dataset with the entire set
            dataset = MathSymbolDataset(**self.opts.data, rank=0,
                                    num_gpus=1, model_dir=os.path.join(self.opts.run_dir, 
                                                                              self.model_name)
                                    )

            # AKS: Combine the two tsv files written by each process
            if not dataset.lg:
                filenames = dataset.data_files
                page_files = []
            else:
                # Since in lg mode, we have only 1 output file (1 document)
                filenames = [self.model_name + "_" + 
                        str(self.opts.init.last_epoch)]
                page_files = dataset.data_files
                self.opts.run_dir = os.path.join(
                        self.opts.run_dir, self.model_name)

            # RZ: Collect results in possibly separate sub-files
            idx = 0
            for filename in filenames:
                pro_tables = []
                # print(filename)
                # Iterate over available GPUs
                for rank in range(n_gpu):
                    # Generate filenames, with rank (GPU id) in suffix.
                    # RZ NOTE: we seem to be iterating over GPUs 
                    #    exhaustively. 
                    if not dataset.lg:
                        tsvfile_part = os.path.join(self.opts.run_dir, 
                                os.path.splitext(filename)[0] +
                                "-out-" + str(rank) + ".tsv")
                    else:
                        tsvfile_part = os.path.join(self.opts.run_dir, 
                                filename + "-out-" + str(rank) + ".tsv")
                    # exit(0)
                    
                    # Look for each TSV partial output from GPU
                    if os.path.exists(tsvfile_part):
                        pro_table, next_page_sizes = \
                            read_pro_tsv(tsvfile_part) 
                        pro_tables.append( pro_table[0][1] )

                        # Delete the individual tsv files after combination
                        os.remove(tsvfile_part)

                # Combine the proTables from the same file on multiple 
                # processes
                # try:
                proTable = reduce(
                        lambda proTableA, proTableB: 
                            merge_pro_tables_torchrun(proTableA, proTableB), 
                        pro_tables)

                objTypeFilter = obj_keep_types(['o', 'R',  'LG', 'MML'])
                _ = filter_pr_table(None, objTypeFilter, proTable)

                docProPairs = [ (filename, proTable)]

                # Write the final combined tsv output
                # RZ: NOTE page sizes are static; retrieve from 'dataset'
                
                docPageSizePairs = []
                if not dataset.lg:
                    docPageSizePairs = dataset.docs_page_sizes[idx:idx+1]
                #DEBUG
                #print("PARSE docPageSizePairs:\n" + str(docPageSizePairs))

                if dataset.tsv:
                    data_dir = dataset.tsv_dir
                else:
                    data_dir = dataset.lg_dir
                
                write_pro_tsv(self.opts.run_dir, docProPairs,
                    symbol_map, docPageSizePairs,
                    suffix='-out',
                    sort_objs=False, page_files=page_files,
                    page_dir=data_dir, lg=dataset.lg, start=0,
                                stop=dataset.tot)
                # except:
                #     print(f"\tWARNING: Empty TSV generated since no formulas: >> {filename}\n")
                #     # import traceback
                #     # traceback.print_exc()
                #     tsvfile = os.path.join(self.opts.run_dir,
                #                         os.path.splitext(filename)[0] + "-out.tsv")
                #     with open(tsvfile, "w") as f:
                #         f.write(f"D\t1\t{tsvfile}")
                # RZ: Adding for page size info
                idx += 1


def parse_args():

    parser = argparse.ArgumentParser(
        description="QDGGA - Expression Parsing"
    )
    parser._action_groups.pop()
    required = parser.add_argument_group('Required Args')
    optional = parser.add_argument_group('Optional Args')

    required.add_argument(
        "-c",
        "--config",
        required=True,
        type=str,
        help="default configuration to be used - inkml or crohme",
    )
    required.add_argument(
        "-r",
        "--run_config",
        type=str,
        required=True,
        help="run configuration to be used",
    )
    required.add_argument(
        "-m",
        "--model_name",
        type=str,
        required=True,
        help="The name of model to be used",
    )
    required.add_argument(
        "-o",
        "--output_dir",
        type=str,
        required=True,
        help="Directory containing TSV output",
    )
    required.add_argument(
        "-md",
        "--model_dir",
        type=str,
        required=True,
        help="Directory containing the saved model",
    )
    required.add_argument(
        "-op",
        "--optional",
        type=int, default=0,
        help="Use optional args to override YAML configs",
    )

    #-----------EXTRA OPTIONAL ARGS------------
    #--------OVERRIDES .YAML CONFIGS!----------

    optional.add_argument(
        "-dd",
        "--data_dir",
        type=str,
        help="Directory containing input images: .inkml or png",
    )
    # RZ: Adding tsv dir arg
    optional.add_argument(
        "-t",
        "--tsv_dir",
        type=str,
        help="Directory containing TSV input files",
    )
    optional.add_argument(
        "-i",
        "--img_list",
        type=str,
        help="The file containing list of images to be used for INFTY for trainig",
    )
    optional.add_argument(
        "-l",
        "--lg_dir",
        type=str,
        help="The directory containing lg inputs or ground truths",
    )
    optional.add_argument(
        "-le",
        "--last_epoch",
        type=str,
        help="Last epoch to use"
    )
    
    
    # RZ -- adding *input* image directory.
    # parser.add_argument(
    #     "-ii",
    #     "--img_dir",
    #     type=str,
    #     default="",
    #     help="Input page image path (use with TSV input)",
    # )

    optional.add_argument(
        "-do",
        "--dot_dir",
        type=str,
        help="Directory for pdf .dot-generated SLT files",
    )
    optional.add_argument(
        "-dp",
        "--d2pdf",
        type=int,
        help="Output dot_pdfs or not",
    )
    optional.add_argument(
        "-gs",
        "--given_sym",
        type=int,
        help="Use the given ground truth symbols or not",
    )
    optional.add_argument(
        "-gr",
        "--given_rel",
        type=int,
        help="Use the given ground truth relations or not",
    )
    optional.add_argument(
        "-v", "--visualize",
        type=int,
        help="Render formula structure as SLTs in PDF",
    )
    optional.add_argument(
        "-lo", "--lg_output",
        type=int,
        help="Produce lg output files",
    )
    optional.add_argument(
        "-n", "--num_gpu",
        type=str,
        help="Number of GPUs to use: `single` or `multi`",
    )
    optional.add_argument(
        "-s", "--seed",
        type=int,
        help="Seed for randomness"
    )
    optional.add_argument(
        "-w", "--workers",
        type=int,
        help="Number of workers to be used for dataloader"
    )
    
    args = parser.parse_args()

    config = get_config(args.config)
    run_opts = get_config(args.run_config)[args.model_name]['test']
    opts = update_config(config, run_opts)
    opts.run_dir = args.output_dir
    opts.model_dir = args.model_dir
    opts.model_name = args.model_name

    # Override with optional args if specified
    if args.optional:
        if args.data_dir is not None:
            opts.data.data_dir = args.data_dir
        
        if args.tsv_dir is not None:
            if opts.data.format == 'tsv':
                opts.data.tsv_dir = args.tsv_dir
            else:
                raise ValueError('''TSV Directory must be specified
                when format is set as tsv OR SET correct config in 
                run config file - \"format\"''')
        elif args.lg_dir is not None:
            if opts.data.format == 'lg':
                opts.data.lg_dir = args.lg_dir
            else:
                raise ValueError('LG Directory must be specified\
                when format is set as lg OR SET correct config in\
                run config file - \"format\"')
        
        if args.img_list is not None:
            opts.data.Img_list = args.img_list
        
        if args.last_epoch is not None:
            opts.init.last_epoch = args.last_epoch

        if args.d2pdf is not None:
            opts.out.sltpdf = args.d2pdf
        
        if args.given_sym is not None:
            opts.init.given_sym = args.given_sym

        if args.given_rel is not None:
            opts.init.given_rel = args.given_rel
        
        if args.visualize is not None:
            opts.init.visualize_slt = True

        if args.num_gpu is not None:
            opts.init.num_gpu = args.num_gpu
        
        if args.seed is not None:
            opts.init.seed = args.seed
        
        if args.dot_dir is not None:
            opts.dot_dir = args.dot_dir

        if args.lg_output is not None:
            opts.out.lg_output = args.lg_output

        if args.workers is not None:
            opts.init.num_workers = args.workers
    return opts

if __name__ == "__main__":
    pstart = time.time()
    print("\n  Processing arguments")
    opts = parse_args()

    # Construct parser
    start = time.time()
    print("  Loading network...")
    # parser = Parser(default_config=config.default_config,
    #                 run_config=config.run_config, InputData=config.InputData,
    #                 model_name=config.model_name, model_dir=config.model_dir,
    #                 last_epoch=config.last_epoch)
    parser = Parser(opts)
    iend = time.time()
    print("  Initialization: {:.2f}".format(iend - start) + " seconds")

    # Execute parser
    # parser.parse( 
    #         tsv_output_dir=config.output_dir, 
    #         given_sym=config.given_sym, ptsv_dir=config.tsv_dir,
    #         pimg_dir=config.img_dir, 
    #         dot2pdf=config.sltpdf, dot_dir=config.dot_dir,
    #         img_list=config.img_list, lg_dir=config.lg_dir,
    #         data_dir=config.data_dir, num_gpu=config.num_gpu,
    #         seed=config.seed)
    parser.parse()

    print("  Network execution: {:.6f}".format( time.time() - iend ) + " seconds")

