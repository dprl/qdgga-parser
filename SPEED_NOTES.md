# Issues and Opportunities for Parser acceleration

## Current acceleration


### 1. Custom DataLoader with dynamic batch size

Created a custom DataLoader for computing dynamic batch size. The algorithm 
for the iterator method, `__iter__()`
roughly given below:

- Iterate through the dataset using a sequential sampler, 
    * Compute the size of the input tensor.
    * Compare the ratio of the size to the free space in GPU to the GPU ratio threshold.
    * If the data instance fits:
        + Append the input tensor to a list
    * Else:
        + Pack the appended tensors (pad and concatenate the appended tensors) in a batch 
        + Return (Yield) the packed batch and start new batch with current data 
            instance as the first data in the new batch

> Compute the size of input tensors in Mb using:

```python
tensor.nelement() * tensor.element_size() / (1024 ** 2)
```

### 2. Features computation on GPU
Pass only formula and primitive images as dataset, computing the images
for primitive pairs
on gpu using saved references included in data. Changes in:
- Size: Could not fit more formulas than before. On inspection, the same memory
    was required on the GPU.
- Time: Some speedup since the primitive pair images now computed on GPU instead
    of CPU

## Issues

### 1. Custom DataLoader overhead
Custom DataLoader’s iterator is time consuming when creating batches.
It accesses each data sample in sequence, computes and checks the tensor size,
and then combines with previous samples to form a batch until the GPU is full.
The GPU was seen waiting for a noticeable time for each batch creation. 

However, once a batch was created, the forward method on the GPU was fast since
huge batch sizes (350-500 formulas/instances in one batch) loaded on to the GPU 
at once instead of loading each formula to the GPU.

#### Possible improvement:
- Creating queues to create batches when the formulas are processed (passed through 
the forward function on the GPU ()).
- Using multiple workers or some form of parallelization using the default
    DataLoader as a reference.

### 2. Exact GPU threshold

The explicit computation of sizes that the tensors would later expand to occupy
on the GPU seems challenging to compute given how PyTorch works. 
Once the tensors are passed on to the gpu, the expansion does not seem to be 
dependent on the edges and the gpu memory is expanded differently at different
stages of the forward operation (passing the tensor to encoder, applying the 
preattentions, multiplying the features with the masks), etc. 

It is extremely difficult to compute the memory the tensor would later occupy 
during the forward method since the gradients and weights are different at each
layer and dependent on various factors. 
No information was found in the official PyTorch documentation or discussion
forums. Also, 
some amount of memory gets accumulated as each data instance in the batch is passed
to the model.

So currently, still using the ratio of the tensor memory before it is passed to
the GPU to the free memory available in the GPU.
It worked for INFTY dataset, and the pipeline in northbay, bob. 
Also, tested on Mart and Abhisek’s system on pipeline 5 ACL collection.
