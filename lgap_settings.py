import os
import os.path as osp

# The Root Directory of the project
LG_DIR = USER = os.getenv('LgEvalDir', default=osp.join(osp.dirname(osp.abspath(__file__)), "lgeval"))
ROOT_DIR = osp.dirname(osp.abspath(__file__))
SRC_DIR = osp.join(ROOT_DIR, "src")
OUTPUTS_DIR = osp.join(ROOT_DIR, "outputs")
DATA_DIR = osp.join(ROOT_DIR, "data")

# RZ Warning: This duplicates definition in the MathSeer pipeline settings (msp_settings.py)
LABELS_TRANS_CSV = os.path.abspath( osp.join(LG_DIR, "translate", "infty_to_crohme.csv") )
MATHML_MAP_DIR = os.path.abspath( osp.join(LG_DIR, "translate", "mathMLMap.csv") )

CROHMELIB_SRC_DIR = osp.join(SRC_DIR, "evaluate", "crohmelib", "src")
CROHMELIB_GRAMMAR_DIR = osp.join(CROHMELIB_SRC_DIR, "Grammar")
MATHML_TXL_FILE = osp.join(CROHMELIB_SRC_DIR, "pprintMathML.Txl")

# Files with error as mentioned in the official dataset
ERROR_FILES_CROHME = ["UN19_1020_em_278.inkml",
                        "UN19_1001_em_0.inkml",
                        "UN19_1011_em_154.inkml",
                        "ISICal19_1206_em_828.inkml",
                        "ISICal19_1205_em_812.inkml",
                        "ISICal19_1204_em_795.inkml",
                        "RIT_2014_25.inkml",
                        "34_em_225.inkml",
                        "form_090_E717.inkml",
                        "form_094_E751.inkml",
                        "form_088_E701.inkml",
                        "form_5_161_E804.inkml",
                        "form_5_191_E953.inkml",
                        "form_5_194_E968.inkml",
                        "form_5_196_E976.inkml",
                        "form_5_196_E977.inkml",
                      ]

# They cause OOM error
ERROR_FILES_INFTY = ["28005023.lg", "28007228.lg"]

SYM_WEIGHTS_DIR = osp.join(ROOT_DIR, "config", "class_weights", "sym_weights.pkl")
SEG_WEIGHTS_DIR = osp.join(ROOT_DIR, "config", "class_weights", "seg_weights.pkl")
REL_WEIGHTS_DIR = osp.join(ROOT_DIR, "config", "class_weights", "rel_weights.pkl")

CC_GRAY_THRESHOLD = 127   
