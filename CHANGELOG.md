## 0.3.1 (2025-03-07)

### Fix

- Handle edge cases when no edges available

## 0.3.0 (2025-03-07)

## 0.2.1 (2025-03-07)

### Feat

- Handle 4 channel and gray scale image inputs
- Update description
- Add default configurations file
- Generalize test for CROHME and Infty datasets
- Add load_infty_input function
- Add modular test script
- Fix scikit-learn version
- Add requirements
- Add gitignore
- Add data and run

### Fix

- Use path.join for joining paths
