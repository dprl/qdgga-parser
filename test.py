#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 2020

@author: Ayush Kumar Shah (as1211@rit.edu)
"""
import logging
import os
import sys
import json
import torch
import argparse
import operator
import numpy as np
import torch.distributed as dist
import torch.multiprocessing as mp
mp.set_sharing_strategy('file_system')
from torch.distributed.algorithms.join import Join, _JoinConfig
from functools import reduce
from torch.utils.data import DataLoader
from src.utils.utils import (get_config, save_config, update_config,
                            get_checkpoint, get_ccs)
from src.utils.log import Logger
from src.models.model import Tester, separate_outputs
from src.datasets.dataset_simple_refactored import MathSymbolDataset 
from src.datasets.dataloader import partition_dataset, CustomDataLoader 
from src.datasets.CleanedDataset import CleanDataset

from src.utils.postprocessing import parse_output_create_lg
from train import set_seed
from tqdm import tqdm
import time
import csv
from protables.ProTables import write_pro_tsv, read_pro_tsv,\
                            merge_pro_tables, filter_pr_table, obj_keep_types, merge_pro_tables_torchrun
import joblib
from io import StringIO
from lgap_settings import LABELS_TRANS_CSV, MATHML_MAP_DIR
from train import set_default
# from src.utils.msp_settings import PARSE_DOT_OUT_DIR
from collections import defaultdict
from lgeval.src.lg import Lg
from lgeval.src.lg2txt import get_MML
from lgeval.src.lg2dot import lgDag

# Python GraphiViz 
from graphviz import Source
from src.utils.common_utils import get_labels_features
import pandas as pd
from glob import glob
from multiprocessing import Manager
from src.utils.debug_fns import *

import queue
import random
from multiprocessing import JoinableQueue, Process
from distributed import Client
from dask.distributed import fire_and_forget
from dask.distributed import wait
from src.datasets.classes.DatasetItem import DatasetItem
from dask import config as cfg

from operator import itemgetter
from itertools import repeat


cfg.set({'distributed.scheduler.worker-ttl': None})

from src.datasets.dataset_simple_refactored import get_nodes_data_from_formula_insolated, get_edges_data_from_formula_insolated, get_last_edge_insolated

import copy
from src.utils.dprl_map_reduce import map_concat, map_reduce, combine_dict_pair


def new_collate(batch):            
    return np.stack(batch, axis=0)


def consumer(q: JoinableQueue):
    while True:
        try:
            res = q.get(block=False)
            write_formula(res)
            q.task_done()
        except queue.Empty:
            pass


# def write_formula(data):    
#     formula_object, formula_id, given_sym, lg_dir, given_rel, lg_output,\
#         symbols,rels,oneRelLimit,node_seg,dataset,extract_mst, server_mode, outputs_acum_dict = data    
#     fileName = formula_id
#     # prim_annotations = formula_level_data["contours"]
#     pdfName = formula_id
#     curr_doc = pdfName
#     # init prev_doc    
    
#     # rel_edge_tuples, rel_edge_predicted_labels, rel_edge_probs = get_edges_data_from_formula_insolated(formula_object, formula_id, type_task="rel")
#     # seg_edge_tuples, seg_edge_predicted_labels, seg_edge_probs = get_edges_data_from_formula_insolated(formula_object, formula_id, type_task="seg")
#     # node_instances, node_probs, contours = get_nodes_data_from_formula_insolated(formula_object, formula_id)
#     rel_edge_tuples = formula_object.rel_edges
#     rel_edge_probs = outputs_acum_dict[formula_id]["rel_edge_probs"]
#     seg_edge_tuples = formula_object.seg_edges
#     node_instances = formula_object.instances
#     node_probs = outputs_acum_dict[formula_id]["node_probs"]
#     seg_edge_predicted_labels = outputs_acum_dict[formula_id]["seg_edge_predicted_labels"]
#     contours = formula_object.contours
#     expr_graph = formula_object.expression_graph
#     # import pdb; pdb.set_trace()
    
#     lgString = postprocessGenORLg(
#         fileName, expr_graph, rel_edge_tuples, rel_edge_probs, 
#         seg_edge_tuples, node_instances, node_probs, symbols, 
#         rels, segments=seg_edge_predicted_labels, removeNoRel=True,
#         oneRelLimit=oneRelLimit, given_sym=given_sym, node_segment=node_seg,
#         given_rel=given_rel, to_lg=lg_output, lg_dir=lg_dir,
#         dataset=dataset, extract_mst=extract_mst,
#         annotations=contours, server_mode=server_mode
#     )  
#     return lgString
    # del dataset.formulas_dict[formula_id] 


'''
    Convert a label graph string to a .dot string.
'''


def get_dot(lg):
    lg.hideUnlabeledEdges()
    (_, _, _, _, correctSegs, segRelDiffs) = lg.compare(lg)

    return lgDag(lg, lg, False, correctSegs, segRelDiffs)


def flattenString(inString):
    # DEBUG: tabs must be replaced by spaces for TSV files!
    # Use '\\n' and '\\t' in strings.
    return ("\"" + inString + "\""). \
        replace('\\', '\\\\'). \
        replace('\n', '\\n'). \
        replace('\t', '\\t')


def getBB(bbList):
    (minX, minY, maxX, maxY) = bbList[0]

    for bb in bbList[1:]:
        (mX, mY, MX, MY) = bb
        minX = min(minX, mX)
        minY = min(minY, mY)
        maxX = max(maxX, MX)
        maxY = max(maxY, MY)

    return (minX, minY, maxX, maxY)


class Parser(object):
    '''QG-GGA Parser Class'''

    # def __init__(self, default_config=None, run_config=None,
    #              InputData=None, model_name=None, model_dir=None,
    #              last_epoch=None):
    def __init__(self, opts, ids=None,lgs_all=None, server_mode=False):
        self.opts = opts
        self.PHASE = "test"
        self.model_name = opts.model_name
        self.InputData = opts.data.format

        #server mode data        
        self.ids_server = ids
        self.lgs_all_server = lgs_all
        self.server_mode = server_mode

        # Load options
        # default_opts = get_config(default_config)
        # run_opts = get_config(run_config)[model_name][self.PHASE]
        # self.opts = update_config(default_opts, run_opts)

        # Load saved model
        self.saved_model_dir = os.path.join(opts.model_dir, self.model_name)
        # if last_epoch:
        #     self.opts.init.last_epoch = last_epoch
        # else:
        #     self.opts.init.last_epoch = "latest"

        self.opts.init.update(get_checkpoint(self.saved_model_dir, \
                self.opts.init.last_epoch, self.PHASE))
        self.opts.data.last_epoch = self.opts.init.last_epoch

    @staticmethod
    def write_dot_pdf(dotString, dot_dir, fileName, pdfName):
        # Write a dot file to disk
        dotObj = Source(dotString)
        dotPdfName = os.path.join(dot_dir, pdfName, fileName + ".pdf")

        # Use pipe() command to avoid source files created w. render()
        if not os.path.exists(os.path.join(dot_dir, pdfName)):
            os.makedirs(os.path.join(dot_dir, pdfName))

        with open(dotPdfName, 'wb') as dotPdf:
            try:
                piped = dotObj.pipe(format="pdf")
                dotPdf.write(piped)
            except Exception as x:
                logging.warning(f"Failed to generate pdf from {fileName}, for pdf {pdfName}")

    @staticmethod
    def update_pro_table(lgString, dataset, indexTuple, instances,
                         includeLg=False, includeMml=False):
        # Update PRO table in data loader with parse results.
        # ( d, p, r ) = ( dataset.currDoc, dataset.currPage, dataset.currRegion )
        # AKS: Use indexTuple passed from the calling function
        (d, p, r) = indexTuple

        # RZ: In a hurry - parsing string rather than using .lg fields and 
        #     functions, which are likely faster.
        regionObjects = dataset.proTableList[d][p][r][1]

        # Sort if instances are unordered since they come from lg file
        if len(regionObjects[0]) >= 7:
            regionObjects.sort(key=lambda x: x[6])

        newObjects = []
        newRelations = []
        for row in csv.reader(StringIO(lgString)):
            if len(row) > 0 and row[0] == 'O':
                (objId, objLabel) = row[1:3]
                objLabel = objLabel.strip()
                primitiveList = row[4:]
                primitiveInts = map(lambda x: instances.index(int(x)), primitiveList)
                bboxList = [regionObjects[i][0:4] for i in primitiveInts]
                (minX, minY, maxX, maxY) = getBB(bboxList)
                newObjects.append(
                    (minX, minY, maxX, maxY, objLabel, 'o'))

            elif len(row) > 0 and row[0] == 'R':
                (parentId, childId, relationship) = row[1:4]
                (parentId, childId, relationship) = \
                    (parentId.strip(), childId.strip(), relationship.strip())
                # Increment symbol references by 1 for TSV 1-based indexing
                parentIndex = int(parentId[3:]) + 1
                childIndex = int(childId[3:]) + 1
                newRelations.append(
                    (parentIndex, childIndex, 0, 0, relationship, 'R'))

        annotations = []
        if includeMml:
            # Hide warnings for MML translation to avoid msgs for valid symbols
            mmlString = get_MML(lgString, MATHML_MAP_DIR, LABELS_TRANS_CSV,
                                warnings=False)
            annotations.append(
                (0, 0, 0, 0, flattenString(mmlString), 'MML'))

        if includeLg:
            # Label graphs can be seen as graphs in the HTML; omit by default.
            annotations.append(
                (0, 0, 0, 0, flattenString(lgString), 'LG'))

        # Empty the list, and add outputs
        regionObjects.clear()
        regionObjects.extend(newObjects)
        regionObjects.extend(newRelations)
        regionObjects.extend(annotations)

    @staticmethod
    def write_tsv_output( loader, docIndex, tsv_output_dir, finalFormula, 
                    sortObjs=True, suffix='-out', lg=False, model_name="", last_epoch=""):
        # RZ: DEBUG -- this is used to write a single file (!)
        #              using write_pro_tsv, designed for lists.
        symbolMap = {",": "COMMA", "\"": "DQUOTE", "fraction(-)": "-"}
        if not lg:
            file_name = loader.dataset.data_files[docIndex]
            pageFiles = []
        else:
            file_name = model_name + "_" + str(last_epoch)
            # Since each lg formula is in a separate page
            pageFiles = loader.dataset.data_files.tolist()
            tsv_output_dir = os.path.join(tsv_output_dir, model_name)

        docProPairs = [ ( file_name, loader.dataset.proTableList[ docIndex ] ) ]
        docPageSizePairs = []
        if not lg:
            docPageSizePairs = loader.dataset.docs_page_sizes[docIndex:docIndex+1]
        #DEBUG
        #print("write_tsv_output Page sizes:\n" + str(docPageSizePairs))
        if loader.dataset.tsv:
            data_dir = loader.dataset.tsv_dir
        else:
            data_dir = loader.dataset.lg_dir


    def get_probabilities(self, nodes_indexes, prob_dict, key_name):
        nodes_indexes.columns = ['index_prob','id_batch']
        matrix = None        
        for _, row in nodes_indexes.iterrows():
            id_batch = int(row['id_batch'])
            tensor_id = int(row['index_prob'])            
            tensor = prob_dict[id_batch][key_name][tensor_id].reshape((1,-1))                     
            
            matrix = np.concatenate((matrix,tensor)) if matrix is not None else tensor         
            
            
        return matrix

    def test_forward_epoch(self, data):                                                          
        
        (symProb, segProb, relProb, ids_treasures) = self.tester.test(data)
        
        node_labels_pred = list(symProb.argmax(1)) if symProb is not None else []
        seg_labels_pred = list(segProb.argmax(1)) if segProb is not None  else []
        rel_labels_pred = list(relProb.argmax(1)) if relProb is not None else []

        return (symProb, segProb, relProb), (node_labels_pred, seg_labels_pred, rel_labels_pred), ids_treasures


    def run_test(self, rank, world_size, dataset, loader,
                 dot2pdf=False, given_sym=False, given_rel=False,
                 seed=0, ddp=False):
        dTimer = DebugTimer("Inference/test mode")
        # dTimer_new = DebugTimer("NEW")
        print(f"  Running parser on gpu {rank}...")
        set_seed(seed)

        self.tester = Tester(self.opts.init, self.opts.net, rank, world_size)
        logger = Logger( self.opts.run_dir, rank, self.opts.init.last_epoch,
                        self.model_name, mode="test")

        ##################################
        # Parse formulas
        ##################################
        # if rank == 0:
        #     pbar = tqdm(total=dataset.tot, desc="  Processing")
        # formulasParsed = 0
        prev_doc = None
        prevDoc = None

        iteration = 0
        merge_dict = {}
        # new_merge_dict = {}
        # merged_files = loader.dataset.data_files
    
        model_dir = os.path.join(self.opts.run_dir, self.model_name)
        if not os.path.exists(model_dir):
            os.makedirs(model_dir, exist_ok=True)

        lg_dir = None
        if self.opts.out.lg_output:
            lg_dir = os.path.join(self.opts.run_dir, self.opts.model_name, 
                                self.opts.model_name  + "_" + str(self.opts.init.last_epoch))
            os.makedirs(lg_dir, exist_ok=True)
        
        # f = open(os.path.join(model_dir, f"merge_dict_test_{rank}.json"), "w")
        # all_files = list(map(lambda x:x.replace(".lg", ""),
        #                      loader.dataset.data_files))
        existing_segmentations = True
        epoch = -1
        dTimer.qcheck("Initialization")

        while existing_segmentations:   
            epoch+=1     
            # json_string = json.dumps(merge_dict, default=set_default)
            # f.write("-------------------------------------------------------------------------\n")
            # f.write(f"Iteration = {iteration}")
            # f.write("\n" + json_string + "\n")
            print("-------------------------------------------------------------------------\n")
            print(f"rank = {rank}, Iteration = {iteration}")
            print("----------------------")

            formulas_with_segmentation = set()
            concatenated_dataframe = None
            previous = set()
            futures = []
            
            counter_batches = -1

            # dTimer_new.qcheck("Start loop")
            
            outputs_acum_dict = defaultdict(lambda:{})

            treasure_path = os.path.join(dataset.formula_level_dir, "treasure.csv")
            loader.dataset.treasure = pd.read_csv(treasure_path,
                                        index_col=0, dtype=DatasetItem.get_types())
            formulas_dict_sqlite_path = loader.dataset.formulas_dict_sqlite_path
            # formula_dict = sqlitedict.SqliteDict(formulas_dict_path, flag='r')

            for data in logger(loader):
                counter_batches+=1
                # dTimer_new.qcheck(f"Init batch")
                #np.concatenate((feature_vector.flatten(), pos_enc, np.array([type_feature, id_treasure])))
                
                #nodes, segs,rels
                all_probabilities, all_predictions, ids_items = self.test_forward_epoch(data)

                ids_treasure = ids_items.astype(int)
                metadata_batch = loader.dataset.treasure[loader.dataset.treasure.index.isin(ids_treasure)]

                metadata_batch_nodes = metadata_batch[metadata_batch.type=="node"].reset_index()
                metadata_batch_edges = metadata_batch[metadata_batch.type=="edge"].reset_index()

                ids_formulas = pd.unique(metadata_batch.id_formula)
                node_count = len(metadata_batch_nodes)
                edge_count = len(metadata_batch_edges)

                for id_formula in ids_formulas:                    
                    formula_object = outputs_acum_dict[id_formula]                    
                    if node_count>0:
                        node_indexes_formula = metadata_batch_nodes[metadata_batch_nodes.id_formula == id_formula].index
                        if len(node_indexes_formula) != 0:                            
                            if "node_probs" not in formula_object:
                                formula_object["node_probs"] = all_probabilities[0][node_indexes_formula]
                            else:
                                formula_object["node_probs"] = np.concatenate((formula_object["node_probs"], all_probabilities[0][node_indexes_formula]))

                    if edge_count>0:
                        edge_indexes_formula = metadata_batch_edges[metadata_batch_edges.id_formula == id_formula].index
                        if len(edge_indexes_formula) != 0:                                                    
                            try:
                                rel_probs = all_probabilities[2][edge_indexes_formula]
                            except:
                                import traceback; print(traceback.format_exc())
                                import pdb; pdb.set_trace()
                            if "rel_edge_probs" not in formula_object:
                                formula_object["rel_edge_probs"] = rel_probs
                            else:                            
                                formula_object["rel_edge_probs"] = np.concatenate((formula_object["rel_edge_probs"], rel_probs))

                            if len(edge_indexes_formula) == 1:
                                labels_seg = [itemgetter(*edge_indexes_formula)(all_predictions[1])]
                            else:
                                labels_seg = list(itemgetter(*edge_indexes_formula)(all_predictions[1]))

                            if "seg_edge_predicted_labels" not in formula_object:                                                         
                                formula_object["seg_edge_predicted_labels"] = labels_seg                            
                            else:                            
                                formula_object["seg_edge_predicted_labels"] = formula_object["seg_edge_predicted_labels"]+labels_seg
                
                # dTimer_new.qcheck(f"Forward batch")
                # formulas_segmented, completed_formulas = dataset.set_predicted_data_batch(ids_items, all_probabilities, all_predictions)
                # dTimer_new.qcheck(f"Acum predicted data")
                # formulas_with_segmentation = formulas_with_segmentation.union(formulas_segmented)        
                
                # For recurrent mode, collect segmentation edges
                if self.opts.init.recurrent:
                    merge_dict = defaultdict(list)
                    offset = 0
                    for idx in range(len(data['id'])):
                        if data["type_feature"][idx] == "edge":
                            merge_dict['ids_formula'].append(data["id_formula"][idx])
                            merge_dict['id_index'].append(data["id"][idx])
                            merge_dict['edge_tuple'].append(data["label_class"][idx])                                   
                            merge_dict['segmentation_label'].append(all_predictions[1][offset])
                            offset+=1
                    dataframe = pd.DataFrame(merge_dict)                
                    if concatenated_dataframe is None:
                        concatenated_dataframe = dataframe
                    else:
                        concatenated_dataframe = pd.concat([concatenated_dataframe, dataframe])
            if self.opts.init.recurrent:
                # Add new query nodes and edges for segmented primitives
                concatenated_dataframe = concatenated_dataframe.groupby(["ids_formula","edge_tuple"])\
                                        .last().reset_index()
                concatenated_dataframe = concatenated_dataframe[concatenated_dataframe['segmentation_label'] == 0]
                formula_to_edges_map = (concatenated_dataframe.groupby('ids_formula')\
                                         .apply(lambda x: zip(x['edge_tuple'],x['id_index'])).to_dict())
                
                if iteration <= 6:
                    formulas_with_segmentation = dataset.create_objects_from_segmentation(formula_to_edges_map, epoch+1)

            dTimer.qcheck("Forward pass")
            if iteration > 6 or not self.opts.init.recurrent:
            # if iteration <6: #desactivate recurrent
                formulas_with_segmentation = set()

            # formula_ids = dataset.get_all_formulas_ids()
            # Extract keys and values from outputs_acum_dict together
            formula_ids, output_data = zip(*outputs_acum_dict.items())
            # Get the corresponding formula objects from the dataset
            # formula_objects = [dataset.formulas_dict[x] for x in formula_ids]
            # del dataset.formulas_dict
            # Use zip and repeat to create the arguments list while keeping the keys and values aligned
            args_list = list(zip(
                formula_ids,                    # formula_id
                repeat(formulas_dict_sqlite_path),      # formulas_dict_path
                repeat(given_sym),              # given_sym
                repeat(lg_dir),                 # lg_dir
                repeat(given_rel),              # given_rel
                repeat(self.opts.out.lg_output),# lg_output
                output_data,                     # output_data
                repeat(dataset.symbols),
                repeat(dataset.rels),
                repeat(dataset.oneRelLimit),
                repeat(dataset.node_seg),
                repeat(dataset.dataset),
                repeat(dataset.extract_mst),
                repeat(self.server_mode),
            ))
            # TODO: With parallelization, it is slower
            if self.opts.data.feat_parallel:
                # lgs_all = parse_output_create_lg_batch(args_list, use_parallel=True)
                lgs_all = map_reduce(parse_output_create_lg, combine_dict_pair, args_list)
                # lgs_all = "".join(lgs_all)
            else:
                lgs_all = {}
                for data in args_list:
                    lgs_all.update(parse_output_create_lg(data))
            # lgs_all = ""
            # for data in args_list:
            #     lgs_all += parse_output_create_lg(data)[0]

            dTimer.qcheck("Parse output graph and create lg string")

            existing_segmentations = len(formulas_with_segmentation)!=0
            # dataset.saved_features = filepaths_segmented_formulas
            iteration += 1
            # dataset.merge_dict = merge_dict
            dataset.curr_epoch += 1

        self.predicted_lgs = lgs_all
        print(dTimer)
        # print("*********************")
        # with open("timing.csv", "w+") as f:
        #     f.write(str(dTimer_new))
        # print(dTimer_new)
        # f.close()

    def parse(self):
        ##################################
        # Config + Data Preparation
        ##################################
        # Update directories, create config file for run
        # if tsv_output_dir:
            # self.opts.tsv_output_dir = tsv_output_dir
        # else:
        # self.opts.tsv_output_dir = self.opts.run_dir
        # self.opts.dot_dir = dot_dir

        print(f"  Output TSV directory: {os.path.abspath(self.opts.run_dir)}")

        # AKS: Modify for new structure
        # if img_list: self.opts.data.Img_list = img_list
        # pimg_dir = self.opts.data.data_dir
        # if not ptsv_dir and lg_dir:
            # self.opts.data.lg_dir = lg_dir

        if not os.path.isdir(self.opts.run_dir):
            os.makedirs(self.opts.run_dir)
        save_config(self.opts,
                    os.path.join(self.opts.run_dir, "00_" + self.PHASE + "_options.yaml"))

        files = []
        if os.path.exists(self.opts.data.Img_list):
            for filename in open(self.opts.data.Img_list):
                if self.opts.data.dataset == "infty" or self.opts.data.dataset == "chem":
                    files.append(os.path.join(filename.strip() + '.lg'))
                elif self.opts.data.dataset == "crohme":
                    files.append(os.path.join(filename.strip() + '.inkml'))

        # Generate input data, and logger.
        self.opts.data.Img_list = files

        if self.opts.init.num_gpu == "single":
            n_gpu = 1
        else:
            n_gpu = torch.cuda.device_count()

        # If single GPU, don't use DDP
        if n_gpu == 1:
            print('Running WITHOUT DP!!!!')
            ddp = False
            rank = 0
            num_gpus = 1
        else:
            print('Running WITH DP!!!')
            # TODO: this works only with torchrun. 
            # Change this back to old mode without torchrun
            # dist.init_process_group(backend='nccl')
            # torch.cuda.set_device(int(os.environ['LOCAL_RANK']))
            ddp = True
            rank = 0
            num_gpus = torch.cuda.device_count()


        # Generate input data, and logger.
        manager = Manager()
        shared_lock = manager.RLock()
        dataset = MathSymbolDataset(**self.opts.data, rank=rank,
                                    num_gpus=num_gpus, model_dir=os.path.join(self.opts.run_dir, 
                                                                              self.model_name),
                                    shared_lock=shared_lock,

                                    #server_data                                    
                                    ids_server = self.ids_server,
                                    lgs_all_server = self.lgs_all_server,
                                    server_mode = self.server_mode
                                    )        

        print(f"  Number of Original files for [GPU: {rank}] in job = {len(dataset.data_files)}")

        # AKS: If empty formula regions
        # if len(dataset) == 0:
        #     print(">> Warning: Skipping parser; no formula regions defined.")
            # sys.exit()        
        
        cleaned_dataset = CleanDataset(dataset_base=dataset)

        # loader = DataLoader(dataset, batch_size=self.opts.init.batch_size, 
        loader = DataLoader(cleaned_dataset, batch_size=self.opts.init.batch_size, 
                            num_workers=self.opts.init.num_workers, 
                            collate_fn=new_collate)
                            # collate_fn=dataset.custom_collate_fn) 

        self.run_test(rank=rank, world_size=num_gpus, dataset=dataset, loader=loader,
                      dot2pdf=self.opts.out.slt_pdf, given_sym=self.opts.init.given_sym, 
                      given_rel=self.opts.init.given_rel,
                      seed=self.opts.init.seed, ddp=ddp)

def parse_args():

    parser = argparse.ArgumentParser(
        description="LGAP - Expression Parsing"
    )
    parser._action_groups.pop()
    required = parser.add_argument_group('Required Args')
    optional = parser.add_argument_group('Optional Args')

    required.add_argument(
        "-c",
        "--config",
        required=True,
        type=str,
        help="default configuration to be used - inkml or crohme",
    )
    required.add_argument(
        "-r",
        "--run_config",
        type=str,
        required=True,
        help="run configuration to be used",
    )
    required.add_argument(
        "-m",
        "--model_name",
        type=str,
        required=True,
        help="The name of model to be used",
    )
    required.add_argument(
        "-o",
        "--output_dir",
        type=str,
        required=True,
        help="Directory containing TSV output",
    )
    required.add_argument(
        "-md",
        "--model_dir",
        type=str,
        required=True,
        help="Directory containing the saved model",
    )
    required.add_argument(
        "-op",
        "--optional",
        type=int, default=0,
        help="Use optional args to override YAML configs",
    )

    #-----------EXTRA OPTIONAL ARGS------------
    #--------OVERRIDES .YAML CONFIGS!----------

    optional.add_argument(
        "-dd",
        "--data_dir",
        type=str,
        help="Directory containing input images: .inkml or png",
    )
    # RZ: Adding tsv dir arg
    optional.add_argument(
        "-t",
        "--tsv_dir",
        type=str,
        help="Directory containing TSV input files",
    )
    optional.add_argument(
        "-i",
        "--img_list",
        type=str,
        help="The file containing list of images to be used for INFTY for trainig",
    )
    optional.add_argument(
        "-l",
        "--lg_dir",
        type=str,
        help="The directory containing lg inputs or ground truths",
    )
    optional.add_argument(
        "-le",
        "--last_epoch",
        type=str,
        help="Last epoch to use"
    )
    
    
    # RZ -- adding *input* image directory.
    # parser.add_argument(
    #     "-ii",
    #     "--img_dir",
    #     type=str,
    #     default="",
    #     help="Input page image path (use with TSV input)",
    # )

    optional.add_argument(
        "-do",
        "--dot_dir",
        type=str,
        help="Directory for pdf .dot-generated SLT files",
    )
    optional.add_argument(
        "-dp",
        "--d2pdf",
        type=int,
        help="Output dot_pdfs or not",
    )
    optional.add_argument(
        "-gs",
        "--given_sym",
        type=int,
        help="Use the given ground truth symbols or not",
    )
    optional.add_argument(
        "-gr",
        "--given_rel",
        type=int,
        help="Use the given ground truth relations or not",
    )
    optional.add_argument(
        "-v", "--visualize",
        type=int,
        help="Render formula structure as SLTs in PDF",
    )
    optional.add_argument(
        "-lo", "--lg_output",
        type=int,
        help="Produce lg output files",
    )
    optional.add_argument(
        "-n", "--num_gpu",
        type=str,
        help="Number of GPUs to use: `single` or `multi`",
    )
    optional.add_argument(
        "-s", "--seed",
        type=int,
        help="Seed for randomness"
    )
    optional.add_argument(
        "-w", "--workers",
        type=int,
        help="Number of workers to be used for dataloader"
    )
    optional.add_argument(
        "-cs", "--chunk_size",
        type=int,
        help="Number of formulas processed at once for feature generation"
    )
    optional.add_argument(
        "-bs", "--batch_size",
        type=int,
        help="Batch size for features to be used for dataloader"
    )
    optional.add_argument(
        "-p", "--feat_parallel",
        type=int,
        help="Parallel flag for feat generation"
    )

    optional.add_argument(
        "--port",
        type=str,
        help="Port number for distributed training"
    )

    optional.add_argument(
        "-rec", "--recurrent",
        type=int,
        help="Flag for recurrent training (on/off)"
    )
    
    args = parser.parse_args()

    config = get_config(args.config)
    run_opts = get_config(args.run_config)[args.model_name]['test']
    opts = update_config(config, run_opts)
    opts.run_dir = args.output_dir
    opts.model_dir = args.model_dir
    opts.model_name = args.model_name

    # Override with optional args if specified
    if args.optional:
        if args.data_dir is not None:
            opts.data.data_dir = args.data_dir
        
        if args.tsv_dir is not None:
            if opts.data.format == 'tsv':
                opts.data.tsv_dir = args.tsv_dir
            else:
                raise ValueError('''TSV Directory must be specified
                when format is set as tsv OR SET correct config in 
                run config file - \"format\"''')
        elif args.lg_dir is not None:
            if opts.data.format == 'lg':
                opts.data.lg_dir = args.lg_dir
            else:
                raise ValueError('LG Directory must be specified\
                when format is set as lg OR SET correct config in\
                run config file - \"format\"')
        
        if args.img_list is not None:
            opts.data.Img_list = args.img_list
        
        if args.last_epoch is not None:
            opts.init.last_epoch = args.last_epoch

        if args.d2pdf is not None:
            opts.out.sltpdf = args.d2pdf
        
        if args.given_sym is not None:
            opts.init.given_sym = args.given_sym

        if args.given_rel is not None:
            opts.init.given_rel = args.given_rel
        
        if args.visualize is not None:
            opts.init.visualize_slt = True

        if args.num_gpu is not None:
            opts.init.num_gpu = args.num_gpu
        
        if args.seed is not None:
            opts.init.seed = args.seed
        
        if args.dot_dir is not None:
            opts.dot_dir = args.dot_dir

        if args.lg_output is not None:
            opts.out.lg_output = args.lg_output

        if args.workers is not None:
            opts.init.num_workers = args.workers

        if args.batch_size is not None:
            opts.init.batch_size = args.batch_size

        if args.feat_parallel is not None:
            opts.data.feat_parallel = args.feat_parallel

        if args.port is not None:
            opts.init.port = args.port

        if args.recurrent is not None:
            opts.init.recurrent = bool(args.recurrent)

        if args.chunk_size is not None:
            opts.data.chunk_size = args.chunk_size

    return opts

def run_from_server():
    pstart = time.time()
    print("\n  Processing arguments")
    opts = parse_args()

    # Construct parser
    start = time.time()
    print("  Loading network...")
    parser = Parser(opts)
    iend = time.time()
    print("  Initialization: {:.2f}".format(iend - start) + " seconds")
    parser.parse()
    print("  Network execution: {:.6f}".format( time.time() - iend ) + " seconds")

def call_parser(ids=None,lgs_all=None, server_mode=False):
    pstart = time.time()
    print("\n  Processing arguments")
    opts = parse_args()

    # Construct parser
    start = time.time()
    print("  Loading network...")    
    parser = Parser(opts,ids=ids,lgs_all=lgs_all, server_mode=server_mode)
    iend = time.time()
    print("  Initialization: {:.2f}".format(iend - start) + " seconds")
    parser.parse()
    print("  Network execution: {:.6f}".format( time.time() - iend ) + " seconds")
    if server_mode:
        return parser.predicted_lgs

if __name__ == "__main__":
    call_parser()
    




