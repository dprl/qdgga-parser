import os
import shutil
import sys
import os.path as osp
import tqdm

from qdgga_settings import DATA_DIR
from src.datasets.math_symbol import get_relation_labels
from src.utils.modify_punc import read_lg, ORstring

def read_modified_edges(file):
    added_edges = []
    removed_edges = []

    with open(file, "r") as tsvFile:
        for line in tsvFile:
            info = [p.strip(",") for p in line.strip().split(" ")]
            entry_type = info[0].lower()
            if entry_type == "#r":
                # Get PUNC modified edges info
                pid = info[1]
                cid = info[2]
                edge_type = info[3]
                
                if edge_type == "ADD":
                    added_edges.append((pid, cid))
                else:
                    removed_edges.append((pid, cid))

    return removed_edges, added_edges

def update_edges(sym_edges, added_edges_cc, removed_edges_cc, cc2O):
    sym_edges_reverse = sym_edges.copy()

    removed_edges = []
    added_edges = []
    for removed_edge, added_edge in zip(removed_edges_cc, added_edges_cc):
        removed_edges.append((cc2O[removed_edge[0]], cc2O[removed_edge[1]]))
        added_edges.append((cc2O[added_edge[0]], cc2O[added_edge[1]]))

    # All edges with PUNC relation
    punc_edges = dict(list(dict(filter(lambda elem: elem[1] == "PUNC",
                                       sym_edges.items())).keys()))
    total_punc = len(punc_edges)

    removed_edges_new = []
    added_edges_new = []
    modified_count = 0
    for added_edge, removed_edge in zip(added_edges, removed_edges):
        if added_edge in sym_edges:
            modified_count += 1
            sym_edges_reverse.pop(added_edge)
            sym_edges_reverse[removed_edge] = "PUNC"
            removed_edges_new.append(added_edge)
            added_edges_new.append(removed_edge)

    return sym_edges_reverse, modified_count, total_punc, removed_edges_new, added_edges_new

def revert_punc(inp_dir, out_dir, modified_dir, punc_filelist):
    print(">> Reverting PUNC edge postprocessing\n")
    print(f">> Input directory: {inp_dir}")
    print(f">> Modified directory: {modified_dir}")
    print(f">> Output (Reverted) directory: {out_dir}\n")

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    with open(punc_filelist, "r") as f:
        punc_files = set(f.read().splitlines())

    print(f">> Number of lg files with PUNC: {len(punc_files)}\n")
    print(">> Reverse Modification in progress\n")

    modified_filecount = 0
    modified_punc_count = 0
    total_punc_count = 0
    modified_files = []
    for file in tqdm.tqdm(os.listdir(inp_dir)):
        if file[:-3] in punc_files:
            removed_edges, added_edges = read_modified_edges(osp.join(modified_dir, file))
            instances, symLabels, comps, bboxes, cc2O = read_lg(osp.join(inp_dir, file))

            # If stroke id missing in Object: Eg: 28011043
            if 0 in set(len(x) for x in list(comps.values())):
                print(f">> Warning: Missing stroke information in file {file}. No modification")
                shutil.copyfile(osp.join(inp_dir, file), osp.join(out_dir, file))
                continue

            stk_bboxes = dict(zip(instances, bboxes))
            sym_edges = get_relation_labels(osp.join(inp_dir, file), level='sym')

            sym_edges_new, modified_count, total_punc, \
            removed_edges, added_edges = update_edges(sym_edges, added_edges,
                                                      removed_edges, cc2O)
            
            if modified_count:
                modified_filecount += 1
                modified_punc_count += modified_count
                modified_files.append(file[:-3])

            total_punc_count += total_punc

            lgString = ORstring(file, symLabels, comps, sym_edges_new,
                                stk_bboxes, removed_edges, added_edges)

            with open(osp.join(out_dir, file), "w") as f:
                f.write(lgString)
        else:
            shutil.copyfile(osp.join(inp_dir, file), osp.join(out_dir, file))

    with open(osp.join(out_dir, "Reverted_files.txt"), "w") as f:
        f.write("\n".join(modified_files))

    print(f"\n>> No. of reverted PUNC lg files: {modified_filecount}/{len(punc_files)}\n")
    print(f">> No. of reverted PUNC edges: {modified_punc_count}/{total_punc_count}\n")
    print(f">> Reverted PUNC lg files saved at: {out_dir}\n")
    print("----------------------------------------------------------------------------------\n")

                    
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python revert_punc.py <input_dir> [<output_dir>]")
        sys.exit()
    elif len(sys.argv) == 2:
        inp_dir = sys.argv[1]
        out_dir = inp_dir.strip("/") + "_revert"
    elif len(sys.argv) == 3:
        inp_dir = sys.argv[1]
        out_dir = sys.argv[2]

    PUNC_TEST_LIST = os.path.join(DATA_DIR, "testing_PUNC.txt")
    INFTY_DIR = os.path.join(DATA_DIR, "InftyMCCDB-2")
    INFTY_TEST_MODIFIED_DIR = os.path.join(INFTY_DIR, "test_merged_class_punc")

    revert_punc(inp_dir=inp_dir, out_dir=out_dir,
                modified_dir=INFTY_TEST_MODIFIED_DIR,
                punc_filelist=PUNC_TEST_LIST)
