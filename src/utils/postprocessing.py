import io
import itertools
import os.path as path
from operator import itemgetter

import networkx as nx
import networkx.algorithms as alg
import numpy as np
from scipy.special import softmax

from src.datasets.math_symbol import INFRELATIONS, INFSYMBOLS, RELATIONS, SYMBOLS
from src.features.Graph_construction import (
    EDGE_ATTRIBUTES,
    NODE_ATTRIBUTES,
    GraphConstruction
)
from .branchings_custom import maximum_spanning_arborescence_custom
from collections import defaultdict, Counter
from src.utils.debug_fns import *
import itertools
import sqlitedict


def ORstring(mst, symbols, predictedLabels, top_k, given_sym, instances, annotations, dataset="infty"):
    # Using StringIO for efficient string concatenation
    output = io.StringIO()
    output.write("### Symbols\n### Format: O, symbolId, class, 1.0 (weight), [ primitiveId list ]\n")
    prefix = "O, "
    middle = ", 1.0, "

    topk_labels = []
    for name in mst.nodes():
        prims = ", ".join(str(x) for x in sorted(symbols[name], key=lambda x: int(x)))
        if given_sym and mst.nodes[name][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS] != "_" or not predictedLabels:
            label = mst.nodes[name][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS].replace(",", "COMMA")
        else:
            label = mst.nodes[name][NODE_ATTRIBUTES.PREDICTED_CLASS].replace(",", "COMMA")
        if top_k:
            topk_labels.append(([x for x in symbols[name]], mst.nodes[name][NODE_ATTRIBUTES.PREDICTED_TOPK_CLASS]))
        output.write(f"{prefix}{name}, {label}{middle}{prims}\n")

    # Write relation lines
    if len(mst.edges()) > 0:
        output.write("\n### Relations\n### Format: R, parentId, childId, class, 1.0 (weight)\n")
        prefix = "R, "
        suffix = ", 1.0"
        for p, c in sorted(mst.edges()):
            edge = mst[p][c]
            if isinstance(edge, list):
                edge = edge[0]
            label = edge['relation']
            output.write(f"{prefix}{p}, {c}, {label}{suffix}\n")
            if dataset == "chem":
                output.write(f"{prefix}{c}, {p}, {label}{suffix}\n")

    # Write top-k symbol classes
    if top_k:
        output.write("\n\n# Top k symbols\n")
        for topk_label in topk_labels:
            output.write(f"#{str(topk_label)}\n")

    # Write annotations if available
    if annotations is not None:
        output.write("\n")
        if isinstance(annotations, dict):
            prefix = "#contours, "
            for line_id, contours in sorted(annotations.items(), key=itemgetter(0)):
                name = str(instances[line_id])
                for contour in contours:
                    contours_str = ", ".join(str(coordinate) for point_list in contour for point in point_list for coordinate in point)
                    output.write(f"{prefix}{name}, {contours_str}\n")
        else:
            prefix = "#ccs, "
            for id, box in enumerate(annotations):
                name = str(instances[id])
                output.write(f"{prefix}{name}, {', '.join(map(str, box))}\n")

    return output.getvalue()


def create_output_graph(filename, exprGraph, rel_edges, edge_prob, seg_ref,
                        instances, sym_prob, sym_list, rel_list,
                        rel_detector=None, predictedLabels=True, segments=None,
                        removeNoRel=False, oneRelLimit=False, given_sym=False,
                        top_k=False, node_segment=True, given_rel=False,
                        to_lg=False, lg_dir=None, dataset="infty",
                        extract_mst=True, annotations=None, server_mode=False):
    """
    Create output graph using optimized merging strategy based on segments output.
    """
    # Step 1: Update seg_ref to Extract Actual Edges from Instances
    rel_ref_lg = [(str(instances[idx[0]]), str(instances[idx[1]])) for idx in rel_edges]
    seg_ref_lg = [(str(instances[idx[0]]), str(instances[idx[1]])) for idx in seg_ref]

    # Step 2: Extract Merge Edges from Segments
    edges_with_merge = extract_merge_edges_from_segments(seg_ref, segments)

    # Step 3: Identify Connected Components Using Segments Output
    ccs, symbols, prim2symbols, symbols_to_primidx = get_connected_components_from_segments(edges_with_merge, 
                                                                                           len(instances),
                                                                                           instances)
    # Step 4: Average Node Probabilities for Connected Components
    merged_node_probs = average_node_probabilities(ccs, sym_prob)

    # Step 5: Merge Edges
    merged_edges, merged_edge_probs = merge_edges(rel_ref_lg, edge_prob,
                                                  prim2symbols, rel_list)

    # Step 6: Construct Symbol Level Graph
    symGraph = construct_sym_level_graph(exprGraph, symbols_to_primidx, merged_node_probs, merged_edges,
                                         merged_edge_probs, rel_list, sym_list)

    # Step 7: Extract Maximum Spanning Tree if Needed
    if extract_mst:
        mst = extract_tree(filename, symGraph, rel_list, removeNoRel, oneRelLimit, given_rel)
        return mst, symbols
    else:
        return symGraph, symbols


def extract_merge_edges_from_segments(seg_ref, segments):
    """
    Extract merge edges based on segment references and segments.
    Only include edges where the corresponding segment value is 0, indicating a merge.
    """
    if len(segments) == 0:
        return []
    return [edge for idx, edge in enumerate(seg_ref) if segments[idx] == 0]

def get_connected_components_from_segments(merge_edges, num_nodes, instances):
    # Create a graph
    cc_graph = nx.Graph()
    
    # Add nodes and edges to the graph
    cc_graph.add_nodes_from(range(num_nodes))
    cc_graph.add_edges_from(merge_edges)
    
    # Get the connected components
    ccs = list(nx.connected_components(cc_graph))
    
    # Initialize symbols and reverse_map
    symbols = {}
    symbols_to_primidx = {}
    reverse_map = {}
    
    # Add disconnected nodes as individual components
    all_nodes = set(range(num_nodes))
    connected_nodes = {node for comp in ccs for node in comp}
    disconnected_nodes = all_nodes - connected_nodes
    
    for node in disconnected_nodes:
        ccs.append({node})
    
    # Build symbols and reverse_map in a single loop
    for i, comp in enumerate(ccs):
        symbol = f"sym{i}"
        symbol_prims = []
        for prim in comp:
            prim_lg = str(instances[prim])
            reverse_map[prim_lg] = symbol
            symbol_prims.append(prim_lg)
        symbols[symbol] = symbol_prims
        symbols_to_primidx[symbol] = sorted(list(comp))
    return ccs, symbols, reverse_map, symbols_to_primidx

def average_node_probabilities(connected_components, sym_prob):
    for component in connected_components:
        nodes = list(component)
        if len(component) >= 2:
            avg_probs = np.mean(sym_prob[nodes, :], axis=0)
            sym_prob[nodes, :] = avg_probs
    return sym_prob

def merge_edges(rel_edges, edge_probs, prim2symbols, rel_list):
    """
    Merge relational edges into symbol-level edges and average probabilities.
    """
    # Use a dictionary to map edges to their probabilities
    edge_to_probs = defaultdict(list)
    
    for edge, prob in zip(rel_edges, edge_probs):
        sym_edge = tuple(sorted([prim2symbols[edge[0]], prim2symbols[edge[1]]]))
        
        # Exclude self-edges (edges with the same symbol on both ends)
        if sym_edge[0] != sym_edge[1]:
            edge_to_probs[sym_edge].append(prob)
    
    # Create a list of (edge, mean_probability) tuples
    merged_edges_and_probs = [(edge, np.mean(probs, axis=0)) for edge, probs in edge_to_probs.items()]
    
    # Unzip the edges and probabilities into separate lists
    if len(merged_edges_and_probs) == 0:
        return np.array([]), np.array([])

    merged_edges, merged_probs = zip(*merged_edges_and_probs)
    
    # Convert to numpy arrays for further processing
    merged_edges = np.array(merged_edges)
    merged_probs = np.array(merged_probs)
    
    # Filter out "NoRelation" edges
    no_relation_index = rel_list.index("NoRelation")
    keep_indices = np.argmax(merged_probs, axis=1) != no_relation_index
    
    return merged_edges[keep_indices], merged_probs[keep_indices]

def construct_sym_level_graph(graph, symbols, node_probs, edges, edge_probs, relation_names, symbol_names, topk=5, key=0, given_rel=False):
    symGraph = nx.Graph()
    symGraph.add_nodes_from(symbols.keys())
    symGraph.add_edges_from(edges)
    symGraph = GraphConstruction.add_edge_attributes(symGraph)
    symGraph = GraphConstruction.add_node_attributes(symGraph)

    # Set edge attributes in bulk
    edge_attributes = {}
    for i, (pid, cid) in enumerate(edges):
        edge_prob = edge_probs[i]
        sort_pred = sorted(edge_prob, reverse=True)
        class_idx = sorted(range(len(edge_prob)), key=lambda k: -edge_prob[k])
        class_names = [relation_names[j] for j in class_idx]
        gt_relation = graph.get_edge_data(pid, cid, {}).get(EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION, None)
        relation = class_names[key]

        weight = 1.0 if given_rel and (relation == gt_relation) else sort_pred[key]
        edge_attributes[(pid, cid)] = {
            EDGE_ATTRIBUTES.POSSIBLE_CLASSES: class_names,
            EDGE_ATTRIBUTES.CLASS_CONFIDENCES: sort_pred,
            "weight": weight,
            "relation": relation,
            "gt_relation": gt_relation
        }

    nx.set_edge_attributes(symGraph, edge_attributes)

    # Set node attributes in bulk
    node_attributes = {}
    for sym_node in symGraph.nodes():
        topk_idx = node_probs[symbols[sym_node][0]].argsort()[::-1][:topk]
        topk_conf = node_probs[symbols[sym_node][0]][topk_idx]
        topk_labels = np.array(symbol_names)[topk_idx]
        [lbl.replace(",", "COMMA") for lbl in topk_labels]
        label = topk_labels[0]
        node_attributes[sym_node] = {
            NODE_ATTRIBUTES.PREDICTED_TOPK_CLASS: list(zip(topk_labels, topk_conf)),
            NODE_ATTRIBUTES.PREDICTED_CLASS: label,
            # NODE_ATTRIBUTES.GROUND_TRUTH_CLASS: graph.nodes[sorted(symbols[sym_node], key=lambda x: int(x))[0]][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]
        }

    nx.set_node_attributes(symGraph, node_attributes)
    return symGraph

def parse_output_create_lg(data):
    output_dict = {}
    formula_id, formulas_dict_sqlite_path, given_sym, lg_dir, given_rel, \
        lg_output, output_data, symbols, rels, oneRelLimit,\
        node_seg, dataset, extract_mst, server_mode = data

    formulas_dict = sqlitedict.SqliteDict(formulas_dict_sqlite_path, flag='r')
    formula_object = formulas_dict[formula_id]
    try:
        output_graph, symbols, predicted_seg_labels,\
            node_instances, contours = parse_output_graph(formula_id, formula_object, 
                                                          given_sym, lg_dir, given_rel, 
                                                          lg_output, output_data, symbols,
                                                          rels, oneRelLimit, node_seg, dataset,
                                                          extract_mst, server_mode)

        lg_string = create_lg_string(output_graph, symbols, predicted_seg_labels, node_instances,
                                     contours, write_file=lg_output, lg_dir=lg_dir, filename=formula_id,
                                     given_sym=given_sym, dataset=dataset, server_mode=server_mode)
        output_dict[formula_id] = lg_string
    except:
        import traceback; print (traceback.format_exc())
        print(f"Postprocessing Error in formula {formula_id}")
    formulas_dict.close()
    return output_dict


def parse_output_graph(formula_id, formula_object, given_sym, lg_dir, 
                       given_rel, lg_output, outputs_acum_dict, symbols,
                       rels, oneRelLimit=False, node_seg=False, dataset="chem",
                       extract_mst=False, server_mode=False):
    """
    Parse output graph from given data and create symbol-level graph.
    """
    rel_edge_probs = outputs_acum_dict.get("rel_edge_probs", np.array([]))
    node_probs = outputs_acum_dict["node_probs"]
    
    seg_edge_predicted_labels = outputs_acum_dict.get("seg_edge_predicted_labels", np.array([]))

    rel_edge_tuples = formula_object.rel_edges
    seg_edge_tuples = formula_object.seg_edges
    node_instances = formula_object.instances
    contours = formula_object.contours
    expr_graph = formula_object.expression_graph

    # Create output graph
    graph, symbols = create_output_graph(
        formula_id, expr_graph, rel_edge_tuples, rel_edge_probs, seg_edge_tuples,
        node_instances, node_probs, symbols, rels, segments=seg_edge_predicted_labels,
        removeNoRel=True, oneRelLimit=oneRelLimit, given_sym=given_sym,
        node_segment=node_seg, given_rel=given_rel, to_lg=lg_output, lg_dir=lg_dir,
        dataset=dataset, extract_mst=extract_mst, annotations=contours,
        server_mode=server_mode
    )

    return graph, symbols, seg_edge_predicted_labels, node_instances, contours


def create_lg_string(graph, symbols, predicted_labels, node_instances, annotations, write_file=False, lg_dir=None, filename=None, top_k=False, given_sym=False, dataset="chem", server_mode=False):
    lgString = ORstring(graph, symbols, predicted_labels, top_k, given_sym, node_instances, annotations, dataset)
    if write_file and not server_mode:
        base_file_name = filename + ".lg"
        with open(path.join(lg_dir, base_file_name), "w") as f:
            f.write(lgString)
    return lgString


#def ORstring( mst, symbols, predictedLabels, top_k, given_sym, instances,
#             annotations, dataset="infty" ):
#    # RZ: Construct .lg format output string
#    prefix = "O, "
#    middle = ", 1.0, "
#    outString = "### Symbols\n"
#    outString += "### Format: O, symbolId, class, 1.0 (weight), [ primitiveId list ]\n"

#    topk_labels = []
#    for name in mst.nodes():
#         # AKS: Fix bug which assign different label in each run
#        prims = ", ".join(str(x) for x in sorted(symbols[name], key=lambda x: int(x)))

#        if given_sym and mst.nodes[name][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS] \
#            != "_" or not predictedLabels:
#            label = mst.nodes[name][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS].\
#                    replace( ",", "COMMA" )
#        else:
#            try:
#                label = mst.nodes[name][NODE_ATTRIBUTES.PREDICTED_CLASS].\
#                    replace( ",", "COMMA" )

#            except ValueError:
#                print("Error: " + name)

#        if (top_k ):
#            topk_labels.append(
#                (
#                    [x for x in symbols[name]],
#                    (mst.nodes[name][NODE_ATTRIBUTES.PREDICTED_TOPK_CLASS]),
#                )
#            )
#        outString += prefix + str(name) + ", " + label + middle + prims + "\n"

#    # Write relation lines
#    if len(mst.edges()) > 0:
#        outString += "\n### Relations\n"
#        outString += "### Format: R, parentId, childId, class, 1.0 (weight)\n"
   
#    prefix = "R, "
#    suffix = ", 1.0"
#    for p, c in sorted(mst.edges()):
#    #for p, c in mst.edges():
#        edge = mst[p][c]
#        #label = edge[EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
#        if isinstance(edge, list):
#            edge = edge[0]
#        label = edge['relation']
#        outString += prefix + str(p) + ", " + str(c) + ", " + label + \
#                suffix + "\n"
#        if dataset == "chem":
#            outString += prefix + str(c) + ", " + str(p) + ", " + label + \
#                    suffix + "\n"
    
#    # write top k symbol classes
#    if ( top_k ):
#        outString += "\n\n# Top k symbols\n"
#        for topk_label in topk_labels:
#            outString += "#" + str(topk_label) + "\n"

#    # If contours or box annotations available:
#    if annotations is not None:
#        outString += "\n"
#        if isinstance(annotations, dict):
#            prefix = "#contours, "
#            for line_id, contours in sorted(annotations.items(), key=itemgetter(0)):
#                name = str(instances[line_id])
#                for contour in contours:
#                    contours_str = ", ".join(str(coordinate) for point_list in contour
#                                          for point in point_list for coordinate in point)
#                    outString += prefix + name + f", {contours_str}" + "\n"
#        else:
#            prefix = "#ccs, "
#            # Write #cc i.e. connected components' box coordinates
#            for id, box in enumerate(annotations):
#                name = str(instances[id])
#                outString += prefix + name + f", {', '.join(map(str, box))}" + "\n"

#    return outString


#def create_output_graph(filename, exprGraph, rel_edges, edge_prob, seg_ref, 
#                        instances, sym_prob, sym_list, rel_list, rel_detector=None,
#                        predictedLabels=True, segments=None, removeNoRel=False, 
#                        oneRelLimit=False, given_sym=False, top_k=False,
#                        node_segment=True, given_rel=False, to_lg=False,
#                        lg_dir=None, dataset="infty", extract_mst=True,
#                        annotations=None, server_mode=False):
#    """
#    Create output graph using optimized merging strategy based on segments output.
#    """
#    dTimer = DebugTimer("Output graph")
#    # Step 1: Update seg_ref to Extract Actual Edges from Instances
#    rel_ref_lg = [(str(instances[idx[0]]), str(instances[idx[1]])) for idx in rel_edges]
#    seg_ref_lg = [(str(instances[idx[0]]), str(instances[idx[1]])) for idx in seg_ref]

#    # Step 2: Extract Merge Edges from Segments
#    edges_with_merge = extract_merge_edges_from_segments(seg_ref, segments)
#    dTimer.qcheck("Extract merge edges")

#    # Step 1: Identify Connected Components Using Segments Output
#    nodes_idx = [instances.index(int(n)) for n in exprGraph.nodes()]
#    connected_components, symbols = get_connected_components_from_segments(edges_with_merge, nodes_idx,
#                                                                           instances)
#    dTimer.qcheck("Get connected components")

#    # Step 2: Average Node Probabilities for Connected Components
#    merged_node_probs, merged_node_map = average_node_probabilities(connected_components, sym_prob)
#    # import pdb; pdb.set_trace()
#    dTimer.qcheck("Average node probabilities")

#    merged_edges, merged_edge_probs = merge_edges(rel_ref_lg, edge_prob,
#                                                  symbols, rel_list)
#    dTimer.qcheck("Merge edges")

#    symGraph = construct_sym_level_graph(exprGraph, symbols, merged_node_probs,
#                                         merged_edges, merged_edge_probs,
#                                         rel_list, sym_list)
#    dTimer.qcheck("Construct sym level graph")
#    # print(dTimer)
#    # import pdb; pdb.set_trace()
#    # # Step 3: Average Edge Probabilities for Merged Edges
#    # collapsed_edge_probs, filtered_edges = average_and_filter_edge_probabilities(
#    #     connected_components, exprGraph, rel_edges, edge_prob, rel_list, removeNoRel
#    # )

#    # # Step 4: Construct Collapsed Graph Using Averaged Probabilities
#    # collapsed_graph, symbols = construct_collapsed_graph(
#    #     connected_components, collapsed_node_probs, filtered_edges, collapsed_edge_probs, sym_list, collapsed_node_map
#    # )

#    # Step 5: Extract Maximum Spanning Tree if Needed
#    if extract_mst:
#        mst = extract_tree(filename, symGraph, rel_list, removeNoRel, oneRelLimit, given_rel)
#        return mst, symbols
#    else:
#        return symGraph, symbols

#def extract_merge_edges_from_segments(seg_ref, segments):
#    """
#    Extract merge edges based on segment references and segments.
#    Only include edges where the corresponding segment value is 0, indicating a merge.
#    """
#    merge_edges = [edge for idx, edge in enumerate(seg_ref) if segments[idx] == 0]
#    return merge_edges

#def get_connected_components_from_segments(merge_edges, nodes_idx, instances):
#    """
#    Create connected components from the list of merge edges.
#    """
#    cc_graph = nx.Graph()
#    cc_graph.add_nodes_from(nodes_idx)
#    cc_graph.add_edges_from(merge_edges)

#    # Find connected components representing merged nodes
#    connected_components = list(nx.connected_components(cc_graph))
#    symbols = {}
#    for i, cc in enumerate(connected_components):
#        newId = "sym" + str(i)
#        symbols[newId] = [str(instances[x]) for x in cc]  # stk group mapping to symbols
#    return connected_components, symbols

#def convert_stk_predictions_to_lg(symbols, segments, symbol_dict):
#    index = 0
#    instances = [index]
#    for seg in segments[1:]:
#        if seg == 0:
#            instances.append(index)
#        else:
#            index += 1
#            instances.append(index)
#    instances = np.array(instances)

#    objects = []
#    for i in range(index + 1):
#        obj_symbols = symbols[instances == i]

#        strokes = np.where(instances == i)[0]
#        sym_idx = sorted(Counter(obj_symbols).items(),
#            key=lambda x: x[1])[-1][0]
#        symbol = symbol_dict[sym_idx]
#        objects.append((symbol, strokes.tolist()))

#    objects_str = ""
#    cnts = defaultdict(int)
#    for obj in objects:
#        symbol, strokes = str(obj[0]), map(str, obj[1])
#        if symbol == ",": symbol = "COMMA"
#        cnts[symbol] += 1
#        objects_str += "O, {}_{}, {}, 1.0, {}\n".format(
#            symbol, cnts[symbol], symbol, ", ".join(strokes))

#    return objects_str

#def average_node_probabilities(connected_components, sym_prob):
#    """
#    Average node probabilities for each connected component.
#    """
#    # sym_prob = softmax(sym_prob, axis=1)
#    collapsed_node_map = {}

#    for i, component in enumerate(connected_components):
#        nodes = list(component)
#        collapsed_node_map[i] = nodes
#        if len(component) < 2:
#            continue
#        # Average the node class probabilities
#        avg_probs = np.mean(sym_prob[nodes, :], axis=0)
#        sym_prob[nodes, :] = avg_probs  # Replace rows with the averaged probabilities

#    return sym_prob, collapsed_node_map

#def merge_edges(rel_edges, edge_probs, symbols, rel_list):
#    """
#    Merge relational edges into symbol-level edges and average probabilities.
    
#    Parameters:
#    - rel_edges: List of tuples representing relational edges (E, 2).
#    - edge_probs: NumPy array of shape (E, 2) representing edge probabilities.
#    - symbols: Dictionary mapping symbols to their node lists.
    
#    Returns:
#    - merged_edges: List of merged symbol-level edges.
#    - merged_probs: NumPy array of averaged probabilities for merged edges.
#    """
#    # edge_probs = softmax(edge_probs, axis=1)
#    # Create a reverse mapping from nodes to their symbol names
#    node_to_symbol = {}
#    for symbol, nodes in symbols.items():
#        for node in nodes:
#            node_to_symbol[node] = symbol

#    # Map relational edges to symbol-level edges
#    sym_edges = [(node_to_symbol[edge[0]], node_to_symbol[edge[1]]) for edge in rel_edges]

#    # Remove self-loops and duplicates (assuming undirected edges are considered the same)
#    unique_edges = set()
#    for u, v in sym_edges:
#        if u != v:
#            unique_edges.add(tuple(sorted((u, v))))  # Sorting to ensure (u, v) == (v, u)

#    # Create a mapping from unique symbol edges to indices for merging probabilities
#    edge_to_indices = defaultdict(list)
#    for i, (u, v) in enumerate(sym_edges):
#        if u != v:
#            edge_to_indices[tuple(sorted((u, v)))].append(i)

#    # Merge probabilities for each unique symbol edge
#    merged_edges = []
#    merged_probs = []
#    for edge, indices in edge_to_indices.items():
#        prob = np.mean(edge_probs[indices], axis=0)
#        # Filter out edges where 'NoRelation' is the predicted class
#        if np.argmax(prob) != rel_list.index("NoRelation"):
#            merged_edges.append(edge)
#            merged_probs.append(prob)
#    merged_probs = np.array(merged_probs)
#    return merged_edges, merged_probs


#def construct_sym_level_graph(graph, symbols, node_probs, edges, edge_probs,
#                              relation_names, symbol_names, topk=5, key=0, 
#                              given_rel=False):
#    symGraph = nx.Graph()
#    symGraph.add_nodes_from(symbols.keys())
#    symGraph.add_edges_from(edges)
#    symGraph = GraphConstruction.add_edge_attributes(symGraph)
#    symGraph = GraphConstruction.add_node_attributes(symGraph)

#    # set edge attributes
#    for i, (pid, cid) in enumerate(edges):
#        edge_prob = edge_probs[i]
#        sort_pred = sorted(edge_prob, reverse=True)
#        class_idx = sorted(range(len(edge_prob)), key=lambda k: -edge_prob[k])
#        class_names = [relation_names[j] for j in class_idx]

#        symGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES] = class_names
#        symGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES] = sort_pred

#        cc_pid = sorted(symbols[pid], key=lambda x: int(x))[0]
#        cc_cid = sorted(symbols[cid], key=lambda x: int(x))[0]
#        if (cc_pid, cc_cid) in graph.edges():
#            # AKS: Add gt relation from primitive level graph
#            symGraph[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = \
#                graph[cc_pid][cc_cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]

#        gt_relation = symGraph[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]
#        relation = symGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][key]

#        # AKS: Assign the gt relation weight to highest so that it is
#        # selected during MST construction
#        if given_rel and (relation == gt_relation):
#            weight = 1.0
#        else:
#            weight = symGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][key]
#        symGraph[pid][cid]["weight"] = weight
#        symGraph[pid][cid]["relation"] = relation
#        symGraph[pid][cid]["gt_relation"] = gt_relation

#    # set node attributes
#    for sym_node in symGraph.nodes():
#        topk_idx = node_probs[int(symbols[sym_node][0])].argsort()[::-1][:topk]
#        topk_conf = node_probs[int(symbols[sym_node][0])][topk_idx]
#        topk_labels = np.array(symbol_names)[topk_idx]
#        [lbl.replace(",", "COMMA") for lbl in topk_labels]
#        label = topk_labels[0]
#        symGraph.nodes[sym_node][NODE_ATTRIBUTES.PREDICTED_TOPK_CLASS] = list(
#            zip(topk_labels, topk_conf)
#        )
#        symGraph.nodes[sym_node][NODE_ATTRIBUTES.PREDICTED_CLASS] = label
#        symGraph.nodes[sym_node][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS] = graph.nodes[
#            sorted(symbols[sym_node], key=lambda x: int(x))[0]][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]

#    return symGraph



#def set_weights(exprGraph, rel_list, removeNoRel, given_rel=False):
#    """
#    Set weights for edges in the graph, optionally removing 'NoRelation' edges.
#    """
#    exprGraph_edges = exprGraph.edges(data=True)
#    exprGraph_multi = nx.MultiDiGraph()
#    exprGraph_multi.add_nodes_from(exprGraph.nodes(data=True))

#    if removeNoRel:
#        # Remove all edges with 'NoRelation' as the highest prob
#        for pid, cid, data in exprGraph.edges(data=True):
#            gt_relation = exprGraph[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]
#            for key in range(len(rel_list)):
#                relation = exprGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][key]
#                if relation == "NoRelation":
#                    continue

#                if given_rel and (relation == gt_relation):
#                    weight = 1.0
#                else:
#                    weight = exprGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][key]
#                exprGraph_multi.add_edge(pid, cid, weight=weight, relation=relation, gt_relation=gt_relation)
#    return exprGraph_multi

#def create_output_graph_old(filename, exprGraph, rel_edges, edge_prob, seg_ref, 
#                       instances, sym_prob, sym_list, rel_list, rel_detector=None, 
#                       predictedLabels=True, segments=None, removeNoRel=False,
#                        oneRelLimit=False, given_sym=False, top_k = False,
#                        node_segment=True, given_rel=False, to_lg=False, 
#                       lg_dir=None, dataset="infty", extract_mst=True,
#                        annotations=None, server_mode=False):
#    # dTimer = DebugTimer("Output graph")
#    # Attach probabilities to primitives and edges
#    symProbs = dict(zip(instances, sym_prob))
#    rel_ref = [ (str(instances[idx[0]]), str(instances[idx[1]])) for idx in rel_edges ]
#    # prune_segment = False
#    seg_ref = [ (str(instances[idx[0]]), str(instances[idx[1]])) for idx in seg_ref ]

#    # dTimer.qcheck("Assign ref")
#    graph = assignProb(
#        exprGraph, rel_ref, edge_prob, sym_list, rel_list, sym_prob=symProbs, sort=False )
#    # dTimer.qcheck("Assign prob")

#    # Prunes edges IF a binary relationship detector is used.
#    if rel_detector is not None:  # remove extra edges
#        for ( i, pair ) in enumerate(rel_ref):
#            ( pid, cid ) = pair
#            if rel_detector[i] == 0:
#                graph.remove_edge(pid, cid)

#    # dTimer.qcheck("Prune edges")
#    # Here we merge primitives (e.g., strokes) into symbols.
#    # RZ: This (should - have not checked) merge primitives and compute
#    # the average class distribution across individual classifications.
#    if segments is not None:
#       ( symbols, symGraph ) = convert_to_symlevel(graph, symProbs, sym_list,
#                                                   rel_list, seg=segments, instances=instances,
#                                                   seg_ref=seg_ref, given_sym=given_sym,
#                                                   node_segment=node_segment)
#    else:
#        ( symbols, symGraph ) = convert_to_symlevel(graph, symProbs, sym_list, rel_list,
#                                                    given_sym=given_sym, node_segment=node_segment)
#    # dTimer.qcheck("Convert to symlevel")

#    if extract_mst:
#        # Get the Maximum Spanning Tree from Edmonds' algorithm
#        mst = extract_tree(filename, symGraph, rel_list, removeNoRel, oneRelLimit,
#                           given_rel=given_rel)
#        # dTimer.qcheck("Extract tree")
#        return mst, symbols
#    else:
#        graph = binary_graph(symGraph, removeNoRel, given_rel=given_rel)
#        # dTimer.qcheck("Extract graph")

#        # with open("timing-graph.csv", "a") as f:
#        #     f.write(str(dTimer))
#        return graph, symbols

#def parse_output_create_lg(data):
#    # dTimer_new = DebugTimer("Parse output graph and create lg")
#    formula_id, formula_object, given_sym, lg_dir, given_rel, lg_output,\
#        output_data, symbols, rels, oneRelLimit, node_seg, dataset, \
#        extract_mst, server_mode = data    

#    output_graph, symbols, predicted_seg_labels, \
#        node_instances, contours = parse_output_graph(formula_id, formula_object, given_sym,
#                                                      lg_dir, given_rel, lg_output, output_data,
#                                                      symbols, rels, oneRelLimit, node_seg,
#                                                      dataset, extract_mst, server_mode)
#    # dTimer_new.qcheck("Parse output graph")

#    lg_string = create_lg_string(output_graph, symbols, predicted_seg_labels,
#                                 node_instances, contours, write_file=lg_output,
#                                 lg_dir=lg_dir, filename=formula_id, given_sym=given_sym,
#                                 dataset=dataset, server_mode=server_mode)
#    # dTimer_new.qcheck("Create lg string")
#    # with open("timing.csv", "w+") as f:
#    #     f.write(str(dTimer_new))

#    return [lg_string + "<end lg>"]


#def parse_output_graph(formula_id, formula_object, given_sym, lg_dir, given_rel,
#                       lg_output, outputs_acum_dict, symbols, rels,
#                       oneRelLimit=False, node_seg=False, dataset="chem", extract_mst=False, 
#                       server_mode=False):    
#    # dTimer = DebugTimer("Parse output graph")
#    rel_edge_probs = outputs_acum_dict["rel_edge_probs"]
#    node_probs = outputs_acum_dict["node_probs"]
#    seg_edge_predicted_labels = outputs_acum_dict["seg_edge_predicted_labels"]

#    fileName = formula_id
#    pdfName = formula_id
#    curr_doc = pdfName
    
#    rel_edge_tuples = formula_object.rel_edges
#    seg_edge_tuples = formula_object.seg_edges
#    node_instances = formula_object.instances

#    contours = formula_object.contours
#    expr_graph = formula_object.expression_graph
    
#    # dTimer.qcheck("Collect data")
#    graph, symbols = create_output_graph( fileName, expr_graph, rel_edge_tuples, rel_edge_probs, 
#                                seg_edge_tuples, node_instances, node_probs, symbols, 
#                                rels, segments=seg_edge_predicted_labels, removeNoRel=True,
#                                oneRelLimit=oneRelLimit, given_sym=given_sym,
#                                node_segment=node_seg,
#                                given_rel=given_rel, to_lg=lg_output, lg_dir=lg_dir,
#                                dataset=dataset, extract_mst=extract_mst,
#                                annotations=contours, server_mode=server_mode
#                            )  

#    # graph, symbols = create_output_graph_old( fileName, expr_graph, rel_edge_tuples, rel_edge_probs, 
#    #                             seg_edge_tuples, node_instances, node_probs, symbols, 
#    #                             rels, segments=seg_edge_predicted_labels, removeNoRel=True,
#    #                             oneRelLimit=oneRelLimit, given_sym=given_sym,
#    #                             node_segment=node_seg,
#    #                             given_rel=given_rel, to_lg=lg_output, lg_dir=lg_dir,
#    #                             dataset=dataset, extract_mst=extract_mst,
#    #                             annotations=contours, server_mode=server_mode
#    #                         )  
#    return graph, symbols, seg_edge_predicted_labels, node_instances, contours

#def create_lg_string(graph, symbols, predicted_labels, node_instances,
#                     annotations, write_file=False,
#                     lg_dir=None, filename=None, top_k=False, given_sym=False,
#                     dataset="chem", server_mode=False):
#    # dTimer = DebugTimer("create_lg_string")
#    lgString = ORstring(graph, symbols, predicted_labels, top_k, given_sym,
#                        node_instances, annotations, dataset )
#    # dTimer.qcheck("Generate Lg string")
#    if write_file and not server_mode:
#        base_file_name = filename + ".lg"
#        with open(path.join(lg_dir, base_file_name), "w") as f:
#            f.write( lgString )

#    # dTimer.qcheck("Write lg string")
#    # with open("timing-lg.csv", "a") as f:
#    #     f.write(str(dTimer))
#    return lgString


## RZ: Change to remove string
## DO NOT generate top-k results by default
#def postprocessGenORLg( filename, exprGraph, rel_edges, edge_prob, seg_ref, 
#                       instances, sym_prob, sym_list, rel_list, rel_detector=None, 
#                       predictedLabels=True, segments=None, removeNoRel=False,
#                        oneRelLimit=False, given_sym=False, top_k = False,
#                        node_segment=True, given_rel=False, to_lg=False, 
#                       lg_dir=None, dataset="infty", extract_mst=True,
#                       annotations=None, server_mode=False):

#    # dTimer = DebugTimer("INIT")
#    # Attach probabilities to primitives and edges
#    symProbs = dict(zip(instances, sym_prob))
#    rel_ref = [ (str(instances[idx[0]]), str(instances[idx[1]])) for idx in rel_edges ]
#    # prune_segment = False
#    seg_ref = [ (str(instances[idx[0]]), str(instances[idx[1]])) for idx in seg_ref ]

#    # dTimer.check("Assign ref")
#    graph = assignProb(
#        exprGraph, rel_ref, edge_prob, sym_list, rel_list, sym_prob=symProbs, sort=False )
#    # dTimer.check("Assign prob")

#    # Prunes edges IF a binary relationship detector is used.
#    if rel_detector is not None:  # remove extra edges
#        for ( i, pair ) in enumerate(rel_ref):
#            ( pid, cid ) = pair
#            if rel_detector[i] == 0:
#                graph.remove_edge(pid, cid)

#    # dTimer.check("Prune edges")
#    # Here we merge primitives (e.g., strokes) into symbols.
#    # RZ: This (should - have not checked) merge primitives and compute
#    # the average class distribution across individual classifications.
#    if segments is not None:
#       ( symbols, symGraph ) = convert_to_symlevel(graph, symProbs, sym_list,
#                                                   rel_list, seg=segments, instances=instances,
#                                                   seg_ref=seg_ref, given_sym=given_sym,
#                                                   node_segment=node_segment)
#    else:
#        ( symbols, symGraph ) = convert_to_symlevel(graph, symProbs, sym_list, rel_list,
#                                                    given_sym=given_sym, node_segment=node_segment)
#    # dTimer.check("Convert to symlevel")
#    #print("given_sym")
#    #print(given_sym)
#    #pcheck(symbols, "symbols")
#    # dTimer.check("Convert to symlevel")

#    if extract_mst:
#        # Get the Maximum Spanning Tree from Edmonds' algorithm
#        mst = extract_tree(filename, symGraph, rel_list, removeNoRel, oneRelLimit,
#                           given_rel=given_rel)
#    else:
#        mst = binary_graph(symGraph, removeNoRel, given_rel=given_rel)
                           
#    # dTimer.check("Extract tree/graph")

#    lgString = ORstring( mst, symbols, predictedLabels, top_k, given_sym,
#                        instances, annotations, dataset )

#    # dTimer.check("ORstring")

#    if to_lg and not server_mode:
#        base_file_name = filename + ".lg"
#        with open(path.join(lg_dir, base_file_name), "w") as f:
#            f.write( lgString )

#    # dTimer.check("Write")
#    return lgString

#def binary_graph(exprGraph, remove_NoRel, given_rel=False, key=0):
#    exprGraph_new = nx.MultiDiGraph()
#    exprGraph_new.add_nodes_from(exprGraph.nodes(data=True))

#    if remove_NoRel:  
#        # remove all edges with Norelation as highest prob
#        for pid, cid, data in exprGraph.edges(data=True):
#            gt_relation = exprGraph[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]
#            # Highest confident edge
#            relation = exprGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][key]

#            if relation == "NoRelation":
#                continue

#            # AKS: Assign the gt relation weight to highest so that it is
#            # selected during MST construction
#            if given_rel and (relation == gt_relation):
#                weight = 1.0
#            else:
#                weight = exprGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][key]

#            exprGraph_new.add_edge(pid, cid, weight=weight,
#                                     relation=relation, gt_relation=gt_relation)
#    return exprGraph_new

#def find_symedge_prob(sympair, symDict, graph, norm=False):
#    pid, cid = sympair[0], sympair[1]
#    stkgroup1, stkgroup2 = symDict[pid], symDict[cid]
#    sum_prob = 0
#    count = 0
#    all_edges = list(itertools.product(stkgroup1, stkgroup2))

#    for pair in all_edges:
#        if pair in graph.edges():
#            edge_prob = graph[pair[0]][pair[1]][EDGE_ATTRIBUTES.CLASS_CONFIDENCES]
#            sum_prob += edge_prob
#            count += 1

#    mean_prob = sum_prob / count
#    # normalize needed if softmax missing
#    if norm:
#        mean_prob = softmax(mean_prob)
#    return mean_prob


#def find_symnode_class(symNode, symDict, graph, nodeProbs, SYMlist, topk=5):
#    stkgroup = symDict[symNode]
#    match = {}

#    for stk in stkgroup:
#        label = graph.nodes[stk][NODE_ATTRIBUTES.PREDICTED_CLASS]
#        topk_labels_conf = graph.nodes[stk][NODE_ATTRIBUTES.PREDICTED_TOPK_CLASS]
#        match[label] = stk

#    if not len(match) == 1:
#        sum_prob = 0
#        for stk in stkgroup:
#            sum_prob += nodeProbs[int(stk)]
#        avg_prob = sum_prob / len(stkgroup)

#        topk_idx = avg_prob.argsort()[::-1][:topk]
#        topk_conf = avg_prob[topk_idx]
#        topk_labels = np.array(SYMlist)[topk_idx]
#        topk_labels = [lbl.replace(",", "COMMA") for lbl in topk_labels]
#        label = topk_labels[0]
#        topk_labels_conf = list(zip(topk_labels, topk_conf))

#    return ( label, topk_labels_conf )

#def convert_to_symlevel(
#    graph,
#    symProbs,
#    SYMlist,
#    RELlist,
#    seg=None,
#    fromGroundTruth=False,
#    instances=False,
#    seg_ref=None,
#    given_sym=False,
#    node_segment=True,
#):
#    symbols = {}
#    # group strokes based on segmentation results
    
#    if seg is not None:
#        # Remove merge edges when symbols given
#        # if given_sym:
#        #     seg = [1] * len(seg)
#        cc_graph = nx.Graph()
#        cc_graph.add_nodes_from(graph.nodes())

#        # if len(seg) == len(ref):
#        if not node_segment:
#            for i, pair in enumerate(seg_ref):                               
#                if seg[i] == 0:
#                    pid, cid = pair
#                    cc_graph.add_edge(pid, cid)
#        else:
#            for i, stk in enumerate(instances):
#                if i == 0:
#                    continue
#                if seg[i] == 0:
#                    cc_graph.add_edge(str(instances[i]), str(instances[i - 1]))
#    # Merge relation included in parsing
#    else:
#        if fromGroundTruth:
#            cc_graph = nx.Graph(
#                [
#                    (u, v, d)
#                    for u, v, d in graph.edges(data=True)
#                    if d[EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] == "MERGE"
#                ]
#            )
#        else:
#            cc_graph = nx.Graph(
#                [
#                    (u, v, d)
#                    for u, v, d in graph.edges(data=True)
#                    if d[EDGE_ATTRIBUTES.POSSIBLE_CLASSES][0] == "MERGE"
#                ]
#            )

#        cc_graph.add_nodes_from(graph.nodes())

#    connected = nx.connected_components(cc_graph)
#    for i, cc in enumerate(connected):
#        newId = "sym" + str(i)
#        symbols[newId] = cc  # stk group mapping to symbols

#    # make a new graph on symbols
#    symGraph = GraphConstruction.create_nodes(symbols.keys())
#    # add edges to symgraph
#    gg = list(zip(symbols.keys(), symbols.values()))
#    for i, sym in enumerate(gg):
#        symtag = sym[0]
#        stkgroup = list(sym[1])
#        for j, sym2 in enumerate(gg[i:]):
#            symtag2 = sym2[0]
#            stkgroup2 = list(sym2[1])
#            if symtag == symtag2:
#                continue

#            inedge = list(itertools.product(stkgroup, stkgroup2))  # comingedges
#            outedge = list(itertools.product(stkgroup2, stkgroup))
#            if any(map(lambda v: v in inedge, graph.edges())):
#                symGraph.add_edge(symtag, symtag2)
#            if any(map(lambda v: v in outedge, graph.edges())):
#                symGraph.add_edge(symtag2, symtag)

#    # add attributes
#    symGraph = GraphConstruction.add_edge_attributes(symGraph)
#    symGraph = GraphConstruction.add_node_attributes(symGraph)

#    # set edge attributes
#    for pid, cid in symGraph.edges():
#        edge_prob = find_symedge_prob((pid, cid), symbols, graph, norm=True)
#        sort_pred = sorted(edge_prob, reverse=True)
#        class_idx = sorted(range(len(edge_prob)), key=lambda k: -edge_prob[k])
#        class_names = [RELlist[j] for j in class_idx]

#        symGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES] = class_names
#        symGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES] = sort_pred

#        cc_pid = sorted(symbols[pid], key=lambda x: int(x))[0]
#        cc_cid = sorted(symbols[cid], key=lambda x: int(x))[0]
#        if (cc_pid, cc_cid) in graph.edges():
#            # AKS: Add gt relation from primitive level graph
#            symGraph[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = \
#                graph[cc_pid][cc_cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]

#    # set node attributes
#    for symtag in symGraph.nodes():
#        label, topk_labels_conf = find_symnode_class(
#            symtag, symbols, graph, symProbs, SYMlist
#        )
#        symGraph.nodes[symtag][NODE_ATTRIBUTES.PREDICTED_CLASS] = label
#        symGraph.nodes[symtag][NODE_ATTRIBUTES.PREDICTED_TOPK_CLASS] = topk_labels_conf
#        # Add ground truth to the symbol
#         # AKS: Fix bug which assign different label in each run
#        symGraph.nodes[symtag][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS] = graph.nodes[
#            sorted(symbols[symtag], key=lambda x: int(x))[0]
#        ][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]

#    return symbols, symGraph


#def assignProb(
#    exprGraph,
#    edge_ref,
#    edge_prob,
#    SYMlist,
#    RELlist,
#    sym_prob=None,
#    normW=True,
#    sort=True,
#    topk=5,
#):
#    # Behavior if a prior graph is computed.
#    if len(exprGraph.edges()) != 0:
#        for i, pair in enumerate(edge_ref):
#            pid, cid = pair            
#            sort_pred = sorted(edge_prob[i], reverse=True)
#            if normW:
#                sort_pred = softmax(sort_pred)

#            class_idx = sorted(range(len(edge_prob[i])), key=lambda k: -edge_prob[i][k])
#            class_names = [RELlist[j] for j in class_idx]

#            exprGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES] = class_names
#            exprGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES] = (
#                sort_pred if sort else edge_prob[i]
#            )

#    if sym_prob:
#        # add node labels (TODO check if there is conflicts)
#        for n in exprGraph.nodes():
#            topk_idx = sym_prob[int(n)].argsort()[::-1][:topk]
#            topk_conf = sym_prob[int(n)][topk_idx]
#            topk_labels = np.array(SYMlist)[topk_idx]

#            [lbl.replace(",", "COMMA") for lbl in topk_labels]
#            label = topk_labels[0]
#            exprGraph.nodes[n][NODE_ATTRIBUTES.PREDICTED_TOPK_CLASS] = list(
#                zip(topk_labels, topk_conf)
#            )
#            exprGraph.nodes[n][NODE_ATTRIBUTES.PREDICTED_CLASS] = label

#    return exprGraph

#'''
#    Exract the Maximum Spanning Tree, using Edmonds' Arborescence Algm.
#    Apply 'one symbol, one child relationhip type' constraint if asked.
#'''
#def extract_tree(filename, graph, RELlist, remove_NoRel, oneRelLimit,
#                 given_rel=False):
#    graph = setWeights(graph, RELlist, remove_NoRel, given_rel=given_rel)
#    if not oneRelLimit:
#        try:
#            mst = alg.maximum_spanning_arborescence(graph, attr='weight',
#                    default=0.0, preserve_attrs=True)
#        except nx.exception.NetworkXException as e:
#            print("  >> Warning: Edmonds' algorithm failed for " + filename + \
#                  "     using complete graph.")
#            print(e)
#            import traceback
#            print(traceback.print_exc())
#            import pdb; pdb.set_trace()
#            mst = graph.copy()
#    else:
#        # Extract an MST with Edmonds until no relationship conflicts.
#        # May not always extract MST - have not proven optimality.
#        removed = True
#        while removed:
#            removed = False
#            parent_rels = defaultdict(lambda:([], [])) # parent: rel, child
#            # Run Edmonds' algorithm to obtained directed MST
#            try:
#                mst = alg.maximum_spanning_arborescence(graph, attr='weight',
#                        default=0.0, preserve_attrs=True)
#            except nx.exception.NetworkXException as e:
#                # print("  >> Warning: Edmonds' algorithm failed for " + filename + \
#                #       "     using complete graph.")
#                # print(e)
#                # import traceback
#                # print(traceback.print_exc())
#                mst = graph.copy()

#            # Iterate over edges in the MST
#            for ( pid, cid, key, data ) in mst.edges( data=True, keys=True ):
#                rel = mst[pid][cid][key]["relation"]

#                # Check if parent has this relationship recorded
#                if rel in parent_rels[pid][0]:
#                    idx = parent_rels[pid][0].index(rel)
#                    cid2 = parent_rels[pid][1][idx]

#                    key1 = list(graph[pid][cid].keys())[0]
#                    key2 = list(graph[pid][cid2].keys())[0]

#                    # If a higher weight edge with the same parent and
#                    # relationship is found, add to table and remove current
#                    # edge from the graph.
#                    if graph[pid][cid][key1]["weight"] > \
#                            graph[pid][cid2][key2]["weight"]:
#                        # Remove old edge from table AND graph
#                        parent_rels[pid][0].remove(rel) 
#                        parent_rels[pid][1].remove(cid2)
#                        graph.remove_edge(pid, cid2, key=key2) 
#                        removed = True
                        
#                        # Add higher weighted edge to table
#                        parent_rels[pid][0].append(rel)
#                        parent_rels[pid][1].append(cid)

#                    else:
#                        # Remove the edge from the graph
#                        graph.remove_edge(pid, cid, key=key1)
#                        removed = True

#                else: 
#                    # Add edge to table for parent's new relationship type
#                    parent_rels[pid][0].append(rel)
#                    parent_rels[pid][1].append(cid)

#    # MM: use the symbol predicts to update node
#    for ( n, data ) in graph.nodes(data=True):
#        nx.set_node_attributes(mst, {n: data})

#    return mst

## def average_node_probabilities(connected_components, sym_prob):
##     """
##     Average node probabilities for each connected component using numpy.ufunc.reduce
##     and replace merged node rows by their average probabilities.
##     """
##     # Apply softmax to the input probabilities
##     sym_prob = softmax(sym_prob, axis=1)
    
##     # Flatten all nodes in connected components and find unique indices
##     flat_indices = np.concatenate([list(component) for component in connected_components])
##     unique_indices = np.unique(flat_indices)
    
##     # Create a mapping to store the indices corresponding to each component
##     index_map = {i: list(component) for i, component in enumerate(connected_components)}
##     import pdb; pdb.set_trace()
    
##     # Prepare an array to store averaged probabilities
##     merged_probs = np.copy(sym_prob)
    
##     # Calculate average probabilities for each connected component
##     for i, indices in index_map.items():
##         avg_probs = np.add.reduce(sym_prob[indices], axis=0) / len(indices)
##         merged_probs[indices] = avg_probs  # Replace rows with the averaged probabilities
    
##     return merged_probs, index_map

## def construct_collapsed_graph(connected_components, collapsed_node_probs, filtered_edges, collapsed_edge_probs, sym_list, collapsed_node_map):
##     """
##     Construct the collapsed graph using the averaged probabilities for nodes and edges.
##     """
##     collapsed_graph = nx.Graph()
##     symbols = {}

##     # Add nodes to the collapsed graph
##     for i, component in enumerate(connected_components):
##         new_node = f'sym_{i}'
##         collapsed_graph.add_node(new_node)
##         # Assign the averaged probabilities to the new node
##         collapsed_graph.nodes[new_node]['probabilities'] = collapsed_node_probs[i]
##         # Create symbols mapping
##         symbols[new_node] = [sym_list[node] for node in component]

##     # Add edges to the collapsed graph
##     for edge, probs in zip(filtered_edges, collapsed_edge_probs):
##         node_u, node_v = edge
##         new_node_u = [k for k, v in collapsed_node_map.items() if node_u in v][0]
##         new_node_v = [k for k, v in collapsed_node_map.items() if node_v in v][0]
##         collapsed_graph.add_edge(new_node_u, new_node_v, probabilities=probs)

##     return collapsed_graph, symbols

## def average_and_filter_edge_probabilities(connected_components, graph, rel_edges, edge_prob,
##                                           rel_list, removeNoRel):
##     """
##     Average edge probabilities for merged edges and filter out 'NoRelation' edges.
##     """
##     collapsed_edge_probs = []
##     filtered_edges = []
##     for i, component in enumerate(connected_components):
##         component_edges = []
##         for node in component:
##             for neighbor in graph.neighbors(node):
##                 if neighbor not in component:
##                     component_edges.append((node, neighbor))

##         # Average the edge probabilities
##         edge_indices = [rel_edges.index(edge) for edge in component_edges if edge in rel_edges]
##         if edge_indices:
##             avg_edge_probs = np.mean(edge_prob[edge_indices, :], axis=0)
##             if removeNoRel:
##                 # Remove edges where 'NoRelation' is the predicted class
##                 if np.argmax(avg_edge_probs) != rel_list.index("NoRelation"):
##                     collapsed_edge_probs.append(avg_edge_probs)
##                     filtered_edges.append((component_edges[0]))  # Use the first edge as representative
##             else:
##                 collapsed_edge_probs.append(avg_edge_probs)
##                 filtered_edges.append((component_edges[0]))  # Use the first edge as representative

##     collapsed_edge_probs = np.array(collapsed_edge_probs)
##     return collapsed_edge_probs, filtered_edges

## def numpy_index_reduce(data, indices, dim, reduction='mean'):
##     """
##     Mimic torch.index_reduce behavior using NumPy.
    
##     Parameters:
##     - data: The input array.
##     - indices: Indices at which to reduce.
##     - dim: The dimension along which to reduce (0 or 1).
##     - reduction: Type of reduction ('sum', 'mean', etc.).
    
##     Returns:
##     - Reduced array.
##     """
##     if dim != 0:
##         raise NotImplementedError("Currently only supports reducing along dimension 0.")

##     # Determine the shape of the reduced output
##     output_shape = list(data.shape)
##     output_shape[0] = indices.max() + 1
##     output = np.zeros(output_shape, dtype=data.dtype)

##     if reduction == 'sum':
##         np.add.at(output, indices, data)
##     elif reduction == 'mean':
##         counts = np.bincount(indices, minlength=output.shape[0])
##         np.add.at(output, indices, data)
##         output /= counts[:, None]  # Broadcast division across all columns
##     else:
##         raise ValueError("Unsupported reduction type. Use 'sum' or 'mean'.")
##     return output


## def setWeights(exprGraph, rel_list, remove_NoRel, oneRelLimit=False,
##                given_rel=False):
##     exprGraph_edges = exprGraph.edges(data=True)
##     exprGraph_multi = nx.MultiDiGraph()
##     exprGraph_multi.add_nodes_from(exprGraph.nodes(data=True))

##     if remove_NoRel:  
##         # remove all edges with Norelation as highest prob
##         for pid, cid, data in exprGraph.edges(data=True):
##             gt_relation = exprGraph[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]
##             for key in range(len(rel_list)):
##                 relation = exprGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][key]

##                 if relation == "NoRelation":
##                     continue

##                 # AKS: Assign the gt relation weight to highest so that it is
##                 # selected during MST construction
##                 if given_rel and (relation == gt_relation):
##                     weight = 1.0
##                 else:
##                     weight = exprGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][key]

##                 exprGraph_multi.add_edge(pid, cid, weight=weight,
##                                          relation=relation, gt_relation=gt_relation)

##     # TODO: Modify else for multiedges
##     else:  # switch to next prob when NoRelation is the highest prob
##         cls_index = {}
##         for pid, cid in exprGraph.edges():
##             # set weights and handle NoRelation
##             cls_index[(pid, cid)] = 0
##             if exprGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][0] == "NoRelation": 
##                 cls_index[(pid, cid)] += 1
##             if (cls_index[(pid, cid)] > len(rel_list) or 
##                 exprGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][cls_index[(pid, cid)]] == 0):
##                 exprGraph.remove_edge(pid, cid)
##                 del cls_index[(pid, cid)]

##         # assigne the highest prob to each edge
##         for (pid, cid), index in cls_index.items():
##             exprGraph[pid][cid]["weight"] = \
##                 exprGraph[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][index]

##             exprGraph[pid][cid][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] = \
##                 exprGraph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][index]

##     return exprGraph_multi
