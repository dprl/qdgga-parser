import sys
import base64
import os
import os.path as osp
import glob
from qdgga_settings import DATA_DIR
from tqdm import tqdm

import streamlit as st
st.set_page_config(layout="wide")

INFTY_DATA_DIR = osp.join(DATA_DIR, "InftyMCCDB-2")
INFTY_PNG_DIR = osp.join(INFTY_DATA_DIR, "png")

TRAIN_DIR = osp.join(INFTY_DATA_DIR, "train")
TEST_DIR = osp.join(INFTY_DATA_DIR, "test_merged_class")

PUNC_DOT_PDF_DIR = osp.join(INFTY_DATA_DIR, "punc_dot_pdfs")
DIFF_PUNC_DOT_PDF_DIR = osp.join(PUNC_DOT_PDF_DIR, "diff")
ORIGINAL_PUNC_DOT_PDF_DIR = osp.join(PUNC_DOT_PDF_DIR, "original")

MODIFIED_FILES_TRAIN_DIR = osp.join(INFTY_DATA_DIR, "train_punc")
MODIFIED_FILES_TEST_DIR = osp.join(INFTY_DATA_DIR, "test_merged_class_punc")

PUNC_FILES_TRAIN_FILE = osp.join(DATA_DIR, "training_PUNC.txt")
PUNC_FILES_TEST_FILE = osp.join(DATA_DIR, "testing_PUNC.txt")


def generate_pdfs(punc_files_train, modified_files_train, punc_files_test,
                  modified_files_test):
    print("Generating dotpdfs (differences + gt) for test punc files")
    for file in tqdm(punc_files_test):
        os.system("python $LgEvalDir/src/lg2dot.py " +
                  osp.join(MODIFIED_FILES_TEST_DIR, file + ".lg") + " " +
                  osp.join(TEST_DIR, file + ".lg") + " >" + 
                  os.path.join(DIFF_PUNC_DOT_PDF_DIR, file + ".dot"))
        os.system("dot -Tpdf " + os.path.join(DIFF_PUNC_DOT_PDF_DIR, file + ".dot") + " -o "
            + os.path.join(DIFF_PUNC_DOT_PDF_DIR, file + ".pdf"))

        # os.system("python $LgEvalDir/src/lg2dot.py " +
        #           osp.join(TEST_DIR, file + ".lg") + " >" + 
        #           os.path.join(ORIGINAL_PUNC_DOT_PDF_DIR, file + ".dot"))
        # os.system("dot -Tpdf " + os.path.join(ORIGINAL_PUNC_DOT_PDF_DIR, file + ".dot") + " -o "
        #     + os.path.join(ORIGINAL_PUNC_DOT_PDF_DIR, file + ".pdf"))


    print("\nGenerating dotpdfs (differences + gt) for train punc files")
    for file in tqdm(punc_files_train):
        os.system("python $LgEvalDir/src/lg2dot.py " +
                  osp.join(MODIFIED_FILES_TRAIN_DIR, file + ".lg") + " " +
                  osp.join(TRAIN_DIR, file + ".lg") + " >" + 
                  os.path.join(DIFF_PUNC_DOT_PDF_DIR, file + ".dot"))
        os.system("dot -Tpdf " + os.path.join(DIFF_PUNC_DOT_PDF_DIR, file + ".dot") + " -o "
            + os.path.join(DIFF_PUNC_DOT_PDF_DIR, file + ".pdf"))

        # os.system("python $LgEvalDir/src/lg2dot.py " +
        #           osp.join(TRAIN_DIR, file + ".lg") + " >" + 
        #           os.path.join(ORIGINAL_PUNC_DOT_PDF_DIR, file + ".dot"))
        # os.system("dot -Tpdf " + os.path.join(ORIGINAL_PUNC_DOT_PDF_DIR, file + ".dot") + " -o "
        #     + os.path.join(ORIGINAL_PUNC_DOT_PDF_DIR, file + ".pdf"))


@st.cache(suppress_st_warning=True)
def disp_table(files, n=100):
    idx = 0
    for i in range(n): # number of rows in your table! = 2
        # c1, c2 = st.columns(1, 2) # number of columns in each row! = 2
        cols = st.columns([1,  4]) # number of columns in each row! = 2
        if i % 2 == 0:
            # file = osp.splitext(osp.split(files[i])[-1])[0]
            file = files[idx]
            # print(osp.split(file))
            # print(file)
            # print(PUNC_DOT_PDF_DIR)
            cols[0].text(file)
            pdf_file = osp.join(ORIGINAL_PUNC_DOT_PDF_DIR, file + ".pdf")
        else:
            file = files[idx]
            cols[0].image(osp.join(INFTY_PNG_DIR, file + ".PNG"), use_column_width=True)
            pdf_file = osp.join(DIFF_PUNC_DOT_PDF_DIR, file + ".pdf")
            idx += 1
    
        with open(pdf_file, "rb") as f:
            base64_pdf = base64.b64encode(f.read()).decode('utf-8')
        pdf_display = f'<iframe src="data:application/pdf;base64,{base64_pdf}" width="1000" height="300" type="application/pdf"></iframe>'
    
        cols[1].markdown(pdf_display, unsafe_allow_html=True)
    
        # with open(diff_pdf_files[i], "rb") as f:
        #     base64_pdf = base64.b64encode(f.read()).decode('utf-8')
        # pdf_display2 = f'<iframe src="data:application/pdf;base64,{base64_pdf}" width="900" height="300" type="application/pdf"></iframe>'
        # cols[2].markdown(pdf_display2, unsafe_allow_html=True)
    
        # first column of the ith row
        # c1.image(files[i], use_column_width=True)
        # c2.image(files[i], use_column_width=True)
        # cols[1].image(files[i], use_column_width=True)
        # cols[1].image(files[i], use_column_width=True)

def visualize(punc_files_train, modified_files_train, punc_files_test,
              modified_files_test):
    # import pdb; pdb.set_trace()
    st.title('PUNC Modification Visualizations')
    
    st.header(f'Modified PUNC files - Train - {len(modified_files_train)}')
    disp_table(modified_files_train)

    unmodified_files_train = list(set(punc_files_train) -
                                  set(modified_files_train))
    st.header(f'Unmodified PUNC files - Train - {len(unmodified_files_train)}')
    disp_table(unmodified_files_train)

    st.header(f'Modified PUNC files - Test - {len(modified_files_test)}')
    disp_table(modified_files_test)

    unmodified_files_test = list(set(punc_files_test) -
                                  set(modified_files_test))
    st.header(f'Unmodified PUNC files - Test - {len(unmodified_files_test)}')
    disp_table(unmodified_files_test)


def main_page(punc_files_train, modified_files_train, punc_files_test,
                      modified_files_test):
    st.markdown("# Main page 🎈")
    st.sidebar.markdown("# Main page 🎈")

def modified_train(punc_files_train, modified_files_train, punc_files_test,
                      modified_files_test):
    st.header(f'Modified PUNC files - Train - {len(modified_files_train)}')
    disp_table(modified_files_train)

def unmodified_train(punc_files_train, modified_files_train, punc_files_test,
                      modified_files_test):
    unmodified_files_train = list(set(punc_files_train) -
                                  set(modified_files_train))
    st.header(f'Unmodified PUNC files - Train - {len(unmodified_files_train)}')
    disp_table(unmodified_files_train)


def modified_test(punc_files_train, modified_files_train, punc_files_test,
                      modified_files_test):
    st.header(f'Modified PUNC files - Test - {len(modified_files_test)}')
    disp_table(modified_files_test)


def unmodified_test(punc_files_train, modified_files_train, punc_files_test,
                      modified_files_test):
    unmodified_files_test = list(set(punc_files_test) -
                                  set(modified_files_test))
    st.header(f'Unmodified PUNC files - Test - {len(unmodified_files_test)}')
    disp_table(unmodified_files_test)
    
if __name__ == "__main__":
    # if len(sys.argv) < 2:
    #     print("Usage: python visualize-punc.py <generate_pdfs>")
        # sys.exit()

    if len(sys.argv) == 2:
        generate = bool(sys.argv[1])
    else:
        generate = False

    with open(PUNC_FILES_TRAIN_FILE, "r") as f:
        punc_files_train = (f.read().strip().split('\n'))
    with open(osp.join(MODIFIED_FILES_TRAIN_DIR, 
                       "MODIFIED_FILES.txt"), "r") as f:
        modified_files_train = (f.read().strip().split('\n'))

    with open(PUNC_FILES_TEST_FILE, "r") as f:
        punc_files_test = (f.read().strip().split('\n'))
    with open(osp.join(MODIFIED_FILES_TEST_DIR, 
                       "MODIFIED_FILES.txt"), "r") as f:
        modified_files_test = (f.read().strip().split('\n'))

    if generate:
        generate_pdfs(punc_files_train, modified_files_train, punc_files_test,
                      modified_files_test)

    page_names_to_funcs = {
        "Main Page": main_page,
        "modified_train": modified_train,
        "modified_test": modified_test,
        "unmodified_train": unmodified_train,
        "unmodified_test": unmodified_test,
    }

    selected_page = st.sidebar.selectbox("Select a page", page_names_to_funcs.keys())
    page_names_to_funcs[selected_page](punc_files_train, modified_files_train, punc_files_test,
                      modified_files_test)

