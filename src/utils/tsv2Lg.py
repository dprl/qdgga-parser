import os
import shutil
import sys
import os.path as osp
import tqdm

def tsv2lg(tsv_file):
    filename = ""
    lg_out = ""
    pairs = []
    
    with open(tsv_file, 'r') as f:
        for line in f.readlines():
            if not line:
                continue
            entries = line.split('\t')
            if entries[0] == "P":
                filename = osp.split(entries[2].strip())[1]
                filename = osp.splitext(filename)[0]
                if filename == "":
                    print(filename)
            if entries[0] == "LG":
                # Avoid reading as new line
                lg_out = entries[2].strip().replace("\\\\neq", "temp_eq")
                if lg_out == "":
                    print(filename)
            if filename and lg_out:
                pairs.append((filename, lg_out))
                filename = ""
                lg_out = ""
    # import pdb; pdb.set_trace()
    print(">> Tsv file read complete")
    return pairs

def writeLg(pairs, out_dir):
    if osp.exists(out_dir):
        shutil.rmtree(out_dir)
    os.makedirs(out_dir)
    
    # import pdb; pdb.set_trace()
    print(">> Writing lg files")
    for (filename, lg_out) in tqdm.tqdm(pairs):
        with open(osp.join(out_dir, filename + ".lg"), 'w') as f:
            for lines in lg_out.split('\\n'):
                f.write(lines.replace("\"","").replace("temp_eq", "\\neq").replace("\\\\", "\\") + "\n")  
    print(f">> {len(pairs)} lg files written to {out_dir}")

def tsv2lgfile(tsv_file, out_dir):
    pairs = tsv2lg(tsv_file)
    writeLg(pairs, out_dir)
                    
if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("Usage: python tsv2Lg.py <tsv_file.tsv> <output_dir>")
        sys.exit()
    tsv_file = sys.argv[1]
    out_dir = sys.argv[2]
    tsv2lgfile(tsv_file=tsv_file, out_dir=out_dir)
