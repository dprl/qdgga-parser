###########################################################################
#
# dprl_map_reduce.py
#   - Minor decoration of the mr4mp parallel map reduce library
#
###########################################################################
# Created by: Richard Zanibbi, Aug 07 2023 11:17:53
###########################################################################
from dprl_map_reduce import combine_int_lists, map_reduce

class Test(object):
    def __init__(self, x):
        self.x = x

    def z(self):
        return 1 

    def map_int_list(self, in_list ):
        y = self.x
        self.z()
        return [[ item + 1 for item in in_list ]]

    def mred(self):
        print( map_reduce( self.map_int_list, combine_int_lists, 
                          [ [1,2,3], [4,5,6], [self.x, self.x, self.x] ] ) )


# DEBUG: Invocation MUST be protected inside a 'main' block to avoid
# loading the multiprocessing library without side effects/state issues.
if __name__ == '__main__' :
    # print( map_reduce( map_int_list, combine_int_lists, [ [1,2,3], [4,5,6] ] ) )
    # print( map_reduce( map_int_list, combine_int_lists, [ [1,2,3], [2,3,4], [3,4,5],
    #                                                  [4,5,6], [5,6,7], [6,7,8],
    #                                                  [7,8,9], [8,9,10] ]))
    # print( map_concat( map_int_list, [ [1,2,3], [2,3,4], [3,4,5],
    #                                      [4,5,6], [5,6,7], [6,7,8],
    #                                      [7,8,9], [8,9,10] ]))
    # print( map_reduce( map_fn, combine_dict_pair, [ { 1: 2, 3: 4}, { 5:6, 7:8 } ] ) )
    # print( map_reduce( map_fn, combine_dict_pair, [ { 1: 2, 3: 4}, { 5:6, 7:8 } ] ) )

    test = Test(2)
    # print( map_reduce( map_int_list, combine_int_lists, [ [1,2,3], [4,5,6] ] ) )
    print( map_reduce( test.map_int_list, combine_int_lists, [ [1,2,3], [4,5,6],
                                                              [test.x, test.x,
                                                               test.x]] ) )
    test.mred()
    # print( map_reduce( map_fn, combine_dict_tuple, [ { 1: 2, 3: 4}, { 5:6, 7:8 } ] ) )

