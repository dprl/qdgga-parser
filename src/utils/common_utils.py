
from operator import itemgetter
import numpy as np

    
def get_type(label):
    if type(label) == int:
        return "node"
    else:
        if "seg" in label and "rel" in label:
            return "both"
        elif "seg" in label:
            return "seg"
        else:
            return "rel"

def get_labels_features(labels, node_labels_pred, seg_labels_pred, rel_labels_pred):
    type_data = []
    node_label = []
    seg_label = []
    rel_label = []
    offset_node = 0
    offset_seg = 0
    offset_rel = 0    

    seg_probs_index = []
    rel_probs_index = []
    node_probs_index = []

    for count, i in enumerate(labels):
        current_type = get_type(i)
        type_data.append(current_type)
        if current_type == "node":
            node_label.append(node_labels_pred[offset_node].item())            
            seg_label.append(-1)
            rel_label.append(-1)
            
            seg_probs_index.append(-1)
            rel_probs_index.append(-1)
            node_probs_index.append(offset_node)

            offset_node += 1
        elif current_type == "seg":
            node_label.append(-1)
            seg_label.append(seg_labels_pred[offset_seg].item())            
            rel_label.append(-1)

            seg_probs_index.append(offset_seg)
            rel_probs_index.append(-1)
            node_probs_index.append(-1)

            offset_seg += 1
        elif current_type == "rel":
            node_label.append(-1)
            seg_label.append(-1)
            rel_label.append(rel_labels_pred[offset_rel].item())   

            seg_probs_index.append(-1)
            rel_probs_index.append(offset_rel)
            node_probs_index.append(-1)

            offset_rel += 1
        elif current_type == "both":
            node_label.append(-1)
            seg_label.append(seg_labels_pred[offset_seg].item())
            rel_label.append(rel_labels_pred[offset_rel].item())

            seg_probs_index.append(offset_seg)
            rel_probs_index.append(offset_rel)
            node_probs_index.append(-1)

            offset_seg += 1
            offset_rel += 1
    return node_label,seg_label, rel_label, type_data, node_probs_index,seg_probs_index, rel_probs_index

def separate_features(data):
        
    label = data["label"]
    feat_type = data["type_feature"]
    
    node_feat, pair_feat, node_pos_enc, pair_pos_enc = [], [], [], []
    rel_indices, seg_indices, rel_labels, seg_labels, sym_labels = [], [], [], [], []
    # TODO: Move this to a function
    feat_type_array = np.array(feat_type)
    unique_values = np.unique(feat_type_array)
    indices_feat = {value: np.where(feat_type_array == value)[0].tolist() 
                    for value in unique_values}
    node_indices = indices_feat.get('node', [])
    if len(node_indices):        
        sym_labels = list(itemgetter(*node_indices)(label))

    pair_indices = indices_feat.get('pair', [])
    if len(pair_indices):        
        edge_labels = list(itemgetter(*pair_indices)(label))

        rel_labels = []
        seg_labels = []
        for idx, edge_label in enumerate(edge_labels):
            for edge_type, lbl in edge_label.items():
                if edge_type == "rel":
                    rel_labels.append(lbl)
                    rel_indices.append(idx)
                elif edge_type == "seg":
                    seg_labels.append(lbl)
                    seg_indices.append(idx)