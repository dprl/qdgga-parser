import os
import shutil
import sys
import os.path as osp
import tqdm
import numpy as np
import math

from collections import defaultdict
from qdgga_settings import DATA_DIR
from src.datasets.math_symbol import get_relation_labels
from protables.ProTables import sort_CCs

def read_lg(file):
    (symLabels, comps, boxes) = {}, {}, {}
    cc2O = {}

    with open(file, "r") as tsvFile:
        for line in tsvFile:
            info = [p.strip(",") for p in line.strip().split(" ")]
            entry_type = info[0].lower()

            if entry_type == "o":
                label = info[2]
                 # AKS: Convert id to int
                instance = info[1]
                symLabels[instance] = label
                # AKS: Convert id to int
                comps[instance] = [int(x) for x in info[4:]]
                cc2O.update({x:instance for x in info[4:]})

            elif entry_type == "#cc":
                # AKS: Convert id to int
                instance = int(info[1])
                boxes[instance] = np.array(info[2:]).astype(int)

        instances = sort_CCs(boxes)
        sym_tags = []
        bboxes = []
        for cc in instances:
            for key, value in comps.items():
                if cc in value:
                    sym_tags.append(key)
                    bbox = boxes[cc]
                    bboxes.append(bbox)

    return instances, symLabels, comps, bboxes, cc2O


def ORstring(filename, symLabels, comps, sym_edges, stk_bboxes, removed_edges,
             added_edges):
    # Write object lines
    prefix = "O, "
    middle = ", 1.0, "
    outString = "# IUD, " + filename + "\n"
    outString += f"# [ OBJECTS ]\n# Objects (O): {len(symLabels)} \n"
    outString += "### Format: O, symbolId, class, 1.0 (weight), [ primitiveId list ]\n"
    for symId, label in symLabels.items():
        prims = ", ".join(str(x) for x in sorted(comps[symId], key=lambda x: int(x)))
        outString += prefix + str(symId) + ", " + label + middle + prims + "\n"
            
    # Write relation lines
    outString += "\n# [ RELATIONSHIPS ]\n"
    outString += f"# Object Relationships (R): {len(sym_edges)}\n"
    outString += "### Format: R, parentId, childId, class, 1.0 (weight)\n"
    prefix = "R, "
    suffix = ", 1.0"
    for (p, c), label in sym_edges.items():
        outString += prefix + str(p) + ", " + str(c) + ", " + label + \
                suffix + "\n"

    # Write bboxes
    prefix = "#cc, "
    outString += "\n# [ PRIMITIVES ]\n"
    outString += f"# Primitives (cc): {len(stk_bboxes)}\n"
    for key, value in sorted(stk_bboxes.items()):
        prims = ", ".join(str(x) for x in value)
        outString += prefix + str(key) + ", " + prims + "\n"

    # Removed and added edges for reversing modification later
    outString += "\n# [PUNC EDGE MODIFICATIONS]\n"
    prefix = "#R, "
    suffix = ", 1.0"
    for (p, c) in added_edges:
        # prims = ", ".join(str(p) for x in edge)
        outString += prefix + str(p) + ", " + str(c) + ", " + "ADD" + \
                suffix + "\n"
    for (p, c) in removed_edges:
        # prims = ", ".join(str(p) for x in edge)
        outString += prefix + str(p) + ", " + str(c) + ", " + "REMOVE" + \
                suffix + "\n"
    return outString

def get_punc_pid_children(stk_bboxes, comps, sym_edges,
                          punc_edges):
    sym_edges_dict = defaultdict(list)
    for (p,c) in sym_edges.keys():
        if not sym_edges_dict[p]:
            sym_edges_dict[p] = [c]
        else:
            sym_edges_dict[p].append(c)

    punc_pid_children = defaultdict(list)
    for (p, c), label in sym_edges.items():
        if p in punc_edges and label != "PUNC" and stk_bboxes[comps[c][0]][1] < stk_bboxes[comps[punc_edges[p]][0]][1]:
            if not punc_pid_children[p]:
                punc_pid_children[p] = [(p, c)]
            else:
                punc_pid_children[p].append((p, c))
            if c in sym_edges_dict:
                for child in sym_edges_dict[c]:
                    punc_pid_children[p].append((c, child))
                    if child in sym_edges_dict:
                        for grandchild in sym_edges_dict[child]:
                            punc_pid_children[p].append((child, grandchild))
    return punc_pid_children


def update_edges(sym_edges, stk_bboxes, comps):
    sym_edges_new = sym_edges.copy()

    # All edges with PUNC relation
    punc_edges = dict(list(dict(filter(lambda elem: elem[1] == "PUNC",
                                       sym_edges.items())).keys()))
    total_punc = len(punc_edges)

    # Edges with all children of PUNC's parent node
    punc_pid_children = get_punc_pid_children(stk_bboxes, comps, sym_edges,
                                              punc_edges, )
    # punc_pid_children_edges = dict(filter(lambda elem: elem[0][0] in punc_edges and 
    #                                       elem[1] != "PUNC", sym_edges.items()))
    # punc_pid_children = defaultdict(list)
    # for k, v in punc_pid_children_edges:
    #     if not punc_pid_children[k]:
    #         punc_pid_children[k] = [v]
    #     else:
    #         punc_pid_children[k].append(v)

    # Modify PUNC edge if child node is between the PUNC's parent and child node
    removed_edges = []
    added_edges = []
    modified_count = 0
    for punc_parent, children in punc_pid_children.items():
        closest_x = -math.inf
        modify = False
        for _, cid in children:
            if stk_bboxes[comps[punc_parent][0]][1] <\
                    stk_bboxes[comps[cid][0]][1] <\
                    stk_bboxes[comps[punc_edges[str(punc_parent)]][0]][1] and \
                    stk_bboxes[comps[cid][0]][1] > closest_x:
                closest_x = stk_bboxes[comps[cid][0]][1]
                modify = True
                # Remove edge between PUNC's parent and PUNC's child node
                remove_edge = (punc_parent, punc_edges[punc_parent])
                # Add edge between PUNC's new child node closest to PUNC's
                # original child node as parent node and PUNC's original child node
                add_edge = (cid, punc_edges[punc_parent]) 
        
        if modify:
            modified_count += 1
            sym_edges_new.pop(remove_edge)
            sym_edges_new[add_edge] = "PUNC"
            removed_edges.append((comps[punc_parent][0],
                                comps[punc_edges[punc_parent]][0]))
            added_edges.append((comps[add_edge[0]][0], comps[punc_edges[punc_parent]][0]))

    return sym_edges_new, modified_count, total_punc, removed_edges, added_edges

def modify_punc(inp_dir, out_dir, punc_filelist):
    print(">> Modifying PUNC Ground truth representation\n")
    print(f">> Input directory: {inp_dir}")
    print(f">> Output directory: {out_dir}\n")

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    with open(punc_filelist, "r") as f:
        punc_files = set(f.read().splitlines())

    print(f">> Number of lg files with PUNC: {len(punc_files)}\n")


    print(">> Modification in progress\n")
    modified_filecount = 0
    modified_punc_count = 0
    total_punc_count = 0
    modified_files = []
    for file in tqdm.tqdm(os.listdir(inp_dir)):
        if file[:-3] in punc_files:
            instances, symLabels, comps, bboxes, _ = read_lg(osp.join(inp_dir, file))

            # If stroke id missing in Object: Eg: 28011043
            if 0 in set(len(x) for x in list(comps.values())):
                print(f">> Warning: Missing stroke information in file {file}. No modification")
                shutil.copyfile(osp.join(inp_dir, file), osp.join(out_dir, file))
                continue

            stk_bboxes = dict(zip(instances, bboxes))
            sym_edges = get_relation_labels(osp.join(inp_dir, file), level='sym')

            sym_edges_new, modified_count, total_punc, removed_edges, added_edges = update_edges(sym_edges, stk_bboxes, comps)
            if modified_count:
                modified_filecount += 1
                modified_punc_count += modified_count
                modified_files.append(file[:-3])

            total_punc_count += total_punc

            lgString = ORstring(file, symLabels, comps, sym_edges_new,
                                stk_bboxes, removed_edges, added_edges)

            with open(osp.join(out_dir, file), "w") as f:
                f.write(lgString)
        else:
            shutil.copyfile(osp.join(inp_dir, file), osp.join(out_dir, file))

    with open(osp.join(out_dir, "Modified_files.txt"), "w") as f:
        f.write("\n".join(modified_files))

    print(f"\n>> No. of modified PUNC lg files: {modified_filecount}/{len(punc_files)}\n")
    print(f">> No. of modified PUNC edges: {modified_punc_count}/{total_punc_count}\n")
    print(f">> Modified PUNC lg files saved at: {out_dir}\n")
    print("----------------------------------------------------------------------------------\n")
        
                    
if __name__ == "__main__":
    # if len(sys.argv) < 3:
    #     print("Usage: python modify_punc.py <input_dir> <output_dir>")
    #     sys.exit()

    INFTY_DIR = os.path.join(DATA_DIR, "InftyMCCDB-2")
    INFTY_TEST_DIR = os.path.join(INFTY_DIR, "test_merged_class")
    INFTY_TRAIN_DIR = os.path.join(INFTY_DIR, "train")

    INFTY_TEST_NEW_DIR = os.path.join(INFTY_DIR, "test_merged_class_punc")
    INFTY_TRAIN_NEW_DIR = os.path.join(INFTY_DIR, "train_punc")

    PUNC_TEST_LIST = os.path.join(DATA_DIR, "testing_PUNC.txt")
    PUNC_TRAIN_LIST = os.path.join(DATA_DIR, "training_PUNC.txt")

    # inp_dir = sys.argv[1]
    # out_dir = sys.argv[2]
    modify_punc(inp_dir=INFTY_TEST_DIR, out_dir=INFTY_TEST_NEW_DIR,
                punc_filelist=PUNC_TEST_LIST)
    modify_punc(inp_dir=INFTY_TRAIN_DIR, out_dir=INFTY_TRAIN_NEW_DIR,
                punc_filelist=PUNC_TRAIN_LIST)
