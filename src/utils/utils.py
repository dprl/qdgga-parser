import os
import sys
import os.path as path
import copy
import yaml
import numpy as np
import matplotlib.pyplot as plt
import math
import networkx as nx
from sklearn.metrics import f1_score, precision_score, recall_score,accuracy_score     
from src.datasets.easydict import EasyDict

def get_ccs(merges):
    G = nx.Graph()
    for p, c in merges:
        G.add_edge(p,c)
    components = nx.connected_components(G)
    return sorted(components)

def get_freer_gpu():
    free = os.popen('nvidia-smi -q -d Memory |grep -A4 GPU|grep Free').read().strip().split('\n')
    memory_available = [int(f.split()[2]) for f in free]
    return np.argmax(memory_available)


def get_config(config_file):
    with open(config_file) as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    def to_easydict(config):
        config = EasyDict(config)
        for ( k, v ) in config.items():
            if type(v) is dict: config[k] = to_easydict(v)
        return config

    return to_easydict(config)


def update_config(config, args):
    if args is None: return config

    config = copy.deepcopy(config)
    for arg, val in config.items():
        if arg in args:
            config[arg] = args[arg]
        elif hasattr(val, 'items'):
            config[arg] = update_config(val, args)
    return config


def save_config(config, config_file, print_opts=True):
    config = copy.deepcopy(config)
    def to_dict(config):
        config = config.opt
        for k, v in config.items():
            if type(v) is EasyDict: config[k] = to_dict(v)
        return config
    config = to_dict(config)

    config_str = yaml.dump(config, default_flow_style=False)
    with open(config_file, 'w') as f: f.write(config_str)


def read_dir(dir_path, predicate=None, name_only=False, recursive=False):
    if type(predicate) is str:
        if predicate in {'dir', 'file'}:
            predicate = {
                'dir': lambda x: path.isdir(path.join(dir_path, x)),
                'file':lambda x: path.isfile(path.join(dir_path, x))
            }[predicate]
        else:
            ext = predicate
            predicate = lambda x: ext in path.splitext(x)[-1]

    def read_dir_(output, dir_path, predicate, name_only, recursive):
        if not path.isdir(dir_path): return
        for f in os.listdir(dir_path):
            d = path.join(dir_path, f)
            if predicate is None or predicate(f):
                output.append(f if name_only else d)
            if recursive and path.isdir(d):
                read_dir_(output, d, predicate, name_only, recursive)

    output = []
    read_dir_(output, dir_path, predicate, name_only, recursive)
    return sorted(output)


def get_last_checkpoint(checkpoint_dir, predicate=None, pattern=None):
    if predicate is None:
        predicate = lambda x: x.endswith('pth') or x.endswith('pt')
    
    checkpoints = read_dir(checkpoint_dir, predicate)
    if len(checkpoints) == 0:
        return None, 0
    checkpoints = sorted(checkpoints, key=lambda x: path.getmtime(x))
    
    checkpoint = checkpoints[-1]
    if pattern is None:
        pattern = lambda x: int(path.basename(x).split('_')[-1].split('.')[0])
    return checkpoint, pattern(checkpoint)


def get_checkpoint(run_dir, which_epoch, task=""):
    if which_epoch == 'latest':
        checkpoint, last_epoch = get_last_checkpoint(run_dir)
    else:
        last_epoch = int(which_epoch)
        checkpoint = path.join(run_dir, "net_{}.pt".format(last_epoch))
        if not path.isfile(checkpoint): 
            checkpoint, last_epoch = None, 0

    if checkpoint and task == "train":
        last_epoch += 1
    return {"checkpoint": checkpoint, "last_epoch": last_epoch}

def plot_one_loss(filePath):
    loss_dict={}
    for count,line in enumerate(open(filePath)):
        if count==0:
            continue
        #epoch,it,sym=line.strip().split(',')
        parts=line.strip().split(',')
        if len(parts)==6:
            epoch,jloss,sym,rel,seg=parts[0],parts[2],parts[3],parts[4],parts[5]
        elif len(parts)==3:
            epoch,it,jloss=parts[0],parts[1],parts[2]

        if epoch in loss_dict.keys():
            loss_dict[epoch].append(float(jloss))
        else:
            loss_dict[epoch]= [float(jloss)]

    avgloss=[np.mean(loss) for loss in loss_dict.values()]
    plt.plot([int(k) for k in loss_dict.keys()],avgloss)
    plt.show()



def plot_loss(filePath,model='Parser3',loss='norm', graph='los',plot=True):
    
    if model=='Segmenter':
        symloss_dict={}
        segloss_dict={}
        joint_loss={}
        for count,line in enumerate(open(filePath)):
            if count==0:
                continue
            epoch,it,sym,seg=line.strip().split(',')
            if epoch in symloss_dict.keys(): 
                symloss_dict[epoch].append(float(sym))
                segloss_dict[epoch].append(float(seg))
                joint_loss[epoch].append(float(sym)+float(seg))  #(2*float(seg)*float(sym)/(float(seg)+float(sym)))          

            else: 
                 symloss_dict[epoch]= [float(sym)]
                 segloss_dict[epoch]=[float(seg)]            
                 joint_loss[epoch]=[float(sym)+float(seg)]           
                 
    if model=='Parser3' and loss=='hmean':
        loss_values=[]
        loss_dict={}
        for count,line in enumerate(open(filePath)):
            if count==0:
                continue
            epoch,it,value,_,_,_,_=line.strip().split(',')
            value=value.replace('[','').replace(']','')
            loss_values.append(float(value))
            if epoch in loss_dict.keys(): loss_dict[epoch].append(float(value)) 
            else: loss_dict[epoch]= [float(value)]

        avgloss=[np.mean(loss) for loss in loss_dict.values()]
        plt.plot([int(k) for k in loss_dict.keys()],avgloss)    
        
        #Sum,=plt.plot(epochs2[:len(epochs)],avgjoint2[:len(epochs)], label='Sum')
        #Hmean,=plt.plot([int(k) for k in loss_dict.keys()],avgloss, label='Hmean')    

        #plt.legend(handles=[Hmean, Sum])
        #plt.xlabel('epochs')
        #plt.ylabel('Loss')
         

    if model=='Parser3':
        symloss_dict={}
        relloss_dict={}
        segloss_dict={}
        joint_loss={}
        brel_loss={}
        brel=None
        for count,line in enumerate(open(filePath)):
            if count==0:
                continue
            parts=line.strip().split(',')
            if len(parts)==5:
                epoch,sym,rel,seg=parts[0],parts[2],parts[3],parts[4]
                jloss=float(sym)+float(rel)+float(seg)
            elif len(parts)==6:
                epoch,jloss,sym,rel,seg=parts[0],parts[2],parts[3],parts[4],parts[5] 
            elif len(parts)==7: #binary rel detector
                 epoch,jloss,sym,rel,seg,brel=parts[0],parts[2],parts[3],parts[4],parts[5],parts[6]            
            if epoch in symloss_dict.keys(): 
                symloss_dict[epoch].append(float(sym))
                relloss_dict[epoch].append(float(rel))
                segloss_dict[epoch].append(float(seg))
                joint_loss[epoch].append(float(jloss)) 
                if brel: brel_loss[epoch].append(float(brel))

            else: 
                 symloss_dict[epoch]= [float(sym)]
                 segloss_dict[epoch]=[float(seg)]   
                 relloss_dict[epoch]=[float(rel)]
                 joint_loss[epoch]=[float(jloss)]           
                 if brel: brel_loss[epoch]=[float(brel)]           
                       
        #plot the average loss for each epoch
        avgseg=[np.mean(loss) for loss in segloss_dict.values()]
        avgsym=[np.mean(loss) for loss in symloss_dict.values()]
        avgrel=[np.mean(loss) for loss in relloss_dict.values()]
        avgjoint=[np.mean(loss) for loss in joint_loss.values()]
        if brel: avgbrel=[np.mean(loss) for loss in brel_loss.values()]
        
        if plot:
            seg,=plt.plot([int(k) for k in joint_loss.keys()],avgseg, label='seg')
            sym,=plt.plot([int(k) for k in joint_loss.keys()],avgsym, label='sym')
            rel,=plt.plot([int(k) for k in joint_loss.keys()],avgrel, label='rel')
            joint,=plt.plot([int(k) for k in joint_loss.keys()],avgjoint, label='joint')
            if brel: binaryRel,=plt.plot([int(k) for k in joint_loss.keys()],avgbrel, label='binary rel')

        
            plt.legend(handles=[seg,sym,rel,joint, binaryRel]) if brel else  plt.legend(handles=[seg,sym,rel,joint])
            plt.xlabel('epochs')
            plt.ylabel('Loss')
            #plt.plot([np.argmin(avg_loss)],[min(avg_loss)],'*',color='r')
            #plt.annotate('Min Loss on epoch: %d'%(np.argmin(avg_loss)), xy=(np.argmin(avg_loss),
            #             min(avg_loss)), xytext=(np.argmin(avg_loss)-10, min(avg_loss)+0.1),color='red')   
            plt.show()
    return avgseg, avgsym,avgrel,avgjoint, [int(k) for k in joint_loss.keys()]

def compare_two_loss(filePath1,filePath2,filePath3):
    avgseg, avgsym,avgrel,avgjoint,epochs=plot_loss(filePath1,plot=False)
    avgseg2, avgsym2,avgrel2,avgjoint2,epochs2=plot_loss(filePath2,plot=False)
    #avgseg3, avgsym3,avgrel3,avgjoint3,epochs3=plot_loss(filePath3,plot=False)
    #avgseg4, avgsym4,avgrel4,avgjoint4,epochs4=plot_loss(filePath4,plot=False)
    #avgseg5, avgsym5,avgrel5,avgjoint5,epochs5=plot_loss(filePath5,plot=False)
    if len(epochs2)>len(epochs):
        seg,=plt.plot(epochs,avgseg, label='allAdj_seg')
        sym,=plt.plot(epochs,avgsym, label='allAdj_sym')
        rel,=plt.plot(epochs,avgrel, label='allAdj_rel')
        joint,=plt.plot(epochs,avgjoint, label='allAdj_joint')
        
        seg2,=plt.plot(epochs2[:len(epochs)],avgseg2[:len(epochs)], label='seg')
        sym2,=plt.plot(epochs2[:len(epochs)],avgsym2[:len(epochs)], label='sym')
        rel2,=plt.plot(epochs2[:len(epochs)],avgrel2[:len(epochs)], label='rel')
        joint2,=plt.plot(epochs2[:len(epochs)],avgjoint2[:len(epochs)], label='joint')

        #joint3,=plt.plot(epochs3[:len(epochs)],avgjoint3[:len(epochs)], label='1kernel_3block')
        #joint4,=plt.plot(epochs4[:len(epochs)],avgjoint4[:len(epochs)], label='4kernels')
        #joint5,=plt.plot(epochs5[:len(epochs)],avgjoint5[:len(epochs)], label='8kernels')

        #plt.legend(handles=[rel,rel2])
        plt.legend(handles=[joint,joint2,rel,rel2,sym,sym2,seg,seg2])
        plt.xlabel('epochs')
        plt.ylabel('Loss')
        
        
def evaluate_lists(preds,gts):
    
    f1_test = f1_score(gts, preds, average='micro')
    precision_test = precision_score(gts, preds, average='micro')
    recall_test = recall_score(gts, preds, average='micro')
    accuracy=accuracy_score(gts, preds)

    print("Precision: " + str(precision_test))
    print("Recall: " + str(recall_test))
    print("F1 score: " + str(f1_test))    
    print('accuracy: '+ str(accuracy))   
    print('-----------------------------------')
       
#calculate field_of_view of network
    
    
#Assume the two dimensions are the same
#Each kernel requires the following parameters:
# - k_i: kernel size
# - s_i: stride
# - p_i: padding (if padding is uneven, right padding will higher than left padding; "SAME" option in tensorflow)
# 
#Each layer i requires the following parameters to be fully represented: 
# - n_i: number of feature (data layer has n_1 = imagesize )
# - j_i: distance (projected to image pixel distance) between center of two adjacent features
# - r_i: receptive field of a feature in layer i
# - start_i: position of the first feature's receptive field in layer i (idx start from 0, negative means the center fall into padding)

def outFromIn(conv, layerIn):
    n_in = layerIn[0]
    j_in = layerIn[1]
    r_in = layerIn[2]
    start_in = layerIn[3]
    k = conv[0]
    s = conv[1]
    p = conv[2]
    
    n_out = math.floor((n_in - k + 2*p)/s) + 1
    actualP = (n_out-1)*s - n_in + k 
    pR = math.ceil(actualP/2)
    pL = math.floor(actualP/2)
      
    j_out = j_in * s
    r_out = r_in + (k - 1)*j_in
    start_out = start_in + ((k-1)/2 - pL)*j_in
    return n_out, j_out, r_out, start_out
      
def printLayer(layer, layer_name):
    print(layer_name + ":")
    print("\t n features: %s \n \t jump: %s \n \t receptive size: %s \t start: %s " % (layer[0], layer[1], layer[2], layer[3]))
     
    
def update_adjSymbols_prob(edge_ref,Adjsym_probs,instances,nodesOnly=False):
    n=len(instances)
    
    if nodesOnly: return Adjsym_probs[:n]
       
    parent_stkID=[instances[i[0]] for i in edge_ref]
    
    AdjsymProbs= dict(zip(list(instances)+parent_stkID, Adjsym_probs))
    
    prob_dict={}
    
    #avg prob for each strokes
    for stkID,prob in AdjsymProbs.items():
        if stkID in prob_dict.keys():
            sum_=map(lambda x,y:x+y, prob_dict[stkID][0], prob)
            count=prob_dict[stkID][1]+1
            prob_dict[stkID]=(sum_,count)
        else: prob_dict[stkID]=(prob,1)
        
    res=[]
    for stkID in instances:
        value,count=prob_dict[stkID][0],prob_dict[stkID][1]
        res.append(value/count)
        
    return res

def updateEdgeLabel(edgeLabels):
    relLables=[]
    segLabels=[]
    for label in edgeLabels:
        if label=='MERGE': 
            segLabels.append(0)#0 identifies merge
            relLables.append('NoRelation')
        else:
            segLabels.append(1)
            relLables.append(label)                           
    return relLables,segLabels

def visualize_SYMembeddings(symProb,stkMasks):
    from scipy import signal    
    from scipy.special import softmax
    import torch
    import torch.nn as nn
    import matplotlib.pyplot as plt
    
    #generate 101 prob images for each stk
    probMasks=[]
    for img,prob in zip(stkMasks,symProb):
        stkprobs=[]
        for x in softmax(prob):stkprobs.append(img*x)
        probMasks.append(stkprobs)
    # get the max prob along each channel (101 channel) 
    probMasks=np.array(probMasks)
    class_embed=[]
    for i in range(probMasks.shape[1]):
        temp=np.zeros((stkMasks.shape[1],stkMasks.shape[2]))
        for j in range(probMasks.shape[0]):
            temp=np.maximum(probMasks[j,i,:,:],temp)
        class_embed.append(temp)  
    #normalize class embeddings (sum pixels to 1)
    class_embed=np.array(class_embed)
    v_min = class_embed.min(axis=(1, 2), keepdims=True)
    v_max = class_embed.max(axis=(1, 2), keepdims=True)
    (class_embed - v_min)/(v_max - v_min)
    #plt.imshow(class_embed[97])
    
    # 101 class to 3 class
    #conv=signal.convolve2d(class_embed,kernel,boundary='symm', mode='same')
    conv1x1=nn.Conv2d(101, 3, kernel_size=1, stride=1, bias=False)
    output=conv1x1(torch.FloatTensor(class_embed).unsqueeze(0))
    output=output.detach().numpy()[0]
    output = output.transpose(1,2,0)
    plt.imshow(output*255)
    
 
        
def cal_fieldofView():
        # [filter size, stride, padding]
    layerInfos = []    
    #MM:simple scenario
    convnet =   [[7,2,3],[3,2,0],[3,1,1],[3,2,0],[3,1,1]]
    layer_names = ['conv1', 'pool1','SE_g1','pool2','SE_g2',]
    imsize = 64
    
    #MM: detailed scenario
    
    #first layer is the data layer (image) with n_0 = image size; j_0 = 1; r_0 = 1; and start_0 = 0.5
    print ("-------Net summary------")
    currentLayer = [imsize, 1, 1, 0.5]
    printLayer(currentLayer, "input image")
    for i in range(len(convnet)):
        currentLayer = outFromIn(convnet[i], currentLayer)
        layerInfos.append(currentLayer)
        printLayer(currentLayer, layer_names[i])
    '''
    print ("------------------------")
    layer_name = raw_input ("Layer name where the feature in: ")
    layer_idx = layer_names.index(layer_name)
    idx_x = int(raw_input ("index of the feature in x dimension (from 0)"))
    idx_y = int(raw_input ("index of the feature in y dimension (from 0)"))
  
    n = layerInfos[layer_idx][0]
    j = layerInfos[layer_idx][1]
    r = layerInfos[layer_idx][2]
    start = layerInfos[layer_idx][3]
    assert(idx_x < n)
    assert(idx_y < n)
  
    print ("receptive field: (%s, %s)" % (r, r))
    print ("center: (%s, %s)" % (start+idx_x*j, start+idx_y*j))
    '''
    

def visualize_trained_masks(tester, data):
    #MM: visualizing trained masks!

    import matplotlib.pyplot as plt
    from scipy import signal

    #visualize att1 & att2 weights for conv1
    model=tester.net
    w1_99=model.att1.conv.weight.detach().cpu().numpy()[0][0]
    # w2_99=model.att2.conv.weight.detach().cpu().numpy()[0][0]

    #----------------------------------------------------------------
    #visualize att1 & att2 $ att3 weights for conv3

    images=data['images'].numpy()[0]
    model=tester.net

    # att1_w1_79=model.att1[0].conv.weight.detach().cpu().numpy()
    # att1_w2_79=model.att1[1].conv.weight.detach().cpu().numpy()
    # att1_w3_79=model.att1[2].conv.weight.detach().cpu().numpy()

    att2_w1_79=model.att2[0].conv.weight.detach().cpu().numpy()
    # att2_w2_79=model.att2[1].conv.weight.detach().cpu().numpy()
    # att2_w3_79=model.att2[2].conv.weight.detach().cpu().numpy()

    # att3_w1_79=model.att3[0].conv.weight.detach().cpu().numpy()
    # att3_w2_79=model.att3[1].conv.weight.detach().cpu().numpy()
    # att3_w3_79=model.att3[2].conv.weight.detach().cpu().numpy()

    for x in range(att2_w1_79.shape[0]):
        kernel=att2_w1_79[x,0,:,:]
        conv=signal.convolve2d(images[1,:,:],kernel,boundary='symm', mode='same')
        
        fig=plt.figure()
        #a=fig.add_subplot(1,2,1)
        #imgplot=plt.imshow(kernel)
        
        #a=fig.add_subplot(1,2,2)
        #plt.plot(212)
        imgplot=plt.imshow(conv)
      

    #----------------------------------------------------------------

    #visualize activation maps
    model=tester.net
    activation = {}
    def get_activation(name):
        def hook(model, input, output):
            activation[name] = output.detach()
        return hook

    model.att1.register_forward_hook(get_activation('att1'))

    symbols_,segs_,rels_,symProb,relProb = tester.testStkPairChild3(data["images"], data["len"], data['graph_edgeRef'])

    act = activation['att1'].squeeze()
    plt.imshow(act.cpu().numpy())
    images=data['images'].numpy()[0]
    plt.show()
    plt.imshow(images[1,:,:])
    plt.show()
    plt.imshow(images[0,:,:])

    conv=signal.convolve2d(images[0,:,:],w1_99,boundary='symm', mode='same')


if __name__ == '__main__':
    path_=sys.argv[1]
    plot_one_loss(path_)
    



