import sys
import os
import shutil
import wandb
import time
import os.path as path
import csv
import numpy as np
from tqdm import tqdm
from collections import defaultdict, OrderedDict
# from torch.utils.tensorboard import SummaryWriter


class Logger(object):
    def __init__(self, log_dir, rank, epoch=0, name="log", mode="train",
                 visualize_loss=False):
        self.log_dir = log_dir
        self.epoch = epoch
        self.name = name if name != "" else "log"
        self.loss_freq = float('inf')
        self.vis_loss_freq = 1
        self.loss = None
        self.save_freq = float('inf')
        self.format_float = \
            lambda x: np.format_float_scientific(x, exp_digits=1, precision=2)
        self.rank = rank

        self.mode = mode
        self.visualize_loss = visualize_loss
        if mode == "train":
            self.log_file = path.join(self.log_dir, "loss.csv")
            tb_logs_dir = path.join(self.log_dir, 'TensorBoard_Logs_train')
        elif mode == "val":
            self.log_file = path.join(self.log_dir, "loss-val.csv")
            tb_logs_dir = path.join(self.log_dir, 'TensorBoard_Logs_val')
        # if rank==0 and (mode == "train" or mode == "val"):
        #     # if os.path.exists(tb_logs_dir):
        #     #     shutil.rmtree(tb_logs_dir)
        #     self.writer = SummaryWriter(tb_logs_dir)


    def add_loss_log(self, loss_fcn, loss_freq, window_size=100):
        self.loss_fcn = loss_fcn
        self.loss_freq = loss_freq
        self.window_size = window_size

    def add_save_log(self, save_fcn, save_freq, seed):
        self.save_fcn = save_fcn
        self.save_freq = save_freq
        
        if hasattr(self.save_fcn, "__self__") and self.mode == "train":
            model = self.save_fcn.__self__
            with open(path.join(self.log_dir, "graph.txt"), "w") as f:
                f.write(self.get_graph(model))
                f.write(f"Seed={seed}")

    def get_graph(self, model):
        model_str = ""
        if hasattr(model, 'parameters'):
            model_str += model.__repr__() + "\n"
        else:
            for k in model.__dir__():
                if not k.startswith("_"):
                    v = getattr(model, k)
                    if hasattr(v, 'parameters'):
                        model_str += k + ":\n"
                        model_str += self.get_graph(v)
        return model_str

    def set_progress(self, progress):
        elapsed_time = time.strftime('%H:%M:%S', time.gmtime(time.time() - self.start_time))
        desc = '[{}][epoch{}] | Time elapsed: {}'.format(self.name, self.epoch, elapsed_time)
        # desc = '[{}][epoch{}]'.format(self.name, self.epoch)
        if self.loss is not None:
            if len(self.loss) < 5:
                #for k, v in self.loss.items():print('k,v',k,v) 
                loss_str = " ".join(["{} {:.2e}({:.2e})".format(
                    k, v[-1], np.mean(v)) for k, v in self.loss.items()])
            else:
                loss_str = " ".join(["{} {}".format(
                    k, self.format_float(np.mean(v)))
                    for k, v in self.loss.items()])

            desc += " | Loss: " + loss_str
            # desc += loss_str

        progress.set_description(desc=desc, refresh=True)
        progress.refresh()

    def __call__(self, iterable):
        self.start_time = time.time()  # Track elapsed time manually if needed
        progress = tqdm(iterable, 
                dynamic_ncols=True,  # Adjust width dynamically
                ascii=True,          # Cleaner bar display
                file=sys.stdout,
                bar_format="{l_bar}{bar} | {n_fmt}/{total_fmt} batch [{elapsed}<{remaining}, {rate_fmt}]"
               )
        # progress = tqdm(iterable)
        for (it, obj) in enumerate( iterable ):  #(rogress):
            yield obj
            progress.update(1)  # Explicitly update tqdm each iteration

            if self.rank == 0:   # Save only on one process
                if hasattr(self, 'loss_fcn'):
                    loss = self.loss_fcn()
                    if self.loss is None:
                        self.loss = defaultdict(list)
                    for k, v in loss.items():
                        # if len(self.loss[k]) > self.window_size:
                        #     self.loss[k].pop(0)
                        self.loss[k].append(v)

                    self.set_progress(progress)
                    if it % self.loss_freq == 0:
                        # self.set_progress(progress)
                        if not path.isfile(self.log_file):
                            with open(self.log_file, 'w') as f:
                                writer = csv.writer(f)
                                writer.writerow([*["epoch", "it"], *loss.keys()])
                        else:
                            with open(self.log_file, 'a') as f:
                                writer = csv.writer(f)
                                mean_running_losses = list(map(lambda x:np.mean(self.loss[x]), 
                                                               loss.keys()))
                                writer.writerow(
                                    [*[self.epoch, it], *mean_running_losses])
                else:
                    # self.set_progress(progress)
                    elapsed_time = time.strftime('%H:%M:%S', time.gmtime(time.time() - self.start_time))
                    progress.set_description(f'[{self.name}] Time elapsed: {elapsed_time}', refresh=True)
                    progress.refresh()
                    # progress.set_description(f'[{self.name}] Batch', refresh=True)

    def save_weights(self):
        if hasattr(self, 'save_fcn') and self.rank == 0:
            if self.epoch % self.save_freq == self.save_freq - 1 and self.mode == "train":
                save_file = path.join(self.log_dir, "net_{}.pt".format(self.epoch))
                print("[Epoch {}] Saving {}".format(self.epoch, save_file), flush=True)
                self.save_fcn(save_file)

            if self.epoch % self.vis_loss_freq == 0:
                # Add loss values to tensorboard every epoch
                # self.writer.add_scalar('Total_loss', np.mean(self.loss['loss']), self.epoch)
                # self.writer.add_scalar('Symbol_loss', np.mean(self.loss['sym']), self.epoch)
                # self.writer.add_scalar('Relation_loss', np.mean(self.loss.get('rel', 0)), self.epoch)
                # self.writer.add_scalar('Segmentation_loss', np.mean(self.loss.get('seg', 0)), self.epoch)

                if self.visualize_loss:
                    if self.mode == "train":
                        wandb.log({'Total_loss': np.mean(self.loss['loss']),
                                   'Symbol_loss': np.mean(self.loss['sym']),
                                   'Relation_loss': np.mean(self.loss.get('rel', [0])),
                                   'Segmentation_loss': np.mean(self.loss.get('seg', [0])),
                                   }, step=self.epoch)
                    else:
                        wandb.log({'Total_loss_val': np.mean(self.loss['loss']),
                                   'Symbol_loss_val': np.mean(self.loss['sym']),
                                   'Relation_loss_val': np.mean(self.loss.get('rel', 0)),
                                   'Segmentation_loss_val': np.mean(self.loss.get('seg', 0)),
                                   }, step=self.epoch)
        self.epoch += 1
