import sys
from collections import OrderedDict
from ast import literal_eval
import numpy as np

import traceback
import torch
import pickle
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.parallel import DistributedDataParallel as DDP
from operator import itemgetter
from src.image_ops.image2traces import imshow

from src.models.network import (
    TrainableParser3_mask_both_3branch_spp_symbols_new,
    TrainableParser3_mask_both_3branch_spp_symbols_segs_new,
    TrainableParser3_mask_both_3branch_spp_new,
    TrainableParser3_mask_both_3branch_spp_symbols_segs_edges_new,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_posenc,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_seggat,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_seggat_posenc,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_bidir,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_bidir_posenc,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v3,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v4,
    TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_2NN,
)

from lgap_settings import SYM_WEIGHTS_DIR, SEG_WEIGHTS_DIR, REL_WEIGHTS_DIR

def separate_outputs(tensors, lengths=None):
    "Separate concatenated outputs in a batch to a list"
    if isinstance(tensors, list):
        return (
            [
                tensor[sum(lengths[: i + 1]) : sum(lengths[: i + 2])]
                for i in range(len(lengths) - 1)
            ]
            for tensor in tensors
        )
    return [
        tensors[sum(lengths[: i + 1]) : sum(lengths[: i + 2])]
        for i in range(len(lengths) - 1)
    ]

class FocalLoss(nn.Module):
    def __init__(self, alpha=0.25, gamma=2.0, reduction='mean'):
        """
        Focal Loss for addressing class imbalance.

        Args:
            alpha (float or torch.Tensor): Weighting factor for the positive class (scalar or tensor for each class).
            gamma (float): Focusing parameter to reduce the impact of easy examples.
            reduction (str): Specifies the reduction to apply to the output: 'none', 'mean', or 'sum'.
        """
        super(FocalLoss, self).__init__()
        if isinstance(alpha, torch.Tensor):
            self.register_buffer('alpha', alpha)  # Save alpha as a buffer for device compatibility
        else:
            self.alpha = alpha
        self.gamma = gamma
        self.reduction = reduction

    def forward(self, logits, targets):
        """
        Compute the focal loss.

        Args:
            logits (torch.Tensor): Predicted logits of shape (batch_size, num_classes).
            targets (torch.Tensor): Ground truth labels of shape (batch_size).

        Returns:
            torch.Tensor: Computed focal loss.
        """
        # Convert targets to one-hot encoding
        num_classes = logits.size(1)
        targets_one_hot = F.one_hot(targets, num_classes=num_classes).float()

        # Compute probabilities and log probabilities
        probs = F.softmax(logits, dim=1)
        log_probs = F.log_softmax(logits, dim=1)

        # Gather probabilities for the true class
        pt = torch.sum(probs * targets_one_hot, dim=1)

        # Compute the focal loss scaling factor
        focal_weight = (1 - pt) ** self.gamma

        # Compute Cross-Entropy Loss
        ce_loss = -torch.sum(targets_one_hot * log_probs, dim=1)

        # Apply alpha (class weighting)
        if isinstance(self.alpha, torch.Tensor):
            alpha_t = self.alpha[targets]
        else:
            alpha_t = self.alpha
        loss = alpha_t * focal_weight * ce_loss

        # Apply reduction
        if self.reduction == 'mean':
            return loss.mean()
        elif self.reduction == 'sum':
            return loss.sum()
        else:
            return loss


class ClassBalancedLoss(nn.Module):
    def __init__(self, class_counts, beta=0.99, reduction='mean'):
        """
        Class-Balanced Loss based on Effective Number of Samples.

        Args:
            class_counts (torch.Tensor): Number of samples per class (num_classes,).
            beta (float): Hyperparameter for effective number calculation. (0 ≤ β < 1)
            reduction (str): Specifies the reduction to apply to the output: 
                             'mean' | 'sum' | 'none'.
        """
        super(ClassBalancedLoss, self).__init__()
        self.beta = beta
        self.reduction = reduction

        # Compute effective number of samples and class weights
        effective_num = 1.0 - torch.pow(self.beta, torch.tensor(class_counts))
        weights = (1.0 - self.beta) / effective_num

        # Handle zero-sample classes by setting their weights to 0
        weights[torch.isnan(weights) | torch.isinf(weights)] = 0.0  # Avoid division by zero

       # Normalize weights such that sum(weights) = count of non-zero weights
        non_zero_count = (weights > 0).sum().item()  # Count of non-zero weights
        self.weights = weights / weights.sum() * non_zero_count
        # self.weights = weights / weights.sum() * len(class_counts)  # Normalize weights
        # self.weights = weights / weights.sum()
        self.register_buffer('class_weights', self.weights)  # Save as buffer for device compatibility

    def forward(self, logits, targets):
        """
        Compute Class-Balanced Loss.

        Args:
            logits (torch.Tensor): Predicted logits of shape (batch_size, num_classes).
            targets (torch.Tensor): Ground truth labels of shape (batch_size).

        Returns:
            torch.Tensor: Computed Class-Balanced Loss.
        """
        # Map targets to one-hot encoding
        targets_one_hot = F.one_hot(targets, num_classes=logits.size(1)).float()

        # Compute log-softmax and per-sample cross-entropy loss
        log_probs = F.log_softmax(logits, dim=1)
        per_sample_loss = -torch.sum(targets_one_hot * log_probs, dim=1)

        # Apply class weights
        weights = self.class_weights[targets]  # Get weights for each target class
        per_sample_loss = per_sample_loss * weights

        # Apply reduction
        if self.reduction == 'mean':
            return per_sample_loss.mean()
        elif self.reduction == 'sum':
            return per_sample_loss.sum()
        else:
            return per_sample_loss


class MacroAveragedLoss(nn.Module):
    def __init__(self, base_loss_fn=nn.CrossEntropyLoss(reduction='none')):
        super(MacroAveragedLoss, self).__init__()
        self.base_loss_fn = base_loss_fn  # Base loss function (e.g., CrossEntropyLoss)

    def forward(self, logits, targets):
        """
        Arguments:
            logits: Tensor of shape (batch_size, num_classes) - raw model outputs.
            targets: Tensor of shape (batch_size) - ground truth class labels.
        Returns:
            Macro-averaged loss scalar.
        """
        num_classes = logits.size(1)
        # import pdb; pdb.set_trace()
        

        # Compute per-sample loss
        per_sample_loss = self.base_loss_fn(logits, targets)  # Shape: (batch_size,)

        # Create a mask for each class
        class_masks = torch.nn.functional.one_hot(targets, num_classes).float()  # Shape: (batch_size, num_classes)

        # Compute total loss for each class
        class_losses = torch.sum(per_sample_loss.unsqueeze(1) * class_masks, dim=0)  # Shape: (num_classes,)

        # Compute number of samples per class
        class_counts = class_masks.sum(dim=0)  # Shape: (num_classes,)

        # Avoid division by zero (only compute for classes with samples)
        non_zero_classes = class_counts > 0
        class_losses[non_zero_classes] /= class_counts[non_zero_classes]  # Average loss per class

        # Macro-average over all classes with non-zero samples
        macro_loss = class_losses[non_zero_classes].mean()
        return macro_loss


class Base(nn.Module):
    def __init__(self, *opts):
        super(Base, self).__init__()
        self._get_opts(*opts)
        self.edges_forward = {}

    def _get_opts(self, *opts):
        for opt in opts:
            for k, v in opt.items():
                setattr(self, k, v)

    def _match_device(self, *data):
        # device = next(self.parameters()).device
        if len(data) == 1:
            return data[0].to(self.device)
        else:
            return (
                [dd.to(self.device) for dd in d]
                if isinstance(d, list)
                else d.to(self.device)
                for d in data
            )
        # else: return (d.to(self.device) for d in data)

    def _get_state_attrs(self):
        state_attrs = {}
        for k in dir(self):
            if k[0] != "_":
                v = getattr(self, k)
                if hasattr(v, "state_dict"):
                    state_attrs[k] = v

        return state_attrs.items()

    def _to_numpy(self, *xs):
        if len(xs) == 1:
            if xs[0] is not None:
                return xs[0].detach().cpu().numpy()
            else:
                return None
        else:
            return (x.detach().cpu().numpy() for x in xs)

    def reduce_encoder(self, size=64):
        self.net.encoder.layer2[-1].conv2 = nn.Conv2d(256, size, stride=(1,1),
                                                      kernel_size = (3,3),
                                                      padding=(1,1), groups=32, bias=False)
        self.net.encoder.layer2[-1].bn2 = nn.GroupNorm(32, size)
        self.net.encoder.layer2[-1].conv3 = None
        self.net.encoder.layer2[-1].bn3 = None
        self.net.encoder.layer2[-1].se_module.fc1 = nn.Conv2d(size, 32, stride=(1,1),
                                                              kernel_size = (1,1),
                                                              bias=False
                                                              )
        self.net.encoder.layer2[-1].se_module.fc2 = nn.Conv2d(32, size, stride=(1,1),
                                                              kernel_size = (1,1),
                                                              bias=False
                                                              )
    def weights_init(self, m):
        if isinstance(m, nn.Conv2d):
            nn.init.xavier_uniform_(m.weight.data, 
                                    gain=nn.init.calculate_gain('relu'))
            if m.bias is not None:
                m.bias.data.fill_(0.01)

        if isinstance(m, nn.Linear):
            nn.init.xavier_uniform_(m.weight.data, 
                                    gain=nn.init.calculate_gain('relu'))
            if m.bias is not None:
                m.bias.data.fill_(0.01)

    def set_model(self, net_opts):
        if net_opts.model == "att_3branch_both":
                self.net = TrainableParser3_mask_both_3branch_spp_new(**net_opts)

        elif net_opts.model == "att_3branch_both_reduced_rel":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel(**net_opts)

        elif net_opts.model == "att_3branch_both_symbols":
            self.net = TrainableParser3_mask_both_3branch_spp_symbols_new(**net_opts)

        elif net_opts.model == "att_3branch_both_256":
            self.net = TrainableParser3_mask_both_3branch_spp_new(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_64":
            self.net = TrainableParser3_mask_both_3branch_spp_new(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new":
            self.net = TrainableParser3_mask_both_3branch_spp_new(**net_opts)
            self.reduce_encoder(net_opts.num_feat)
            
        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_posenc":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_posenc(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_seggat":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_seggat(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_seggat_posenc":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_seggat_posenc(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_gti":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_gti_v2":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_gti_v2_2NN":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_2NN(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_gti_v2_bidir":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_bidir(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_gti_v2_bidir_posenc":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_bidir_posenc(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_gti_v3":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v3(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_gti_v4":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v4(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_symbols_new":
            self.net = TrainableParser3_mask_both_3branch_spp_symbols_new(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_symbols_segs_new":
            self.net = TrainableParser3_mask_both_3branch_spp_symbols_segs_new(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_symbols_segs_edges_new":
            self.net = TrainableParser3_mask_both_3branch_spp_symbols_segs_edges_new(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        elif net_opts.model == "att_3branch_both_spp_new_reduced_rel_posenc_SP":
            self.net = TrainableParser3_mask_both_3branch_spp_new_reduced_rel_posenc(**net_opts)
            self.reduce_encoder(net_opts.num_feat)

        else:
            print("Model not found")
            sys.exit(0)

    def data_processing(self, data):
        feature = data["feature_vector"]
        pos_enc = data["positional_encoding"]
        label = data["label"]
        label_classes = data["label_class"]
        feat_type = data["type_feature"]

        # Move data to GPU
        feature, pos_enc = self._match_device(feature, pos_enc)

        # Separate features into node and edge features
        node_feat, pair_feat, node_pos_enc, pair_pos_enc = torch.tensor([], dtype=torch.float32, device=self.device), torch.tensor([], dtype=torch.float32, device=self.device), torch.tensor([], dtype=torch.float32, device=self.device), torch.tensor([], dtype=torch.float32, device=self.device)
        # rel_indices, seg_indices, = torch.tensor([], dtype=torch.int8, device=self.device), torch.tensor([], dtype=torch.int8, device=self.device), 
        rel_indices_list, seg_indices_list = [], []
        
        rel_labels, seg_labels, sym_labels = torch.LongTensor([]).to(self.device), torch.LongTensor([]).to(self.device), torch.LongTensor([]).to(self.device)
        sym_label_classes, rel_label_classes, seg_label_classes, edge_label_classes = [], [], [], []

        feat_type_array = np.array(feat_type)
        unique_values = np.unique(feat_type_array)
        indices_feat = {value: np.where(feat_type_array == value)[0].tolist() for value in unique_values}
        node_indices = indices_feat.get('node', [])

        if len(node_indices):
            node_feat = feature[node_indices]
            node_pos_enc = pos_enc[node_indices]
            sym_labels = torch.LongTensor(np.array(label)[node_indices].astype(int)).to(self.device)
            sym_label_classes = np.array(label_classes, dtype=object)[node_indices].tolist()

        pair_indices = indices_feat.get('edge', [])
        if len(pair_indices):
            pair_feat = feature[pair_indices]
            pair_pos_enc = pos_enc[pair_indices]
            edge_labels = np.array(label)[pair_indices].tolist()
            edge_label_classes = np.array(label_classes, dtype=object)[pair_indices].tolist()

            for idx, (edge_label, edge_label_class) in enumerate(zip(edge_labels, edge_label_classes)):
                for edge_type, lbl in edge_label.items():
                    if edge_type == "rel":
                        rel_labels = torch.cat([rel_labels, torch.tensor([lbl], dtype=torch.int8, device=self.device)])
                        rel_label_classes.append(edge_label_class)
                        rel_indices_list.append(idx)
                    elif edge_type == "seg":
                        seg_labels = torch.cat([seg_labels, torch.tensor([lbl], dtype=torch.int8, device=self.device)])
                        seg_label_classes.append(edge_label_class)
                        seg_indices_list.append(idx)
            
        rel_indices = torch.tensor(rel_indices_list, dtype=torch.int64, device=self.device)
        seg_indices = torch.tensor(seg_indices_list, dtype=torch.int64, device=self.device)

        return node_feat, pair_feat, node_pos_enc, pair_pos_enc, \
               rel_indices, seg_indices, sym_labels, rel_labels, seg_labels,\
               sym_label_classes, rel_label_classes, seg_label_classes, edge_label_classes
        

class Trainer(Base):
    def __init__(self, init_opts, learn_opts, net_opts, device, size,
                 symbol_class_counts=None, rel_class_counts=None,
                 seg_class_counts=None):
        super(Trainer, self).__init__(init_opts, learn_opts)
        
        torch.cuda.set_device(device)
        self.device = torch.device("cuda:{}".format(device))
        self.node_segment = net_opts.node_seg
        self.size = size

        self.set_model(net_opts)
        
        # Loss function setup
        if self.loss_function == "Weighted_CE":
            sym_weights = self.get_weights(symbol_class_counts)
            seg_weights = self.get_weights(seg_class_counts)
            rel_weights = self.get_weights(rel_class_counts)

            self.criterion_sym = nn.CrossEntropyLoss(weight=sym_weights)
            self.criterion_seg = nn.CrossEntropyLoss(weight=seg_weights)
            self.criterion_rel = nn.CrossEntropyLoss(weight=rel_weights)

        elif self.loss_function == "Macro_CE":
            self.criterion = MacroAveragedLoss()
            self.criterion_sym = self.criterion_seg = self.criterion_rel = self.criterion

        elif self.loss_function == "Class_balanced_CE":
            self.criterion_sym = ClassBalancedLoss(symbol_class_counts, beta=self.cb_loss_beta)
            self.criterion_rel = ClassBalancedLoss(rel_class_counts, beta=self.cb_loss_beta)
            self.criterion_seg = ClassBalancedLoss(seg_class_counts, beta=self.cb_loss_beta)

        elif self.loss_function == "Focal_CE":
            sym_weights = self.get_weights(symbol_class_counts)
            seg_weights = self.get_weights(seg_class_counts)
            rel_weights = self.get_weights(rel_class_counts)
            self.criterion_sym = FocalLoss(alpha=sym_weights, gamma=self.focal_gamma)
            self.criterion_rel = FocalLoss(alpha=rel_weights, gamma=self.focal_gamma)
            self.criterion_seg = FocalLoss(alpha=rel_weights, gamma=self.focal_gamma)

        else:
            self.criterion = nn.CrossEntropyLoss()
            self.criterion_sym = self.criterion_seg = self.criterion_rel = self.criterion

        if self.optim == "SGD":
            self.optimizer = torch.optim.SGD(
                self.net.parameters(), lr=self.lr, momentum=self.momentum,
                weight_decay=literal_eval(self.weight_decay)
            )
        else:
            self.optimizer = torch.optim.Adam(
                self.net.parameters(), lr=self.lr,
                weight_decay=literal_eval(self.weight_decay)
            )

        self.scheduler = torch.optim.lr_scheduler.StepLR(
            self.optimizer, step_size=self.lr_step, gamma=self.gamma
        )

        if self.print_module:
            print(self)

        if self.use_gpu:
            self.cuda()

        # Load checkpoint if provided
        if self.checkpoint:
            map_location = torch.device("cuda:%d" % device)
            checkpoint = torch.load(self.checkpoint, map_location=map_location)

            for (k, v) in self._get_state_attrs():
                if self.module_only and not issubclass(type(v), nn.Module):
                    continue
                if k in checkpoint:
                    v.load_state_dict(checkpoint[k])
            if device == 0:
                print(f"  Weights: {self.checkpoint} loaded\n")

        # Wrap model in DataParallel if multiple GPUs are used
        if size >= 2:
            self.net = nn.DataParallel(self.net.to(device))

    def get_weights(self, freq):
        freq = torch.tensor(freq).float()
        # freq = torch.clamp(freq, min=1)
        weights = 1.0 / freq.float()
        weights[torch.isnan(weights) | torch.isinf(weights)] = 0.0  # Avoid division by zero
        # weights = weights / weights.sum() * len(freq)
        non_zero_count = (weights > 0).sum().item()  # Count of non-zero weights
        weights = weights / weights.sum() * non_zero_count
        return weights

    def compute_loss(self, pred_symbols, pred_rels, pred_segs, sym_labels, rel_labels, seg_labels):
        batch_loss_sym, batch_loss_rel, batch_loss_seg = 0, 0, 0
        # Initialize individual losses to zero
        losses = OrderedDict([ ("sym", 0), ("rel", 0), ("seg", 0), ("loss", 0) ])
        # Compute losses only if labels are provided
        if len(sym_labels):
            # print(f"GPU {torch.cuda.current_device()}: {sym_labels.shape}, {pred_symbols.shape}")
            batch_loss_sym = self.criterion_sym(pred_symbols, sym_labels)
            losses["sym"] = batch_loss_sym.item()
        if len(rel_labels):
            # print(f"GPU {torch.cuda.current_device()}: {rel_labels.shape}, {pred_rels.shape}")
            batch_loss_rel = self.criterion_rel(pred_rels, rel_labels)
            losses["rel"] = batch_loss_rel.item()
        if len(seg_labels):
            # print(f"GPU {torch.cuda.current_device()}: {seg_labels.shape}, {pred_segs.shape}")
            batch_loss_seg = self.criterion_seg(pred_segs, seg_labels)
            losses["seg"] = batch_loss_seg.item()
        
        batch_loss = batch_loss_sym + batch_loss_rel + batch_loss_seg
        losses["loss"] = batch_loss.item()
        return losses, batch_loss

    def train(self, data, mode="train"):

        node_feat, pair_feat, node_pos_enc, pair_pos_enc, \
            rel_indices, seg_indices, sym_labels, rel_labels, seg_labels,\
            sym_label_classes, rel_label_classes, seg_label_classes, edge_label_classes = self.data_processing(data)

        # NEed to send labels so that dataparallel divides the labels into parts too
        pred_symbols, pred_segs, pred_rels, sym_labels, seg_labels, rel_labels = \
            self.net(node_feat, pair_feat, node_pos_enc, pair_pos_enc, sym_labels, seg_labels, rel_labels)
        
        losses, batch_loss = self.compute_loss(pred_symbols, pred_rels, pred_segs,
                                               sym_labels, rel_labels, seg_labels)
                
        # import pdb; pdb.set_trace()
        # imshow([x for x in node_feat[:20, 0].cpu()], ncols=5)label_classes
        # imshow([x for x in node_feat[:20, 1].cpu()], ncols=5)
        # print(sym_labels[:20])

        self.optimizer.zero_grad()
        try:
            batch_loss.backward()
        except:
            import traceback; print(traceback.format_exc())
            import pdb; pdb.set_trace()
            
        # average_gradients(self.net)
        self.optimizer.step()

        if mode == "val":
            self.loss_val = losses
        else:
            self.loss = losses

        torch.cuda.empty_cache()
        return self._to_numpy(pred_segs)

    def get_loss(self):
        # for t, v in self.loss.items():
        #     self.loss[t] = self._to_numpy(v)
        return self.loss

    def get_val_loss(self):
        # for t, v in self.loss_val.items():
        #     self.loss_val[t] = self._to_numpy(v)
        return self.loss_val

    def save(self, checkpoint_file):
        checkpoint = {k: v.state_dict() for k, v in self._get_state_attrs()}

        # Ensure only leading 'module.' is removed
        if "net" in checkpoint:
            checkpoint["net"] = {
                key[7:] if key.startswith("module.") else key: val 
                for key, val in checkpoint["net"].items()
            }
        torch.save(checkpoint, checkpoint_file)


class Tester(Base):
    def __init__(self, init_opts, net_opts, device, size):
        super(Tester, self).__init__(init_opts)

        # RZ: Selects single GPU for execution
        # gpu_id = int(get_freer_gpu())
        torch.cuda.set_device(device)
        self.device = torch.device("cuda:{}".format(device))
        self.node_segment = net_opts.node_seg

        if self.print_module:
            print(self)

        self.num_node_classes = net_opts.num_node_classes
        self.num_edge_classes = net_opts.num_edge_classes

        self.set_model(net_opts)
        self.symbol_height = net_opts.symbol_height

        if self.use_gpu:
            self.cuda()

        if self.checkpoint:
            map_location = torch.device("cuda:%d" % device)
            checkpoint = torch.load(self.checkpoint, map_location=map_location)

            for k, v in self._get_state_attrs():
                if self.module_only and not issubclass(type(v), nn.Module):
                    continue
                if k in checkpoint:
                    v.load_state_dict(checkpoint[k])

            if device == 0:
                print(f"  Weights: {self.checkpoint} loaded\n")

        # Wrap model in DataParallel if multiple GPUs are used
        if size >= 2:
            self.net = nn.DataParallel(self.net.to(device))
        self.net.eval()

    def test(self, data):
        offset = 2 * self.symbol_height * self.symbol_height
        encoding_size = 10 #TODO: avoid hardcoding
        features = data[:,:offset] 
        pos_encondings = data[:,offset:offset+encoding_size]
        feature_types = data[:,offset+encoding_size:offset+encoding_size+1].reshape(-1)
        ids_treasure = data[:,offset+encoding_size+1:offset+encoding_size+2].reshape(-1)

        node_features_indexes = np.argwhere(feature_types==0).reshape(-1)
        pair_features_indexes = np.argwhere(feature_types==1).reshape(-1)

        #asumming each edge is both seg and rel
        rel_indices = range(len(pair_features_indexes))
        seg_indices = rel_indices

        node_feat = features[node_features_indexes].reshape((len(node_features_indexes),2,
                                                 self.symbol_height, self.symbol_height))#TODO: avoid hardcoding
        pair_feat = features[pair_features_indexes].reshape((len(pair_features_indexes),2, 
                                                 self.symbol_height, self.symbol_height))#TODO: avoid hardcoding
        node_pos_enc = pos_encondings[node_features_indexes]
        pair_pos_enc = pos_encondings[pair_features_indexes]

        node_feat = torch.FloatTensor(node_feat).to(self.device)
        pair_feat = torch.FloatTensor(pair_feat).to(self.device)

        node_pos_enc = torch.FloatTensor(node_pos_enc).to(self.device)
        pair_pos_enc = torch.FloatTensor(pair_pos_enc).to(self.device)

        with torch.no_grad():
            pred_symbols, pred_segs, pred_rels, _, _, _ = \
                self.net(node_feat, pair_feat, node_pos_enc, pair_pos_enc)
        torch.cuda.empty_cache()

        # Concatenate outputs in a batch
        return self._to_numpy(pred_symbols), self._to_numpy(pred_segs), self._to_numpy(pred_rels), ids_treasure
    


