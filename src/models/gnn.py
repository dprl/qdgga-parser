import torch
import torch.nn as nn
from torch.nn import Sequential as Seq, Linear, ReLU
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, MessagePassing
from torch_geometric.utils import add_self_loops, to_dense_adj

from src.models.simplenet import linearBlock

def weights_init(m):
    if isinstance(m, nn.Linear):
        # nn.init.xavier_uniform_(m.weight.data, 
        #                         gain=nn.init.calculate_gain('relu'))
        nn.init.xavier_uniform_(m.weight.data, gain=1.414)
        # if m.bias is not None:
        #     m.bias.data.fill_(0.01)

class VanillaGNN(MessagePassing):
    def __init__(self, in_channels, hidden_channels, out_channels, aggr='mean',
                 layers=2, num_aggr_features=0):
        super().__init__(aggr=aggr, flow='target_to_source') #  "Max" aggregation.
        # 2 layer GNN
        if len(hidden_channels) == 1:
            self.mlp = linearBlock(in_channels, hidden_channels[0], out_channels)

        # 3 layer GNN
        elif len(hidden_channels) == 2:
            self.mlp = Seq(Linear(in_channels, hidden_channels[0]),
                           ReLU(),
                           Linear(hidden_channels[0], hidden_channels[1]),
                           ReLU(),
                           Linear(hidden_channels[1], out_channels))

    def forward(self, x, edge_index):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]
        edge_index, _ = add_self_loops(edge_index, num_nodes=x.size(0))
        # print(x)
        # print(edge_index)
        return self.propagate(edge_index, x=x)

    def message(self, x_j):
        # x_i has shape [E, in_channels]
        # x_j has shape [E, in_channels]
        # import pdb; pdb.set_trace()
        # tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        return self.mlp(x_j)


class CustomGNN(MessagePassing):
    def __init__(self, in_channels, hidden_channels, out_channels, aggr='mean',
                 layers=2, num_aggr_features=0):
        super().__init__(aggr=aggr, flow='target_to_source') #  "Max" aggregation.
        # self.mlp = linearBlock(in_channels*2, hidden_channels[0], out_channels)
        in_channels *= (2 + num_aggr_features)
        # self.linear = nn.Linear(in_channels, out_channels)
        self.linear = linearBlock(in_channels, hidden_channels, out_channels)
        self.los = None

    def forward(self, x, edge_index, los=None):
        # x has shape [N, in_channels]
        # edge_index has shape [2, E]
        # import pdb; pdb.set_trace()
        edge_index, _ = add_self_loops(edge_index, num_nodes=x.size(0))
        # print(x)
        # print(edge_index)
        if los is not None:
            self.los = los
        return self.propagate(edge_index, x=x, los=los)

    def message(self, x_j):
        # x_i has shape [E, in_channels]
        # x_j has shape [E, in_channels]
        # tmp = torch.cat([x_i, x_j], dim=1)  # tmp has shape [E, 2 * in_channels]
        # return self.mlp(x_j)
        return x_j

    def update(self, aggr_out, x):
        # import pdb; pdb.set_trace()
        # new_embedding = torch.cat([x, aggr_out], dim=1)
        if self.los is not None:
            new_embedding = torch.cat([x, self.los, aggr_out], dim=1)
        else:
            new_embedding = torch.cat([x, aggr_out], dim=1)
        # new_embedding = self.mlp(new_embedding)
        new_embedding = self.linear(new_embedding)
        return new_embedding


class GCN(nn.Module):
    def __init__(self, num_node_features, num_hidden_features,
                       num_classes, num_aggr_features=0):
        super().__init__()
        self.conv1 = GCNConv(num_node_features, num_hidden_features)
        self.conv2 = GCNConv(num_hidden_features, num_classes)

    def forward(self, data_x, data_edge_index):
        x, edge_index = data_x, data_edge_index

        x = self.conv1(x, edge_index)
        x = F.relu(x)
        x = F.dropout(x, training=self.training)
        x = self.conv2(x, edge_index)

        # return F.log_softmax(x, dim=1)
        return x


class GATLayer(nn.Module):
    def __init__(self, in_features, dropout=0, alpha=0.2, concat=True):
        super(GATLayer, self).__init__()
        self.dropout       = dropout        # drop prob = 0.6
        self.in_features   = in_features * 2    # 
        # self.out_features  = out_features   # 
        self.alpha         = alpha          # LeakyReLU with negative input slope, alpha = 0.2
        self.concat        = concat         # conacat = True for all layers except the output layer.

        # Xavier Initialization of Weights
        # Alternatively use weights_init to apply weights of choice 
        # in_features_W = in_features * 2 + num_aggr_features + num_pos_enc

        # self.W = nn.Parameter(torch.zeros(size=(in_features_W, out_features)))
        # nn.init.xavier_uniform_(self.W.data, gain=1.414)
        
        # self.mlp = linearBlock(in_features_W, hidden_features, out_features)
        # self.mlp.apply(weights_init)
        
        self.a = nn.Parameter(torch.zeros(size=(self.in_features, 1)))
        nn.init.xavier_uniform_(self.a.data, gain=1.414)
        
        # LeakyReLU
        self.leakyrelu = nn.LeakyReLU(self.alpha)

    def forward(self, input, adj):
        N = input.size()[0]
        # print(N)

        # Attention Mechanism
        a_input = torch.cat([input.repeat(1, N).view(N * N, -1), 
                             input.repeat(N, 1)], dim=1).view(N, -1, self.in_features)
        e       = self.leakyrelu(torch.matmul(a_input, self.a).squeeze(2))

        # Masked Attention
        zero_vec  = -9e15*torch.ones_like(e)
        attention = torch.where(adj > 0, e, zero_vec)
        
        attention = F.softmax(attention, dim=1)
        attention = F.dropout(attention, self.dropout, training=self.training)
        h   = torch.matmul(attention, input)

        if self.concat:
            h= F.elu(h)
        return h

class CustomGATLayer(GATLayer):
    def __init__(self, in_features, dropout=0, alpha=0.2, concat=True):
        super(CustomGATLayer, self).__init__(in_features, dropout, alpha, concat)

    def forward(self, query, neighbors):
        """
        Modified to support aggreagting query and neighbors directly
        """
        N = query.size(0)
        D = query.size(1)  # Feature length
        K = neighbors.size(1) # Number of neighbors

        # Step 2: Compute attention scores
        # Concatenate query with neighbors for attention calculation
        combined = torch.cat([query.unsqueeze(1), neighbors], dim=1)  # [64, 5, 224]
        
       # This involves correctly pairing the query with its neighbors
        a_input = torch.cat([combined.unsqueeze(2).expand(-1, -1, K+1, -1),
                             combined.unsqueeze(1).expand(-1, K+1, -1, -1)], dim=3)
        a_input = a_input.view(N*(K+1)*(K+1), 2*D)
        e = self.leakyrelu(torch.matmul(a_input, self.a).squeeze(1))

        # Reshaping e back to a 3D tensor to apply softmax correctly
        # Adjusting dimensions according to the revised approach
        e = e.view(N, K+1, K+1)

        # Reshape attention scores to [N, 5, N] and apply softmax
        attention = F.softmax(e, dim=1)

        # Apply dropout to attention coefficients
        attention = F.dropout(attention, self.dropout, training=self.training)

        # Step 3: Aggregate features using the computed attention scores
        # Use attention coefficients to compute weighted average of neighbors
        h = torch.bmm(attention, combined)
        
        # Taking the aggregated features for the query itself (ignoring self-attention here)
        # Since we're interested in the aggregated features of queries,
        # we focus on the attention scores related to the query itself (first in the combined tensor)
        h = h[:, 0, :]

        if self.concat:
            h= F.elu(h)
        return h

        # # Now add context features and positional encodings
        # if context is not None:
        #     h_new = torch.cat([query, h, context, pos_enc], dim=1)
        # else:
        #     h_new = torch.cat([query, context, pos_enc], dim=1)

        # # Linear Transformation
        # # Apply MLP and optional non-linearity (e.g., ELU)
        # # h_prime = torch.mm(h_new, self.W) # matrix multiplication
        # h_prime = self.mlp(h_new)
        # if self.concat:
        #     h_prime = F.elu(h_prime)
        # if return_gat:
        #     return h_prime, h
        # else: 
        #     return h_prime

class CustomGAT(nn.Module):
    def __init__(self, in_channels):
        super(CustomGAT, self).__init__() 
        # self.gat = GATLayer(in_channels, hidden_channels, out_channels, concat=False,
        #                     num_aggr_features=num_aggr_features)
        self.gat = CustomGATLayer(in_channels, concat=False)

    def forward(self, query, neighbors):
        # x has shape [N, in_channels]
        # edge_index, _ = add_self_loops(edge_index, num_nodes=x.size(0))
        # adj = to_dense_adj(edge_index).squeeze(0)
        return self.gat(query, neighbors)

