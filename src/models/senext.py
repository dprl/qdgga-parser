"""
Code modified from a third party implementation of the SE-Net
https://github.com/Cadene/pretrained-models.pytorch/blob/master/pretrainedmodels/models/senet.py
"""

from __future__ import print_function, division, absolute_import
from collections import OrderedDict
import math
import types

import torch.nn as nn
from torch.utils import model_zoo


def conv3x3(in_planes, out_planes, stride=1, groups=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=1, groups=groups, bias=False)


def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


class SEModule(nn.Module):

    def __init__(self, channels, reduction):
        super(SEModule, self).__init__()
        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.fc1 = nn.Conv2d(channels, channels // reduction, kernel_size=1,
                             padding=0)
        self.relu = nn.ReLU(inplace=True)
        self.fc2 = nn.Conv2d(channels // reduction, channels, kernel_size=1,
                             padding=0)
        self.sigmoid = nn.Sigmoid()

    def forward(self, x):
        module_input = x
        x = self.avg_pool(x)
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)
        x = self.sigmoid(x)
        if x.shape[1] == module_input.shape[1]:
            x = module_input * x
        return x


class SEResNeXtBottleneck(nn.Module):
    expansion = 4

    def __init__(self, inplanes, planes, groups, reduction, norm, stride=1,
                 downsample=None, base_width=4):
        super(SEResNeXtBottleneck, self).__init__()
        width = math.floor(planes * (base_width / 64)) * groups
  
        self.conv1 = nn.Conv2d(inplanes, width, kernel_size=1, bias=False,
                               stride=1)
        self.bn1 = norm(width)
        self.conv2 = nn.Conv2d(width, width, kernel_size=3, stride=stride,
                               padding=1, groups=groups, bias=False)
        self.bn2 = norm(width)
        self.conv3 = nn.Conv2d(width, planes * 4, kernel_size=1, bias=False)
        self.bn3 = norm(planes * 4)
        self.relu = nn.ReLU(inplace=True)
        self.se_module = SEModule(planes * 4, reduction=reduction)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        residual2 = out
        out = self.relu(out)

        if self.conv3:
            out = self.conv3(out)
            out = self.bn3(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        if residual.shape[1] == self.se_module.fc2.out_channels:
            out = self.se_module(out) + residual
        elif residual2.shape[1] == self.se_module.fc2.out_channels:
            out = self.se_module(out) + residual2
        else:
            out = self.se_module(out)

        # clone() used to prevent gradient errors due to conditions
        out = self.relu(out.clone())

        return out

class SEResNeXtBasic(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, groups, reduction, norm, stride=1,
                 downsample=None, base_width=None):
        super(SEResNeXtBasic, self).__init__()

        if groups != 1:
            raise ValueError('BasicBlock only supports groups=1 and base_width=64')

        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = norm(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = norm(planes)
        self.se_module = SEModule(planes, reduction=reduction)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            residual = self.downsample(x)

        out = self.se_module(out) + residual
        out = self.relu(out)

        return out


class SENeXt(nn.Module):
    def __init__(self, input_ch, block, layers, groups, reduction, norm='batch',
                 inplanes=128, downsample_kernel_size=3, multi=False,
                 downsample_padding=1, layer0_kernel_size=7, layer0_stride=2,
                 layer0_maxpool=2, layer0_padding=1):
        super(SENeXt, self).__init__()
        self.inplanes = inplanes
        self.multi = multi
        norm = dict(
               batch = nn.BatchNorm2d,
            instance = nn.InstanceNorm2d,
               group = lambda x: nn.GroupNorm(32, x))[norm]
        block = dict(
            bottleneck = SEResNeXtBottleneck,
            basic = SEResNeXtBasic)[block]

        layer0_modules = [
            ('conv1', nn.Conv2d(input_ch, inplanes,
                                kernel_size=layer0_kernel_size, stride=layer0_stride, 
                                padding=layer0_padding, bias=False)),
            ('bn1', norm(inplanes)),
            ('relu1', nn.ReLU(inplace=True)),
        ]
        # To preserve compatibility with Caffe weights `ceil_mode=True`
        # is used instead of `padding=1`.
        if layer0_maxpool:
            layer0_modules.append(('pool', nn.MaxPool2d(layer0_maxpool, stride=2,
                                                        ceil_mode=True)))
        self.layer0 = nn.Sequential(OrderedDict(layer0_modules))
        self.layer1 = self._make_layer(
            block,
            planes=64,
            blocks=layers[0],
            groups=groups,
            reduction=reduction,
            norm=norm,
            downsample_kernel_size=1,
            downsample_padding=0
        )

        for i in range(1, len(layers)):
            setattr(self, "layer{}".format(i+1),
                self._make_layer(
                    block,
                    planes=64 * (2 ** i),
                    blocks=layers[i],
                    stride=2,
                    groups=groups,
                    reduction=reduction,
                    norm=norm,
                    downsample_kernel_size=downsample_kernel_size,
                    downsample_padding=downsample_padding))
        self.num_layers = len(layers) + 1

    def _make_layer(self, block, planes, blocks, groups, reduction, norm, stride=1,
                    downsample_kernel_size=1, downsample_padding=0):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=downsample_kernel_size, stride=stride,
                          padding=downsample_padding, bias=False),
                norm(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, groups, reduction, norm, stride,
                            downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups, reduction, norm))

        return nn.Sequential(*layers)

    def forward(self, x):
        if self.multi:
            out=[]
            for i in range(self.num_layers):
                x = getattr(self, "layer{}".format(i))(x)
                out.append(x)
            return out
        else:
            for i in range(self.num_layers):
                x = getattr(self, "layer{}".format(i))(x)           
            return x


#MM remove the first  pooling layer from SENext
class SENeXt_1(nn.Module):
    def __init__(self, input_ch, block, layers, groups, reduction, norm='batch',
                 inplanes=128, downsample_kernel_size=3,
                 downsample_padding=1):
        super(SENeXt_1, self).__init__()
        self.inplanes = inplanes
        norm = dict(
               batch = nn.BatchNorm2d,
            instance = nn.InstanceNorm2d,
               group = lambda x: nn.GroupNorm(32, x))[norm]
        block = dict(
            bottleneck = SEResNeXtBottleneck,
            basic = SEResNeXtBasic)[block]

        layer0_modules = [
            ('conv1', nn.Conv2d(input_ch, inplanes, kernel_size=3, stride=1, padding=1, bias=False)),
            ('bn1', norm(inplanes)),
            ('relu1', nn.ReLU(inplace=True)),
        ]
        # To preserve compatibility with Caffe weights `ceil_mode=True`
        # is used instead of `padding=1`.
        self.layer0 = nn.Sequential(OrderedDict(layer0_modules))
        self.layer1 = self._make_layer(
            block,
            planes=64,
            blocks=layers[0],
            groups=groups,
            reduction=reduction,
            norm=norm,
            downsample_kernel_size=1,
            downsample_padding=0
        )

        for i in range(1, len(layers)):
            setattr(self, "layer{}".format(i+1),
                self._make_layer(
                    block,
                    planes=64 * (2 ** i),
                    blocks=layers[i],
                    stride=2,
                    groups=groups,
                    reduction=reduction,
                    norm=norm,
                    downsample_kernel_size=downsample_kernel_size,
                    downsample_padding=downsample_padding))
        self.num_layers = len(layers) + 1

    def _make_layer(self, block, planes, blocks, groups, reduction, norm, stride=1,
                    downsample_kernel_size=1, downsample_padding=0):
        downsample = None
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                nn.Conv2d(self.inplanes, planes * block.expansion,
                          kernel_size=downsample_kernel_size, stride=stride,
                          padding=downsample_padding, bias=False),
                norm(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, groups, reduction, norm, stride,
                            downsample))
        self.inplanes = planes * block.expansion
        for i in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups, reduction, norm))

        return nn.Sequential(*layers)

    def forward(self, x):
        for i in range(self.num_layers):
            x = getattr(self, "layer{}".format(i))(x)
        return x

if __name__=='__main__':

    model=SENeXt_1(2,'bottleneck', [6,6], 32, 16, norm='group',
                 inplanes=128, downsample_kernel_size=3,
                 downsample_padding=1)

