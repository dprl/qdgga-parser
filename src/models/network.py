from collections import OrderedDict
import numpy as np
import cv2
import src.utils.debug_fns as debug_fns
from src.utils.debug_fns import DebugTimer
dTimer = DebugTimer("Testing")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.
import itertools
from itertools import chain
from operator import itemgetter
from functools import reduce, partial
import torch
import networkx as nx
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from torchvision import models
from torchvision.transforms import InterpolationMode as InterpolationMode
from torchvision.transforms.functional import center_crop, resize

from src.models.senext import SENeXt
from src.models.squeezenet import SqueezeNet
from src.models.simplenet import SimpleNet, attBlock, temporalBlock, linearBlock
from src.models.gnn import GCN, VanillaGNN, CustomGNN, CustomGAT, weights_init

from src.image_ops.image2traces import imshow

class RelativeAvgPool(nn.Module):
    """Applies a 2D non-zero average pooling over an input signal composed of several input planes.

    The output is of size H x W, for any input size.

    Args:
        output_size: the target output size of the image of the form H x W.
                     Can be a tuple (H, W) or a single H for a square image H x H.
                     H and W can be either a ``int``, or ``None`` which means the size will
                     be the same as that of the input.
    """
    def __init__(self, output_size):
        super(RelativeAvgPool, self).__init__()
        self.output_size = output_size

    def forward(self, tensor):
        tensor = torch.flatten(tensor, start_dim=2, end_dim=3)
        mask = tensor != 0
        tensor = (tensor * mask).sum(dim=2) / mask.sum(dim=2)
        # AKS: If all non-zero values, assign inf as 0
        tensor[tensor != tensor] = 0
        return tensor.unsqueeze(-1).unsqueeze(-1)

class SpatialPyramidPool(nn.Module):
    """Applies a 2D spatial pyramidal pooling over an input signal composed of several input planes.

    The output is of size 1 x sum(H x W), for any input size.

    Args:
        output_sizes: a list of the target output sizes of the pooling levels
    """
    def __init__(self, pool_out, method="avg"):
        super(SpatialPyramidPool, self).__init__()
        # For Spatial pyramid pooling, convert to horizontal and vertical
        ## sub-divisions
        ## Eg: [1, 2, 4] -> [(1,1), (1,2), (1,4), (2,1), (4,1)]
        pool_out = [*itertools.product([1], pool_out),
                    *itertools.product(pool_out, [1])]
        pool_out = [i for n, i in enumerate(pool_out) if i not in pool_out[:n]]
        self.output_sizes = pool_out
        self.method = method


    def get_window(self, tensor, minimum, maximum):
        tensor = tensor.clone()
        y1, x1 = minimum
        y2, x2 = maximum
        # while (x1, y1) == (x2, y2):
        #     y2 += 1
        #     x2 += 1
        #     x1 -= 1
        #     y1 -= 1
        return tensor[:, y1: y2+2, x1: x2+1]

    def forward(self, tensor, window=False, minimum=None, maximum=None):
        '''
        tensor: an input tensor
        
        returns: a tensor vector with shape [1 x n], which is the concentration 
                 of multi-level pooling
        '''    

        N, C, _, _ = tensor.shape
        if window:
            tensor = [self.get_window(tensor[i], minimum[i], maximum[i]) for i in
                      range(len(tensor))]
        for i in range(len(self.output_sizes)):
            # h_wid = int(math.ceil(previous_conv_size[0] / out_pool_size[i]))
            # w_wid = int(math.ceil(previous_conv_size[1] / out_pool_size[i]))
            # h_pad = (h_wid*out_pool_size[i] - previous_conv_size[0] + 1)/2
            # w_pad = (w_wid*out_pool_size[i] - previous_conv_size[1] + 1)/2
            # maxpool = nn.MaxPool2d((h_wid, w_wid), stride=(h_wid, w_wid), padding=(h_pad, w_pad))
            if self.method == "avg":
                pool = nn.AdaptiveAvgPool2d(self.output_sizes[i])
            elif self.method == "max":
                pool = nn.AdaptiveMaxPool2d(self.output_sizes[i])
            else:
                raise RuntimeError("Unknown pooling type: %s, please use \"max\" or \"avg\".")

            if window:
                x = torch.stack([pool(t) for t in tensor])
            else:
                x = pool(tensor)

            if i == 0:
                spp = x.view(N, C, -1)
            else:
                spp = torch.cat((spp, x.view(N, C, -1)), dim=2)
        return spp.unsqueeze(-1)

class TrainableParser3_mask_both_3branch_spp_symbols_new(nn.Module):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        **encoder_opts
    ):
        super(TrainableParser3_mask_both_3branch_spp_symbols_new, self).__init__()
        self.encoder = dict(
            se=lambda: SENeXt(**encoder_opts[encoder]),
            simple=lambda: SimpleNet(**encoder_opts[encoder]),
            squeeze=lambda: SqueezeNet(**encoder_opts[encoder]),
            resnet=lambda: nn.Sequential(*list(models.resnet18().children())[:-2])
        )[encoder]()
        # print(num_temp)
        # print(num_edge_classes)
        # print(num_node_classes)
        # exit(0)

        if encoder == "resnet":
            # Use 1 channel input
            self.encoder[0] = nn.Conv2d(1, 64, kernel_size=(7,7), stride=(2,2),
                                     padding=(3,3), bias=False)


        self.pool = dict(avg=nn.AdaptiveAvgPool2d, 
                         max=nn.AdaptiveMaxPool2d,
                         rel_avg=RelativeAvgPool,
                         spp_avg=SpatialPyramidPool,
                         )[pool](pool_out)
        self.window = window
        # self.conv1d = conv1d

        # pre attentions--------------------------------------------------------------
        # self.preatt1 = attBlock(att_out_channels)
        # self.preatt2 = attBlock(att_out_channels)
        # self.preatt3 = attBlock(att_out_channels)
        self.sigmoid = nn.Sigmoid()
        self.att_activation = att_activation
        self.att_multiply = att_multiply

        # post attentions--------------------------------------------------------------
        # self.postatt2 = nn.Conv2d(
        #     num_feat * 2, num_feat * 2, kernel_size=3, stride=1, padding=1
        # )
        self.final_activation = final_activation
        self.relu = nn.ReLU(num_feat)
        self.flatten = nn.Flatten()

        # num_hidden_classifier = int(num_feat / 2)
        # num_hidden_segmenter = int(num_feat / 2)
        # num_hidden_parser = int(num_feat / 2)

        if isinstance(pool_out, list):
            pool_dim = sum(pool_out) * 2 - 1
            # num_features_last = last_dim * int(pool_dim / 2)
            self.num_features_last = num_feat * int(pool_dim)
        else:
            self.num_features_last = num_feat
        # num_features_last = last_dim 

        # Temporal/Context layers
        # self.temporal = temporalBlock(num_temp, num_features_last)
        ##len of features are twice due to concat
        # self.temporal2 = temporalBlock(num_temp, num_features_last * 2)
        # self.relu_last = nn.ReLU(last_dim)

        # additional FC layers
        # num_hidden_classifier = int(num_feat / 2)
        # num_hidden_segmenter = int(num_feat / 2)
        # num_hidden_parser = int(num_feat / 2)
        self.classifier = nn.Linear(self.num_features_last * 2, num_node_classes)
        if gnn:
            self.gnn = dict(vanilla_gnn=VanillaGNN,
                            gcn=GCN,
                            custom_gnn=CustomGNN,
                            gat=CustomGAT)[gnn]
            self.gnn_classifier = self.gnn(self.num_features_last)
            self.classifier.apply(weights_init)
        else: 
            self.gnn = False

    @staticmethod
    def get_kneighbors(stk, los_edges, k=2):
        # neighbors = []
        # for i in range(stk.shape[0]):
        #     neighbors.append(reduce(lambda a, b: a + b, 
        #             [stk[c] for count, c in enumerate(los_edges[i]) if count < k],
        #             torch.zeros_like(stk[0])))

        neighbors = torch.stack([reduce(lambda a, b: a + b, 
                    [stk[c] for count, (_, c) in enumerate(los_edges[i]) if count < k],
                    torch.zeros_like(stk[0])) for i in
                                 range(stk.shape[0])]).clip(max=1)
        # neighbors = torch.stack(neighbors)
        return neighbors

    @staticmethod
    def get_kneighbors_parents(stk, los_edges, num_edges, k=2):
        neighbors = torch.stack([reduce(lambda a, b: a + b, 
                    [stk[c] for count, (_, c) in enumerate(los_edges[i]) if count < k],
                    torch.zeros_like(stk[0])) for i in
                                 range(num_edges)]).clip(max=1)
        # neighbors = torch.stack(neighbors)
        return neighbors


    @staticmethod
    def get_bbox(img):
        nonzero = (img == 1).nonzero() 
        minimum = nonzero.min(axis=0)[0]
        maximum = nonzero.max(axis=0)[0]
        bbox = torch.cat((minimum, maximum))
        return bbox

    @staticmethod
    def get_bboxes(imgs):
        nonzeros = [(imgs[i] == 1).nonzero() for i in range(len(imgs))]
        minimum = torch.stack([nonzero.min(axis=0)[0] for nonzero in nonzeros])
        maximum = torch.stack([nonzero.max(axis=0)[0] for nonzero in nonzeros])
        bboxes = torch.cat((minimum, maximum), axis=1)
        return bboxes

    @staticmethod
    def crop(imgs, sizes):
        opimgs = []
        for i, img in enumerate(imgs):
            y1, x1, y2, x2 = sizes[i]
            # TODO: Adding extra pixels (+1), otherwise some pixels missed
            opimgs.append(img[y1: y2+2, x1:x2+2])
        return opimgs


    @staticmethod
    def reconstruct_img(primitives, bbox, img_width):
        primitives_re = torch.zeros(*primitives.shape[:2],
                                    img_width).to(primitives.device)
        image = torch.zeros((primitives.shape[1],
                             img_width)).to(primitives.device)
        sizes = bbox[:, 2:] - bbox[:, :2] + 1
        for i, primitive in enumerate(primitives):
            y1, x1, y2, x2 = bbox[i]
            prim = primitive[:sizes[i][0], :sizes[i][1]]
            image[y1:y2+1, x1:x2+1] += prim
            primitives_re[i, y1:y2+1, x1:x2+1] = prim
        image = image.clip(max=1)
        return image, primitives_re


    @staticmethod
    def crop_resize_single(img, size, new_size=64, cut=False):
        ## 1 Resize longer dimension to given new_size (64) and other shorter
        # dimension to maintain aspect ratio
        max_sizes = size.max(axis=0)
        min_sizes = size.min(axis=0)
        resize_ratio = new_size / max_sizes[0]
        # Clip to 1 if resize ratio is 0
        min_wh = (resize_ratio *
                  min_sizes[0]).round().to(torch.int32).clip(min=1)
        new_shape = [0, 0]
        new_shape[max_sizes[1].item()] = new_size
        # AKS: Handle cases when height = width
        if max_sizes[1].item() == min_sizes[1].item():
            new_shape[1] = min_wh.item()
        new_shape[min_sizes[1].item()] = min_wh.item()
        img = resize(img.unsqueeze(0).unsqueeze(0), size=new_shape, 
                         interpolation=InterpolationMode.NEAREST).squeeze().squeeze()
        # If images are of height or width 1, prevent loss of dimension
        if len(img.shape) == 1:
            img = img.unsqueeze(0)

        # 2 Pad and center crop along the shorter dim to create square (64 x 64)
        img = center_crop(img, new_size)
        return img

    @staticmethod
    def crop_resize(imgs, sizes, new_size=64, cut=True):
        cropped_imgs = []
        resize1 = [0, 0]
        max_sizes = sizes.max(axis=1)
        min_sizes = sizes.min(axis=1)
        resize_ratio = new_size / max_sizes[0]
        # Clip to 1 if resize ratio is 0
        min_wh = (resize_ratio *
                  min_sizes[0]).round().to(torch.int32).clip(min=1)
        
        for i, img in enumerate(imgs):
            if cut:
                img = img[:sizes[i][0], :sizes[i][1]]
            resize1[max_sizes[1][i].item()] = new_size
            # AKS: Handle cases when height = width
            if max_sizes[1][i].item() == min_sizes[1][i].item():
                resize1[1] = min_wh[i].item()
            resize1[min_sizes[1][i].item()] = min_wh[i].item()
            img = resize(img.unsqueeze(0).unsqueeze(0), size=resize1, 
                             interpolation=InterpolationMode.NEAREST).squeeze().squeeze()
            # If images are of height or width 1, prevent loss of dimension
            if len(img.shape) == 1:
                img = img.unsqueeze(0)
            img = center_crop(img, new_size)
            cropped_imgs.append(img)
        return torch.stack(cropped_imgs)

    def extract_context_features(self, los_imgs, stk_imgs, ccs_bbox, edges=None):
        mean_x = torch.index_select(ccs_bbox, 1, torch.tensor([0,2]).to(ccs_bbox.device))\
                                    .to(torch.float32).mean(axis=1).round().int()
        mean_y = torch.index_select(ccs_bbox, 1, torch.tensor([1,3]).to(ccs_bbox.device))\
                                    .to(torch.float32).mean(axis=1).round().int()
        mean = torch.stack([mean_x, mean_y], dim=1)

        if edges is not None:
            mean = torch.index_select(mean, 0, edges[:,0].to(mean.device))

        minmax_los = self.get_bboxes(los_imgs)

        diff = torch.cat((mean - minmax_los[:,:2], minmax_los[:, 2:] - mean), axis=1)
        diff_tb = torch.index_select(diff, 1, torch.tensor([0,2]).to(diff.device))
        diff_lr = torch.index_select(diff, 1, torch.tensor([1,3]).to(diff.device))
        tb_pad_idx = diff_tb.argmin(dim=1)
        lr_pad_idx = diff_lr.argmin(dim=1)

        pad = (diff[:,:2] - diff[:,2:]).abs()
        pad_tb = pad[:,0]
        pad_lr = pad[:,1]

        sizes = []
        padded_los_imgs = []
        padded_context_imgs = []

        for i, los_img in enumerate(los_imgs):
            pad_dim = [0, 0, 0, 0]
            pad_dim[lr_pad_idx[i]] = pad_lr[i].item()
            pad_dim[tb_pad_idx[i] + 2] = pad_tb[i].item()

            pad_f = nn.ZeroPad2d((pad_dim))
            # print(pad_f)

            y1, x1, y2, x2 = minmax_los[i]
            los_img = los_img[y1: y2+1, x1: x2+1]
            context_img = stk_imgs[i][y1: y2+1, x1: x2+1]

            # imshow(los_img)
            padded_los_img = pad_f(los_img)
            padded_context_img = pad_f(context_img)
            sizes.append(padded_los_img.shape)
            # print(sizes, i)
            # imshow(padded_los_img)
            padded_los_imgs.append(padded_los_img)
            padded_context_imgs.append(padded_context_img)
        sizes = torch.tensor(sizes).to(stk_imgs.device)
        return padded_los_imgs, padded_context_imgs, sizes


    def forward(self, x1, x2, x3, x4, lengths, formula_widths, widths, bbox):
        classes = []
        lengths.insert(0, 0)
        for ins in range(len(lengths) - 1):
            # Cropping the individual formula features from the concatenated batch
            # tensor and removing pads along widths
            # x1_sample = x1[
            #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
            # ]
            primitives = x1[sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]),
                            ..., :widths[ins]]
            # import pdb; pdb.set_trace()
            # imshow([s.cpu() for s in primitives])

            bbox_sample = bbox[sum(lengths[: ins + 1]) : sum(lengths[: ins + 2])]
            sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
            # Pass formula image to encoder stack
            _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths[ins])

            # import pdb; pdb.set_trace()
            # imshow(img.cpu(), figsize=(30,20))
            # imshow([s.cpu() for s in stk])
            # import pdb; pdb.set_trace()
            cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
            # import pdb; pdb.set_trace()
            # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))

            # los_edges = [list(item[1]) for item in itertools.groupby(
            #                                 x2[ins].tolist(), key=lambda x: x[0])]

            los_edges = x2[ins]
            if len(los_edges) > 1:
                neighbors = self.get_kneighbors(stk, los_edges, k=2)
                neighbors += stk
                neighbors = neighbors.clip(max=1)
                # imshow([s.cpu() for s in neighbors])
                # padded_los_imgs, padded_context_imgs, sizes = self.extract_context_features(
                #                                                         neighbors, stk, bbox_sample)
                padded_los_imgs, _, sizes = self.extract_context_features(
                                                                        neighbors, stk, bbox_sample)
                # imshow([s.cpu() for s in padded_los_imgs], ncols=1, figsize=(20, 15))
                # imshow([s.cpu() for s in padded_context_imgs], ncols=1, figsize=(20, 15))

                cropped_los_imgs = self.crop_resize(padded_los_imgs, sizes, new_size=64, cut=False)
                # cropped_context_imgs = self.crop(padded_context_imgs, sizes,
                #                                  new_size=64, cut=False)
                # feat1 = torch.cat([cropped_ccs, cropped_context_imgs, cropped_los_imgs])
                feat1 = torch.cat([cropped_ccs, cropped_los_imgs])
                # import pdb; pdb.set_trace()
                # imshow([cropped_ccs[61].cpu(), cropped_los_imgs[61].cpu(),
                #         cropped_context_imgs[61].cpu()], ncols=3, figsize=(30,20))
                # imshow([c.cpu() for c in cropped_los_imgs[16:24]], ncols=10, figsize=(20, 15))
                # imshow([c.cpu() for c in cropped_context_imgs[16:24]], ncols=10, figsize=(20, 15))
            else:
                # feat1 = cropped_ccs.repeat((3, 1, 1))
                feat1 = cropped_ccs.repeat((2, 1, 1))

            # import pdb; pdb.set_trace()
            # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

            feat1 = self.encoder(feat1.unsqueeze(1))

            # Pooling and temporal context
            # Apply Pooling 
            feat1 = self.pool(feat1)

            # Apply temporal context
            feat1 = self.flatten(self.relu(feat1))
            # feat1 = feat1.reshape([3, len(primitives), -1]).transpose(0, 1)
            feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

            # Final outputs
            if self.gnn:
                k = 2
                edge_idx = list(chain.from_iterable([v[:k] for v in
                                                 los_edges.values()]))
                edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
                sym_cl = self.classifier(feat1[:, 0, :], edge_idx,
                                         self.flatten(feat1[:, 1, :]))

            else:
                feat1 = self.flatten(feat1)
                sym_cl = self.classifier(feat1)

            classes.append(sym_cl)

        lengths.pop(0)
        return classes


class TrainableParser3_mask_both_3branch_spp_symbols_segs_new(TrainableParser3_mask_both_3branch_spp_symbols_new):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            **encoder_opts
        )
        self.segmenter = nn.Linear(self.num_features_last + (num_node_classes * 1), 2)
        if gnn:
            # self.segmenter = self.gnn(self.num_features_last, gnn_hidden_features, 2,
            #                           num_aggr_features=(self.num_features_last + num_node_classes))
            self.gnn_segmenter = self.gnn(self.num_features_last)
            self.segmenter.apply(weights_init)
        else:
            self.gnn = None

    def forward(self, x1, x2, x3, x4, lengths, formula_widths, widths, bbox):
        classes = []
        segments = []
        lengths.insert(0, 0)
        for ins in range(len(lengths) - 1):
            # Cropping the individual formula features from the concatenated batch
            # tensor and removing pads along widths
            # x1_sample = x1[
            #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
            # ]
            primitives = x1[sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]),
                            ..., :widths[ins]]
            # import pdb; pdb.set_trace()
            # imshow([s.cpu() for s in primitives])

            bbox_sample = bbox[sum(lengths[: ins + 1]) : sum(lengths[: ins + 2])]
            sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
            # Pass formula image to encoder stack
            _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths[ins])

            # import pdb; pdb.set_trace()
            # imshow(img.cpu(), figsize=(30,20))
            # imshow([s.cpu() for s in stk])
            # import pdb; pdb.set_trace()
            cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
            # import pdb; pdb.set_trace()
            # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))

            # los_edges = [list(item[1]) for item in itertools.groupby(
            #                                 x2[ins].tolist(), key=lambda x: x[0])]

            los_edges = x2[ins]
            if len(los_edges) > 1:
                neighbors = self.get_kneighbors(stk, los_edges, k=2)
                neighbors += stk
                neighbors = neighbors.clip(max=1)
                # imshow([s.cpu() for s in neighbors])
                # padded_los_imgs, padded_context_imgs, sizes = self.extract_context_features(
                #                                                         neighbors, stk, bbox_sample)
                padded_los_imgs, _, sizes = self.extract_context_features(
                                                                        neighbors, stk, bbox_sample)
                # imshow([s.cpu() for s in padded_los_imgs], ncols=1, figsize=(20, 15))
                # imshow([s.cpu() for s in padded_context_imgs], ncols=1, figsize=(20, 15))

                cropped_los_imgs = self.crop_resize(padded_los_imgs, sizes, new_size=64, cut=False)
                # cropped_context_imgs = self.crop(padded_context_imgs, sizes,
                #                                  new_size=64, cut=False)
                # feat1 = torch.cat([cropped_ccs, cropped_context_imgs, cropped_los_imgs])
                feat1 = torch.cat([cropped_ccs, cropped_los_imgs])
                feat2 = torch.cat([cropped_ccs, cropped_los_imgs])
                # import pdb; pdb.set_trace()
                # imshow([cropped_ccs[61].cpu(), cropped_los_imgs[61].cpu(),
                #         cropped_context_imgs[61].cpu()], ncols=3, figsize=(30,20))
                # imshow([c.cpu() for c in cropped_los_imgs[16:24]], ncols=10, figsize=(20, 15))
                # imshow([c.cpu() for c in cropped_context_imgs[16:24]], ncols=10, figsize=(20, 15))
            else:
                # feat1 = cropped_ccs.repeat((3, 1, 1))
                feat1 = cropped_ccs.repeat((2, 1, 1))
                feat2 = cropped_ccs.repeat((2, 1, 1))

            # import pdb; pdb.set_trace()
            # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

            feat1 = self.encoder(feat1.unsqueeze(1))
            feat2 = self.encoder(feat2.unsqueeze(1))

            # Pooling and temporal context
            # Apply Pooling 
            feat1 = self.pool(feat1)
            feat2 = self.pool(feat2)

            # Apply temporal context
            feat1 = self.flatten(self.relu(feat1))
            feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

            feat2 = self.flatten(self.relu(feat2))
            feat2 = feat2.reshape([2, len(primitives), -1]).transpose(0, 1)


            # Final outputs
            if self.gnn:
                k = 2
                edge_idx = list(chain.from_iterable([v[:k] for v in
                                                 los_edges]))
                edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
                sym_cl = self.classifier(feat1[:, 0, :], edge_idx,
                                         self.flatten(feat1[:, 1, :]))

                # append_feat = torch.cat([self.flatten(feat2[:, 1, :]),
                #                          sym_cl], dim=1)
                append_feat = self.flatten(feat2[:, 1, :]) 
                segment = self.segmenter(feat2[:, 0, :], edge_idx, append_feat)
                                             
            else:
                feat1 = self.flatten(feat1)
                sym_cl = self.classifier(feat1)
                segment = self.segmenter(torch.cat([feat2, sym_cl]))

            classes.append(sym_cl)
            segments.append(segment)
            # rels.append(rel)

        lengths.pop(0)
        return classes, segments


class TrainableParser3_mask_both_3branch_spp_symbols_segs_edges_new(TrainableParser3_mask_both_3branch_spp_symbols_new):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            **encoder_opts
        )
        self.segmenter = linearBlock(self.num_features_last * 2, [512], 2)
        self.prune_seg = prune_seg

    def forward(self, x1, x2, x3, x4, lengths, formula_widths, widths, bbox):
        classes = []
        segments = []
        lengths.insert(0, 0)
        for ins in range(len(lengths) - 1):
            # Cropping the individual formula features from the concatenated batch
            # tensor and removing pads along widths
            # x1_sample = x1[
            #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
            # ]
            primitives = x1[sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]),
                            ..., :widths[ins]]
            # import pdb; pdb.set_trace()
            # imshow([s.cpu() for s in primitives])

            bbox_sample = bbox[sum(lengths[: ins + 1]) : sum(lengths[: ins + 2])]
            sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
            # Pass formula image to encoder stack
            _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths[ins])

            los_edges = x2[ins]
            k = 2
            if self.prune_seg:
                edges = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                               los_edges]))).to(x3.device)
            else:
                edges = x3[ins]
            los_edges_parents = [[[x, y] for (x, y) in los_edges[p] if y != c] for p, c in edges.cpu().numpy()]

            pairstk = torch.stack([stk[p] + stk[c] for p, c in
                                   edges]).clip(max=1)
            bbox_pair = self.get_bboxes(pairstk)
            sizes_pair = bbox_pair[:, 2:] - bbox_pair[:, :2] + 1
            cropped_pairs = self.crop(pairstk, bbox_pair)
            cropped_pairs = self.crop_resize(cropped_pairs, sizes_pair,
                                             new_size=64, cut=False)
            
            # import pdb; pdb.set_trace()
            # imshow(img.cpu(), figsize=(30,20))
            # imshow([s.cpu() for s in stk])
            # import pdb; pdb.set_trace()
            cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
            # import pdb; pdb.set_trace()
            # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
            # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

            if len(los_edges) > 1:
                neighbors = self.get_kneighbors(stk, los_edges, k=2)
                neighbors += stk
                neighbors = neighbors.clip(max=1)
                # imshow([s.cpu() for s in neighbors])
                # padded_los_imgs, padded_context_imgs, sizes = self.extract_context_features(
                #                                                         neighbors, stk, bbox_sample)
                padded_los_imgs, _, sizes = self.extract_context_features(neighbors, stk, bbox_sample)
                # import pdb; pdb.set_trace()
                # imshow([s.cpu() for s in padded_los_imgs], ncols=1, figsize=(20, 15))
                # imshow([s.cpu() for s in padded_context_imgs], ncols=1, figsize=(20, 15))

                cropped_los_imgs = self.crop_resize(padded_los_imgs, sizes, new_size=64, cut=False)
                # feat1 = torch.cat([cropped_ccs, cropped_context_imgs, cropped_los_imgs])
                feat1 = torch.cat([cropped_ccs, cropped_los_imgs])
                # import pdb; pdb.set_trace()
                # imshow([c.cpu() for c in cropped_los_imgs], ncols=1, figsize=(20, 15))
                # imshow([c.cpu() for c in cropped_context_imgs], ncols=10, figsize=(20, 15))
            else:
                feat1 = cropped_ccs.repeat((2, 1, 1))

            if len(los_edges_parents) > 1:
                neighbors = self.get_kneighbors_parents(stk, los_edges_parents,
                                                        pairstk.shape[0], k=2)
                neighbors += pairstk
                neighbors.clip(max=1)

                padded_los_imgs, _, sizes = self.extract_context_features(neighbors, pairstk,
                                                                          bbox_sample, edges=edges)
                # import pdb; pdb.set_trace()
                # imshow([s.cpu() for s in padded_los_imgs], ncols=3, figsize=(20, 15))
                # imshow([s.cpu() for s in padded_context_imgs], ncols=1, figsize=(20, 15))

                cropped_los_imgs = self.crop_resize(padded_los_imgs, sizes, new_size=64, cut=False)
                # import pdb; pdb.set_trace()
                # imshow([c.cpu() for c in cropped_los_imgs], ncols=3, figsize=(20, 15))
                feat2 = torch.cat([cropped_pairs, cropped_los_imgs])
            else:
                feat2 = cropped_pairs.repeat((2, 1, 1))

            # import pdb; pdb.set_trace()
            # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

            feat1 = self.encoder(feat1.unsqueeze(1))
            feat2 = self.encoder(feat2.unsqueeze(1))

            # Pooling and temporal context
            # Apply Pooling 
            feat1 = self.pool(feat1)
            feat2 = self.pool(feat2)

            # Apply temporal context
            feat1 = self.flatten(self.relu(feat1))
            feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

            feat2 = self.flatten(self.relu(feat2))
            feat2 = feat2.reshape([2, len(edges), -1]).transpose(0, 1)

            # feat2 = torch.stack(
            #     [
            #         torch.cat((feat2[i], cropped_ccs[pair[0].cpu().numpy(), 0]), 0)
            #         for i, pair in enumerate(edges)
            #     ]
            # )

            # Final outputs
            if self.gnn:
                k = 2
                edge_idx = list(chain.from_iterable([v[:k] for v in
                                                 los_edges]))
                edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
                sym_cl = self.classifier(feat1[:, 0, :], edge_idx,
                                         self.flatten(feat1[:, 1, :]))
            else:
                feat1 = self.flatten(feat1)
                sym_cl = self.classifier(feat1)

            # segment = self.segmenter(torch.cat([feat2, sym_cl]))
            feat2 = self.flatten(feat2)
            segment = self.segmenter(feat2)

            classes.append(sym_cl)
            segments.append(segment)
            # rels.append(rel)

        lengths.pop(0)
        return classes, segments

class TrainableParser3_mask_both_3branch_spp_new(TrainableParser3_mask_both_3branch_spp_symbols_segs_edges_new):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.parser = linearBlock(self.num_features_last * 3,
                                  gnn_hidden_features, num_edge_classes)
        # TODO: Replace this by reading symbol height
        self.new_size = 64

    def get_pairstk(self, edge, img, bbox):
        '''
        Get pair primitive features - add parent and child prim
        ''' 
        p, c = edge
        stk_p = torch.zeros_like(img)
        stk_c = torch.zeros_like(img)
        bbox_pair = torch.stack((bbox[p], bbox[c]))
        minY, minX, maxY, maxX = bbox_pair[0]
        stk_p[minY: maxY + 1, minX: maxX + 1] = img[minY: maxY + 1, minX: maxX + 1]
        minY, minX, maxY, maxX = bbox_pair[1]
        stk_c[minY: maxY + 1, minX: maxX + 1] = img[minY: maxY + 1, minX: maxX + 1]
        pair_stk = (stk_p + stk_c).clip(max=1)
        return pair_stk, bbox_pair

    def get_cropped_pairs(self, edge, img, bbox, new_size=64):
        # 1. Get pair primitive features - add parent and child prim
        pair_stk, bbox_pair = self.get_pairstk(edge, img, bbox)
        
        # 2. Tightly crop pair primitive features
        bbox_min = bbox_pair[:, :2].min(axis=0)[0]
        bbox_max = bbox_pair[:, 2:].max(axis=0)[0]
        bbox_pair = torch.hstack((bbox_min, bbox_max))
        minY, minX, maxY, maxX = bbox_pair
        pair_stk_cropped = pair_stk[minY: maxY + 1, minX: maxX + 1]

        # 3. Resize (+ center crop) tightly cropped pair primitives to uniform
        # size (e.g. 64x64)
        size_pair = bbox_pair[2:] - bbox_pair[:2] + 1
        pair_stk_cropped = self.crop_resize_single(pair_stk_cropped, size_pair,
                                                   new_size)
        return pair_stk_cropped


    def get_pair_feat(self, los_edges, edges, img, bbox, new_size=64):
        los_edges_parents = [[[x, y] for (x, y) in los_edges[p] if y !=
                                  c] for p, c in edges.cpu().numpy()]


        cropped_pairs = torch.stack(list(map(partial(self.get_cropped_pairs, bbox=bbox, img=img,
                                         new_size=new_size), edges))).clip(max=1)
        # cropped_pairs = [self.get_cropped_pairs(edge, img, bbox, new_size=new_size) for
        #                  edge in edges]
        # imshow([c.cpu() for c in cropped_pairs[:12]], ncols=4)
        # import pdb; pdb.set_trace()
        return los_edges_parents, cropped_pairs

    def get_context_feat(self, los_edges, img, bbox,
                         cropped_feat, edges=None, pos_enc_sizes=False, k=2):
        if len(los_edges) > 1:
            # if edges is not None:
            #     # feat = pairstk
            #     for i, los_edge in enumerate(los_edges):
            #         neighbors = torch.zeros_like(img)
            #         stk_i = torch.zeros_like(img)
            #         minY, minX, maxY, maxX = bbox[i]
            #         stk_i[minY: maxY + 1, minX: maxX + 1] = img[minY: maxY + 1, minX: maxX + 1]
            #         for count, (_, c) in enumerate(los_edge):
            #             if count < k:
            #                 minY, minX, maxY, maxX = bbox[c]
            #                 neighbors[minY: maxY + 1, minX: maxX + 1] += img[minY: maxY + 1, minX: maxX + 1]
            #         neighbors += stk_i
            #         neighbors = neighbors.clip(max=1)
            #         # aaa.append(neighbors)
            #         # imshow(neighbors.cpu())
            #     # imshow([x.cpu() for x in aaa[:12]], ncols=2)
            #     # import pdb; pdb.set_trace()
            # else:
            cropped_los_imgs = []
            padded_los_img_shapes = []

            if edges is not None:
                pairfeat = True
            else:
                pairfeat = False
            for i, los_edge in enumerate(los_edges):
                if pairfeat:
                    feat_i, _ = self.get_pairstk(edges[i], img, bbox)
                    bbox_i = bbox[edges[i][0]]
                    minY, minX, maxY, maxX = bbox_i
                else:
                    feat_i = torch.zeros_like(img)
                    bbox_i = bbox[i]
                    minY, minX, maxY, maxX = bbox_i
                    feat_i[minY: maxY + 1, minX: maxX + 1] = img[minY: maxY + 1, minX: maxX + 1]

                mean_stk = torch.stack([(minY + maxY) * 0.5, (minX + maxX) *
                                        0.5]).round().int()

                neighbors = torch.zeros_like(img)
                for count, (_, c) in enumerate(los_edge):
                    if count < k:
                        minY, minX, maxY, maxX = bbox[c]
                        neighbors[minY: maxY + 1, minX: maxX + 1] += img[minY: maxY + 1, minX: maxX + 1]
                neighbors += feat_i
                neighbors = neighbors.clip(max=1)

                # if edges is not None:
                #     mean_stk = torch.index_select(mean_stk, 0, edges[:,0].to(mean_stk.device))

                neighbors_bbox = self.get_bbox(neighbors)
                diff = torch.cat((mean_stk - neighbors_bbox[:2], neighbors_bbox[2:] - mean_stk))
                diff_tb = torch.index_select(diff, 0, torch.tensor([0,2]).to(diff.device))
                diff_lr = torch.index_select(diff, 0, torch.tensor([1,3]).to(diff.device))
                tb_pad_idx = diff_tb.argmin()
                lr_pad_idx = diff_lr.argmin()

                pad = (diff[:2] - diff[2:]).abs()
                pad_tb = pad[0]
                pad_lr = pad[1]

                pad_dim = [0, 0, 0, 0]
                pad_dim[lr_pad_idx] = pad_lr.item()
                pad_dim[tb_pad_idx + 2] = pad_tb.item()

                pad_f = nn.ZeroPad2d((pad_dim))
                # print(pad_f)

                y1, x1, y2, x2 = neighbors_bbox
                los_img = neighbors[y1: y2+1, x1: x2+1]
                # context_img = stk_imgs[i][y1: y2+1, x1: x2+1]

                # imshow(los_img)
                padded_los_img = pad_f(los_img)
                padded_los_img_shape = padded_los_img.shape
                padded_los_img_shapes.append(padded_los_img_shape)
                cropped_los_img = self.crop_resize_single(padded_los_img,
                                                          torch.tensor(padded_los_img_shape),
                                                          new_size=self.new_size)
                cropped_los_imgs.append(cropped_los_img)
                
                # padded_context_img = pad_f(context_img)
                # sizes.append(padded_los_img.shape)
                # print(sizes, i)
                # imshow(padded_los_img)
                # padded_neighbors.append(padded_los_img)
                # padded_context_imgs.append(padded_context_img)
                # sizes = torch.tensor(sizes).to(stk_imgs.device)
                # return padded_neighbors, padded_context_imgs, sizes
                # imshow(neighbors.cpu())
                # aaa.append(neighbors)
                # imshow(neighbors.cpu())
            # imshow([x.cpu() for x in aaa[:12]], ncols=2)
            # import pdb; pdb.set_trace()

            # padded_los_imgs, _, sizes = self.extract_context_features(neighbors, feat,
            #                                                           bbox_sample, edges=edges)
            # import pdb; pdb.set_trace()
            # imshow([s.cpu() for s in padded_los_imgs], ncols=3, figsize=(20, 15))
            # imshow([s.cpu() for s in padded_context_imgs], ncols=1, figsize=(20, 15))

            # cropped_los_imgs = self.crop_resize(padded_los_imgs, sizes, new_size=64, cut=False)
            # import pdb; pdb.set_trace()
            # imshow([c.cpu() for c in cropped_los_imgs], ncols=3, figsize=(20, 15))
            sizes = torch.tensor(padded_los_img_shapes).to(img.device)
            # imshow([x.cpu() for x in cropped_los_imgs[:12]], ncols=2)
            # import pdb; pdb.set_trace()
            cropped_los_imgs = torch.stack(cropped_los_imgs)
            feat = torch.cat([cropped_feat, cropped_los_imgs])
        else:
            feat = cropped_feat.repeat((2, 1, 1))
            sizes = bbox[:, 2:] - bbox[:, :2] + 1
        if pos_enc_sizes:
            return feat, sizes
        else:
            return feat

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1  = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat2 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat2 = self.encoder(feat2.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat2 = self.pool(feat2)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat2 = self.flatten(self.relu(feat2))
        feat2 = feat2.reshape([2, len(edges), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        if self.gnn:
            k = 2
            edge_idx = list(chain.from_iterable([v[:k] for v in
                                             los_edges]))
            edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
            sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                     self.flatten(feat1[:, 1, :]), return_gat=True)

        else:
            feat1 = self.flatten(feat1)
            sym_cl = self.classifier(feat1)

        feat2 = self.flatten(feat2)
        # feat3 = self.flatten(feat3)

        edges_seg_idx = torch.tensor([torch.where(torch.all(edges==edge,
                                                            dim=1))[0][0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat2, 0, edges_seg_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        return sym_cl, segment, rel

class TrainableParser3_mask_both_3branch_spp_new_reduced_rel(TrainableParser3_mask_both_3branch_spp_new):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1 = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        if self.gnn:
            k = 2
            edge_idx = list(chain.from_iterable([v[:k] for v in
                                             los_edges]))
            edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
            sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                     self.flatten(feat1[:, 1, :]), return_gat=True)

        else:
            feat1 = self.flatten(feat1)
            sym_cl = self.classifier(feat1)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        return sym_cl, segment, rel


class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_posenc(TrainableParser3_mask_both_3branch_spp_new):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        context_num_NN=2,
        symbol_height=64,
        dropout=0,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.context_num_NN = context_num_NN
        if gnn:
            self.gnn_classifier = self.gnn(self.num_features_last)
            # self.gnn_edge_classifier = self.gnn(self.num_features_last * 2)
            self.num_features_last = self.num_features_last * 3 + 10
        else:
            self.num_features_last = self.num_features_last * 2 + 10

        self.classifier = linearBlock(self.num_features_last,
                                      gnn_hidden_features, num_node_classes,
                                      dropout)
        self.segmenter = linearBlock(self.num_features_last,
                                     gnn_hidden_features, 2, dropout)
        self.parser = linearBlock(self.num_features_last, gnn_hidden_features, 
                                  num_edge_classes, dropout)

        if gnn:
            self.classifier.apply(weights_init)
            self.segmenter.apply(weights_init)
            self.parser.apply(weights_init)


        self.new_size = symbol_height
        # self.dropout_prob = dropout
        # if dropout:
        #     self.dropout = nn.Dropout(dropout)
        self.computed_forward_edges = {}

    def get_expanded_tensor(self, encoder_out, indices, filtered_indices, labels, 
                            existing_tensors, indentifiers):
        if not len(encoder_out):
            return []
        expanded_encoder_out = torch.zeros((len(labels),
                                            encoder_out.shape[1])).to(encoder_out.device)
        offset_rel = 0
        offset_existing_rel = 0
        for counter,i in enumerate(indices):
            if i in filtered_indices:                      
                expanded_encoder_out[counter] = encoder_out[offset_rel]                                                                
                self.computed_forward_edges[indentifiers[counter]] = encoder_out[offset_rel]                
                offset_rel+=1
            else:                           
                expanded_encoder_out[counter] = existing_tensors[offset_existing_rel]                                    
                offset_existing_rel += 1                
        return expanded_encoder_out
            
    def encode_forward(self, feat, pos_enc):
        """
        SE-ResNext -> Pooling -> Dropout -> Relu -> Pos enc -> Flatten
        """
        N = feat.shape[0]
        # feat = feat[:, [0, 1]].reshape((N*2, H, W))
        # feat = feat.reshape((2, N, H, W))
        # feat = torch.cat((feat[:, 0], feat[:, 1]))
        if self.gnn:
            num_queries = feat.shape[1]
        else:
            num_queries = 2 ## Just query and context
        feat = torch.cat([feat[:, i] for i in range(num_queries)])
        feat = self.encoder(feat.unsqueeze(1))

        # Apply Pooling 
        feat = self.pool(feat)
        # if self.dropout_prob:
        #     feat = self.dropout(feat)

        # Apply temporal context
        feat = self.flatten(self.relu(feat))
        feat = feat.reshape([num_queries, N, -1]).transpose(0, 1)

        # Final outputs
        if self.gnn:
            query_feat = feat[:, 0]
            context_feat = feat[:, 1]
            neighbors_feat = feat[:, 2:]
            gnn_feat = self.gnn_classifier(query_feat, neighbors_feat)
            feat = torch.cat([query_feat, gnn_feat, context_feat], dim=1)
        else:
            feat = self.flatten(feat)

        # Add positional encoding
        feat = torch.cat((feat, pos_enc), 1)
        return feat    


    def forward(self, node_feat, pair_feat, node_pos_enc, pair_pos_enc,
                sym_labels=None, seg_labels=None, rel_labels=None):
        sym_cl, segment, rel = None, None, None        
        if len(node_feat):
            node_feat = self.encode_forward(node_feat, node_pos_enc)
            sym_cl = self.classifier(node_feat)

        if len(pair_feat):
            pair_feat = self.encode_forward(pair_feat, pair_pos_enc)

            # seg_feat = pair_feat[seg_indices]
            # rel_feat = pair_feat[rel_indices]
            rel = self.parser(pair_feat)
            segment = self.segmenter(pair_feat)

        return sym_cl, segment, rel, sym_labels, seg_labels, rel_labels

    def forward_old(self, feat, pos_enc, num_primitives, rel_edges, seg_edges,
                rel_seg_edges, los_edges):
        feat1 = feat[:num_primitives*2]
        # imshow([c.cpu() for c in feat1[:num_primitives]], ncols=4, figsize=(30, 20))
        # imshow([c.cpu() for c in feat1[num_primitives:]], ncols=4, figsize=(30, 20))

        # print(rel_seg_edges[:16].tolist())
        feat2_3 = feat[num_primitives*2:]
        # imshow([c.cpu() for c in feat2_3[:len(rel_seg_edges)]], ncols=4, figsize=(30, 20))
        # imshow([c.cpu() for c in feat2_3[:16]], ncols=4, figsize=(30, 20))
        # imshow([c.cpu() for c in feat2_3[len(rel_seg_edges):len(rel_seg_edges)+16]],
               # ncols=4, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat2_3 = self.encoder(feat2_3.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat2_3 = self.pool(feat2_3)

        if self.dropout_prob:
            feat1 = self.dropout(feat1)
            feat2_3 = self.dropout(feat2_3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, num_primitives, -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat2_3 = self.flatten(self.relu(feat2_3))
        feat2_3 = feat2_3.reshape([2, len(rel_seg_edges), -1]).transpose(0, 1)

        # Final outputs
        if self.gnn:
            edge_idx = list(chain.from_iterable([v[:self.context_num_NN] for v in los_edges]))
            edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
            sym_cl, feat_gnn = self.classifier(torch.cat([feat1[:, 0, :],
                                                          pos_enc], 1), edge_idx,
                                     torch.cat([self.flatten(feat1[:, 1, :]),
                                                pos_enc], 1), return_gat=True)

        else:
            feat1 = self.flatten(feat1)
            sym_cl = self.classifier(feat1)

        feat2_3 = self.flatten(feat2_3)
        feat2_3 = torch.stack(
            [
                torch.cat((feat2_3[i], pos_enc[pair[0].cpu().numpy()], pos_enc[pair[1].cpu().numpy()]), 0)
                for i, pair in enumerate(rel_seg_edges)
            ]
        )

        edges_rel_idx = torch.tensor([torch.where(torch.all(rel_seg_edges==edge, dim=1))[0]
                                        for edge in rel_edges]).to(rel_seg_edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(rel_seg_edges==edge, dim=1))[0]
                                        for edge in seg_edges]).to(rel_seg_edges.device)
        feat3 = torch.index_select(feat2_3, 0, edges_seg_idx)
        feat2 = torch.index_select(feat2_3, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(rel_edges)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        return sym_cl, segment, rel

class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_seggat(TrainableParser3_mask_both_3branch_spp_new_reduced_rel):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.segmenter = linearBlock(self.num_features_last * 3,
                                     gnn_hidden_features, 2)

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1 = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        if self.gnn:
            k = 2
            edge_idx = list(chain.from_iterable([v[:k] for v in
                                             los_edges]))
            edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
            sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                     self.flatten(feat1[:, 1, :]), return_gat=True)

        else:
            feat1 = self.flatten(feat1)
            sym_cl = self.classifier(feat1)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        return sym_cl, segment, rel


class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_seggat_posenc(TrainableParser3_mask_both_3branch_spp_new_reduced_rel_posenc):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.segmenter = linearBlock(self.num_features_last * 3,
                                     gnn_hidden_features, 2)

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1, ctx_sizes = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None, pos_enc_sizes=True)

        pos_enc = self.get_positional_encodings(bbox_sample, sizes, ctx_sizes,
                                               formula_widths, x3.device)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        if self.gnn:
            k = 2
            edge_idx = list(chain.from_iterable([v[:k] for v in
                                             los_edges]))
            edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
            sym_cl, feat_gnn = self.classifier(torch.cat([feat1[:, 0, :],
                                                          pos_enc], 1), edge_idx,
                                     torch.cat([self.flatten(feat1[:, 1, :]),
                                                pos_enc], 1), return_gat=True)

        else:
            feat1 = self.flatten(feat1)
            sym_cl = self.classifier(feat1)

        feat23 = self.flatten(feat23)
        feat23 = torch.stack(
            [
                torch.cat((feat23[i], pos_enc[pair[0].cpu().numpy()], pos_enc[pair[1].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_combined)
            ]
        )

        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        return sym_cl, segment, rel

# C_1 = f(C_0), R_1 = f(R_0, C_0), S_1 = f(S_0, C_0)
class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti(TrainableParser3_mask_both_3branch_spp_new_reduced_rel_seggat):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.classifier2 = self.gnn(num_node_classes, [num_node_classes], num_node_classes)
        # self.segmenter2 = linearBlock(self.num_features_last * 3,
        #                              gnn_hidden_features, 2)
        # self.parser = linearBlock(self.num_features_last * 3,
        #                           gnn_hidden_features, num_edge_classes)

        self.postparser = linearBlock(num_node_classes, [int(num_node_classes / 2)], num_edge_classes)
        self.postsegmenter = linearBlock(num_node_classes, [int(num_node_classes / 2)], 2)

        self.parser2 = linearBlock(num_edge_classes * 2, [num_edge_classes], num_edge_classes)
        self.segmenter2 = linearBlock(2 * 2, [2], 2)

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1 = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        k = 2
        edge_idx = list(chain.from_iterable([v[:k] for v in
                                         los_edges]))
        edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
        sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                 self.flatten(feat1[:, 1, :]), return_gat=True)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        # 2nd pass
        ## Symbol Classifier (C)
        sym_cl2, feat_gnn = self.classifier2(sym_cl, edge_idx, None, return_gat=True)

        ## Relations Classifier (R)
        rel_parent_idx = edges[:, 0]
        feat2 = torch.index_select(feat_gnn, 0, rel_parent_idx)
        feat2 = self.postparser(feat2)
        feat2 = torch.cat((rel, feat2), dim=1)
        rel2 = self.parser2(feat2)
        
        ## Segmentation (S)
        seg_parent_idx = edges_seg[:, 0]
        feat3 = torch.index_select(feat_gnn, 0, seg_parent_idx)
        feat3 = self.postsegmenter(feat3)
        feat3 = torch.cat((segment, feat3), dim=1)
        segment2 = self.segmenter2(feat3)

        return sym_cl2, segment2, rel2

# C_1 = f(C_0, S_0), R_1 = f(R_0, C_0), S_1 = f(S_0, C_0)
class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2(TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.postclassifier = linearBlock(num_node_classes + 2, [num_node_classes], num_node_classes)
        # self.softmax = nn.Softmax(dim=-1)

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1 = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        k = 2
        edge_idx = list(chain.from_iterable([v[:k] for v in
                                         los_edges]))
        edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
        sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                 self.flatten(feat1[:, 1, :]), return_gat=True)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        # 2nd pass
        ## Symbol Classifier (C)
        sym_cl2, feat_gnn = self.classifier2(sym_cl, edge_idx, None, return_gat=True)

        # parent_node_indices = [torch.where(edges_seg[:, 0] == parent_node)[0]
        #                        for parent_node in range(len(primitives))]
        parent_node_indices = torch.stack([torch.where(edges_seg[:, 0] == parent_node)[0][0]
                               for parent_node in range(len(primitives))])
        segment_node = torch.index_select(segment, 0, parent_node_indices)
        sym_cl2 = torch.cat((sym_cl2, segment_node), dim=1)
        sym_cl2 = self.postclassifier(sym_cl2)

        # for i in range(len(primitives)):
        #     for idx in parent_node_indices[i]
        #         ff = torch.cat((sym_cl2[i], segment[idx]), 0)
#         ff = torch.stack([reduce(lambda a: torch.cat((sym_cl[i], a), 0), 
#                                  [segment[i] for count, (_, c) in enumerate(los_edges[i]) if count < k],
#                     torch.zeros_like(stk[0])) for i in
#                                  range(num_edges)]).clip(max=1)


        ## Relations Classifier (R)
        rel_parent_idx = edges[:, 0]
        feat2 = torch.index_select(feat_gnn, 0, rel_parent_idx)
        feat2 = self.postparser(feat2)
        feat2 = torch.cat((rel, feat2), dim=1)
        rel2 = self.parser2(feat2)
        
        ## Segmentation (S)
        seg_parent_idx = edges_seg[:, 0]
        feat3 = torch.index_select(feat_gnn, 0, seg_parent_idx)
        feat3 = self.postsegmenter(feat3)
        feat3 = torch.cat((segment, feat3), dim=1)
        segment2 = self.segmenter2(feat3)

        return sym_cl2, segment2, rel2

# C_1 = f(C_0, S_0, S_0_R), R_1 = f(R_0, C_0), S_1 = f(S_0, S_0_R, C_0)
class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_bidir(TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.postclassifier = linearBlock(num_node_classes + 2 * 2, [num_node_classes], num_node_classes)
        self.def_seg = torch.zeros((2,))
        self.def_seg[-1] = 1
        self.def_rel = torch.zeros((num_edge_classes,))
        self.def_rel[-1] = 1

        self.segmenter2 = linearBlock(2 * 3, [2], 2)
        # self.parser2 = linearBlock(num_edge_classes * 3, [num_edge_classes], num_edge_classes)

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1 = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        k = 2
        edge_idx = list(chain.from_iterable([v[:k] for v in
                                         los_edges]))
        edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
        sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                 self.flatten(feat1[:, 1, :]), return_gat=True)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        # 2nd pass
        ## Symbol Classifier (C)
        sym_cl2, feat_gnn = self.classifier2(sym_cl, edge_idx, None, return_gat=True)

        # parent_node_indices = [torch.where(edges_seg[:, 0] == parent_node)[0]
        #                        for parent_node in range(len(primitives))]
        parent_node_indices = torch.stack([torch.where(edges_seg[:, 0] == parent_node)[0][0]
                               for parent_node in range(len(primitives))])


        segment_node = torch.index_select(segment, 0, parent_node_indices)

        ## Compute outptus for reverse edges (child, parent)
        seg_indices_reverse = [torch.where(torch.all(edges_seg == row, dim=1))[0].item() 
                                       if torch.any(torch.all(edges_seg == row, dim=1)) else None
                               for row in edges_seg[:, [1, 0]]]
        segment_reverse = torch.stack([segment[i] if i is not None else
                                    self.def_seg.to(segment.device)
                                    for i in seg_indices_reverse])
        segment_node_reverse = torch.index_select(segment_reverse, 0, parent_node_indices)

        sym_cl2 = torch.cat((sym_cl2, segment_node, segment_node_reverse), dim=1)
        sym_cl2 = self.postclassifier(sym_cl2)

        ## Relations Classifier (R)
        ## Compute outptus for reverse edges (child, parent)
        # rel_indices_reverse = [torch.where(torch.all(edges == row, dim=1))[0].item() 
        #                                if torch.any(torch.all(edges == row, dim=1)) else None
        #                        for row in edges[:, [1, 0]]]
        # rel_reverse = torch.stack([rel[i] if i is not None else
        #                             self.def_rel.to(rel.device)
        #                             for i in rel_indices_reverse])
        rel_parent_idx = edges[:, 0]
        feat2 = torch.index_select(feat_gnn, 0, rel_parent_idx)
        feat2 = self.postparser(feat2)
        # feat2 = torch.cat((rel, rel_reverse, feat2), dim=1)
        feat2 = torch.cat((rel, feat2), dim=1)
        rel2 = self.parser2(feat2)
        
        ## Segmentation (S)
        seg_parent_idx = edges_seg[:, 0]
        feat3 = torch.index_select(feat_gnn, 0, seg_parent_idx)
        feat3 = self.postsegmenter(feat3)
        feat3 = torch.cat((segment, segment_reverse, feat3), dim=1)
        segment2 = self.segmenter2(feat3)

        return sym_cl2, segment2, rel2


class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_bidir_posenc(TrainableParser3_mask_both_3branch_spp_new_reduced_rel_seggat_posenc):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )

        self.classifier2 = self.gnn(num_node_classes, [num_node_classes], num_node_classes)
        self.postparser = linearBlock(num_node_classes, [int(num_node_classes / 2)], num_edge_classes)
        self.postsegmenter = linearBlock(num_node_classes, [int(num_node_classes / 2)], 2)

        self.parser2 = linearBlock(num_edge_classes * 2, [num_edge_classes], num_edge_classes)
        self.segmenter2 = linearBlock(2 * 2, [2], 2)

        self.postclassifier = linearBlock(num_node_classes + 2 * 2, [num_node_classes], num_node_classes)
        self.def_seg = torch.zeros((2,))
        self.def_seg[-1] = 1
        self.def_rel = torch.zeros((num_edge_classes,))
        self.def_rel[-1] = 1

        self.segmenter2 = linearBlock(2 * 3, [2], 2)
        # self.parser2 = linearBlock(num_edge_classes * 3, [num_edge_classes], num_edge_classes)

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1, ctx_sizes = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None, pos_enc_sizes=True)

        pos_enc = self.get_positional_encodings(bbox_sample, sizes, ctx_sizes,
                                               formula_widths, x3.device)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        k = 2
        edge_idx = list(chain.from_iterable([v[:k] for v in
                                         los_edges]))
        edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
        sym_cl, feat_gnn = self.classifier(torch.cat([feat1[:, 0, :],
                                                      pos_enc], 1), edge_idx,
                                 torch.cat([self.flatten(feat1[:, 1, :]),
                                            pos_enc], 1), return_gat=True)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)
        feat23 = torch.stack(
            [
                torch.cat((feat23[i], pos_enc[pair[0].cpu().numpy()], pos_enc[pair[1].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_combined)
            ]
        )

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        # 2nd pass
        ## Symbol Classifier (C)
        sym_cl2, feat_gnn = self.classifier2(sym_cl, edge_idx, None, return_gat=True)

        # parent_node_indices = [torch.where(edges_seg[:, 0] == parent_node)[0]
        #                        for parent_node in range(len(primitives))]
        parent_node_indices = torch.stack([torch.where(edges_seg[:, 0] == parent_node)[0][0]
                               for parent_node in range(len(primitives))])


        segment_node = torch.index_select(segment, 0, parent_node_indices)

        ## Compute outptus for reverse edges (child, parent)
        seg_indices_reverse = [torch.where(torch.all(edges_seg == row, dim=1))[0].item() 
                                       if torch.any(torch.all(edges_seg == row, dim=1)) else None
                               for row in edges_seg[:, [1, 0]]]
        segment_reverse = torch.stack([segment[i] if i is not None else
                                    self.def_seg.to(segment.device)
                                    for i in seg_indices_reverse])
        segment_node_reverse = torch.index_select(segment_reverse, 0, parent_node_indices)

        sym_cl2 = torch.cat((sym_cl2, segment_node, segment_node_reverse), dim=1)
        sym_cl2 = self.postclassifier(sym_cl2)

        ## Relations Classifier (R)
        ## Compute outptus for reverse edges (child, parent)
        # rel_indices_reverse = [torch.where(torch.all(edges == row, dim=1))[0].item() 
        #                                if torch.any(torch.all(edges == row, dim=1)) else None
        #                        for row in edges[:, [1, 0]]]
        # rel_reverse = torch.stack([rel[i] if i is not None else
        #                             self.def_rel.to(rel.device)
        #                             for i in rel_indices_reverse])
        rel_parent_idx = edges[:, 0]
        feat2 = torch.index_select(feat_gnn, 0, rel_parent_idx)
        feat2 = self.postparser(feat2)
        # feat2 = torch.cat((rel, rel_reverse, feat2), dim=1)
        feat2 = torch.cat((rel, feat2), dim=1)
        rel2 = self.parser2(feat2)
        
        ## Segmentation (S)
        seg_parent_idx = edges_seg[:, 0]
        feat3 = torch.index_select(feat_gnn, 0, seg_parent_idx)
        feat3 = self.postsegmenter(feat3)
        feat3 = torch.cat((segment, segment_reverse, feat3), dim=1)
        segment2 = self.segmenter2(feat3)

        return sym_cl2, segment2, rel2

# C_1 = f(C_0, S_0, R_0), R_1 = f(R_0, C_0), S_1 = f(S_0, C_0)
class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2_2NN(TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.postclassifier = linearBlock(num_node_classes + 2 * 2, [num_node_classes], num_node_classes)
        self.def_seg = torch.zeros((2,))
        self.def_seg[-1] = 1
        self.def_rel = torch.zeros((num_edge_classes,))
        self.def_rel[-1] = 1

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1 = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        k = 2
        edge_idx = list(chain.from_iterable([v[:k] for v in
                                         los_edges]))
        edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
        sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                 self.flatten(feat1[:, 1, :]), return_gat=True)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        # 2nd pass
        ## Symbol Classifier (C)
        sym_cl2, feat_gnn = self.classifier2(sym_cl, edge_idx, None, return_gat=True)

        ### Get segmentation outputs

        ### Get relation outputs
        parent_node_indices = [torch.where(edges_seg[:, 0] == parent_node)[0]
                               for parent_node in range(len(primitives))]
        parent_node_indices_1N = [t[0].item() if len(t) > 0 else None for t in
                               parent_node_indices]
        parent_node_indices_2N = [t[1].item() if len(t) > 1 else None for t in
                               parent_node_indices]

        # segment_node = torch.index_select(segment, 0, parent_node_indices_1N)
        segment_node_1N = torch.stack([segment[i] if i is not None else
                                    self.def_seg.to(segment.device)
                                for i in parent_node_indices_1N])
        segment_node_2N = torch.stack([segment[i] if i is not None else
                                    self.def_seg.to(segment.device)
                                for i in parent_node_indices_2N])

        sym_cl2 = torch.cat((sym_cl2, segment_node_1N, segment_node_2N), dim=1)
        sym_cl2 = self.postclassifier(sym_cl2)

        ## Relations Classifier (R)
        rel_parent_idx = edges[:, 0]
        feat2 = torch.index_select(feat_gnn, 0, rel_parent_idx)
        feat2 = self.postparser(feat2)
        feat2 = torch.cat((rel, feat2), dim=1)
        rel2 = self.parser2(feat2)
        
        ## Segmentation (S)
        seg_parent_idx = edges_seg[:, 0]
        feat3 = torch.index_select(feat_gnn, 0, seg_parent_idx)
        feat3 = self.postsegmenter(feat3)
        feat3 = torch.cat((segment, feat3), dim=1)
        segment2 = self.segmenter2(feat3)

        return sym_cl2, segment2, rel2


# C_1 = f(C_0, S_0, R_0), R_1 = f(R_0, C_0), S_1 = f(S_0, C_0)
class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v3(TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.postclassifier = linearBlock(num_node_classes + 2 +
                                          num_edge_classes, [num_node_classes], num_node_classes)
        # self.def_rel = 1 / num_edge_classes
        self.def_rel = torch.zeros((num_edge_classes,))
        self.def_rel[-1] = 1

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1 = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        k = 2
        edge_idx = list(chain.from_iterable([v[:k] for v in
                                         los_edges]))
        edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
        sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                 self.flatten(feat1[:, 1, :]), return_gat=True)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        # 2nd pass
        ## Symbol Classifier (C)
        sym_cl2, feat_gnn = self.classifier2(self.softmax(sym_cl), edge_idx, None, return_gat=True)

        ### Get segmentation outputs
        # parent_node_indices = [torch.where(edges_seg[:, 0] == parent_node)[0]
        #                        for parent_node in range(len(primitives))]
        parent_node_indices = torch.stack([torch.where(edges_seg[:, 0] == parent_node)[0][0]
                               for parent_node in range(len(primitives))])
        segment_node = torch.index_select(self.softmax(segment), 0, parent_node_indices)

        ### Get relation outputs
        ## Sort the edges by NN distance
        edges_list = [list(item[1]) for item in itertools.groupby(edges.tolist(), key=lambda x: x[0])]
        # create a dictionary to store the index of each tuple in the order list
        order_dict = {t: i for i, t in enumerate(sum(los_edges, []))}
        # sort the inner lists based on the corresponding indices in the order list
        sorted_list = [[t for t in sorted(inner_list, key=lambda x: order_dict.get((x[0], x[1])))]
                       for inner_list in edges_list]
        sorted_edges = torch.tensor(list(chain.from_iterable(sorted_list))).to(x3.device).to(x3.dtype)

        parent_node_indices = [torch.where(sorted_edges[:, 0] == parent_node)[0]
                               for parent_node in range(len(primitives))]
        parent_node_indices = [t[0].item() if len(t) > 0 else None for t in
                               parent_node_indices]
        # Map index back to the original edge tensor
        idx_dict = {i: torch.where((edges == sorted_edges[i]).all(dim=1))[0][0].item()
                               for i in range(len(sorted_edges))}

        rel_node = torch.stack([self.softmax(rel)[idx_dict[i]] if i is not None else self.def_rel.to(rel.device)
                                for i in parent_node_indices])

        sym_cl2 = torch.cat((self.softmax(sym_cl2), segment_node, rel_node), dim=1)
        sym_cl2 = self.postclassifier(sym_cl2)

        ## Relations Classifier (R)
        rel_parent_idx = edges[:, 0]
        feat2 = torch.index_select(feat_gnn, 0, rel_parent_idx)
        feat2 = self.postparser(feat2)
        feat2 = torch.cat((self.softmax(rel), self.softmax(feat2)), dim=1)
        rel2 = self.parser2(feat2)
        
        ## Segmentation (S)
        seg_parent_idx = edges_seg[:, 0]
        feat3 = torch.index_select(feat_gnn, 0, seg_parent_idx)
        feat3 = self.postsegmenter(feat3)
        feat3 = torch.cat((self.softmax(segment), self.softmax(feat3)), dim=1)
        segment2 = self.segmenter2(feat3)

        return sym_cl2, segment2, rel2

# C_1 = f(C_0, S_0), R_1 = f(R_0, C_0, S_0), S_1 = f(S_0, C_0, R_0)
class TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v4(TrainableParser3_mask_both_3branch_spp_new_reduced_rel_gti_v2):
    """Model with edge masks used for segmentation"""

    def __init__(
        self,
        encoder="se",
        pool="avg",
        pool_out=1,
        window=False,
        # conv1d=True,
        num_feat=2048,
        # num_temp=3,
        num_edge_classes=8,
        num_node_classes=101,
        # att_out_channels=1,
        att_activation=False,
        att_multiply=False,
        # postAtt_blocks=4,
        final_activation="",
        gnn="",
        gnn_hidden_features=[],
        prune_seg=False,
        **encoder_opts
    ):
        super().__init__(
            encoder,
            pool,
            pool_out,
            window,
            # conv1d,
            num_feat,
            # num_temp,
            num_edge_classes,
            num_node_classes,
            # att_out_channels,
            att_activation,
            att_multiply,
            # postAtt_blocks,
            final_activation,
            gnn,
            gnn_hidden_features,
            prune_seg,
            **encoder_opts
        )
        self.parser2 = linearBlock(num_edge_classes * 2 + 2, [num_edge_classes], num_edge_classes)
        self.post_rel_in_seg = linearBlock(num_edge_classes, [int(num_edge_classes / 2)], 2)

        self.segmenter2 = linearBlock(2 * 3, [2], 2)
        self.def_rel = 1 / num_edge_classes

    def forward(self, primitives, x2, x3, formula_widths, bbox_sample):
        # Cropping the individual formula features from the concatenated batch
        # tensor and removing pads along widths
        # x1_sample = x1[
        #     sum(lengths[: ins + 1]) : sum(lengths[: ins + 2]), ..., : widths[ins]
        # ]
        # import pdb; pdb.set_trace()
        # imshow([s.cpu() for s in primitives])

        sizes = bbox_sample[:, 2:] - bbox_sample[:, :2] + 1
        cropped_ccs = self.crop_resize(primitives, sizes, new_size=64)
        _, stk = self.reconstruct_img(primitives, bbox_sample, formula_widths)

        los_edges = x2
        k = 2
        edges = x3

        if self.prune_seg:
            edges_seg = torch.tensor(list(chain.from_iterable([v[:k] for v in
                                                           los_edges]))).to(x3.device).to(x3.dtype)
        else:
            edges_seg = edges

        edges_combined = torch.cat((edges, edges_seg), dim=0).unique(dim=0)
        # los_edges_parents_seg, cropped_pairs_seg, pairstk_seg = self.get_pair_feat(los_edges, edges_seg, stk)
        # los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges, stk)
        los_edges_parents, cropped_pairs, pairstk = self.get_pair_feat(los_edges, edges_combined, stk)

        # import pdb; pdb.set_trace()
        # imshow(img.cpu(), figsize=(30,20))
        # imshow([s.cpu() for s in stk])
        # import pdb; pdb.set_trace()
        # import pdb; pdb.set_trace()
        # imshow([cc.cpu() for cc in cropped_ccs], ncols=1, figsize=(20, 15))
        # imshow([cc.cpu() for cc in cropped_pairs], ncols=3, figsize=(20, 15))

        feat1 = self.get_context_feat(los_edges, stk, pairstk, bbox_sample,
                                     cropped_ccs, edges=None)
        feat23 = self.get_context_feat(los_edges_parents, stk, pairstk, bbox_sample,
                                     cropped_pairs, edges=edges_combined)
        # feat3 = self.get_context_feat(los_edges_parents_seg, stk, pairstk_seg, bbox_sample,
        #                              cropped_pairs_seg, edges=edges_seg)

        # imshow([c.cpu() for c in feat1], ncols=5, figsize=(30, 20))

        feat1 = self.encoder(feat1.unsqueeze(1))
        feat23 = self.encoder(feat23.unsqueeze(1))
        # feat3 = self.encoder(feat3.unsqueeze(1))

        # Pooling and temporal context
        # Apply Pooling 
        feat1 = self.pool(feat1)
        feat23 = self.pool(feat23)
        # feat3 = self.pool(feat3)

        # Apply temporal context
        feat1 = self.flatten(self.relu(feat1))
        feat1 = feat1.reshape([2, len(primitives), -1]).transpose(0, 1)

        # import pdb; pdb.set_trace()
        feat23 = self.flatten(self.relu(feat23))
        feat23 = feat23.reshape([2, len(edges_combined), -1]).transpose(0, 1)

        # feat3 = self.flatten(self.relu(feat3))
        # feat3 = feat3.reshape([2, len(edges_seg), -1]).transpose(0, 1)

        # Final outputs
        k = 2
        edge_idx = list(chain.from_iterable([v[:k] for v in
                                         los_edges]))
        edge_idx = torch.tensor(edge_idx).mT.long().to(feat1.device)
        sym_cl, feat_gnn = self.classifier(feat1[:, 0, :], edge_idx,
                                 self.flatten(feat1[:, 1, :]), return_gat=True)

        feat23 = self.flatten(feat23)
        # feat3 = self.flatten(feat3)

        edges_rel_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges]).to(edges.device)
        edges_seg_idx = torch.tensor([torch.where(torch.all(edges_combined==edge, dim=1))[0]
                                        for edge in edges_seg]).to(edges.device)
        feat3 = torch.index_select(feat23, 0, edges_seg_idx)
        feat2 = torch.index_select(feat23, 0, edges_rel_idx)

        feat2 = torch.stack(
            [
                torch.cat((feat2[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges)
            ]
        )

        feat3 = torch.stack(
            [
                torch.cat((feat3[i], feat_gnn[pair[0].cpu().numpy()]), 0)
                for i, pair in enumerate(edges_seg)
            ]
        )

        rel = self.parser(feat2)
        segment = self.segmenter(feat3)

        # 2nd pass
        ## Symbol Classifier (C)
        sym_cl2, feat_gnn = self.classifier2(sym_cl, edge_idx, None, return_gat=True)

        # parent_node_indices = [torch.where(edges_seg[:, 0] == parent_node)[0]
        #                        for parent_node in range(len(primitives))]
        parent_node_indices = torch.stack([torch.where(edges_seg[:, 0] == parent_node)[0][0]
                               for parent_node in range(len(primitives))])
        segment_node = torch.index_select(segment, 0, parent_node_indices)
        sym_cl2 = torch.cat((sym_cl2, segment_node), dim=1)
        sym_cl2 = self.postclassifier(sym_cl2)

        ## Relations Classifier (R)
        rel_parent_idx = edges[:, 0]
        feat2 = torch.index_select(feat_gnn, 0, rel_parent_idx)
        feat2 = self.postparser(feat2)

        seg_rel_indices = [torch.where(torch.all(edges_seg == row, dim=1))[0].item() 
                           if torch.any(torch.all(edges_seg == row, dim=1)) else None
                           for row in edges]
        seg_in_rel = torch.stack([segment[i] if i is not None else
                                  torch.full((segment.shape[1],), 0.5).to(rel.device)
                                for i in seg_rel_indices])

        feat2 = torch.cat((rel, feat2, seg_in_rel), dim=1)
        rel2 = self.parser2(feat2)
        
        ## Segmentation (S)
        seg_parent_idx = edges_seg[:, 0]
        feat3 = torch.index_select(feat_gnn, 0, seg_parent_idx)
        feat3 = self.postsegmenter(feat3)

        rel_seg_indices = [torch.where(torch.all(edges == row, dim=1))[0].item() 
                           if torch.any(torch.all(edges == row, dim=1)) else None
                           for row in edges_seg]
        rel_in_seg = torch.stack([rel[i] if i is not None else
                                  torch.full((rel.shape[1],), self.def_rel).to(rel.device)
                                for i in rel_seg_indices])
        rel_in_seg = self.post_rel_in_seg(rel_in_seg)
        feat3 = torch.cat((segment, feat3, rel_in_seg), dim=1)
        segment2 = self.segmenter2(feat3)

        return sym_cl2, segment2, rel2
