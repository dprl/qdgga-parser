import torch
import numpy as np
from torch import nn

def linearBlock(in_channels, hidden_channels, out_channels, dropout_prob=0):
    if len(hidden_channels) == 0:
        mlp = nn.Linear(in_channels, out_channels)
    else:
        mlp = nn.Sequential(nn.Linear(in_channels, hidden_channels[0]),
                           nn.ReLU(), nn.Dropout(dropout_prob),
                           nn.Linear(hidden_channels[0], out_channels))
    return mlp

def temporalBlock(num_temp, num_feat, kernel_size=3, stride=1, padding=1):
    temporal = []
    for _ in range(num_temp):
        temporal += [
            nn.Conv1d(num_feat, num_feat, kernel_size, stride, padding),
            nn.InstanceNorm1d(num_feat),
            nn.ReLU(num_feat)]
    temporal = nn.Sequential(*temporal)
    return temporal

def attBlock(att_out_channels, in_channels=1, out_channels=1):
    att = [ConvolutionBlock(in_channels=in_channels, out_channels=att_out_channels, 
                            kernel_size=7, stride=2, padding=3, activ='relu'),
           ConvolutionBlock(in_channels=att_out_channels, out_channels=att_out_channels*2,
                            kernel_size=5, stride=2, padding=2, activ='relu'),
           ConvolutionBlock(in_channels=att_out_channels*2,
                            out_channels=out_channels, 
                            kernel_size=5, stride=2, padding=2, activ='relu')]

    preatt = nn.Sequential(*att)
    return preatt

class ConvolutionBlock(nn.Module):
    def __init__(self, conv='conv2d', norm='instance', activ='relu', padding=0, **conv_opts):
        super(ConvolutionBlock, self).__init__()

        self.pad = nn.ZeroPad2d(padding)
        self.conv = nn.Conv2d(**conv_opts)

        out_channels = conv_opts['out_channels']
        self.norm = dict(
             none = lambda x: lambda x: x,
            batch = nn.BatchNorm2d,
            group = lambda x: nn.GroupNorm(32, x),
         instance = nn.InstanceNorm2d)[norm](out_channels)

        self.activ = dict(
              none = lambda: lambda x: x,
              relu = lambda: nn.ReLU(inplace=True),
             lrelu = lambda: nn.LeakyReLU(0.2, inplace=True))[activ]()

    def forward(self, x):
        return self.activ(self.norm(self.conv(self.pad(x))))


class ResidualBlock(nn.Module):
    def __init__(self, channels, norm='instance', activ='relu'):
        super(ResidualBlock, self).__init__()

        block = []
        block += [ConvolutionBlock(
            in_channels=channels, out_channels=channels, kernel_size=3,
            stride=1, padding=1, norm=norm, activ=activ)]
        block += [ConvolutionBlock(
            in_channels=channels, out_channels=channels, kernel_size=3,
            stride=1, padding=1, norm=norm, activ='none')]
        self.model = nn.Sequential(*block)

    def forward(self, x):
        return self.model(x) + x


class SimpleNet(nn.Module):
    def __init__(self, input_ch, base_ch, num_down, num_res, norm='instance'):
        super(SimpleNet, self).__init__()

        self.conv0 = ConvolutionBlock(
            in_channels=input_ch, out_channels=base_ch, kernel_size=7, stride=1,
            padding=3, norm=norm, activ='relu')
        
        output_ch = base_ch
        for i in range(1, num_down+1):
            m = ConvolutionBlock(
                in_channels=output_ch, out_channels=output_ch * 2, kernel_size=4,
                stride=2, padding=1, norm=norm, activ='relu')
            setattr(self, "conv{}".format(i), m)
            output_ch *= 2

        for i in range(num_res):
            setattr(self, "res{}".format(i),
                ResidualBlock(output_ch, norm=norm, activ='relu'))

        self.layers = [getattr(self, "conv{}".format(i)) for i in range(num_down+1)] + \
            [getattr(self, "res{}".format(i)) for i in range(num_res)]

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

if __name__=='__main__':
    model=SimpleNet(1,64,3,6,norm='group')
    print(model)

