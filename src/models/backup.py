        for ins in range(len(lengths)):
            if ins == 0:
                x1_sample = x1[:lengths[ins], ..., :widths[ins]]
            else:
                x1_sample = x1[sum(lengths[:ins]): sum(lengths[:ins+1]), ..., :widths[ins]]
            

            # encode image
            img = x1_sample[:1].unsqueeze(0)
            # img = x1[:, :1]
            feat = self.encoder(img).squeeze(0)
            endPoint= x2[ins].cpu().numpy()
            
            #convolve masks into their corrosponding att layer 
            stk = x1_sample[1:]
            node_stk = torch.stack([
                self.preatt1(stk[i:i+1, ...].unsqueeze(0)).squeeze(0).squeeze(0) for i in range(endPoint)])
            node2_stk = torch.stack([
                self.preatt3(stk[i:i+1, ...].unsqueeze(0)).squeeze(0).squeeze(0) for i in range(endPoint)])
            pair_stk= torch.stack([
                self.preatt2(stk[i:i+1, ...].unsqueeze(0)).squeeze(0).squeeze(0) for i in range(endPoint,stk.shape[0])])

            # downsample 
            node2_stk = F.interpolate(node2_stk.unsqueeze(0), feat.shape[-2:], mode='area')
            node2_stk /= node2_stk.sum((2, 3), keepdim=True)
            node2_stk = node2_stk.squeeze(0)

            node_stk = F.interpolate(node_stk.unsqueeze(0), feat.shape[-2:], mode='area')
            node_stk /= node_stk.sum((2, 3), keepdim=True)
            node_stk = node_stk.squeeze(0)

            pair_stk = F.interpolate(pair_stk.unsqueeze(0), feat.shape[-2:], mode='area')
            pair_stk /= pair_stk.sum((2, 3), keepdim=True)
            pair_stk = pair_stk.squeeze(0)

            # used to be temp_node2
            feat3 = torch.stack([
                node2_stk[i:i+1, ...] * feat for i in range(endPoint)])  
            feat1 = torch.stack([
                node_stk[i:i+1, ...] * feat for i in range(endPoint)])
            # apply pair masks on feat
            temp_edge= torch.stack([
                pair_stk[i:i+1, ...] * feat for i in range(pair_stk.shape[0])]) 

            #concat each pair and parent mask FIRST then apply POSTATT
            temp=torch.stack([
                    torch.cat((temp_edge[i],feat1[pair[0].cpu().numpy()]),0) for i,pair in enumerate(x3[ins])]) #concat with parent 
            feat2= torch.stack([
                self.relu(self.postatt2(temp[i:i+1, ...])).squeeze(0) for i in range(temp.shape[0])])                    

            #segmenter features
            feat3 = self.pool(feat3)
            feat3 = feat3.squeeze(2).squeeze(2)
            feat3 = feat3.transpose(0, 1)
            if feat3.shape[1] != 1:
                feat3 = self.temporal(feat3.unsqueeze(0)).squeeze(0)
            else:
                feat3=self.relu(feat3.unsqueeze(0)).squeeze(0)
                
            feat3 = feat3.transpose(0, 1)

            #classifiers features
            feat1 = self.pool(feat1)
            feat1 = feat1.squeeze(2).squeeze(2)
            feat1 = feat1.transpose(0, 1)
            if feat1.shape[1] != 1:
                feat1 = self.temporal(feat1.unsqueeze(0)).squeeze(0)
            else:
                feat1=self.relu(feat1.unsqueeze(0)).squeeze(0)
                
            feat1 = feat1.transpose(0, 1)

            #parser features
            feat2 = self.pool(feat2)
            feat2 = feat2.squeeze(2).squeeze(2)
            feat2 = feat2.transpose(0, 1)
            if feat2.shape[1] != 1:        
                feat2 = self.temporal2(feat2.unsqueeze(0)).squeeze(0)
            else: feat2=self.relu(feat2.unsqueeze(0)).squeeze(0)
            
            feat2 = feat2.transpose(0, 1)
            
            sym_cl = self.classifier(feat1)
            segment = self.segmenter(feat3)
            rel = self.parser(feat2)

            classes.append(sym_cl)
            segments.append(segment)
            rels.append(rel)

        classes = torch.cat(classes)
        segments = torch.cat(segments)
        rels = torch.cat(rels)
        return classes, segments, rels    
