import torch.nn as nn
import numpy as np
import math
import os
import cv2
import torch
from torchvision.transforms.functional import center_crop, resize
from torchvision.transforms import InterpolationMode as InterpolationMode
from operator import itemgetter
from .cc_ops import CCOps
import matplotlib.pyplot as plt
from skimage import measure
from lgap_settings import CC_GRAY_THRESHOLD
from shapely.geometry import Polygon, Point
# RZ: Add debug operations
from src.utils.debug_fns import *
import src.utils.debug_fns as debug_fns
dTimer = DebugTimer("LGAP dataloader")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

#mode: contour
reduc_factor = 1
smooth_dist = 2
reduc_type = 'Smooth'

#mode: stroke
skel_type = "Zhang" #
eval_type = "BoxRatio" #"contourbounds"
win_fac = 8 #-1

cbound_min_ratio = .6
cbound_dist_fac = 1.25

# Create a grid of points within the bounds of the hull
def points_inside_boundary(boundary):
    boundary = Polygon(boundary.reshape(-1, 2))
    min_x, min_y, max_x, max_y = boundary.bounds
    x_coords, y_coords = np.meshgrid(np.arange(min_x, max_x+1, 1), 
                                     np.arange(min_y, max_y+1, 1))  # Adjust the step for resolution
    grid_points = np.vstack([x_coords.ravel(), y_coords.ravel()]).T

    # Define a vectorized function for point-in-polygon test including boundary
    inside_or_boundary = np.vectorize(lambda x, y: boundary.contains(Point(x, y))
                                      or boundary.touches(Point(x, y)))

    # Apply the inside_or_boundary function over the grid
    points_inside_or_on_boundary_mask = inside_or_boundary(grid_points[:, 0], grid_points[:, 1])

    # Filter points that are inside the hull or on its boundary
    points_inside_or_on_boundary = grid_points[points_inside_or_on_boundary_mask].astype(int)

    # Create a blank image
    # Determine the bounding box of the points
    # Create an image of the size of the bounding box
    # width, height = max_x - min_x + 1, max_y - min_y + 1
    # Adjust points to the new coordinate system
    adjusted_points = points_inside_or_on_boundary - points_inside_or_on_boundary.min(0)
    
    return points_inside_or_on_boundary, adjusted_points

def scale_primitives(image, boxes, new_size=64, filename=None, contours=None,
                     lg_mode="cc", merge_edges=[]):
    # Resize longer dimension to new_size and other dimension maintaining aspect
    # ratio
    image = torch.FloatTensor(image)
    prim_full_img = torch.zeros((len(boxes), *image.shape))
    primitives = []
    scaled_primitives = []
    primitives_batch = torch.zeros((len(boxes), new_size, new_size))

    # sort CC boxes from left-right, top-down
    boxes = sorted(boxes.items(), key=lambda x:x[1][0])
    boxes = sorted(boxes, key=lambda x:x[1][1])
    id_map = {new:old for new, (old, _) in enumerate(boxes)}
    prim_bbox = np.array(list(map(itemgetter(1), boxes)))
    if lg_mode == "contours":
        contours_new = {new:contours[old] for new, old in id_map.items()}
    else:
        contours_new = {}

    box_sizes = prim_bbox[:, 2:] - prim_bbox[:, :2] + 1
    box_sizes = torch.tensor(box_sizes)

    max_sizes = box_sizes.max(axis=1)
    min_sizes = box_sizes.min(axis=1)
    resize_ratio = new_size / max_sizes[0]

    # Clip to 1 if resize ratio is 0
    min_wh = (resize_ratio * min_sizes[0]).round().to(torch.int32).clip(min=1)

    new_shape = [0, 0]
    scaled_sizes = []
    for i, box in enumerate(prim_bbox):
        (minY, minX, maxY, maxX) = box
        if lg_mode == "contours":
            contour = contours_new[i]
            # all_points, adjusted_points = points_inside_boundary(contour[0])
            contour = [cnt - np.array([minX, minY]) for cnt in contour]
            # cols, rows = all_points.T
            # cols_prim, rows_prim = adjusted_points.T
            # prim_size = adjusted_points.max(0) - adjusted_points.min(0) + 1
            # prim_img = torch.zeros(box_sizes[i][0], box_sizes[i][1])
            prim_img = np.zeros((box_sizes[i][0], box_sizes[i][1]))
            # prim_img2 = np.zeros((box_sizes[i][0], box_sizes[i][1]))
            # if contours:
            # if i ==79 or i == 80 or i == 90:
            #     import pdb; pdb.set_trace()
            cv2.drawContours(prim_img, contour, contourIdx=-1, color=1, thickness=-1)
            # prim_img2[rows_prim, cols_prim] = image[rows, cols]
            # imshow([prim_img, prim_img2])
            # imshow(prim_img)
            # import pdb; pdb.set_trace()
            prim_img = torch.from_numpy(prim_img)
            # prim_full_img[i, minY: maxY+1, minX: maxX+1] = prim_img
        else:
            prim_img = image[ minY : maxY + 1, minX : maxX + 1]
            # AKS: If primitive has no pixels, expand the box
            if prim_img.size == 0:
                minY -= 1
                minX -= 1
                maxY += 1
                maxX += 1
                prim_img = image[ minY : maxY + 1, minX : maxX + 1 ]
                print(f"\nFilename with empty primitive = {filename}, expand bbox ")
        # if len(merge_edges):
        #     prim_full_img[i, minY: maxY+1, minX: maxX+1] = prim_img
            
        prim_img, prim_img_scaled, new_shape = scale(prim_img,
                                                     max_sizes[1][i].item(), 
                                                     min_sizes[1][i].item(),
                                                     min_wh[i].item(), new_size, filename)
        primitives.append(prim_img)
        scaled_sizes.append(new_shape)
        scaled_primitives.append(prim_img_scaled)
        primitives_batch[i, :new_shape[0], :new_shape[1]] = prim_img_scaled

    # if len(merge_edges):
    #     # Combine primitive features to symbol level features
    #     for prim_ids in merge_edges:
    #         prim_bbox_combined = prim_bbox[list(prim_ids)]
    #         minY, minX = prim_bbox_combined[:, :2].min(0)
    #         maxY, maxX = prim_bbox_combined[:, 2:].max(0)
    #         combined_size = np.array([maxY - minY + 1, maxX - minX + 1])

    #         max_size_idx = combined_size.argmax()
    #         min_size_idx = combined_size.argmin()
    #         resize_ratio = new_size / combined_size[max_size_idx]
    #         min_wh = int((resize_ratio * combined_size[min_size_idx]).round())
    #         # Clip to 1 if resize ratio is 0
    #         if min_wh < 1:
    #             min_wh = 1

    #         prim_full_img_group = torch.index_select(prim_full_img, dim=0,
    #                                    index=torch.tensor(list(prim_ids)))
    #         merged_sym = torch.max(prim_full_img_group, dim=0).values
    #         for node in prim_ids:
    #             prim_bbox[node] = np.array([minY, minX, maxY, maxX])
    #             prim_full_img[node] = merged_sym
    #             prim_img = merged_sym[minY: maxY+1, minX: maxX+1]
    #             prim_img, prim_img_scaled, new_shape = scale(prim_img, max_size_idx, min_size_idx,
    #                                                          min_wh, new_size, filename)
    #             primitives[node] = prim_img
    #             scaled_sizes[node] = new_shape
    #             scaled_primitives[node] = prim_img_scaled
    #             primitives_batch[node] = torch.zeros((new_size, new_size)) 
    #             primitives_batch[node, :new_shape[0], :new_shape[1]] = prim_img_scaled
    return primitives, scaled_primitives, primitives_batch, prim_bbox, scaled_sizes, contours_new

def scale(prim_img, max_size_idx, min_size_idx, min_wh, new_size, filename,
          interpolation=InterpolationMode.NEAREST):
    new_shape = [0, 0]
    if not prim_img.any():
        prim_img = torch.ones(prim_img.shape)
        print(f"\nFilename with empty primitive = {filename}, assign pixel 1")

    new_shape[max_size_idx] = new_size
    # AKS: Handle cases when height = width
    if max_size_idx == min_size_idx:
        new_shape[1] = min_wh
    new_shape[min_size_idx] = min_wh

    # print(interpolation)
    prim_img_scaled = resize(prim_img.unsqueeze(0).unsqueeze(0), size=new_shape, 
                     interpolation=interpolation).squeeze().squeeze()
    # If images are of height or width 1, prevent loss of dimension
    if len(prim_img_scaled.shape) == 1:
        prim_img_scaled = prim_img_scaled.unsqueeze(new_shape.index(1))
    return prim_img, prim_img_scaled, tuple(new_shape)

def get_bbox(img):
    nonzero = (img == 1).nonzero() 
    minimum = nonzero.min(axis=0)[0]
    maximum = nonzero.max(axis=0)[0]
    bbox = torch.cat((minimum, maximum))
    return bbox

def crop_resize_single(img, size, new_size=64,
                       interpolation=InterpolationMode.NEAREST):
    ## 1 Resize longer dimension to given new_size (64) and other shorter
    # dimension to maintain aspect ratio
    max_sizes = size.max(axis=0)
    min_sizes = size.min(axis=0)
    resize_ratio = new_size / max_sizes[0]
    # Clip to 1 if resize ratio is 0
    min_wh = (resize_ratio *
              min_sizes[0]).ceil().to(torch.int32).clip(min=1)
    new_shape = [0, 0]
    new_shape[max_sizes[1].item()] = new_size
    # AKS: Handle cases when height = width
    if max_sizes[1].item() == min_sizes[1].item():
        new_shape[1] = min_wh.item()
    new_shape[min_sizes[1].item()] = min_wh.item()
    # print(interpolation)
    img = resize(img.unsqueeze(0).unsqueeze(0), size=new_shape, 
                     interpolation=interpolation).squeeze().squeeze()
    # If images are of height or width 1, prevent loss of dimension
    if len(img.shape) == 1:
        img = img.unsqueeze(0)

    # 2 Pad and center crop along the shorter dim to create square (64 x 64)
    img = center_crop(img, new_size)
    return img

def image2strokes(boxes, instances, tsv=False, in_img=None, resize=None,
                  filename=None):
    image = in_img
    strokes = []

    rescale = None
    if resize:
        image, rescale = image_resize(image, height=resize["height"])
        # imshow(image)
        # import pdb; pdb.set_trace()
        # image, rescale = image_resize(image)

    for objectId in instances:
        box = boxes[ objectId ]
        if rescale:
            box = box * rescale
            box[:2] = np.floor(box[:2])
            box[2:] = np.ceil(box[2:])

        (minY, minX, maxY, maxX) = box.astype('int')

        stroke = np.zeros(image.shape)
        
        stroke[ minY : maxY + 1, minX : maxX + 1 ] = image[ minY : maxY + 1, minX : maxX + 1 ]
        # import pdb; pdb.set_trace()
        
        # AKS: If stroke has no pixels, expand the box
        if stroke[ minY: maxY + 1, minX : maxX + 1 ].size == 0:
            minY -= 1
            minX -= 1
            maxY += 1
            maxX += 1
            stroke[ minY : maxY + 1, minX : maxX + 1 ] = image[ minY : maxY + 1, minX : maxX + 1 ]
            print(f"\nFilename with empty stroke = {filename}, expand bbox ")

        # AKS: If stroke has all 0 pixels, assign 1 pixel
        if not stroke.any():
            stroke[ minY: maxY + 1, minX : maxX + 1 ] = np.ones(
                                                    stroke[ minY: maxY + 1, minX : maxX + 1 ].shape)
            print(f"\nFilename with empty stroke = {filename}, assign pixel 1")
        strokes.append(stroke)
    return strokes, image

def get_minmax(images):
    """Get the top-left (minimum) and bottom-right (maximum) coordinates

    :img: input images, N x H x W
    :returns: top-left and bottom-right coordinates for each image

    """

    nonzeros = [(images[i] == 1).nonzero() for i in range(len(images))]
    minimum = np.stack([np.array(nonzero).min(axis=1) for nonzero in nonzeros])
    maximum = np.stack([np.array(nonzero).max(axis=1) for nonzero in nonzeros])
    minmax = np.concatenate((minimum, maximum), axis=1)
    return minmax

def imshow(image, title=None, ncols=2, figsize=(15, 10), cmap='gray'):
    '''
    Visualize an image or a set of images using subplot

    Parameters
    ----------
    image : numpy.ndarray or list of numpy.ndarray
             Image array or the collection of image arrays to be displayed
    title : str ot list of str, optional, default: None
            The title or collection of titles corresponding to the images
    ncols: int, optional, default: 2
           Number of columns for subplot
    figsize: (float, float), optional, default: (12, 15)
             width, height in inches for single image

    Returns
    -------
    None   
    '''
    
    if not isinstance(image, list):
        plt.figure(figsize=figsize)
        plt.imshow(image, cmap=cmap)
        if title:
            plt.title(title, fontsize=30)
    else:
        plt.figure(figsize=figsize)
        # calculate the number of rows
        nrows = int(np.ceil((len(image)) / ncols))
        for i, img in enumerate(image):
            plt.subplot(nrows, ncols, i+1)
            plt.imshow(img, cmap=cmap)
            if title:
                plt.title(title[i], fontsize=50)
    plt.show(block=False)
    plt.pause(0.001)


def draw_points(points, shape):
    # Create plot
    image = np.zeros(shape)
    cols, rows = points.T
    image[rows, cols] = 255
    # imshow(image)
    return image

def draw_hull(hull_points, shape):
    image = np.zeros(shape)
    # cols, rows = points.T
    # image[rows, cols] = 255
    hull_points_reshaped = hull_points.reshape((-1, 1, 2))
    # Draw the convex hull
    cv2.polylines(image, [hull_points_reshaped], isClosed=True, color=255, thickness=2)
    # imshow(image)
    return image

def draw_points_mul(points, shape):
    # Create plot
    shape = shape.astype(int)
    image = np.zeros(shape)
    for point in points:
        point = point.astype(int)
        cols, rows = point.T
        image[rows, cols] = 255
    # imshow(image)
    return image

def image_resize(image, width=None, height=None, inter=cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    # import pdb; pdb.set_trace()
    
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image, 1

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        # dim = (height, int(w * r),)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        # dim = (int(h * r), width)
        dim = (width, int(h * r))

    resized = cv2.resize(image, dim, interpolation = inter)
    # AKS: To preserve binary image form
    _, resized = cv2.threshold(resized, 0.1, 1, cv2.THRESH_BINARY)
    
    # AKS: Ignore warnings of loss of precision and negative sign
    # with warnings.catch_warnings():
    #     warnings.simplefilter("ignore")
    #     # resize the image
    #     resized = img_as_bool(resize(image, dim, mode='reflect'))

    # return the resized image
    return resized, r


def createTraces(input_file, box, img, mode='contour'):
    trace_type = mode
    if trace_type.lower() == "contour" or trace_type.lower() == "raw": #takes one CC image

        contours = imageContours(img)
        # conImg = pts2img(np.concatenate(contours), np.array(img.shape) + 1)
        # imshow(conImg)
        # import pdb; pdb.set_trace()
        if len(contours) == 0:
            contours = imageContours_old(img)
            print(f'>> Image2trace (contour): {input_file}: empty contour list for bounding box')
            print('   '+ str(box) + ' in:', end="")
            print('   ' + os.path.basename( input_file))
        traces = traceReduction(contours, type=reduc_type,
                                factor=reduc_factor, dist=smooth_dist)
        # else:
        #     traces = traceReduction(contours, type="simple", factor=1)
        if len(traces) > 0:
            return traces
        else:
            print(f'>> Image2trace (contour): {input_file}: empty trace list for bounding box')
            print('   '+ str(box) + ' in:', end="")
            print('   ' + os.path.basename( input_file))
            return [np.array([np.array(img.shape)/2])]

        # plt.imshow(img, cmap='gray')
        # for contour in traces:
        #     plt.plot(contour[:,1], contour[:,0], linewidth=1)
        # plt.show()
        # exit(0)
        # print(traces)
        # exit(0)
   
    elif trace_type.lower() == "stroke":
        # MM convert single cc to strokes and trace points
        img = getSkeleton(img,skel_type)
        indices = np.where(img ==1)
        trace = list(zip(indices[0], indices[1]))
        
        if len(trace) > 0:
            return [np.array(trace)]
        else:
            print('>> Image2trace (stroke): empty trace list for bounding box')
            print('   '+ str(box) + ' in:')
            print('   ' + input_file)
            return [np.array([np.array(img.shape)/2])]
    else:
        raise ValueError
    


def traceReduction(traces, type="simple", factor=1, dist=0):
    traces_original = traces.copy()
    if type.lower() == "simple" or dist == 0:
        traces = [t[::factor] for t in traces]
    elif type.lower() == "smooth":
        traces = [np.concatenate((t[0:dist],[np.array(t[i-dist:i+dist]).mean(axis=0) \
            for i in range(dist, len(t)-dist, factor)],t[-dist:])) \
            for t in traces if len(t) > 2 * dist]
    else:
        raise ValueError
    if len(traces) == 0:
        return traces_original
    return traces

def imageContours_old(img):
    imb = np.pad(img, 1, 'constant', constant_values=0)
    contours = measure.find_contours(imb, 0)
    return [c.astype(int)-1 for c in contours if len(c) > 1]

def imageContours(img):
    imb = np.pad(img, 1, 'constant', constant_values=0)
    # Extract CCs with OpenCV (as 'outer' contours)
    ( _, thresh ) = cv2.threshold(imb * 255, CC_GRAY_THRESHOLD, 255, 0)
    thresh = np.array(thresh, dtype=np.uint8)

    contours, _ = cv2.findContours(
        thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE
    )
    return [np.flip(c.squeeze(), axis=1).astype(int) for c in contours if len(c) > 1]

def getSkeleton(img, type="Zhang"):
    from skimage.morphology import medial_axis
    from skimage.morphology import skeletonize

    """

    Args:
        img: 2D Binary image array, should contain a single connected component

    Returns: 2D binary image array, the connected component is preserved but reduced to single pixel thickness.

    """
    imb = np.pad(img, 1, mode='constant', constant_values=0)
    if type == "Medial":
        skel = medial_axis(imb)
    elif type == "Zhang":
        skel = skeletonize(imb)
    elif type == "Guo":
        skel = guo_hall_thinning(imb.astype(np.uint8))
    else:
        skel = skeletonize(imb)

    return skel[1:-1,1:-1]

def connectedComp(img):
    from skimage import measure
    labelImg, numLabels = measure.label(img, return_num=True, connectivity=2, background=0)
    return [np.argwhere(labelImg == label) for label in range(1,numLabels+1)]


def pts2img(pts, size, value=1):
    """

    Args:
        pts: array of [y,x] points with integer values

    Returns: greyscale image as 2D array where every point from pts has been set to 0 and all other pixels in the image
    are 255.

    """
    img = np.zeros(np.array(size)+1)
    for p in pts:
        img[p[0],p[1]] = value
    return img


def pts2traces(pts,img,maxDistance,eval_type, min_ratio):
    """

    Args:
        pts: list of [y,x] points to be turned into traces (sequence of points)
        img: Binary image as 2D array with values 0 or 1, 1 being symbol pixels. Original source of the points in pts.
        maxDistance: A double value represent the furthest distance allowed when connecting end points of a trace to another trace.
                     Used in the final step of creating the traces.
        config: Image configuration with processing and path parameters.

    Returns: List of traces (sequence of points)

    """
    if len(pts) == 0:
        return []

    if eval_type == "ContourBounds":
        contourImg = CCOps.doubleContourImg(img)
    else:
        contourImg = None
    pool = pts[:]
    traces = []
    curtrace = []
    center = np.array([np.array(pts)[:,0].mean(),np.array(pts)[:,1].mean()])
    start = np.argmax([np.linalg.norm(center - np.array(p2)) for p2 in pool])
    curtrace.append(pool.pop(start))
    frontDone = False
    backDone = False
    while len(pool) > 0:
        if not(frontDone):
            unchecked = pool[:]
            distances = [np.linalg.norm(curtrace[0]-pt) for pt in np.array(unchecked)]
            near = np.argmin(distances)
            dist = distances[near]
            while dist <= maxDistance:
                if evalSegment(curtrace[0], unchecked[near], img, eval_type, min_ratio, contourImg):
                    curtrace = [pool.pop(near)] + curtrace
                    break
                else:
                    distances[near] = np.inf
                    near = np.argmin(distances)
                    dist = distances[near]

            if np.isinf(dist) or np.linalg.norm(curtrace[0] - np.array(unchecked[near])) >= maxDistance:
                frontDone = True
        elif not(backDone):
            unchecked = pool[:]
            distances = [np.linalg.norm(curtrace[-1]-pt) for pt in np.array(unchecked)]
            near = np.argmin(distances)
            dist = distances[near]
            while dist <= maxDistance:
                if evalSegment(curtrace[0], unchecked[near], img, eval_type, min_ratio, contourImg):
                    curtrace.append(pool.pop(near))
                    break
                else:
                    distances[near] = np.inf
                    near = np.argmin(distances)
                    dist = distances[near]

            if np.isinf(dist) or np.linalg.norm(curtrace[-1] - np.array(unchecked[near])) >= maxDistance:
                backDone = True
        else:
            traces.append(np.array(curtrace[:]))
            curtrace = []
            start = np.argmax([np.linalg.norm(np.array(center) - np.array(p2)) for p2 in pool])
            curtrace.append(pool.pop(start))
            frontDone = False
            backDone = False
    if len(curtrace) != 0:
        traces.append(np.array(curtrace[:]))

    # Concatinate traces with close ends
    closed = []
    for i in range(len(traces)):
        t1 = traces[i]
        t1start = np.array(t1[0])
        t1end = np.array(t1[-1])
        for j in range(len(traces)):
            if traces[j] == []:
                continue
            t2 = traces[j]
            t2start = t2[0]
            t2end = t2[-1]
            if np.linalg.norm(t1start - t2start) <= maxDistance and evalSegment(t1start, t2start, img, eval_type, min_ratio, contourImg) and not(np.array_equal(t1start, t2start)):
                traces[i] = []
                traces[j] = np.concatenate((t2[::-1],t1))
            elif np.linalg.norm(t1start - t2end) <= maxDistance and evalSegment(t1start, t2end, img, eval_type, min_ratio, contourImg) and not(np.array_equal(t1start, t2end)):
                if np.array_equal(t1,t2) and len(t2) > 2:
                    traces[j] = []
                    closed.append(np.concatenate((t2,[t1start])))
                elif not(np.array_equal(t1,t2)):
                    traces[i] = []
                    traces[j] = np.concatenate((t2,t1))
            elif np.linalg.norm(t1end - t2start) <= maxDistance and evalSegment(t1end, t2start, img, eval_type, min_ratio, contourImg) and not(np.array_equal(t1end, t2start)):
                traces[i] = []
                traces[j] = np.concatenate((t1,t2))
            elif np.linalg.norm(t1end - t2end) <= maxDistance and evalSegment(t1end, t2end, img, eval_type, min_ratio, contourImg) and not(np.array_equal(t1end, t2end)):
                traces[i] = []
                traces[j] = np.concatenate((t1, t2[::-1]))

    traces = [t for t in traces if len(t) > 0]

    # connect end points to near points in other traces
    for i in range(len(traces)):
        tstart = np.array(traces[i][0])
        tend = np.array(traces[i][-1])
        possibles = [p for p in pts if p not in traces[i]]
        for c in closed:
            possibles += list(c)
        if len(possibles) == 0:
            continue
        nearStart = np.argmin([np.linalg.norm(tstart-pt) for pt in possibles])
        nearEnd = np.argmin([np.linalg.norm(tend-pt) for pt in possibles])
        startDist = np.linalg.norm(tstart - possibles[nearStart])
        endDist = np.linalg.norm(tend - possibles[nearEnd])
        if startDist <= maxDistance and evalSegment(tstart, possibles[nearStart], img, eval_type, min_ratio, contourImg):
            traces[i] = np.concatenate(([possibles[nearStart]], traces[i]))
        if endDist <= maxDistance and evalSegment(tend, possibles[nearEnd], img, eval_type, min_ratio, contourImg):
            traces[i] = np.concatenate((traces[i], [possibles[nearEnd]]))

    return closed+traces


def evalSegment(p1, p2, img, eval_type="BoxRatio", min_ratio=.6, con_img = None):
    """

    Args:
        p1: [y,x] point and start of a segment
        p2: [y,x] point and end of a segment
        img: binary image as 2D array with 0 and 1 values.

    Returns: The number of pixels in the image corresponding to a symbol in a
    bounding box around the two points divided by the area of the bounding box.

    """
    if eval_type == "BoxRatio":
        max_y = math.ceil(max(p1[0], p2[0]))
        max_x = math.ceil(max(p1[1], p2[1]))
        min_y = math.floor(min(p1[0], p2[0]))
        min_x = math.floor(min(p1[1], p2[1]))
        if max_x == min_x:
            max_x += 1
        if max_y == min_y:
            max_y += 1
        count = (img[min_y:max_y,min_x:max_x]).flatten().sum()
        area = (max_y-min_y) * (max_x-min_x)
        return count/area > min_ratio
    elif eval_type == "ContourBounds":
        if con_img is None:
            con_img = CCOps.contourImg(img)
        return CCOps.boundsCheck(p1, p2, con_img)
    elif eval_type == "SegmentRatio":
        ray = rayTrace(p1,p2)
        count = np.array([img[p[0], p[1]] for p in ray if p[0] < img.shape[0] and p[1] < img.shape[1]]).sum()
        return count/len(ray) > min_ratio


def rayTrace(pt1, pt2):
    """

    :param pt1: y,x values of starting point in line segment
    :param pt2: y,x values of ending point in line segment
    :return:
    """
    pt1 = np.array([round(i) for i in pt1]).astype(int)
    pt2 = np.array([round(i) for i in pt2]).astype(int)
    if pt1[0] == pt2[0]:
        low = min([pt1[1], pt2[1]])
        high = max([pt1[1], pt2[1]])
        return [[pt1[0], j] for j in range(low,high+1)]
    elif pt1[1] == pt2[1]:
        low = min([pt1[0], pt2[0]])
        high = max([pt1[0], pt2[0]])
        return [[i, pt1[1]] for i in range(low,high+1)]
    else:
        points = []
        xsign = int(np.sign(pt2[1]-pt1[1]))
        ysign = int(np.sign(pt2[0]-pt1[0]))
        deltaErr = float(pt1[0] - pt2[0])/float(pt1[1]-pt2[1])
        dsign = np.sign(deltaErr)
        error = 0.0
        y = pt1[0]
        for x in range(pt1[1], pt2[1]+xsign, xsign):
            points.append([y,x])
            error += deltaErr
            while abs(error) > 1:
                y += ysign
                points.append([y,x])
                error -= dsign
    return points


def skel2pts(skel, winSize):
    """

    Args:
        skel: Skeleton Image as 2D array with boolean 0 and 1 values

    Returns: List of points representing the skeleton.

    """
    points = []
    for r in range(0, skel.shape[0], winSize):
        for c in range(0, skel.shape[1], winSize):
            pts = np.where(skel[r:r+winSize,c:c+winSize] == 1)
            if len(pts[0]) > 0:
                points.append((pts[0].mean()+r, pts[1].mean()+c))
    return points

