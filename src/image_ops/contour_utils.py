from operator import itemgetter
import numpy as np
import pandas as pd
import math
import torch
import cv2
from torchvision.transforms.functional import center_crop
from torchvision.transforms import InterpolationMode as InterpolationMode
# RZ: Add debug operations
from src.utils.debug_fns import *
import src.utils.debug_fns as debug_fns
dTimer = DebugTimer("LGAP dataloader")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.
from  src.image_ops.image2traces import imshow

def translate_contours(contours):
    minY, minX = get_bbox_min_contours(contours)
    contours = [cnt - np.array([minX, minY]) for cnt in contours]
    maxY, maxX = get_bbox_max_contours(contours)
    return contours, np.array([maxY+1, maxX+1])

def get_bbox_min_contours(contours):
    minX, minY = np.concatenate(contours).min(axis=0)[0]
    return [minY, minX]

def get_bbox_max_contours(contours):
    maxX, maxY = np.concatenate(contours).max(axis=0)[0]
    return [maxY, maxX]

def get_bbox(contours):
    minY, minX = get_bbox_min_contours(contours)
    maxY, maxX = get_bbox_max_contours(contours)
    return np.array([minY, minX, maxY, maxX])

def get_cropped_shape(contours, crop=False):
    minY, minX, maxY, maxX = get_bbox(contours)
    shape = np.array([maxY + 1, maxX + 1])
    if crop:
        shape = shape - np.array([minY, minX])
    return shape

def contours2img(contours, shape=None, crop=False):
    if shape is not None:
        img = np.zeros(shape)
    else:
        img = empty_img_from_contours(contours, crop=crop)
    fill_contours(img, contours)
    return img

def empty_img_from_contours(contours, crop=False):
    shape = get_cropped_shape(contours, crop=crop)
    img = np.zeros(shape)
    return img

def fill_contours(pair_img, contour, minX=0, minY=0, thickness=1):
    if minX or minY:
        contour = [cnt - np.array([minX, minY]) for cnt in contour]
    cv2.drawContours(pair_img, contour, contourIdx=-1, color=1,
                     thickness=thickness)

def scale_contours(contours, resize_ratio, max_size_idx, min_size_idx, min_wh, 
                   new_size):
    new_shape = [0, 0]
    new_shape[max_size_idx] = new_size
    # AKS: Handle cases when height = width
    if max_size_idx == min_size_idx:
        new_shape[1] = min_wh
    new_shape[min_size_idx] = min_wh

    # Correctly adjust the scaling process to fit OpenCV's expected contour format
    scaled_contour_points = [np.round(contour * resize_ratio).astype(int)
                             for contour in contours]
    return scaled_contour_points, new_shape

def crop_resize_single_contours(padded_contours, size, new_size=64, thickness=1):
    ## 1 Resize longer dimension to given new_size (64) and other shorter
    # dimension to maintain aspect ratio
    max_sizes = size.max(axis=0)
    min_sizes = size.min(axis=0)
    resize_ratio = new_size / max_sizes[0]
    # Clip to 1 if resize ratio is 0
    min_wh = (resize_ratio *
              min_sizes[0]).ceil().to(torch.int32).clip(min=1)
    new_shape = [0, 0]
    new_shape[max_sizes[1].item()] = new_size
    # AKS: Handle cases when height = width
    if max_sizes[1].item() == min_sizes[1].item():
        new_shape[1] = min_wh.item()
    new_shape[min_sizes[1].item()] = min_wh.item()

    # minX, minY = contours_min_xy
    # minY = int((minY * resize_ratio.item()).ceil())
    # minX = int((minX * resize_ratio.item()).ceil())
    context = np.zeros(new_shape)

    contours = [np.round(contour * resize_ratio.item()).astype(int) for contour in padded_contours]
    fill_contours(context, contours, thickness=thickness)

    # # 2 Pad and center crop along the shorter dim to create square (64 x 64)
    context = torch.from_numpy(context)
    img = center_crop(context, new_size)
    return img

def create_each_feature_backup(contour_indices, contours, new_size=64):
    all_contours = []
    for idx in contour_indices:
        all_contours.extend(contours[idx])

    all_contours, contour_size = translate_contours(all_contours)

    max_size = contour_size.max()
    min_size = contour_size.min()

    max_size_idx = contour_size.argmax()
    min_size_idx = 1 - max_size_idx

    resize_ratio = new_size / max_size
    min_wh = max(1, math.ceil(resize_ratio * min_size))
    img_scaled = scale_contours(all_contours, resize_ratio, max_size_idx, min_size_idx,
                                min_wh, new_size, thickness=-1)
    img_cropped = center_crop(img_scaled, new_size)
    return img_cropped

def get_cropped_pairs_contours(edge, contours_new, new_size=64):
    p, c = edge
    contours = contours_new[p] + contours_new[c]
    cnt_min = np.concatenate(contours).min(0)[0]

    # cnt_max = np.concatenate(contours).max(0)[0]
    # bbox_size = (cnt_max - cnt_min + 1)[[1, 0]]

    minX, minY = cnt_min
    contours = [cnt - np.array([minX, minY]) for cnt in contours]
    # pair_img = np.zeros(bbox_size)
    # fill_contours(pair_img, contours, thickness=-1)

    max_point = np.concatenate(contours).max(0)[0]
    min_point = np.concatenate(contours).min(0)[0]
    
    contour_size = (max_point - min_point + 1)[[1, 0]]
    
    max_size = contour_size.max()
    min_size = contour_size.min()

    max_size_idx = contour_size.argmax()
    min_size_idx = 1 - max_size_idx

    resize_ratio = new_size / max_size
    min_wh = max(1, math.ceil(resize_ratio * min_size))
    pair_img_scaled = scale_contours(contours, resize_ratio, max_size_idx, min_size_idx,
                                     min_wh, new_size, thickness=-1)
    pair_cropped = center_crop(pair_img_scaled, new_size)
    return pair_cropped

def get_each_positional_encodings(prim_bbox, context_sizes, image_shape):
    height, width = image_shape
    pos_enc = np.zeros(10)
    pos_enc[0] = prim_bbox[1] / width
    # pos_enc[2] = sizes[1] / formula_widths.item()
    pos_enc[2] = prim_bbox[3] / width
    pos_enc[4] = prim_bbox[1] / context_sizes[1]
    # pos_enc[6] = sizes[1] / context_sizes[1]
    pos_enc[6] = prim_bbox[3] / context_sizes[1]
    pos_enc[8] = context_sizes[1] / width

    pos_enc[1] = prim_bbox[0] / height
    # pos_enc[3] = sizes[0] / 64
    pos_enc[3] = prim_bbox[2] / height
    pos_enc[5] = prim_bbox[0] / context_sizes[0]
    # pos_enc[7] = sizes[0] / context_sizes[0]
    pos_enc[7] = prim_bbox[2] / context_sizes[0]
    pos_enc[9] = context_sizes[0] / height
    return pos_enc

def get_positional_encodings(prim_bbox, context_sizes, image_shape):
    height, width = image_shape
    pos_enc = np.zeros((len(prim_bbox), 10))
    pos_enc[:, 0] = prim_bbox[:, 1] / width
    # pos_enc[:, 2] = sizes[:, 1] / formula_widths.item()
    pos_enc[:, 2] = prim_bbox[:, 3] / width
    pos_enc[:, 4] = prim_bbox[:, 1] / context_sizes[:, 1]
    # pos_enc[:, 6] = sizes[:, 1] / context_sizes[:, 1]
    pos_enc[:, 6] = prim_bbox[:, 3] / context_sizes[:, 1]
    pos_enc[:, 8] = context_sizes[:, 1] / width

    pos_enc[:, 1] = prim_bbox[:, 0] / height
    # pos_enc[:, 3] = sizes[:, 0] / 64
    pos_enc[:, 3] = prim_bbox[:, 2] / height
    pos_enc[:, 5] = prim_bbox[:, 0] / context_sizes[:, 0]
    # pos_enc[:, 7] = sizes[:, 0] / context_sizes[:, 0]
    pos_enc[:, 7] = prim_bbox[:, 2] / context_sizes[:, 0]
    pos_enc[:, 9] = context_sizes[:, 0] / height
    return pos_enc

def create_features_from_contours(image, boxes, new_size=64, contours=None,
                                 merge_groups={}, all_edges=None, los_edges=None,
                                  k=4, sorted_distances=None, thickness=-1,
                                  correct_contours=True):
    dTimer.qcheck("Initial")
    # sort CC boxes from left-right, top-down
    boxes = sorted(boxes.items(), key=lambda x:x[1][0])
    boxes = sorted(boxes, key=lambda x:x[1][1])
    prim_bbox = np.array(list(map(itemgetter(1), boxes)))
    id_map = {new:old for new, (old, _) in enumerate(boxes)}
    if correct_contours:
        contours_new = {new:contours[old] for new, old in id_map.items()}
    else:
        contours_new = contours

    # PRIMITIVE FEATURES
    prim_feat_list = []
    prim_context_feat_list = []
    context_sizes = []

    for prim_idx, contours in contours_new.items():
        # Add indices of merged primitive groups
        prim_indices = [prim_idx]
        if prim_idx in merge_groups:
            prim_indices.extend(list(merge_groups[prim_idx]))

        prim_cropped, _ = create_each_feature_contours(prim_indices, contours=contours_new,
                                               new_size=new_size, thickness=thickness)
        prim_feat_list.append(prim_cropped)
        ### CONTEXT FEATURES
        # K nearest primitives, plus the primitives itself
        knn_idx = [*prim_indices, *list(map(itemgetter(1), los_edges[prim_idx][:k]))]
        
        prim_context_feat, context_size = create_each_feature_contours(knn_idx, prim_indices, contours_new, 
                                                  new_size=new_size, thickness=thickness)
        context_sizes.append(context_size)
        prim_context_feat_list.append(prim_context_feat)

    context_sizes = np.array(context_sizes)
    pos_enc = get_positional_encodings(prim_bbox, context_sizes, image.shape)
    
    prim_feat_batch = torch.stack(prim_feat_list).clip(max=1)
    prim_context_feat_batch = torch.stack(prim_context_feat_list).clip(max=1)
    dTimer.qcheck("Prim features")
    
    # PAIR FEATURES
    pair_feat_list = []
    pair_context_feat_list = []
    knn_indices = []
    for edge in all_edges:
        # Add indices of merged primitive groups
        prim_indices = list(edge)
        for prim_idx in edge:
            if prim_idx in merge_groups:
                prim_indices.extend(list(merge_groups[prim_idx]))

        pair_feat, _ = create_each_feature_contours(prim_indices, contours=contours_new, 
                                                 new_size=new_size, thickness=thickness)
        pair_feat_list.append(pair_feat)

        ### CONTEXT FEATURES
        # K nearest primitives, plus the pair itself
        knn_idx, knn_idx_with_prims = get_knn_prims_edge(edge, prim_indices, los_edges, sorted_distances, k)
        pair_context_feat, _ = create_each_feature_contours(knn_idx_with_prims, prim_indices, contours_new, 
                                                      new_size=new_size, thickness=thickness)
        pair_context_feat_list.append(pair_context_feat)
        knn_indices.append(knn_idx)

    pair_feat_batch = torch.stack(pair_feat_list).clip(max=1)
    pair_context_feat_batch = torch.stack(pair_context_feat_list).clip(max=1)
    dTimer.qcheck("Pair features")
    # print(dTimer)
    return prim_feat_batch, prim_context_feat_batch, pair_feat_batch, \
        pair_context_feat_batch, pos_enc, contours_new, knn_indices

def get_knn_prims_edge(edge, prim_indices, los_edges, sorted_distances, k):
    p, c = edge
    knn_parent_idx = list(map(itemgetter(1), los_edges[p]))
    knn_child_idx = list(map(itemgetter(1), los_edges[c]))
    knn_idx = np.array([*knn_parent_idx, *knn_child_idx])
    parent_distances = sorted_distances[p]
    child_distances = sorted_distances[c]
    all_distances = np.array([*parent_distances, *child_distances])
    knn_idx_indices = all_distances.argsort()
    knn_idx = knn_idx[knn_idx_indices]
    # Remove the query from neighbors
    knn_idx = knn_idx [~np.isin(knn_idx, list(edge))]
    # knn_idx = np.append(knn_idx, edge)
    # Used pandas since pandas.unique() preserves original order
    knn_idx = pd.unique(knn_idx)[:k]
    # extra step for merged primitives
    knn_idx_with_prims = np.append(knn_idx, prim_indices)
    knn_idx_with_prims = set(pd.unique(knn_idx_with_prims))
    return set(knn_idx), knn_idx_with_prims

def create_features_from_contours_old(image, boxes, new_size=64, contours=None,
                     merge_edges=[], all_edges=None,
                     los_edges=None):
    dTimer.qcheck("Initial")
    image = torch.FloatTensor(image)

    # sort CC boxes from left-right, top-down
    boxes = sorted(boxes.items(), key=lambda x:x[1][0])
    boxes = sorted(boxes, key=lambda x:x[1][1])
    id_map = {new:old for new, (old, _) in enumerate(boxes)}
    contours_new = {new:contours[old] for new, old in id_map.items()}

    prim_feat_list = []
    prim_context_feat_list = []
    for i, contours in contours_new.items():
        contours, contours_shape = translate_contours(contours)
        # img = np.zeros(contours_shape)
        # fill_contours(img, contours, thickness=1)
        # imshow(img)

        max_point = np.concatenate(contours).max(0)[0]
        min_point = np.concatenate(contours).min(0)[0]
        
        contour_size = (max_point - min_point + 1)[[1, 0]]
        
        max_size = contour_size.max()
        min_size = contour_size.min()

        max_size_idx = contour_size.argmax()
        min_size_idx = 1 - max_size_idx

        resize_ratio = new_size / max_size
        min_wh = max(1, math.ceil(resize_ratio * min_size))
        prim_img_scaled = scale_contours(contours, resize_ratio, max_size_idx, min_size_idx,
                                         min_wh, new_size, thickness=-1)

        prim_cropped = center_crop(prim_img_scaled, new_size)
        prim_feat_list.append(prim_cropped)

        ### CONTEXT FEATURES
        prim_context_feat = get_context_feat_contours_old([i], los_edges[i], contours_new, 
                                                      new_size=new_size, k=4, thickness=1)
        prim_context_feat_list.append(prim_context_feat)

    prim_feat_batch = torch.stack(prim_feat_list).clip(max=1)
    prim_context_feat_batch = torch.stack(prim_context_feat_list).clip(max=1)
    dTimer.qcheck("Prim features")
    
    pair_feat_list = []
    pair_context_feat_list = []
    for edge in all_edges:
        p, c = edge
        # pair_feat = get_cropped_pairs(edge, prim_bbox, contours_new,
        #                             lg_mode, new_size, interpolation=interpolation)
        pair_feat = get_cropped_pairs_contours(edge, contours_new, new_size=new_size)
        pair_feat_list.append(pair_feat)

        los_edge = los_edges[p]
        # pair_context_feat = get_context_feat([p, c], image, los_edge, prim_bbox,
        #                                      contours_new, lg_mode,
        #                                      new_size=new_size, k=4,
        #                                      interpolation=interpolation)
        pair_context_feat = get_context_feat_contours_old([p, c], los_edge, 
                                             contours_new, new_size=new_size, k=4, thickness=1)
        pair_context_feat_list.append(pair_context_feat)

    pair_feat_batch = torch.stack(pair_feat_list).clip(max=1)

    pair_context_feat_batch = torch.stack(pair_context_feat_list).clip(max=1)
    dTimer.qcheck("Pair features")
    print(dTimer)
    
    imshow([x for x in prim_feat_batch], ncols=4)
    imshow([x for x in prim_context_feat_batch], ncols=4)
    imshow([x for x in pair_feat_batch[:16]], ncols=4)
    imshow([x for x in pair_context_feat_batch[:16]], ncols=4)
    import pdb; pdb.set_trace()
    return []
    
    

def pad_contours(contour_points, padding_vector):

    # Apply padding
    padded_contours = [contour + np.array([padding_vector[0], padding_vector[2]]) 
                       for contour in contour_points]
    
    # Find minimum x and y after padding
    min_x, min_y = np.concatenate(padded_contours).min(axis=0)[0]
    max_x, max_y = np.concatenate(padded_contours).max(axis=0)[0]
    
    # Translate to ensure no negative coordinates
    if min_x < 0 or min_y < 0:
        translation_vector = np.array([-min(min_x, 0), -min(min_y, 0)])
        adjusted_contours = [contour + translation_vector for contour in padded_contours]
    else:
        adjusted_contours = padded_contours

    adjusted_max_x = max_x + padding_vector[1] + 1
    adjusted_max_y = max_y + padding_vector[3] + 1
    output_shape = np.array([adjusted_max_y, adjusted_max_x])
    return adjusted_contours, output_shape


def create_each_feature_contours(contour_indices, center_contour_idx=[],
                              contours=[], new_size=64, thickness=1):
    """
    Pad and scale contours first and then fill them directly onto the output
    context image of given output size
    """
    # neighbors = np.zeros_like(img)
    all_contours = []
    for idx in contour_indices:
        all_contours.extend(contours[idx])

    bbox = get_bbox(all_contours)
    # img = contours2img(all_contours)
    # if center_contour_idx is not None:
    #     imshow(img)
    #     import pdb; pdb.set_trace()
    
    all_contours, contour_size = translate_contours(all_contours)

    # Cetnter around the given contour (padding)
    if len(center_contour_idx):
        center_bboxes = []
        for center_idx in center_contour_idx:
            center_bboxes.append(get_bbox(contours[center_idx]))
        center_bboxes = np.array(center_bboxes)
        minY, minX = center_bboxes[:,:2].min(0)
        maxY, maxX = center_bboxes[:,2:].max(0)

        mean_stk = np.round(np.stack([(minY + maxY) * 0.5, (minX + maxX) * 0.5])).astype(int)
        all_min, all_max = bbox[:2], bbox[2:]
        
        diff = np.concatenate((mean_stk - all_min, all_max - mean_stk))
        diff_tb = diff[[0, 2]]
        diff_lr = diff[[1, 3]]

        tb_pad_idx = diff_tb.argmin()
        lr_pad_idx = diff_lr.argmin()

        pad = np.abs(diff[:2] - diff[2:])
        pad_tb = pad[0]
        pad_lr = pad[1]

        pad_dim = [0, 0, 0, 0]
        pad_dim[lr_pad_idx] = pad_lr
        pad_dim[tb_pad_idx + 2] = pad_tb

        padded_contours, contour_size = pad_contours(all_contours, pad_dim)
        all_contours = padded_contours
        # img2 = contours2img(padded_contours, contour_size)

    max_size = contour_size.max()
    min_size = contour_size.min()

    max_size_idx = contour_size.argmax()
    min_size_idx = 1 - max_size_idx

    resize_ratio = new_size / max_size
    min_wh = max(1, math.ceil(resize_ratio * min_size))

    scaled_contours, scaled_shape = scale_contours(all_contours, resize_ratio, max_size_idx, 
                                                   min_size_idx, min_wh, new_size)
    # Reinitialize the blank image
    img_scaled = np.zeros(scaled_shape, dtype=np.float32)
    # Draw the scaled contours correctly this time
    fill_contours(img_scaled, scaled_contours, thickness=thickness)
    img_scaled = torch.from_numpy(img_scaled)

    img_cropped = center_crop(img_scaled, new_size)
    # imshow([img2, cropped_los_img, img_cropped])
    # import pdb; pdb.set_trace()
    return img_cropped, contour_size

def get_context_feat_contours_old(prim_idx, los_edges, contours_new, 
                     new_size=64, k=4, thickness=1):
    """
    Pad and scale contours first and then fill them directly onto the output
    context image of given output size
    """
    # neighbors = np.zeros_like(img)
    all_contours = []
    for idx in prim_idx:
        all_contours.extend(contours_new[idx])
        # fill_contours(neighbors, contours_new[idx])
    # if len(prim_idx) > 1:
    #     import pdb; pdb.set_trace()
        
    for count, (_, c) in enumerate(los_edges):
        if count < k:
            # minY, minX, maxY, maxX = prim_bbox[c]
            # fill_contours(neighbors, contours_new[c])
            all_contours.extend(contours_new[c])

    
    all_contours_flat = np.concatenate(all_contours).squeeze(1)
    nei_min = all_contours_flat.min(axis=0)
    all_contours = [contour - nei_min for contour in all_contours]

    nei_min = torch.from_numpy(nei_min[[1,0]])
    nei_max = torch.from_numpy(all_contours_flat.max(axis=0)[[1,0]])

    minY, minX, maxY, maxX = torch.FloatTensor(get_bbox(contours_new[prim_idx[0]]))
    mean_stk = torch.stack([(minY + maxY) * 0.5, (minX + maxX) * 0.5]).round().int()
    
    diff = torch.cat((mean_stk - nei_min, nei_max - mean_stk))
    diff_tb = torch.index_select(diff, 0, torch.tensor([0,2]))
    diff_lr = torch.index_select(diff, 0, torch.tensor([1,3]))
    tb_pad_idx = diff_tb.argmin()
    lr_pad_idx = diff_lr.argmin()

    pad = (diff[:2] - diff[2:]).abs()
    pad_tb = pad[0]
    pad_lr = pad[1]

    pad_dim = [0, 0, 0, 0]
    pad_dim[lr_pad_idx] = pad_lr.item()
    pad_dim[tb_pad_idx + 2] = pad_tb.item()

    # pad_f = nn.ZeroPad2d((pad_dim))
    # y1, x1 = nei_min
    # y2, x2 = nei_max
    # neighbors = torch.from_numpy(neighbors)
    # los_img = neighbors[y1: y2+1, x1: x2+1]
    # padded_los_img = pad_f(los_img)
    # padded_los_img_shape = padded_los_img.shape
    # contours_max = np.concatenate(all_contours).max(axis=0)[0]
    # new_shape = (contours_max + 1)[[1,0]]
    # los_img_new = np.zeros(new_shape)
    # fill_contours(los_img_new, all_contours)
    # imshow([los_img, los_img_new])

    padded_contours, new_shape = pad_contours(all_contours, pad_dim)
    # new_img = np.zeros(new_shape)
    # contours_min = np.concatenate(padded_contours).min(axis=0)[0]
    # contours_max = np.concatenate(padded_contours).max(axis=0)[0]
    # fill_contours(new_img, padded_contours)
    # imshow([padded_los_img, new_img])
    # import pdb; pdb.set_trace()

    cropped_los_img = crop_resize_single_contours(padded_contours, torch.tensor(new_shape), 
                                                  new_size=new_size, thickness=thickness)

    # los_img_shape = los_img.shape
    # padded_los_img_shapes.append(padded_los_img_shape)
    # cropped_los_img = crop_resize_single(padded_los_img, torch.tensor(padded_los_img_shape),
    #                                       new_size=new_size, interpolation=interpolation)

    return cropped_los_img
    
    # cropped_los_imgs.append(cropped_los_img)
