import os
from qdgga_settings import DATA_DIR
from src.datasets.math_symbol import check_for_merge

labels = ['fractionalLine','minus','overline','hyphen', 'ldots', 'cdots',
          'dot','cdot','period']

def check_for_merge__(label):
    if label in ['fractionalLine','minus','overline','hyphen']:
        label='minus'
    if label in ['ldots', 'cdots']:
        label= 'ldots'
    if label in ['dot','cdot','period']:
        label= 'dot'
    return label   

if __name__ == "__main__":
    INFTY_DIR = os.path.join(DATA_DIR, "InftyMCCDB-2")
    if not os.path.exists(os.path.join(INFTY_DIR, "test_merged_class")):
        os.makedirs(os.path.join(INFTY_DIR, "test_merged_class"))
    for file in os.listdir(os.path.join(INFTY_DIR, "test")):
        with open(os.path.join(INFTY_DIR, "test", file), "r") as f:
            lg = f.read()
            for label in labels:
                lg = lg.replace(label, check_for_merge(label))
        with open(os.path.join(INFTY_DIR, "test_merged_class", file), "w") as f:
            f.write(lg)
        
    if not os.path.exists(os.path.join(INFTY_DIR, "train_merged_class")):
        os.makedirs(os.path.join(INFTY_DIR, "train_merged_class"))
    for file in os.listdir(os.path.join(INFTY_DIR, "train")):
        with open(os.path.join(INFTY_DIR, "train", file), "r") as f:
            lg = f.read()
            for label in labels:
                lg = lg.replace(label, check_for_merge(label))
        with open(os.path.join(INFTY_DIR, "train_merged_class", file), "w") as f:
            f.write(lg)
