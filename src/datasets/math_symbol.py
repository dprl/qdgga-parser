import os.path as path
import numpy as np
import xml.etree.ElementTree as ET
from PIL import Image
from collections import Counter
from collections import defaultdict
from ..features.preprocessing import fixTrace, removeDuplicatedPoints
import itertools
from ..image_ops.image2traces import createTraces, getSkeleton, imshow, image_resize

SYMBOLS = ['!','(',')','+',',','-','.','/','0','1','2','3','4','5','6','7','8','9','=',
 'A','B','C','E','F','G','H','I','L','M','N','P','R','S','T','V','X','Y','[',
 '\\Delta','\\alpha','\\beta','\\cos','\\div','\\exists','\\forall','\\gamma',
 '\\geq','\\gt','\\in','\\infty','\\int','\\lambda','\\ldots','\\leq','\\lim',
 '\\log','\\lt','\\mu','\\neq','\\phi','\\pi','\\pm','\\prime','\\rightarrow',
 '\\sigma','\\sin','\\sqrt','\\sum','\\tan','\\theta','\\times','\\{','\\}',']',
 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s',
 't','u','v','w','x','y','z','|', '_']

# Line-of-Sight (LOS) pairs (excluding merge)
RELATIONS =['Right','Sup','Sub','Above','Below', 'Inside','NoRelation']

CHEM_RELATIONS = [ 'NoRelation', 'CONNECTED']

INFRELATIONS=['RSUP', 'HORIZONTAL', 'RSUB', 'UNDER', 'LSUB', 'UPPER', 'LSUP','NoRelation','PUNC']

INFSYMBOLS=['A', 'Acirc', 'Ahat', 'B', 'BigLeftPar', 'BigRightPar', 'C', 'D',
        'Delta', 'DoubleBeginQuartation', 'E', 'F', 'G', 'Gamma', 'H', 'I',
        'J', 'K', 'L', 'Lambda', 'LeftPar', 'Leftrightarrow', 'M',
        'MiddleLeftPar', 'MiddleRightPar', 'N', 'NULL', 'O', 'Omega', 'Oslash',
        'P', 'Phi', 'Pi', 'Psi', 'Q', 'R', 'Reject1', 'Reject2', 'Reject3',
        'RightPar', 'Rightarrow', 'S', 'Sigma', 'SingleEndQuartation',
        'Subset', 'T', 'Theta', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'alpha',
        'approx', 'ast', 'b', 'backslash', 'beta', 'bigcap', 'bigcup',
        'bigoplus', 'bigotimes', 'c', 'cap', 'check', 'chi', 'circ', 'colon',
        'comma', 'cong', 'cup', 'd', 'delta', 'dot', 'doubleprime',
        'downarrow', 'e', 'eight', 'emptyset', 'epsilon', 'equal', 'equiv',
        'eta', 'exclamation', 'exists', 'f', 'fi', 'five', 'forall', 'four',
        'g', 'gamma', 'geq', 'geqq', 'gg', 'greater', 'h', 'hat',
        'hookrightarrow', 'i', 'ihat', 'iint', 'in', 'infty', 'int', 'iota',
        'j', 'k', 'kappa', 'l', 'lambda', 'langle', 'lceil', 'ldots',
        'leftarrow', 'leq', 'leqq', 'less', 'll', 'longHyphen', 'm', 'mapsto',
        'minus', 'mu', 'n', 'nabla', 'ni', 'nine', 'nmid', 'notequal',
        'notequiv', 'notin', 'notsimeq', 'notsubset', 'nu', 'o', 'oint',
        'omega', 'ominus', 'one', 'oplus', 'otimes', 'p', 'partial', 'perp',
        'phi', 'pi', 'plus', 'pm', 'prec', 'prime', 'prod', 'psi', 'q',
        'question', 'r', 'rangle', 'rceil', 'rho', 'rightarrow', 's',
        'semicolon', 'seven', 'sharp', 'sigma', 'sim', 'simeq', 'six', 'slash',
        'sqrt', 'subset', 'subseteq', 'subsetnoteqq', 'succ', 'sum', 'supset',
        'supseteq', 'supsetnoteqq', 't', 'tau', 'theta', 'three', 'tilde',
        'times', 'triangle', 'tripleprime', 'ttilde', 'two', 'u', 'underbrace',
        'underline', 'upsilon', 'v', 'varphi', 'vartheta', 'vec', 'vert', 'w',
        'wedge', 'x', 'xi', 'y', 'z', 'zero', 'zeta']


def extract_cc(image, box):
    return image[box[0]:box[2]+1,box[1]:box[3]+1]

def processTrace(raw_trace,offset):
    traces=[]
    for ( tid, tp ) in enumerate(raw_trace):
        trace = [t[::-1] + np.array(offset[::-1]) for t in tp]
        traces.append(trace)
    return traces
        
#working from points in traces genearted from CC images 
def Box2Traces(input_file, boxes, instances, mode='contour', \
        in_img = None):

    traces=[]
    for objectId in instances:
        # Note the row/column, vs. X/Y ordering of coordinates
        box = boxes[ objectId ]
        (minY, minX, maxY, maxX ) = box

        offset = ( minY, minX )

        image = None
        image = in_img
        
        #objImage = image[box[0]:box[2]+1,box[1]:box[3]+1]
        # Use CC/Symbol bounding box to crop region image.
        objImg = image[ minY : maxY + 1, minX : maxX + 1 ]
        # import pdb; pdb.set_trace()
        # imshow(objImg)

        # Traces created as numpy arrays.
        trace = createTraces(input_file, box, objImg, mode)
        traces.append(processTrace(trace, offset))

    return traces



def get_image_params( traces, height=64, padding=2, width_mul=16, \
        ind_sym=False, data='CROHME'):
    # RZ: file is not used by this 'image params' - only traces and other
    # passed constants.

    #print(">> get_image_params for " + fileName + "  traces: " \
    #        + str(len(traces)))

    if ind_sym: 
        points= np.array(traces)
    elif data=='infty' or data=='chem':
        points=[]
        for trace in traces:
            for tr in trace: points.extend(tr)
        points=np.array(points)
    else: 
        points = np.array(sum(sum(traces, []), [])) #crohme dataset
    
    # RZ: Debug -- if 'points' is empty, return.
    if len(points) < 1:
        return ( (-1,-1), lambda x: x)

    points = points[:, ::-1]
    st = points.min(0)
    ed = points.max(0)
    b0 = st

    if (ed[0] - st[0])==0 and (data=='infty' or data == 'chem'): 
        k=1  #if cc is made of one point 
    else: 
        k = (height - 2 * padding) / (ed[0] - st[0])
    
    points -= b0
    points =points*k
    points = np.round(points).astype(int)
    st = points.min(0)
    ed = points.max(0)

    (h, w) = ed - st
    width = np.ceil((w + padding * 2) / width_mul).astype(int) * width_mul
    b1 = (((height, width) - (ed - st)) / 2).astype(int)
    rescale = lambda x: np.round((x - b0) * k).astype(int) + b1
    image_size = (height, width)

    return ( image_size, rescale )
  

def check_for_merge(label):
    if label in ['fractionalLine','minus','overline','hyphen']:
        label='minus'
    if label in ['ldots', 'cdots']:
        label= 'ldots'
    if label in ['dot','cdot','period']:
        label= 'dot'
    return label   


def get_relation_labels(lg_file, level='stk', symtags2instances=None,
                        task='train', MergeEdge=True):
    symbols = {}
    segments = []
    symrel={}
    stkrel={}

    # RZ: Test if file exists (lg path that doesn't exst, return empty
    # relation list).
    if not lg_file or not path.exists(lg_file):
        if task == "train":
            print(f">> Warning: lg file {lg_file} not found for relations ground truth")
        return symrel

    # ASSUMES OR format, read objects and relations.
    symLg = False
    with open(lg_file) as f:
        for line in f.readlines():
            items = line.strip().split(', ')
            if line.startswith('O'):
                symbol = items[1]
                # segment = list(map(int, items[4:]))
                symbols[symbol]= items[4:]
                if items[4].startswith('O'):
                    symLg = True
                # segments.append(segment)
            if line.startswith('R'):
                symrel[(items[1],items[2])]=items[3]

    if level=='sym':
        return symrel

    else:
        # Generate stroke level edge relations:
        for (pair, rel) in symrel.items():
            if symLg:
                parent_stks=symtags2instances[pair[0]]
                child_stks=symtags2instances[pair[1]]
            else:
                parent_stks=symbols[pair[0]]
                child_stks=symbols[pair[1]]
            for stk1 in parent_stks:
                for stk2  in child_stks:
                    stkrel[stk1,stk2]=rel

        if MergeEdge:
            # Add stroke relations
            if symLg:
                strokes = symtags2instances.values()
            else:
                strokes = symbols.values()
            for stks in strokes:
                if len(stks)>1:              
                    for pair in list(itertools.permutations(stks, 2)):
                        stkrel[pair]='i'
        return stkrel


def interpolate(trace):
    new_points = []
    for i in range(1, len(trace)):
        p1, p0 = trace[i], trace[i - 1]
        n = np.abs(p1 - p0).max()
        if n > 0:
            d = (p1 - p0) / n
            for j in range(1, n):
                new_points.append(p0 + d * j)
    if new_points:
        new_points = np.stack(new_points)
        new_points = np.round(new_points).astype(int)
        return np.concatenate((trace, new_points))
    else: return trace


def add_trace(image, trace, rescale, value=1.0, mode='contour'):
    trace = np.array(trace)[:, ::-1]
    trace = rescale(trace)
    if mode == 'stroke': #no interpolate for Infty skeleton mode
        image[trace[:, 0], trace[:, 1], ...] = value
    else:   
        trace = interpolate(trace) #crohme and infty contours here
        image[trace[:, 0], trace[:, 1], ...] = value
    
