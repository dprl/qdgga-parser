'''
Main DataLoader for Visual Parser (Math+Chem)
Document and Pattern Recognition Lab, RIT
Creator: Ayush Kumar Shah
Version 2: Abhisek Dey
Changes: Multi-Worker Batching + Queuing through iterable stream
'''

# RZ: Add debug operations
from src.utils.debug_fns import *
import src.utils.debug_fns as debug_fns
dTimer = DebugTimer("LGAP dataloader")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

import joblib
from collections import defaultdict
from logging import raiseExceptions
import os
from itertools import chain
import torch
import math
import numpy as np
import cv2
import random
import os.path as path
import matplotlib.pyplot as plt
from torch.utils import data
from torch.utils.data import IterableDataset, Dataset
from torch import distributed as dist
from protables.ProTables import tsv2pro, lg2pro, inkml2pro

from src.utils.utils import updateEdgeLabel, get_ccs
from src.utils.utils import updateEdgeLabel, get_ccs
from src.datasets.math_symbol import RELATIONS, INFRELATIONS, CHEM_RELATIONS,\
        get_image_params, add_trace, Box2Traces, check_for_merge
from src.features.Graph_construction import  GraphConstruction
from src.features.geometric_feature_extractor import getGeofeat
from lgap_settings import ERROR_FILES_CROHME, ERROR_FILES_INFTY
from src.image_ops.image2traces import image2strokes, get_minmax, imshow, scale_primitives
from src.image_ops.contour_utils import create_features_from_contours, create_features_from_contours_old
from src.datasets.classes.DatasetItem import NodeItem, EdgeItem
from src.datasets.classes.Formula import Formula
from glob import glob


from joblib import Parallel, delayed

import random
from operator import itemgetter
from itertools import repeat

# from src.utils.dprl_map_reduce import map_reduce, combine_lists

def unwrap_self(arg, **kwarg):
    return MathSymbolDataset.create_features(*arg, **kwarg)

class MathSymbolDataset(Dataset):

    ################################################################
    # Built-ins (constructor, len() )
    ################################################################
    def __init__(self, dataset='', level="stk", find_punc=False, data_dir="data/train",
                 Img_list= "", lg_dir=None, graph="los", balance_edges=False,
                 symbol_height=64, image_primitive=False, traces_only=False,
                 symbol_table=None, rel_table=None, node_seg=False,
                 prune_seg=False, prune_rel=False, reduced_rel=False,
                 task='train', symContext=False, allAdj=False, symCrop=False,
                 INFmode='contour', geoFeat=False, format="tsv",
                 tsv_dir=None, dot_dir=None, inputRegionTypes=['c','S'],
                 oneRelLimit=True, lg_mode="contours", inputs="primitives", 
                 prune_num_NN=6, context_num_NN=4, extract_mst=True, gpu_memory_threshold=0.0155, 
                 shuffle=False, max_feat_size=64, rank=0, num_gpus=1,
                 model_dir="outputs/run/infty_contour/",
                 remove_opposite_edges=True, last_epoch=0,
                 feat_parallel=True, oversample_nodes=True):

        super(MathSymbolDataset, self).__init__()
        
        # RZ: Removed trainMST and associated code.
        
        # Revmoe self.gt_tree and associated code.
        # RZ: Removed self.balance_edges=balance_edges
        # RZ: Readded self.balance_edges=balance_edges
        # RZ: Removed max_strokes attribute.
        # RZ: Removing use_gt -- this belongs in an external fn/elsewhere.
        # RZ: Removed CROHME dataset specific options.

        self.feature_generation_parallel = feat_parallel

        self.task=task
        self.dataset = dataset

        self.GRAPH=graph
        self.node_seg=node_seg # to check if seg represented by node or edges
        self.prune_seg = prune_seg # to prune the segmentation edge input space
        self.prune_rel = prune_rel # to prune the relationship edge input space
        self.reduced_rel = reduced_rel # to prune the relationship edge input space
        self.level=level
        self.find_punc = find_punc

        self.symbol_height = symbol_height
        self.image_primitive = image_primitive #if true no primitive attention
        self.traces_only = traces_only # if True no label will be returned
        self.balance_edges=balance_edges
        
        self.expr_graph={}
        self.symContext=symContext
        self.allAdj=allAdj
        self.symCrop=symCrop
        
        self.mode=INFmode
        self.geoFeat=geoFeat
        self.data_dir = data_dir
        self.format = format

        # AD Mod: Additional Parameters
        self.gpu_memory_threshold = gpu_memory_threshold
        self.shuffle = shuffle
        self.max_feat_size = max_feat_size
        # self.rank = rank
        # self.world_size = num_gpus

        self.symbols=[line.strip() for line in open(symbol_table)]
        
        self.inputRegionTypes = set( inputRegionTypes )
        self.remove_opposite_edges = remove_opposite_edges


        self.last_epoch = last_epoch
        self.curr_epoch = last_epoch

        self.currDoc = -1  #AD HACK Change to -1 if first index is 0 in dataset
        self.currPage = -1
        self.currRegion = 0
        self.currPageImg = -1  # RZ HACK: Avoding None data error from logger.

        self.Img_list = Img_list
        self.tsv = False
        self.lg = True

        self.oneRelLimit = oneRelLimit
        self.lg_mode = lg_mode
        self.inputs = inputs

        # number of nearest neighbors to consider for segmentation edge hypotheses
        self.prune_num_NN = prune_num_NN
        self.context_num_NN = context_num_NN
        self.extract_mst = extract_mst

        self.merge_dict = {}
        self.oversample_nodes = oversample_nodes

        # Use common dir since features are same for all models
        self.feat_dir = os.path.join(os.path.dirname(model_dir), "input_features", self.task, "primitives")
        os.makedirs(self.feat_dir, exist_ok=True)        
        self.formula_level_dir = os.path.join(self.feat_dir,"formula_level_features")
        os.makedirs(self.formula_level_dir, exist_ok=True)

        if self.inputs == "primitives_gtsymbols":
            self.symbols_feat_dir = os.path.join(os.path.dirname(self.feat_dir), "whole_symbols")
            os.makedirs(self.symbols_feat_dir, exist_ok=True)        
            self.symbols_formula_level_dir = os.path.join(self.symbols_feat_dir,"formula_level_features")
            os.makedirs(self.symbols_formula_level_dir, exist_ok=True)


        dTimer.check("Initialization")
        #region OLD CODE
        if self.format == 'tsv':
            self.tsv = True
            self.lg = False
            self.tsv_dir = tsv_dir
            self.data_files, self.proTableList, self.regionTriples, \
                    self.docs_page_sizes, self.currPageImg, self.lg \
                    = tsv2pro(self.tsv_dir, self.data_dir)
            if self.dataset == 'infty':
                self.rels=INFRELATIONS
            elif self.dataset == 'chem':
                self.rels = CHEM_RELATIONS

        elif self.format == 'lg':
            self.tsv = False
            self.lg = True
            self.lg_dir = lg_dir
            if self.dataset == 'infty' or self.dataset == 'chem':
                # self.tsv_dir = self.lg_dir
                self.data_files, self.proTableList, self.regionTriples, \
                        self.currPageImg, self.lg = lg2pro(self.lg_dir, self.data_dir,
                                                           self.Img_list,
                                                           mode=self.lg_mode,
                                                           inputs=inputs
                                                           # excludeFiles=ERROR_FILES_INFTY,
                                                           )
                

                dTimer.check("Build ProTables")
                if self.dataset == "chem":
                    _, self.currPageImg = cv2.threshold(self.currPageImg, 0.7, 1, cv2.THRESH_BINARY)
                    # self.currPageImg = 1. - self.currPageImg
                if self.dataset == 'infty':
                    self.rels=INFRELATIONS
                else: # Chem Data
                    self.rels=CHEM_RELATIONS

                self.symbols_dict = {s:i for i, s in enumerate(self.symbols)}
                self.rels_dict = {s:i for i, s in enumerate(self.rels)}
                self.store_data_items()
                dTimer.check("Create and store features")

            elif dataset == 'crohme':
                # self.tsv_dir = data_dir
                self.data_files, self.proTableList, self.regionTriples, \
                        self.currPageImg, self.lg = inkml2pro(self.data_dir,
                                                              self.Img_list,
                                                              task=self.task,
                                                              excludeFiles=ERROR_FILES_CROHME,
                                                              )
                self.rels=RELATIONS
            self.docs_page_sizes = []
        else:
            raise ValueError('Dataset format should either be tsv or lg!')

        # TODO: Remove hard code
        # self.regionTriples = [(*regionTriple, 0) for regionTriple in
        #                       self.regionTriples]
        if self.format == "lg":
            self.file2triples = dict(zip(self.data_files, np.array(self.regionTriples)))

        # AKS: If empty formula regions
        if len(self.regionTriples) == 0:
            print(">> Warning: No formula regions present in:")
            print("   " + str(self.data_files))
            self.data_files = []

        # Set up class dictionaries (one entry per symbol/relation class)
        self.symbols_dict = {s:i for i, s in enumerate(self.symbols)}
        self.rels_dict = {s:i for i, s in enumerate(self.rels)}
        self.data_files = np.array(self.data_files)
        self.regionTriples = np.array(self.regionTriples)
        #endregion
    
    def create_features(self,index, triple):
        # dTimer = DebugTimer("MIERDA")
        ( doc, page, region, merge_idx ) = triple
        current_item_id = 0                
        if self.format == 'tsv':
            fileName = path.splitext(self.data_files[ doc ])[0]
            regionName =  fileName + '-P' + str(page + 1) + '-R' + str(region + 1)
            lg_path = ""
        else:
            fileName = self.dataset
            # regionName = path.splitext(self.data_files[ page ])[0]
            regionName = path.splitext(self.data_files[page])[0]
            lg_path = path.join(self.lg_dir, regionName + ".lg")
        
        if merge_idx == 1:
            feat_dir = self.symbols_feat_dir
        else:
            feat_dir = self.feat_dir

        if os.path.exists(os.path.join(feat_dir, "formula_level_features", regionName + ".ptk")):
            print(f"**Features already on {feat_dir}.")
            return []

        # TODO: @Bryan: Currently, it assumes if formul level features are
        # present, query level features are also present, but may not be true
        # Save/Load the num_primitives and num_edges from the formula level 
        # features to use the current_formula.already_on_disk() function as well
        # dTimer.qcheck("Init")
        single = self.get_region_data(fileName, regionName, index, lg_path,
                                    merge_idx)
        # dTimer.qcheck("get_region_data")
        num_primitives = single["num_primitives"]
        num_pairs = single["num_edges"]
                                
        current_formula = Formula(data=single,index_formula=index,id_formula=single["file_name"],
                                num_neighbors=self.context_num_NN)
        # formulas.append(current_formula)                       
        # if current_formula.already_on_disk(feat_dir):
        #     print(f"**Features already on {feat_dir}.")
        #     return []
        current_formula.save_to_disk_test_data(os.path.join(feat_dir, "formula_level_features"))

        os.makedirs(os.path.join(feat_dir,"node_features"), exist_ok=True)
        os.makedirs(os.path.join(feat_dir,"edge_features"), exist_ok=True)
        # dTimer.qcheck("puta")
        for id in range(num_primitives):
            #NODE
            neighbors = list(map(itemgetter(1), single["graph_los_edges"][id][:self.context_num_NN]))        
            feature_vector_node = current_formula.build_feature_vector(index_node=id, mode="primitives", 
                                                                    neighbors=neighbors)
            pos_enc = single["pos_enc"][id]
            data_item_node = NodeItem(id_formula=current_formula.id_formula, id=f"{current_formula.id_formula}_{current_item_id}",
                                      feature_vector=feature_vector_node, pos_enc=pos_enc,
                                      label=single["symbols"][id], label_class=single["symbol_classes"][id])
            current_formula.append_id(data_item_node.id)
            feat_file_node = os.path.join(feat_dir,"node_features", f"{data_item_node.id}.ptk")
            current_item_id += 1
            with open(feat_file_node, "wb") as fo:
                joblib.dump(data_item_node, fo, compress=True)
        
        rel_edges = single["graph_rel_edges"]
        seg_edges = single["graph_seg_edges"]
        rel_edges_set = set(rel_edges)
        seg_edges_set = set(seg_edges)
        # dTimer.qcheck("nodes ended")
        for id in range(num_pairs):
        #PAIR
            edge_knn_prims = single["edge_knn_prims"][id][:self.context_num_NN]
            feature_vector_pair = current_formula.build_feature_vector(index_node=id, mode="pairs", 
                                                                    neighbors=edge_knn_prims)
            edge = single["graph_rel_seg_edges"][id]

            # TODO: Update to compute pair positional encodings
            pos_enc = single["pos_enc"][edge[0]]
            type_edges = []
            edge_label = {}
            
            if edge in rel_edges_set:
                type_edges.append("rel")
                label_idx = rel_edges.index(edge)
                edge_label["rel"] = single["edgeLabels"][label_idx]

            if edge in seg_edges_set:
                type_edges.append("seg")
                label_idx = seg_edges.index(edge)
                edge_label["seg"] = single["segments"][label_idx]
                # if current_formula.id_formula == "10224292" and not edge_label["seg"]:
                #     import pdb; pdb.set_trace()

            data_item_pair = EdgeItem(id_formula=current_formula.id_formula, 
                                      id=f"{current_formula.id_formula}_{current_item_id}",
                                       feature_vector=feature_vector_pair, label=edge_label,
                                       type_edges=type_edges, pos_enc=pos_enc, label_class=edge)
            current_formula.append_id(data_item_pair.id)
            feat_file_pair = os.path.join(feat_dir,"edge_features", f"{data_item_pair.id}.ptk")
            with open(feat_file_pair, "wb") as fo:
                joblib.dump(data_item_pair, fo, compress=True)
            
            current_item_id += 1  
        # dTimer.qcheck("pairs ended")
        # print(dTimer)
        

    def store_data_items(self):
        regionTriples = self.regionTriples

        
        print(f"Generating and saving features to disk: {self.feat_dir}")
        self.feature_generation_parallel = True
        
        dTimer = DebugTimer("AYUSH")

        if self.feature_generation_parallel:
            Parallel(n_jobs= -1, backend="threading")(delayed(unwrap_self)(i) for i in zip([self]*len(regionTriples), 
                                                                                           range(len(regionTriples)), regionTriples))
        else:    

            for index, triple in enumerate(regionTriples):
                self.create_features(index, triple)  
        node_features_paths = sorted(glob(os.path.join(self.feat_dir,"node_features", "*.ptk")),
                                        key=lambda x:(os.path.basename(x).split("_")[0], 
                                                    int(os.path.basename(x).split("_")[-1][:-4])))
        edge_features_paths = sorted(glob(os.path.join(self.feat_dir,"edge_features", "*.ptk")),
                                    key=lambda x:(os.path.basename(x).split("_")[0], 
                                                int(os.path.basename(x).split("_")[-1][:-4])))  
        dTimer.qcheck("end")
        print(dTimer)
        import pdb;pdb.set_trace()
        if self.task == "train" and self.oversample_nodes:            
            if len(node_features_paths) < len(edge_features_paths):
                padding = np.random.choice(node_features_paths, 
                                           size=len(edge_features_paths)-len(node_features_paths),
                                           replace=True).tolist()            
                node_features_paths += padding
        self.saved_features = node_features_paths + edge_features_paths

    def __len__(self): 
        return len(self.saved_features)

    # def __len__(self): 
    #     regionTupleCount = len( self.regionTriples )
    #     if regionTupleCount > 0:
    #         return regionTupleCount
    #     else:
    #         return len(self.Img_list)

    def __getitem__(self, idx):
        feature_file = self.saved_features[idx]
        with open(feature_file, "rb") as fi:
            feature = joblib.load(fi)
       
        feature_dict = {"feature_vector": feature.feature_vector,
                        "positional_encoding": feature.pos_enc,
                        "id": feature.id,
                        "id_formula": feature.id_formula,
                        "label": feature.label,
                        "type_feature": feature.type_feature,
                        "label_class": feature.label_class}
        # import pdb; pdb.set_trace()
        
        return feature_dict

    def custom_collate_fn(self, batch):
        # Initialize containers for batched data
        batched_feature_vectors = []
        batched_pos_enc = []
        batched_ids = []
        batched_id_formulas = []
        batched_labels = []
        batched_label_classes = []
        batched_type_features = []

        # Process each item in the batch
        for item in batch:
            batched_feature_vectors.append(item["feature_vector"])
            batched_ids.append(item["id"])
            batched_id_formulas.append(item["id_formula"])
            batched_labels.append(item["label"])
            batched_label_classes.append(item["label_class"])
            batched_type_features.append(item["type_feature"])
            batched_pos_enc.append(item["positional_encoding"])

        # Convert lists to the appropriate formats
        batched_feature_vectors = torch.stack(batched_feature_vectors, dim=0)  # Stack tensors
        batched_pos_enc = torch.stack(batched_pos_enc, dim=0)  # Stack tensors
        # The rest remain lists as they contain strings or dicts which don't need tensor conversion

        # Return a dictionary with the same structure, but batched
        return {
            "feature_vector": batched_feature_vectors,
            "positional_encoding": batched_pos_enc,
            "id": batched_ids,
            "id_formula": batched_id_formulas,
            "label": batched_labels,
            "label_class": batched_label_classes,
            "type_feature": batched_type_features
        }
    # def collate_fn(self, data):
    #     import pdb; pdb.set_trace()
        
    # Let the epoch be known
    # def __call__(self, epoch):
    #     self.epoch = epoch

    def showCurrentTSVRegion( self ):
        # Method to check the data object state (assuming TSV data!)
        (i, l, p, b) = self.getTSVRegion( \
                (self.currDoc, self.currPage, self.currRegion ) )
        print( "Current Region: Index list, primitive id names, bbboxes:" )
        print( (l, p, b ) )
        #show_image( i, "Region Image" )

    def getTSVRegion( self, indexTuple ):
        # Reads the region, updates current doc/page/region as a side-effect.
        # dTimer.check("Before start reading protables")
        ( doc, page, region, merge_idx ) = indexTuple
        traces = []
        
        imagePath = ""
        # If page image has changed, update.
        if doc != self.currDoc or page != self.currPage:
            if self.format == 'tsv':
                ## AKS: If formula from doc pages (TSV mode)
                imagePath = path.join( self.data_dir, 
                            path.splitext(self.data_files[doc])[0],
                            str(page + 1) + ".png" )
            elif self.format == 'lg':
                ## AKS: If formula images from dataset (LG mode)
                if self.dataset=='infty' or self.dataset == "chem":
                    imagePath = path.join(self.data_dir, 
                             path.splitext(self.data_files[page])[0].\
                                          replace("_line", "").replace("_pdf", "") 
                                          + ".PNG")

            # Image is inverted (*every time -- earlier there was a cond.)
            # '0' argument tells OpenCV to read as grayscale image
            if imagePath:
                # print(imagePath)
                self.currPageImg = cv2.imread( imagePath, 0 )
                # TODO: Maybe move to where this is done for dataset = chem
                if not self.lg:
                    self.currPageImg = 255. - self.currPageImg

                self.currPageImg = self.currPageImg / 255.0

        # Retrieve bounding box for region, and crop the current page
        # at the region's location.
        regionEntry = self.proTableList[doc][page][region]
        hasSegmentations = regionEntry[-2]
        ( minX, minY, maxX, maxY ) = regionEntry[0][:4]
        # Overlay Test
        # for field in regionEntry[1]:
        #     c_box = field[:4]
        #     c_box = [int(c) for c in c_box]
        #     self.currPageImg = cv2.rectangle(self.currPageImg, (c_box[0], c_box[1]), (c_box[2], c_box[3]), (0,255,0), 1)
        # plt.imshow(self.currPageImg, cmap='gray')
        # plt.show()
        # exit(0)
        
        # RZ DEBUG: TSV stores coordinates in X-Y order, OpenCV / numpy
        # operations assume Y-X (i.e., row-column) order for coordinates.
        
        # import pdb; pdb.set_trace()
        
        if self.currPageImg is not None:
            regionImg = self.currPageImg[ minY : maxY + 1, minX : maxX + 1] 
        else:
            regionImg = self.currPageImg
        # Binarize Image
        _, regionImg = cv2.threshold(regionImg, 0.7, 1.0, cv2.THRESH_BINARY)
        
        # dTimer.check("Read formula image")
        # if self.dataset == "chem":
            # _, self.currPageImg = cv2.threshold(self.currPageImg, 128, 255, cv2.THRESH_BINARY)
            # _, self.currPageImg = cv2.threshold(self.currPageImg, 0.7, 1, cv2.THRESH_BINARY)
            # regionImg = 1. - regionImg

        offsetVector = np.array([ minY, minX, minY, minX ]).astype(int)
        contour_offset = np.array([minX, minY])
        
        primitiveIds = []
        classLabels = []
        bboxes = {}
        contours = {}
        symtags = []

       # RZ: Add object type filter; pass only indicated types for parsing
        objects = regionEntry[1]
        for i in range(len(objects)):
            # Get label and type, construct id.
            (oLabel, oType ) = objects[i][4:6]
            if oType not in self.inputRegionTypes:
                continue

            # Use numeric identifiers as given in the input.
            try:
                # If instance id and symtags is present in data, use them
                objId = objects[i][6]
                symtags.append(objects[i][7])
            except:
                objId = int(i) 

            #  if traces present
            if len(objects[i]) >= 9:
                if self.dataset=='infty' or self.dataset == "chem":
                    # import pdb; pdb.set_trace()
                    # contour = np.array(objects[i][8])
                    contour = objects[i][8]
                    contour = [np.array(c) - contour_offset for c in contour]
                    # import pdb; pdb.set_trace()
                    
                    # contour = contour - offsetVector[:2][::-1]
                    contours[ objId ] = contour
                    traces.append(np.concatenate(contour).reshape(1, -1, 2))
                else:
                    # For CROHME dataset,
                    traces.append([objects[i][8]])
            # The 'merge' here are label mappings (e.g, all lines to one repr.)
            primitiveIds.append( objId )
            classLabels.append( check_for_merge( oLabel ) )

            # RZ DEBUG: Boxes require row-column (Y-X) coordinates.
            ( bbminX, bbminY, bbmaxX, bbmaxY ) = objects[i][:4]
            bboxes[ objId ] = np.array( ( bbminY, bbminX, bbmaxY, bbmaxX ) )\
                    .astype(int) - offsetVector

        # Update object state.
        ( self.currDoc, self.currPage, self.currRegion, _) = indexTuple
        # dTimer.check("Read protables information")

        return ( regionImg, primitiveIds, classLabels, bboxes, symtags, traces,
                contours, hasSegmentations)




    ################################################################
    # Region data access method,  used to feed a PyTorch DataLoader 
    ################################################################
    # RZ: Adding index parameter to use TSV region loading
    # All 'ground truth'-based processing removed for simplification.
    def get_region_data(self, input_file, regionName, indexTSV = -1,
                        lg_path="", merge_idx=0, regionTriples=None):
        timer_object = DebugTimer("get_region_data")
        # dTimer.check("Start reading data")
        # TSV
        # RZ: use Doc/Page/Region triple as file representative.
        fileName = regionName

        # print("Features construct")
        # RZ: ASSUMING TSV format data -- CROHME code removed.
        regionImg = None
        symtags = None
        traces = []

        # Get next region details (image and object properties) 
        if regionTriples is not None:
            nextIndexTuple = regionTriples[ indexTSV ]
        else:
            nextIndexTuple = self.regionTriples[ indexTSV ]
        timer_object.qcheck("Init")
        ( regionImg, instances, symbols, boxes, symtags, traces, contours, hasSegmentations ) = \
                self.getTSVRegion( nextIndexTuple )
        timer_object.qcheck("getTSVRegion")
        # Used for making masks and image + graphs
        if self.dataset == "infty" or (self.dataset == "chem" and self.lg_mode == "ccs"):
        # or self.dataset == 'chem':
            traces = Box2Traces(input_file, boxes, instances, mode=self.mode, 
                        in_img = regionImg )
        timer_object.qcheck("Box2Traces")
        # if self.dataset == "chem":
        #     use_contours = True
            # traces = Box2Traces(input_file, boxes, instances, mode=self.mode, 
            #             in_img = regionImg )
            

        # Check contours (traces), collect primitve ids, 
        # outer countours and image props
        if len(traces) < 1:
            print(">> Warning: SKIPPING " + fileName + ": no traces found")
            return { 'error': True }

        instances = [ int(x) for x in instances ]
        outer_traces = [ x[0] for x in traces ] 

        ################################################################
        # Graph Construction
        ################################################################
        
        ## Old: when nodes were used to represent segments
        # Create segments ground truth if available
        # segments = []
        # if self.node_seg and symtags:
        #     segments = [0]
        #     for i in range(1, len(symbols)):
        #         if symtags[i] == symtags[i-1]:
        #             segments.append(0)
        #         else:
        #             segments.append(1)
        # dTimer.check("Construct initial segmentation gt")
        
        if self.GRAPH=='complete' and len(instances) > 25 and \
                self.task=='test':
            self.GRAPH = 'los'

        # import pdb; pdb.set_trace()
        # Construct and store the input graph (e.g. LOS/Complete)
        ( rel_edges, edgeLabels, exprGraph, seg_ref, los_edges,
            sorted_distances )= \
                GraphConstruction.getExprGraph( self.GRAPH,
                                                instances, symbols, outer_traces, symtags=symtags,
                                                lg=lg_path, level=self.level, seg=self.node_seg,
                                                task=self.task, find_punc=self.find_punc
                                                )
        timer_object.qcheck("getExprGraph")
        # dTimer.check("Construct LOS graph")

        if len(rel_edges) == 0:
            rel_edges.append((0, 0))
            los_edges[0].append((0, 0))
            edgeLabels.append('NoRelation')
        
        # TODO: Check if segments, rel_edges and edgeLabels are correct for all conditions:
        # reduced_rel, prune_seg and prune_rel
        # UPDATE: no longer using reduced_rel and prune_rel
        # If segementations are represented with edges (Default)
        # Construct segmentation edges using (k) nearest neighbors from LOS edges
        if not self.node_seg: #and self.task=='train':
            if self.prune_seg:
                seg_edges = list(chain.from_iterable([v[:self.prune_num_NN] for v in los_edges]))
            else:
                seg_edges = list(chain.from_iterable(los_edges))

            if self.remove_opposite_edges:
                sorted_seg_edges = np.sort(seg_edges, axis=1)
                seg_edges = list(map(tuple, np.unique(sorted_seg_edges, axis=0)))

            segments = []
            for edge in seg_edges:
                if edge in seg_ref:
                    segments.append(0)
                else:
                    segments.append(1)

            # If balance = True, reduce number of segmentation edges
            if self.balance_edges and not self.prune_seg:
                seg_edges, segments = self.balance_neg_instances(seg_edges, segments,
                                                                    len(instances), negative=1)
        timer_object.qcheck("self.node_seg")
        # print(len(rel_edges))
        rel_edges2labels = dict(zip(rel_edges, edgeLabels))
        if self.prune_rel:
            # Tolerance of 2 since should not miss positive edges
            rel_edges = list(set(chain.from_iterable([v[:self.prune_num_NN+2] for v in
                                        los_edges])).intersection(set(rel_edges)))
            edgeLabels = list(map(lambda x:rel_edges2labels[x], rel_edges))
        
        if self.remove_opposite_edges:
            sorted_rel_edges = np.sort(rel_edges, axis=1)
            rel_edges, unique_indices = np.unique(sorted_rel_edges, axis=0,
                                                    return_index=True)
            rel_edges = list(map(tuple, rel_edges)) 
            edgeLabels = np.array(edgeLabels)[unique_indices].tolist()

        if self.task == "train" and self.balance_edges and not self.prune_rel:
        # if self.task == "train" and self.balance_edges:
            # rel_edges, edgeLabels = self.hard_neg(rel_edges, edgeLabels, len(instances), 
            #                                       negative="NoRelation", los_edges=los_edges)

            rel_edges, edgeLabels = self.balance_neg_instances(rel_edges, edgeLabels, 
                                                                len(instances),
                                                                negative="NoRelation")

        # print(len(rel_edges))
        
        rel_seg_edges = np.unique(np.concatenate((rel_edges, seg_edges)), axis=0)
        rel_seg_edges_actual = list(map(lambda x: (str(instances[x[0]]), 
                                                    str(instances[x[1]])), rel_seg_edges))
        # rel_seg_edges_str = list(map(tuple, rel_seg_edges.astype(str))) 
        rel_seg_edges = list(map(tuple, rel_seg_edges)) 

        # print(len(rel_seg_edges))
        # import pdb; pdb.set_trace()
        

        # Keep only the edges that goes to the model in the expression graph
        # TODO: Check if this has negative effect on test results
        ## This is only used in test postprocessingGenORLg and not in train
        # exprGraph = exprGraph.edge_subgraph(rel_seg_edges_str)
        edges_to_remove = list(set(exprGraph.edges()) -
                                set(rel_seg_edges_actual))
        exprGraph.remove_edges_from(edges_to_remove)
        
        # Store LOS/complete graph for the file in the dataloader.
        self.expr_graph[ fileName ] = exprGraph

        # dTimer.check("Construct updated segmentation gt")
        merge_groups = defaultdict(set)
        timer_object.qcheck("merge_groups")
        if merge_idx == 1:
            merge_edges = np.array(seg_edges)[np.where(np.array(segments) == 0)].tolist()
            if len(merge_edges):
                merge_edges = get_ccs(merge_edges)
                for merges in merge_edges:
                    for prim in merges:
                        merge_groups[prim] = merge_groups[prim].union(merges - {prim})

        # if self.curr_epoch <= self.last_epoch:
        #     if self.inputs == "primitives_gtsymbols" and hasSegmentations:
        #     # If segmeentation present in formula i.e.,
        #     # multi-primitive symbols present, add gt segmentation entry
        #         merge_edges = np.array(seg_edges)[np.where(np.array(segments) == 0)].tolist()
        #         self.merge_dict[fileName] = [[], get_ccs(merge_edges)]
        #     else:
        #         self.merge_dict[fileName] = [[]]
        # else:
        #     if self.inputs == "primitives_gtsymbols" and hasSegmentations:
        #         merge_filename = self.merge_dict.get(fileName, [])
        #         if merge_filename:
        #             merge_edges = self.merge_dict[fileName][merge_idx]
        #             if merge_edges:
        #                 for merges in merge_edges:
        #                     for prim in merges:
        #                         merge_groups[prim] = merge_groups[prim].union(merges - {prim})

        # Features construction
        if self.mode == "raw":
            # Old code to extract primitive features only and construct rest
            # on GPU. Supports both contour and cc mode, so works for math
            # INFTY gt data which has ccs
            # primitives, primitives_scaled, primitives_batch, prim_bbox,\
            # scaled_sizes, contours = scale_primitives(regionImg, boxes, new_size=self.symbol_height, 
            #                                 filename=fileName, contours=contours,
            #                                 lg_mode=self.lg_mode, merge_edges=[])

            # Do not merge primitive features here, done in model/network
            prim_feat_batch, prim_context_feat_batch, \
                pair_feat_batch, pair_context_feat_batch, pos_enc, contours, edge_knn_prims = \
                create_features_from_contours(regionImg, boxes, new_size=self.symbol_height, 
                                                contours=contours, merge_groups=merge_groups,
                                                all_edges=rel_seg_edges, los_edges=los_edges,
                                                k=self.context_num_NN, sorted_distances=sorted_distances,
                                                thickness=-1)
            # print(rel_seg_edges)
            # if merge_idx == 1:
            # import pdb; pdb.set_trace()
            # imshow([x for x in prim_feat_batch], ncols=4)
            # # imshow([x for x in prim_context_feat_batch], ncols=4)
            # imshow([x for x in pair_feat_batch[:16]], ncols=4)
            # # imshow([x for x in pair_context_feat_batch[:16]], ncols=4)
            # # print(dict(zip(rel_edges, edgeLabels)))
            # # print(dict(zip(seg_edges, segments)))
            # import pdb; pdb.set_trace()
            # imshow(regionImg)
            
        # No support for this currently
        # TODO: Modify simnilarly to raw mode to construct scaled primitves batch
        else:
            # RZ: Begin constructing images as numpy arrays, with 'traces'
            # copied into the image.
            ( image_size, rescale ) = \
                get_image_params(traces, height=self.symbol_height,
                                    data=self.dataset)
            # print(image_size, rescale)
            # exit(0)

            kernel = np.ones((2,2), np.uint8)
            primitives = []
            image = np.zeros(image_size)
            for ( _, trace ) in enumerate(traces):
                primitive = np.zeros(image_size)
                for tr in trace:
                    add_trace(image, tr, rescale, mode=self.mode)
                    add_trace(primitive, tr, rescale, mode=self.mode)
                if self.dataset == "crohme":
                    primitive = cv2.dilate(primitive, kernel, iterations=2)
                primitives.append(primitive)

            if self.dataset == "crohme":
                image = cv2.dilate(image, kernel, iterations=2)
        timer_object.qcheck("check")
        # Binary edge labels for relation detector
        binaryrel_labels=[]
        for rel in edgeLabels:
            binaryrel_labels.append(1) if rel != 'NoRelation' \
                    else binaryrel_labels.append(0)
        
        # Geometric features for edges
        if self.geoFeat:
            geoFeat=getGeofeat(rel_edges, outer_traces)
        
        edgeLabels = [ self.rels_dict[s] for s in edgeLabels ] \
                if self.task=='train' or self.node_seg is False else []

        # Symbol labels for primitives
        ## Use id equal to length of symbols dict if unknown symbol 
        ## (unknown symbols are found in pipeline mode and symbols not used in test mode)
        
        symbol_ids = [self.symbols_dict.get(s, self.symbols_dict["_"]) for s in symbols]
        # num_ind_masks = len(symbols_img) if self.level=='sym' and self.use_gt \
        #         else len(prim_feat_batch)

        # import pdb; pdb.set_trace()
        # import pickle
        # pickle.dump(images, open("notebooks/pickles/images_symContext.pkl",
        #                          "wb"))
        # image_features = torch.cat([prim_feat_batch,
        #                             prim_context_feat_batch,
        #                             pair_feat_batch, pair_context_feat_batch])
        ################################################################
        # Generate output record for use in loading
        ################################################################
        timer_object.qcheck("end")
        # print(timer_object)
        out = {
                # Formula metadata
            "file_name": fileName,
            "indexTuple": ( self.currDoc, self.currPage, self.currRegion ),
            "pdf_name": input_file, # Document name
            
            # The contours of all the primitives
            "contours": contours,
            
            # Main features used in the model
            #IMAGES IS PRIMITIVE,CONTEXT,PAIR,CONTEXT
            # "images": image_features, # All the query and context features concatenated
            "primitives": prim_feat_batch,
            "primitives_context": prim_context_feat_batch,
            "pairs": pair_feat_batch,
            "pairs_context": pair_context_feat_batch,
            "pos_enc": torch.FloatTensor(pos_enc), # Positional encodings for all prims
            "edge_knn_prims": edge_knn_prims,
            
            # All types of edges
            'graph_rel_edges': rel_edges, # Relationship edges
            "graph_seg_edges": seg_edges, # Segmentation edges
            'graph_rel_seg_edges': rel_seg_edges, # Union of both
            'graph_los_edges': los_edges, # All LOS edges for each primitive
            
            # Ground truth Labels for training
            "symbol_classes": symbols,
            "symbols": symbol_ids,
            "segments": segments,
            "edgeLabels": edgeLabels,
            
            # Additional metadata for separating the features 
            "num_primitives": len(prim_feat_batch), # To separate primitive and pairs
            "num_edges": len(pair_feat_batch), # Not used, just for record
            # "num_features": len(image_features), # To separate formulas
            
            # Networkx pruned LOS graph, used only during test for postprocessing
            "expr_graph": exprGraph,
            # List of primitives in sorted order, used only during test 
            # later for postprocessing, to output original primitive id in ground truth
            "instances": instances,
            
            # Used to access merge_dict for the recurrent merging of primitives
            "merge_idx": merge_idx,    
            # Flag indicating if this formula has at least one segmentation
            "hasSegmentations": hasSegmentations,

            # Keep track of files failed to process, NOT USED
            "error": False,
            
            # Extra record keeping, NOT USED
            "image_shape": regionImg.shape,
            
            # The primitive bounding boxes, NOT USED
            "boxes": boxes,
            
            # Sorted distances of all the primitives from each primitive, NOT USED
            "sorted_distances": sorted_distances,
            
            # Formula image, NOT USED
            "regionImg": regionImg,
            
            ## NOT USED
            "binary_edgelabels": torch.LongTensor(binaryrel_labels),
        } 

        return out

    def balance_neg_instances(self, data_instances, data_labels, num_primitives,
                              negative, seed=0):
        neg_idx = np.where(np.array(data_labels) == negative)[0]
        len_neg = len(neg_idx)
        len_pos = len(data_labels) - len(neg_idx) 
        sample_size = int(round(max(len_pos, num_primitives)))
        if len_neg > len_pos and len_neg > num_primitives:
            random.seed(seed)
            sampling = random.sample(list(neg_idx), sample_size)
            new_instances = []
            new_labels = []
            for i, label in enumerate(data_labels):
                if i in sampling or label != negative:
                    new_labels.append(data_labels[i])
                    new_instances.append(data_instances[i])
            data_instances = new_instances
            data_labels = new_labels
        return data_instances, data_labels

    # def hard_neg(self, data_instances, data_labels, num_primitives,
    #               negative, los_edges):
    #     pruned_edges = set(chain.from_iterable([v[:self.prune_num_NN] for v in los_edges]))
    #     neg_idx = np.where(np.array(data_labels) == negative)[0]
    #     len_neg = len(neg_idx)
    #     len_pos = len(data_labels) - len(neg_idx) 
    #     sample_size = int(round(max(len_pos, num_primitives)))
    #     if len_neg > len_pos and len_neg > num_primitives:
    #         # random.seed(seed)
    #         sampling = random.sample(list(neg_idx), sample_size)
    #         new_instances = []
    #         new_labels = []
    #         for i, label in enumerate(data_labels):
    #             if i in sampling or label != negative:
    #                 new_labels.append(data_labels[i])
    #                 new_instances.append(data_instances[i])
    #         data_instances = new_instances
    #         data_labels = new_labels
    #     return data_instances, data_labels

class Partition(object):

    def __init__(self, data, index):
        self.data = data
        self.index = index
        self.task = data.task
        self.curr_epoch = data.curr_epoch

    def __len__(self):
        return len(self.index)

    def __getitem__(self, index):
        data_idx = self.index[index]
        return self.data[data_idx]


class DataPartitioner(object):
    def __init__(self, data, size=2, shuffle=True, seed=0):
        self.data = data
        self.partitions = []
        data_len = len(data)
        rng = random.Random()
        rng.seed(seed)
        data_len = len(data)
        indexes = list(range(data_len))
        rng.shuffle(indexes)
        self.partitions = np.array_split(indexes, size)

    def use(self, partition):
        return Partition(self.data, self.partitions[partition])


""" Partitioning dataset """
def partition_dataset(dataset, shuffle=True, seed=0):
    size = dist.get_world_size()
    partition = DataPartitioner(dataset, size, shuffle, seed)
    partition = partition.use(dist.get_rank())
    return partition, len(partition)


def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)

