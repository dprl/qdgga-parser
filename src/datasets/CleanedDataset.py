import pickle
from torch.utils.data import Dataset
from src.datasets.dataset_simple_refactored import MathSymbolDataset
import os
from copy import copy, deepcopy
import pandas as pd
from src.datasets.classes.DatasetItem import DatasetItem
import numpy as np
from src.utils.dprl_map_reduce import map_reduce, combine_dict_pair, map_concat, combine_dict_lists
from src.datasets.dataset_utilities import create_merged_queries
from src.utils.debug_fns import *
from src.image_ops.contour_utils import create_each_feature_contours,\
    get_each_positional_encodings, get_knn_prims_edge
from src.image_ops.image2traces import imshow
from operator import itemgetter
import torch
import sqlitedict
# import src.utils.debug_fns as debug_fns
# debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

class CleanDataset(Dataset):
    def __init__(self, dataset_base: MathSymbolDataset) -> None:        
        #data
        self.task = dataset_base.task
        # self.treasure = None
        self.treasure = dataset_base.treasure
        # self.formulas_dict = None
        # self.formulas_dict = dataset_base.formulas_dict
        self.formulas_dict_sqlite_path = dataset_base.formulas_dict_sqlite_path
        self.curr_epoch = dataset_base.curr_epoch
        self.symbol_height = deepcopy(dataset_base.symbol_height)
        
        # self.lg_list = deepcopy(dataset_base.lg_list)
        self.server_mode = deepcopy(dataset_base.server_mode)
        self.indexes_features_in_treasure = deepcopy(dataset_base.indexes_features_in_treasure)
        self.formula_level_dir = deepcopy(dataset_base.formula_level_dir)                

        self.context_num_NN = dataset_base.context_num_NN
        self.shared_lock = dataset_base.shared_lock
        self.read_feature_from_disk = dataset_base.read_feature_from_disk
        self.write_feature_to_disk = dataset_base.write_feature_to_disk
    
    def __len__(self):
        return len(self.indexes_features_in_treasure)

    def __getitem__(self, idx_item):
        id_treasure = self.indexes_features_in_treasure[idx_item]
        data = self.build_item(id_treasure)
        return data
    
    def get_all_formulas_ids(self):
        return list(self.formulas_dict.keys())
        
    def get_nodes_data_from_formula(self, id_formula):
        iterator_objects = self.formulas_dict[id_formula].dict_nodes.values()
        node_instances, node_probs = [],[]
        contours = {}
        for node in iterator_objects:
            node_instances.append(node.instance_lg)
            node_probs.append(node.predicted_probabilities)
            if node.type_derivation == "primitive":
                contours[node.id] = node.contours
        return node_instances, node_probs, contours

    def get_edges_data_from_formula(self, id_formula, type_task):
        edge_tuples, edge_predicted_labels, edge_probs = [], [], []
                
        iterator_object = self.formulas_dict[id_formula].dict_edges.values()        
        edges_set = set()

        for edge in iterator_object:
            if (edge.get_type_task() == type_task or edge.get_type_task() == "both") and edge.edge_tuple not in edges_set:
                edges_set.add(edge.edge_tuple)
                edge_tuples.append(edge.edge_tuple)
                last_edge = self.get_last_edge(edge, id_formula)
                edge_predicted_labels.append(last_edge.get_predicted_value(type_task))            
                edge_probs.append(last_edge.get_predicted_probabilities(type_task))
        return edge_tuples, edge_predicted_labels, edge_probs

    def get_last_edge(self, edge, id_formula):
        if edge.predicted_segmented_partner_id == -1:
            return edge
        else:
            return self.formulas_dict[id_formula].dict_edges[edge.predicted_segmented_partner_id]

    def get_contours_map(self, id_treasure):
        contours_object = self.get_contours_object(id_treasure)
        contours_context_object = self.get_contours_context_object(id_treasure)

        # COMENTED FOR NO GAT
        # contours_neighbors_object = self.get_contours_neighbors_object(id_treasure)

        combined_contours = combine_dict_pair(contours_object, contours_context_object)        

        # COMENTED FOR NO GAT
        # for neighboor in contours_neighbors_object:
        #     combined_contours = combine_dict_pair(combined_contours, neighboor)
        
        metadata = {
            "query": np.unique(list(contours_object.keys())).tolist(),
            "context": np.unique(list(contours_context_object.keys())).tolist(),

            # COMENTED FOR NO GAT
            # "neighbors": [np.unique(list(i.keys())).tolist() for i in contours_neighbors_object]
        }
        return metadata, combined_contours        
    
    def get_label_class(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].label_string
        elif row_data.type == "edge": 
            return self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].edge_tuple
        else:
            return None
    
    def get_label(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].label_class
        elif row_data.type == "edge": 
            return self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].get_label_classes()
        else:
            return None
 
    def get_label_string(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].label_string
        elif row_data.type == "edge": 
            return self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].label_string
        else:
            return None

    def get_contours_with_id(self, id_formula, id_item):
        return {id_item:self.formulas_dict[id_formula].dict_nodes[id_item].contours}

    def get_node_ids_edge(self, id_formula, id_item):
        return self.formulas_dict[id_formula].dict_edges[id_item].edge_tuple    

    def get_contours_node(self, row_data, segmented_version=False, predicted_segmentation=False):
        if row_data.type_derivation == "primitive":
            if predicted_segmentation:
                segmented_partner_id = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].predicted_segmented_partner_id
            else:
                segmented_partner_id = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].ground_truth_segmented_partner_id

        #             self.ground_truth_segmented_partner_id = -1
        # self.predicted_segmented_partner_id = -1
            if not segmented_version or segmented_partner_id == -1:
                return self.get_contours_with_id(row_data.id_formula, row_data.id_item)
            else:                                
                contours_ids = self.formulas_dict[row_data.id_formula].dict_nodes[segmented_partner_id].contours_ids
                contours = {}
                for prim_contour_id in contours_ids:
                    contours = combine_dict_pair(contours,self.get_contours_with_id(row_data.id_formula, prim_contour_id)) 
                return contours
        elif row_data.type_derivation == "segmentation":            
            contours_ids = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].contours_ids                        
            contours = {}
            for prim_contour_id in contours_ids:
                contours = combine_dict_pair(contours,self.get_contours_with_id(row_data.id_formula, prim_contour_id)) 
            return contours
        else:
            return None

    def get_contours_edge(self, row_data):
        is_edge_from_segmentation = row_data.type_derivation == "segmentation"        
        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].is_predicted_segmentation
    
        parent, child = self.get_node_ids_edge(row_data.id_formula, row_data.id_item)
        row_parent = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == parent) & (self.treasure.type=="node")].iloc[0]                
        contours_parent = self.get_contours_node(row_parent, segmented_version=is_edge_from_segmentation, predicted_segmentation=is_predicted_segmentation)
        row_child = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == child) & (self.treasure.type=="node")].iloc[0]                
        contours_child = self.get_contours_node(row_child, segmented_version=is_edge_from_segmentation, predicted_segmentation=is_predicted_segmentation)          
        return combine_dict_pair( contours_parent,  contours_child)
                       
    def get_contours_context_node(self, row_data):
        context_ids = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].context_ids
        
        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].is_predicted_segmentation

        contours = {}        
        for id in context_ids:
            row_data_neighbor = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == id) & (self.treasure.type=="node")].iloc[0]            
            contours = combine_dict_pair(contours, self.get_contours_node(row_data_neighbor, segmented_version=False,
                                                                           predicted_segmentation=is_predicted_segmentation))
        return contours

    def get_contours_context_edge(self, row_data):
        
        context_ids = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].context_ids

        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].is_predicted_segmentation
        
        contours = {}
        for id in context_ids:            
            row_data_neighbor = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == id) & (self.treasure.type=="node")].iloc[0]
            # contours = combine_dict_pair(contours, self.get_contours_node(row_data_neighbor, segmented_version=False))           
            contours = combine_dict_pair(contours, self.get_contours_node(row_data_neighbor, segmented_version=False, 
                                                                          predicted_segmentation=is_predicted_segmentation))
        return contours
        
    def get_contours_context_object(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.get_contours_context_node(row_data)
        elif row_data.type == "edge":            
            return self.get_contours_context_edge(row_data)
        else:
            return None
    
    def get_contours_object(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.get_contours_node(row_data)
        elif row_data.type == "edge":                        
            return self.get_contours_edge(row_data)
        else:
            return None 

    def get_contours_neighbors_object(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.get_contours_neighbors_node(row_data)    
        elif row_data.type == "edge":            
            return self.get_contours_neighbors_edge(row_data)
        else:
            return None 
        
    def get_contours_neighbors_node(self, row_data):
        neighbors_ids = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].neighbors_ids  
        
        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].is_predicted_segmentation

        contours = []
        for id in neighbors_ids:
            row_data_neighbor = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == id) & (self.treasure.type=="node")].iloc[0]                        
            contours.append(self.get_contours_node(row_data_neighbor, segmented_version=row_data.type_derivation=="segmentation",
                                                   predicted_segmentation=is_predicted_segmentation))
        return contours
    
    def get_contours_neighbors_edge(self, row_data):
        contours = []        
        neighbors_ids = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].neighbors_ids

        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].is_predicted_segmentation

        for id in neighbors_ids:
            row_data_neighbor = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == id) & (self.treasure.type=="node")].iloc[0]
            contours.append(self.get_contours_node(row_data_neighbor, segmented_version=row_data.type_derivation=="segmentation",
                                                   predicted_segmentation=is_predicted_segmentation))
        return contours

    def get_filename(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        id_formula = row_data.id_formula
        query_id = row_data.id_item
        data_source = row_data.data_source
        return f"{id_formula}_{data_source}_{row_data.type}_{query_id}"

    def get_expression_graph_from_formula(self, id_formula):
        return self.formulas_dict[id_formula].expression_graph

    def formula_has_predicted_segmentations(self, id_formula):
        asdas = self.formulas_dict[id_formula]

    def get_edge_identifier(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]                    
        return self.formulas_dict[row_data.id_formula].dict_segs[row_data.id_item].edge_tuple

    def flush_formula(self, id_formula):
        ids_to_remove = self.treasure[self.treasure.id_formula == id_formula].index
        # del self.formulas_dict[id_formula]
        self.indexes_features_in_treasure = list(set(self.indexes_features_in_treasure) - set(ids_to_remove))        

    def flush_from_ids(self, ids_remove_from_treasure):
        for id_treasure in ids_remove_from_treasure:
            row_data = self.treasure.iloc[id_treasure]
            if row_data.type_derivation == "segmentation":
                if row_data.type == "node" and row_data.id_item in self.formulas_dict[row_data.id_formula].dict_nodes:                    
                    del self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item]
                elif row_data.id_item in self.formulas_dict[row_data.id_formula].dict_edges:                    
                    del self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item]

    def build_item(self, id_treasure):
        dTimer_item = DebugTimer("MIERDA")
        row_data = self.treasure.iloc[id_treasure]
        id_formula = row_data.id_formula
        with sqlitedict.SqliteDict(self.formulas_dict_sqlite_path, flag='r') as formulas_dict:
            formula_object = formulas_dict[id_formula]

        formula_contours = formula_object.contours
        query_id = row_data.id_item

        data_source = row_data.data_source
        if data_source == "primitive":
            merge_dict = {}
        elif data_source == "ground_truth":
            merge_dict = formula_object.gt_merge_dict
        elif data_source == "predicted":
            merge_dict = formula_object.predicted_merge_dict
        else:
            print("Error: data_source not recognized")
        dTimer_item.qcheck("INIT MIERDA")
        if row_data.type == "node":
            type_feature = 0
            query_ids = {query_id}
            if query_id in merge_dict:
                query_ids.update(merge_dict[query_id])

            neighbor_edges = formula_object.neighbors[query_id]
            # K nearest primitives, plus the primitives itself
            context_ids = {*query_ids, *set(map(itemgetter(1),
                                             neighbor_edges[:self.context_num_NN]))}

            if self.task != "test":
                label = formula_object.get_node_label_class_int(query_id)
                label_class = formula_object.get_node_label_class(query_id)

        elif row_data.type == "edge":
            type_feature = 1
            edge = formula_object.rel_edges[query_id]
            query_ids = set(edge)
            for query in edge:
                if query in merge_dict:                    
                    query_ids.update(merge_dict[query])
            neighbor_ids, context_ids = get_knn_prims_edge(edge, list(query_ids),
                                                           formula_object.rel_seg_edges_matrix,
                                                           formula_object.edge_distances_matrix,
                                                           self.context_num_NN)
            if self.task != "test":
                label = formula_object.get_edge_label_class(query_id)
                label_class = edge
        dTimer_item.qcheck("packing")

        ids_per_type = {
            "query": query_ids,
            "context": context_ids
            # COMENTED FOR NO GAT
            # "neighbors": [np.unique(list(i.keys())).tolist() for i in contours_neighbors_object]
        }

        # ids_per_type, combined_contours = self.get_contours_map(id_treasure)
        image_shape = formula_object.shape
        feature_vector, pos_enc = self.create_query_features(ids_per_type,
                                                             formula_contours, image_shape)
        dTimer_item.qcheck("feature building")
        
        if self.task != "test":
            feature_dict = {"feature_vector": feature_vector, #2x28x28
                            "positional_encoding": pos_enc, #10
                            "id": row_data.id_item,
                            "id_formula": id_formula,
                            "type_feature": row_data.type,
                            "label_class": label_class, 
                            "label": label, 
                            }
            return feature_dict
        else:
            return np.concatenate((feature_vector.flatten(), pos_enc, 
                                   np.array([type_feature, id_treasure])))

    def build_item_old(self, id_treasure):
        dTimer_item = DebugTimer("build_item")
        row_data = self.treasure.iloc[id_treasure]
        ids_per_type, combined_contours = self.get_contours_map(id_treasure)

        image_shape = self.formulas_dict[row_data.id_formula].shape
        dTimer_item.qcheck("metadata")
        feature_vector, pos_enc = self.create_query_features(ids_per_type, combined_contours, image_shape)
        dTimer_item.qcheck("create_query_features")
        feat_id = id_treasure
        formula_id = row_data.id_formula
        label = self.get_label_class(id_treasure) #H, Single, (0,3), etc
        type_feature = row_data.type
        label_class = self.get_label(id_treasure) #0, 20, 1, etc
        feature_dict = {"feature_vector": feature_vector, #2x28x28
                        "positional_encoding": pos_enc, #10
                        "id": feat_id, 
                        "id_formula": formula_id,
                        "label": label_class, #TODO: NAMING MESSED UP
                        "type_feature": type_feature,
                        "label_class": label #TODO: NAMING MESSED UP
                        }
        # dTimer_item.qcheck("finish")
        # print(dTimer_item)
        return feature_dict
    
    def create_query_features(self, ids_per_type, combined_contours, image_shape, thickness=-1):
        ### NEEDED
        # image_shape = None
        # main_contours = None
        # combined_contours = None

        # Dicionary of lists, key- neighbors of the query, value- list of
        # [neighbor itself, other prim ids which this was merged with] OR
        # [neighbor_itself] if no merge
        
        query_indices = ids_per_type["query"]
        context_indices = ids_per_type["context"]

        # COMENTED FOR NO GAT
        # neighbor_indices = ids_per_type["neighbors"]

        query_contours = []
        for idx in query_indices:
            query_contours.extend(combined_contours[idx])

        points_np = np.concatenate(query_contours)
        bbox_min = points_np.min(axis=0)[0] #added extra dimension
        bbox_max = points_np.max(axis=0)[0] #added extra dimension        
        prim_bbox = [bbox_min[1], bbox_min[0], bbox_max[1], bbox_max[0]]
        
        ### QUERY FEATURES
        query_feat, _ = create_each_feature_contours(query_indices, contours=combined_contours,
                                                       new_size=self.symbol_height, thickness=thickness)
        ### NEIGHBOR FEATURES
        # COMENTED FOR NO GAT
        # neighbors_feat = []
        # for nei_idx in neighbor_indices:
        #     neighbors_feat_i, _ = create_each_feature_contours(nei_idx, contours=combined_contours,
        #                                                    new_size=self.symbol_height, thickness=thickness)
        #     neighbors_feat.append(neighbors_feat_i)
        # neighbors_feat = torch.stack(neighbors_feat)

        ### CONTEXT FEATURES
        # K nearest primitives, plus the primitives itself
        context_feat, context_size = create_each_feature_contours(context_indices, query_indices,
                                                               combined_contours, new_size=self.symbol_height, 
                                                               thickness=thickness)
        ## POSITIONAL ENCODINGS        
        pos_enc = get_each_positional_encodings(prim_bbox, context_size, image_shape)
        

        # PADDING

        # COMENTED FOR NO GAT
        # num_neighbors = len(neighbor_indices)
        # total_neighbors = self.context_num_NN
        # total_padding = total_neighbors - num_neighbors
        # if total_padding > 0:
        #     neighbors_feat = torch.cat((neighbors_feat, torch.zeros((total_padding,
        #                                              *neighbors_feat.shape[1:]))))

        ### FINAL FEATURES
        features = torch.cat((query_feat.unsqueeze(0),
                              context_feat.unsqueeze(0)))
                              # COMENTED FOR NO GAT
                            #   neighbors_feat))
        
        return features, pos_enc
