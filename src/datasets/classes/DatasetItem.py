from torch import tensor
from typing_extensions import List, Tuple
import pandas as pd
import numpy as np
from collections import defaultdict
import itertools
from operator import itemgetter

class DatasetItem:
    def __init__(self, id_formula:str, id:str, type_name: str, type_derivation:str) -> None:
        self.id_formula = id_formula        
        self.id = id                
        self.type_name = type_name        
        self.predicted_value = None
        self.predicted_probabilities = None
        self.was_segmented = False
        self.ground_truth_segmented_partner_id = -1
        self.predicted_segmented_partner_id = -1
        self.is_predicted_segmentation = False
        self.type_derivation = type_derivation
        self.epoch = 0
    
    def get_data_frame_row(self):
        pass

    def get_filename(self):
        pass

    @staticmethod
    def get_scheme():
        return {
            'id_formula': pd.Series(dtype='str'), 
            'id_item': pd.Series(dtype='Int64'), 
            'type': pd.Series(dtype='str'), 
            # 'type_derivation': pd.Series(dtype='str'),#primitive, segmentation            
            'data_source': pd.Series(dtype='str'), 
            'epoch':pd.Series(dtype='Int8'),
            # 'predicted_value': pd.Series(dtype='Int64'),
            'label_class_node':pd.Series(dtype='Int8'),
            # 'label_class_node_str':pd.Series(dtype='str'),
            'label_class_seg':pd.Series(dtype='Int8'),
            'label_class_rel':pd.Series(dtype='Int8'),
            # 'parent_id':pd.Series(dtype='Int64'),
            # 'child_id':pd.Series(dtype='Int64'),
            # 'parent_str':pd.Series(dtype='str'),
            # 'child_str':pd.Series(dtype='str'),
        }
    
    @staticmethod
    def get_types():
        return {
            'id_formula': 'string', 
            'id_item': 'Int64', 
            'type': 'string', 
            # 'type_derivation': 'string',#primitive, segmentation            
            'data_source': 'string',
            'epoch':'Int8',
            # 'predicted_value': 'Int64',
            'label_class_node':'Int8',
            # 'label_class_node_str':'string',
            'label_class_seg':'Int8',
            'label_class_rel':'Int8',
            # 'parent_id':'Int64',
            # 'child_id':'Int64',
            # 'parent_str':'string',
            # 'child_str':'string'
        }


class PrimNodeItem(DatasetItem):    
    def __init__(self, id:int, id_formula:str, label_class:str, contours:np.array, label_string:str, 
                 instance_lg: str, box: np.array, neighbors_ids: List, context_ids) -> None:
        super().__init__(id_formula, id, type_name="node", type_derivation="primitive")        
        self.label_class = label_class
        self.label_string = label_string
        self.instance_lg = instance_lg
        self.contours = contours
        self.contours_ids = [id]
        self.box = box
        self.neighbors_ids = neighbors_ids
        self.is_base_primitive = True
        self.context_ids = context_ids        
    
    def get_data_frame_row(self):        
        data = {'id_formula': [self.id_formula], 
                'id_item': [self.id], 
                'type': [self.type_name], 
                'type_derivation': [self.type_derivation],
                'epoch':[self.epoch],                                
                'predicted_value': [-1],
                'label_class_node':[self.label_class],
                # 'label_class_node_str':[self.label_string],
                'label_class_seg':[pd.NA],
                'label_class_rel':[pd.NA],
                # 'parent_id':[pd.NA],
                # 'child_id':[pd.NA],
                'parent_str':[pd.NA],
                'child_str':[pd.NA],
                }        
        return pd.DataFrame(data=data).astype(DatasetItem.get_types())
    
    def get_filename(self):
        return f"{self.id_formula}_Prim_{self.id}"
    
    def create_segmentation_node(self, contours_ids:set, id:int):            
        return SegNodeItem(id=id, id_formula=self.id_formula, label_class=self.label_class, contours_ids=contours_ids,
                        label_string=self.label_string, instance_lg=self.instance_lg, box=self.box, parent_id=self.id,
                        neighbors_ids=self.neighbors_ids, context_ids=self.context_ids)

class SegNodeItem(DatasetItem):
    def __init__(self, id:int, id_formula:str, label_class:str, contours_ids:set, 
                 label_string:str, instance_lg: str, box: np.array, parent_id:int, neighbors_ids: List, context_ids: List) -> None:
        super().__init__(id_formula, id, type_name="node", type_derivation="segmentation")
        self.label_class = label_class
        self.label_string = label_string
        self.instance_lg = instance_lg
        self.contours_ids = contours_ids
        self.box = box
        self.parent_id = parent_id
        self.neighbors_ids = neighbors_ids
        self.is_base_primitive = False
        self.context_ids = context_ids
        self.type_node = "segmentation"
    
    def get_data_frame_row(self):
        data = {'id_formula': [self.id_formula], 
                'id_item': [self.id], 
                'type': [self.type_name], 
                'type_derivation': [self.type_derivation],
                'epoch':[self.epoch],
                'predicted_value': [-1],
                'label_class_node':[self.label_class],
                # 'label_class_node_str':[self.label_string],
                'label_class_seg':[pd.NA],
                'label_class_rel':[pd.NA],
                # 'parent_id':[pd.NA],
                # 'child_id':[pd.NA],
                'parent_str':[pd.NA],
                'child_str':[pd.NA],
                }
        return pd.DataFrame(data=data).astype(DatasetItem.get_types())
    
    def get_filename(self):
        return f"{self.id_formula}_SegPrim_{self.id}"
    
    def create_segmentation_node(self, contours_ids:set, id:int):            
        return SegNodeItem(id=id, id_formula=self.id_formula, label_class=self.label_class, contours_ids=contours_ids,
                        label_string=self.label_string, instance_lg=self.instance_lg, box=self.box, parent_id=self.id,
                        neighbors_ids=self.neighbors_ids, context_ids=self.context_ids)

class EdgeItem(DatasetItem):

    def __init__(self, id_formula:str, id:int, label_class_seg:str, label_class_rel:str, edge_tuple:tuple, edge_tuple_lg:tuple, neighbors_ids, context_ids, edge_tuple_str) -> None:
        super().__init__(id_formula, id, type_name="edge", type_derivation="primitive",)        
        self.edge_tuple = edge_tuple
        self.edge_tuple_lg = edge_tuple_lg        
        self.neighbors_ids = neighbors_ids
        self.context_ids = context_ids
        self.label_class_seg = label_class_seg
        self.label_class_rel = label_class_rel
        self.edge_tuple_str = edge_tuple_str
    
    def get_type_task(self):
        if self.label_class_seg!=-1 and self.label_class_rel!=-1:
            return "both"
        elif self.label_class_seg!=-1:
            return "seg"
        else:
            return "rel"        

    def get_label_classes(self):
        label_classes = {}
        if self.label_class_seg != -1:
            label_classes["seg"] = self.label_class_seg
        if self.label_class_rel != -1:
            label_classes["rel"] = self.label_class_rel
        return label_classes


    def get_predicted_value(self, type_task):        
        return self.predicted_value[type_task]
    
    def get_predicted_probabilities(self, type_task):        
        return self.predicted_probabilities[type_task]

    def get_data_frame_row(self):
        data = {'id_formula': [self.id_formula], 
                'id_item': [self.id], 
                'type': [self.type_name], 
                'type_derivation': [self.type_derivation],                
                'epoch':[self.epoch],
                'predicted_value': [-1],
                'label_class_node':[pd.NA],
                # 'label_class_node_str':[pd.NA],
                'label_class_seg':[self.label_class_seg],
                'label_class_rel':[self.label_class_rel],
                # 'parent_id':[self.edge_tuple[0]],
                # 'child_id':[self.edge_tuple[1]],
                'parent_str':[self.edge_tuple_str[0]],
                'child_str':[self.edge_tuple_str[1]],
                }
        return pd.DataFrame(data=data).astype(DatasetItem.get_types())
    
    def get_filename(self):
        return f"{self.id_formula}_Edge_{self.get_type_task()}_{self.id}"

    def create_segmentation_edge(self, id:int):               
        return NodeSegmentatioEdgeItem(id=id, id_formula=self.id_formula, label_class_seg=self.label_class_seg, label_class_rel=self.label_class_rel,
                                       edge_tuple=self.edge_tuple, edge_tuple_lg=self.edge_tuple_lg, neighbors_ids=self.neighbors_ids, context_ids=self.context_ids, parent=self.id, edge_tuple_str=self.edge_tuple_str)


class NodeSegmentatioEdgeItem(DatasetItem):
    def __init__(self, id_formula:str, id:int,  label_class_seg:str, label_class_rel:str, edge_tuple:tuple, edge_tuple_lg:tuple, parent:int, neighbors_ids, context_ids, edge_tuple_str) -> None:
        super().__init__(id_formula, id, type_name="edge",type_derivation="segmentation")
        self.edge_tuple = edge_tuple
        self.edge_tuple_lg = edge_tuple_lg              
        self.parent_id = parent
        self.neighbors_ids = neighbors_ids        
        self.context_ids = context_ids
        self.label_class_seg = label_class_seg
        self.label_class_rel = label_class_rel        
        self.edge_tuple_str = edge_tuple_str

    def get_data_frame_row(self):
        data = {'id_formula': [self.id_formula], 
                'id_item': [self.id], 
                'type': [self.type_name], 
                'type_derivation': [self.type_derivation],                
                'epoch':[self.epoch],
                'predicted_value': [-1],
                'label_class_node':[pd.NA],
                # 'label_class_node_str':[pd.NA],
                'label_class_seg':[self.label_class_seg],
                'label_class_rel':[self.label_class_rel],
                # 'parent_id':[self.edge_tuple[0]],
                # 'child_id':[self.edge_tuple[1]],
                'parent_str':[self.edge_tuple_str[0]],
                'child_str':[self.edge_tuple_str[1]],
                }
        return pd.DataFrame(data=data).astype(DatasetItem.get_types())

    def get_type_task(self):
        if self.label_class_seg!=-1 and self.label_class_rel!=-1:
            return "both"
        elif self.label_class_seg!=-1:
            return "seg"
        else:
            return "rel"        

    def get_label_classes(self):
        label_classes = {}
        if self.label_class_seg != -1:
            label_classes["seg"] = self.label_class_seg
        if self.label_class_rel != -1:
            label_classes["rel"] = self.label_class_rel
        return label_classes


    def get_predicted_value(self, type_task):
        try:
            return self.predicted_value[type_task]
        except:
            import pdb;pdb.set_trace()
    
    def get_predicted_probabilities(self, type_task):        
        return self.predicted_probabilities[type_task]

    def get_filename(self):
        return f"{self.id_formula}_NodeSegEdge_{self.get_type_task()}_{self.id}"
    
    def create_segmentation_edge(self, id:int):          
        return NodeSegmentatioEdgeItem(id=id, id_formula=self.id_formula, label_class_seg=self.label_class_seg, label_class_rel=self.label_class_rel,
                                        edge_tuple=self.edge_tuple, edge_tuple_lg=self.edge_tuple_lg, neighbors_ids=self.neighbors_ids, context_ids=self.context_ids, parent=self.id,
                                       edge_tuple_str=self.edge_tuple_str)


# class Formula:
#     def __init__(self, id_formula:str, expression_graph:object,dict_edges:dict,
#                  dict_nodes:dict, treasure:pd.DataFrame, shape:Tuple):
#         self.id_formula = id_formula
#         self.expression_graph = expression_graph
#         self.dict_nodes = dict_nodes        
#         self.dict_edges = dict_edges
#         self.treasure = treasure
#         self.shape = shape
#         self.segmented_nodes_ids = []
#         self.merge_groups_test = {}
#         self.hasSegmentation = False
#         self.ids_remove_from_treasure_for_testing = []        
        
    # def update_with_segmentations(self):
    #     ids_segmentations_offset = len(self.dict_edges.keys())
    #     new_edges = {}
    #     for id, edge in self.dict_edges.items():            
    #         if edge.get_predicted_value("seg") == 0:
    #             new_edge = edge.create_segmentation_edge(ids_segmentations_offset)
    #             new_edges[ids_segmentations_offset] = new_edge
    #             ids_segmentations_offset+=1
    #     return 


class Formula:
    def __init__(self, id_formula:str, expression_graph:object, shape:Tuple,
                 instances:list, symbol_classes:list, symbol_classes_int:list,
                 gt_merge_dict:dict, pred_merge_dict:dict, contours:dict,
                 rel_edges:list, seg_edges:list, rel_labels:list,
                 seg_labels:list, rel_seg_edges_matrix, edge_distances_matrix,
                 task):

        """
        Additional information:
        There are 2 ids: contour_id and instance
        1. contour_id or node_id: the primitive index, which is 0 to number of primitives - 1
        2. instance: the actual primitive id in the lg file

        contour_id or node_id is used throughout the code to access any
        primitive level information from a formula level object
        (diciontary/list), including accessing the instance id

        instance is used only at the end in inference mode to create the final lg file
        """

        self.id_formula = id_formula
        self.expression_graph = expression_graph        
        self.shape = shape

        self.contours = contours
        self.instances = instances #lg file primitives e.g. 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
        self.symbol_classes = symbol_classes # corresponding symbol class for
                                            # primtives e.g. ['+', '1', 'C', 'H', 'O', '2', '3', '4', '5', '6']
        self.symbol_classes_int = symbol_classes_int # corresponding integer
                                                    # class for symbol class e.g. [43, 41, 70, 12, ...]

        # New additions
        self.num_nodes = len(contours)
        self.num_edges = len(rel_edges)

        ## contour/node_id is the index of the neighbor matrix
        self.neighbors = rel_seg_edges_matrix

        self.rel_edges = rel_edges
        self.seg_edges = seg_edges

        self.rel_labels = rel_labels#0 NOT_CONNECTED 1 CONNECTED
        self.seg_labels = seg_labels#0 merge 1 no merge
        
        self.rel_seg_edges_matrix = rel_seg_edges_matrix
        self.edge_distances_matrix = edge_distances_matrix

        self.gt_merge_dict = gt_merge_dict
        # if gt_merge_dict:
        #     self.gt_merge_groups = self.create_merge_groups(gt_merge_dict)
        self.predicted_merge_dict = pred_merge_dict
        # if pred_merge_dict:
        #     self.pred_merge_groups = self.create_merge_groups(pred_merge_dict)

        #TEST DATA INIT
        self.node_probs = None
        self.rel_edge_probs = None
        self.seg_edge_predicted_labels = None
        

        self.create_dataframe(task)

    # def create_merge_groups(self, merge_dict):
    #     merge_groups = defaultdict(set)
    #     for merges in merge_dict:
    #         for prim in merges:
    #             merge_groups[prim] = merge_groups[prim].union(merges - {prim})
    #     return merge_groups

    def set_test_data_from_batch(self, data):
        pass

    def get_node_label_class(self, idx):
        return self.symbol_classes[idx]

    def get_node_label_class_int(self, idx):
        return self.symbol_classes_int[idx]

    def get_edge_label_class(self, idx):
        label_class = {}
        label_class["seg"] = self.seg_labels[idx]
        label_class["rel"] = self.rel_labels[idx]
        return label_class

    def create_dataframe(self, task):
        #PRIMITIVES
        #nodes
        ids = np.arange(self.num_nodes)
        id_formula_array = np.repeat(self.id_formula,self.num_nodes)
        type_array = np.repeat("node",self.num_nodes)
        data_source_array = np.repeat("primitive",self.num_nodes)
        epoch_array = np.repeat(0, self.num_nodes)          
        data_nodes = np.stack((id_formula_array, ids, type_array,
                               data_source_array, epoch_array,
                               self.symbol_classes_int, np.repeat(pd.NA, self.num_nodes),
                               np.repeat(pd.NA, self.num_nodes)), axis=1)

        #edges
        ids = np.arange(self.num_edges)#assumes rel and seg are the same
        id_formula_array = np.repeat(self.id_formula,self.num_edges)
        type_array = np.repeat("edge",self.num_edges)
        data_source_array = np.repeat("primitive",self.num_edges)
        epoch_array = np.repeat(0, self.num_edges)        
        data_edges = np.stack((id_formula_array, ids, type_array,
                               data_source_array, epoch_array, np.repeat(pd.NA, self.num_edges),
                               self.seg_labels, self.rel_labels), axis=1)
                

        data_all = np.concatenate((data_nodes, data_edges))
        if task == "train":
            #GROUND TRUTH
            indexes_edges_segmentation = np.argwhere(np.array(self.seg_labels) == 0).squeeze(1)
            #nodes        
            nodes_segmented_ids = np.unique(np.array(self.seg_edges)[indexes_edges_segmentation])
            num_nodes_segmented = len(nodes_segmented_ids)
            if num_nodes_segmented:
                id_formula_array = np.repeat(self.id_formula, num_nodes_segmented)
                type_array = np.repeat("node",num_nodes_segmented)
                data_source_array = np.repeat("ground_truth",num_nodes_segmented)
                epoch_array = np.repeat(0, num_nodes_segmented)        
                data_nodes_gt = np.stack((id_formula_array, nodes_segmented_ids, type_array,
                                          data_source_array, epoch_array,
                                          np.array(self.symbol_classes_int)[nodes_segmented_ids],
                                          np.repeat(pd.NA, num_nodes_segmented),
                                          np.repeat(pd.NA, num_nodes_segmented)), axis=1)
                data_all = np.concatenate((data_all, data_nodes_gt))
            #edges
            if num_nodes_segmented:
                segmented_edges = set(itertools.chain(*list(itemgetter(*nodes_segmented_ids)(self.neighbors))))
                rel_edges_set = set(self.rel_edges)
                rel_edges_idx = list(enumerate(self.rel_edges))
                ids = list(map(itemgetter(0), list(filter(lambda x: x[1] in
                                                                     segmented_edges,
                                                                     rel_edges_idx))))
                num_gt_edges = len(ids)
                if num_gt_edges:
                    id_formula_array = np.repeat(self.id_formula, num_gt_edges)
                    type_array = np.repeat("edge", num_gt_edges)
                    data_source_array = np.repeat("ground_truth", num_gt_edges)
                    epoch_array = np.repeat(0, num_gt_edges)
                    data_edges_gt = np.stack((id_formula_array, ids, type_array, 
                                              data_source_array, epoch_array,
                                              np.repeat(pd.NA, num_gt_edges),
                                              np.array(self.seg_labels)[ids],
                                              np.array(self.rel_labels)[ids]), axis=1)
                    data_all = np.concatenate((data_all, data_edges_gt))

        dataframe = pd.DataFrame(data_all)
        dataframe.columns = ["id_formula","id_item","type","data_source","epoch", 
                             "label_class_node", "label_class_seg", "label_class_rel"]
        dataframe = dataframe.astype(DatasetItem.get_types())
        self.dataframe = dataframe

