'''
Main DataLoader for Visual Parser (Math+Chem)
Document and Pattern Recognition Lab, RIT
Creator: Ayush Kumar Shah
Version 2: Abhisek Dey
Changes: Multi-Worker Batching + Queuing through iterable stream
'''

# RZ: Add debug operations
from src.utils.debug_fns import *
import src.utils.debug_fns as debug_fns
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

# import psutil
import re
import pickle
import gc
import sys
import traceback
from collections import Counter
import itertools
from collections import defaultdict
from logging import raiseExceptions
import os
from itertools import chain
import torch
import math
import numpy as np
import cv2
import random
import os.path as path
import matplotlib.pyplot as plt
from torch.utils import data
from torch.utils.data import IterableDataset, Dataset
from torch import distributed as dist
from protables.ProTables import tsv2pro, lg2pro, inkml2pro, lg2pro_sqlite

from src.utils.utils import updateEdgeLabel, get_ccs
from src.datasets.math_symbol import RELATIONS, INFRELATIONS, CHEM_RELATIONS,\
        get_image_params, add_trace, Box2Traces, check_for_merge
from src.features.Graph_construction import  GraphConstruction
from src.features.geometric_feature_extractor import getGeofeat
from lgap_settings import ERROR_FILES_CROHME, ERROR_FILES_INFTY
from src.image_ops.image2traces import image2strokes, get_minmax, imshow, scale_primitives
from src.image_ops.contour_utils import create_each_feature_contours, get_each_positional_encodings
from src.datasets.classes.DatasetItem import PrimNodeItem, SegNodeItem, EdgeItem, DatasetItem, Formula
from glob import glob
import random
from operator import itemgetter
from itertools import repeat
import pandas as pd
import copy

from src.utils.dprl_map_reduce import map_reduce, combine_dict_pair, map_concat, combine_dict_lists, parallel_progress
from src.datasets.dataset_utilities import get_names_formula, get_region_data, create_merged_queries
from multiprocessing import Manager
# from src.utils.postprocessing import postprocessGenORLg, create_output_graph, ORstring
from src.utils.postprocessing import create_output_graph, ORstring

import sqlitedict
from multiprocessing import Value

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None

def get_edges_data_from_formula_insolated(formula_object, id_formula, type_task):
    edge_tuples, edge_predicted_labels, edge_probs = [], [], []
            
    iterator_object = formula_object.dict_edges.values()        
    edges_set = set()

    for edge in iterator_object:
        if (edge.get_type_task() == type_task or edge.get_type_task() == "both") and edge.edge_tuple not in edges_set:
            edges_set.add(edge.edge_tuple)
            edge_tuples.append(edge.edge_tuple)
            last_edge = get_last_edge_insolated(formula_object, edge, id_formula)
            edge_predicted_labels.append(last_edge.get_predicted_value(type_task))            
            edge_probs.append(last_edge.get_predicted_probabilities(type_task))
    return edge_tuples, edge_predicted_labels, edge_probs

def get_nodes_data_from_formula_insolated(formula_object, id_formula):
    iterator_objects = formula_object.dict_nodes.values()
    node_instances, node_probs = [],[]
    contours = {}
    for node in iterator_objects:
        node_instances.append(node.instance_lg)
        node_probs.append(node.predicted_probabilities)
        if node.type_derivation == "primitive":
            contours[node.id] = node.contours
    return node_instances, node_probs, contours

def get_last_edge_insolated(formula_object, edge, id_formula):
    if edge.predicted_segmented_partner_id == -1:
        return edge
    else:
        return formula_object.dict_edges[edge.predicted_segmented_partner_id]


def process_formula(data):
    global CURR_COUNT, MAX_COUNT
    # dTimer = DebugTimer("Process Formula")
    self_dict, triple, protables_sqlite_path, server_mode = data
    formulas_dict = {}
    ( doc, page, region, merge_idx ) = triple
    protables_doc = sqlitedict.SqliteDict(protables_sqlite_path, flag='r')
    regionEntry = protables_doc[page][region]
    _, regionName, lg_path = get_names_formula(self_dict, doc, page, region)    

    region_data = get_region_data(self_dict, triple, regionName, lg_path,
                                  regionEntry)
    if region_data is None:
        return formulas_dict

    formulas_dict[regionName] = region_data
    protables_doc.close()
    if not server_mode:
        parallel_progress(CURR_COUNT, MAX_COUNT, "formulas processed")
    return formulas_dict

class MathSymbolDataset(Dataset):

    ################################################################
    # Built-ins (constructor, len() )
    ################################################################
    def __init__(self, dataset='', level="stk", find_punc=False, data_dir="data/train",
                 Img_list= "", lg_dir=None, graph="los", balance_edges=False,
                 symbol_height=64, image_primitive=False, traces_only=False,
                 symbol_table=None, rel_table=None, node_seg=False,
                 prune_seg=False, prune_rel=False, reduced_rel=False,
                 task='train', symContext=False, allAdj=False, symCrop=False,
                 INFmode='contour', geoFeat=False, format="tsv",
                 tsv_dir=None, dot_dir=None, inputRegionTypes=['c','S'],
                 oneRelLimit=True, lg_mode="contours", inputs="primitives", 
                 prune_num_NN=6, context_num_NN=4, extract_mst=True, gpu_memory_threshold=0.0155, 
                 shuffle=False, max_feat_size=64, rank=0, num_gpus=1,
                 model_dir="outputs/run/infty_contour/",
                 remove_opposite_edges=True, last_epoch=0,
                 feat_parallel=True, oversample_nodes=True,
                 oversample_segmentation=False, oversample_relation=False,
                 sample_nodes_uniform=True, sample_edges_uniform=True,
                 shared_lock=None, num_workers=4, sample_reduction=1,
                 chunk_size=1500,
                 #server data                
                 ids_server = None, lgs_all_server = None, server_mode = False):
        super(MathSymbolDataset, self).__init__()
        self.dTimer = DebugTimer("LGAP dataloader")

        self.feature_generation_parallel = feat_parallel
        self.num_workers = num_workers

        self.task=task
        self.dataset = dataset

        self.GRAPH=graph
        self.node_seg=node_seg # to check if seg represented by node or edges
        self.prune_seg = prune_seg # to prune the segmentation edge input space
        self.prune_rel = prune_rel # to prune the relationship edge input space
        self.reduced_rel = reduced_rel # to prune the relationship edge input space
        self.level=level
        self.find_punc = find_punc

        self.symbol_height = symbol_height
        self.image_primitive = image_primitive #if true no primitive attention
        self.traces_only = traces_only # if True no label will be returned
        self.balance_edges=balance_edges
        
        self.expr_graph={}
        self.symContext=symContext
        self.allAdj=allAdj
        self.symCrop=symCrop
        
        self.mode=INFmode
        self.geoFeat=geoFeat
        self.data_dir = data_dir
        self.format = format

        # AD Mod: Additional Parameters
        self.gpu_memory_threshold = gpu_memory_threshold
        self.shuffle = shuffle
        self.max_feat_size = max_feat_size
        self.rank = rank
        self.world_size = num_gpus

        self.symbols=[line.strip() for line in open(symbol_table)]
        
        self.inputRegionTypes = set( inputRegionTypes )
        self.remove_opposite_edges = remove_opposite_edges


        self.last_epoch = last_epoch
        self.curr_epoch = last_epoch

        self.currDoc = -1  #AD HACK Change to -1 if first index is 0 in dataset
        self.currPage = -1
        self.currRegion = 0
        self.currPageImg = -1  # RZ HACK: Avoding None data error from logger.
        
        self.server_mode = server_mode
        if self.server_mode:
            self.Img_list = ids_server
        else:
            self.Img_list = Img_list
        self.tsv = False
        self.lg = True

        self.oneRelLimit = oneRelLimit
        self.lg_mode = lg_mode
        self.inputs = inputs

        # number of nearest neighbors to consider for segmentation edge hypotheses
        self.prune_num_NN = prune_num_NN
        self.context_num_NN = context_num_NN
        self.extract_mst = extract_mst

        self.merge_dict = {}
        self.oversample_nodes = oversample_nodes
        self.oversample_segmentation = oversample_segmentation
        self.oversample_relation = oversample_relation

        self.sample_nodes_uniform = sample_nodes_uniform
        self.sample_edges_uniform = sample_edges_uniform
        self.sample_reduction = sample_reduction

        # Use common dir since features are same for all models
        task_dir = os.path.join(os.path.dirname(model_dir), "input_features", self.task)
        # self.feat_dir = os.path.join(task_dir, "query_features")
        # os.makedirs(self.feat_dir, exist_ok=True)        
        self.formula_level_dir = os.path.join(task_dir, "formula_metadata")
        os.makedirs(self.formula_level_dir, exist_ok=True)

        # self.memory = Memory(self.feat_dir, verbose=0, compress=True)
        excludeFiles = None

        self.chunk_size = chunk_size
        #region OLD CODE
        if self.format == 'tsv':
            self.tsv = True
            self.lg = False
            self.tsv_dir = tsv_dir
            self.data_files, self.proTableList, self.regionTriples, \
                    self.docs_page_sizes, self.currPageImg, self.lg \
                    = tsv2pro(self.tsv_dir, self.data_dir)
            if self.dataset == 'infty':
                self.rels=INFRELATIONS
            elif self.dataset == 'chem':
                self.rels = CHEM_RELATIONS

        elif self.format == 'lg':
            self.tsv = False
            self.lg = True
            self.lg_dir = lg_dir
            if self.dataset == 'infty' or self.dataset == 'chem':
                # self.tsv_dir = self.lg_dir
                self.dTimer.check("Initializations of classes and states")                
                if self.server_mode:                    
                    from_lg_list = True
                    lg_list = lgs_all_server
                else:
                    from_lg_list = False
                    lg_list = None

                self.protables_sqlite_path = os.path.join(self.formula_level_dir, "pro_table.sqlite")
                self.formulas_dict_sqlite_path = os.path.join(self.formula_level_dir, "formulas_dict.sqlite")
                if os.path.exists(self.protables_sqlite_path) \
                        and os.path.exists(os.path.join(self.formula_level_dir, "regionTriples.pkl")):
                    self.currPageImg = None
                    if excludeFiles:
                        self.data_files = list(filter(lambda x: x not in excludeFiles, tsv_files))
                    else:
                        self.data_files = self.Img_list
                    if not from_lg_list:
                        image_path = os.path.join(self.data_dir, path.splitext(self.data_files[0])[0].\
                                                        replace("_line", "").replace("_pdf", "") + ".PNG")
                        if os.path.exists(image_path):
                            self.currPageImg = cv2.imread(image_path, 0) / 255.0
                    self.lg = True
                    # self.proTableList = self.read_feature_from_disk(os.path.join(self.formula_level_dir, "proTableList.pkl"))
                    # self.proTableList = [sqlitedict.SqliteDict(self.protables_sqlite_path)]
                    self.regionTriples = self.read_feature_from_disk(os.path.join(self.formula_level_dir, "regionTriples.pkl"))
                    self.dTimer.check(f"Load PROTables from {self.formula_level_dir}")
                else:
                    self.data_files, self.regionTriples, \
                            self.currPageImg, self.lg = lg2pro_sqlite(self.lg_dir, self.data_dir,
                                                                      self.Img_list,
                                                                      sqlite_db_path=os.path.join(self.formula_level_dir, "pro_table.sqlite"), 
                                                                      mode=self.lg_mode, inputs=inputs, from_lg_list=from_lg_list,
                                                                      lg_list=lg_list,
                                                                      server_mode=server_mode)
                    # Save proTableList and regionTriples to file in
                    # input_features directory
                    # self.write_feature_to_disk(self.proTableList, os.path.join(self.formula_level_dir, "proTableList.pkl"))
                    self.write_feature_to_disk(self.regionTriples, os.path.join(self.formula_level_dir, "regionTriples.pkl"))
                    self.dTimer.check(f"Build and write PROTables from input files (lg/tsv) to {self.formula_level_dir}")

                # self.get_symbol_dist()
                # self.dTimer.check("Build PROTables from input files (lg/tsv)")
                if self.dataset == "chem":
                    if self.currPageImg is not None:
                        _, self.currPageImg = cv2.threshold(self.currPageImg, 0.7, 1, cv2.THRESH_BINARY)
                    # self.currPageImg = 1. - self.currPageImg
                if self.dataset == 'infty':
                    self.rels=INFRELATIONS
                else: # Chem Data
                    self.rels=CHEM_RELATIONS

                self.symbols_dict = {s:i for i, s in enumerate(self.symbols)}
                self.rels_dict = {s:i for i, s in enumerate(self.rels)}

                
                self.store_data_items()
                # self.dTimer.check("Create and store or read features")

            elif dataset == 'crohme':
                # self.tsv_dir = data_dir
                self.data_files, self.proTableList, self.regionTriples, \
                        self.currPageImg, self.lg = inkml2pro(self.data_dir,
                                                              self.Img_list,
                                                              task=self.task,
                                                              excludeFiles=ERROR_FILES_CROHME,
                                                              )
                self.rels=RELATIONS
            self.docs_page_sizes = []
        else:
            raise ValueError('Dataset format should either be tsv or lg!')

        # TODO: Remove hard code
        # self.regionTriples = [(*regionTriple, 0) for regionTriple in
        #                       self.regionTriples]
        if self.format == "lg":
            self.file2triples = dict(zip(self.data_files, np.array(self.regionTriples)))

        # AKS: If empty formula regions
        if len(self.regionTriples) == 0:
            print(">> Warning: No formula regions present in:")
            print("   " + str(self.data_files))
            self.data_files = []

        # Set up class dictionaries (one entry per symbol/relation class)
        self.symbols_dict = {s:i for i, s in enumerate(self.symbols)}
        self.rels_dict = {s:i for i, s in enumerate(self.rels)}
        self.data_files = np.array(self.data_files)
        self.regionTriples = np.array(self.regionTriples)
        # LOCK for accessing shared memory
        self.shared_lock = shared_lock

    def get_symbol_dist(self, write=True):
        formulas = self.proTableList[0]
        num_formulas = len(formulas)
        all_sym_labels = [[formulas[j][0][1][i][4] for i in range(len(formulas[j][0][1]))]
                          for j in range(num_formulas)]
        all_sym_labels = list(itertools.chain(*all_sym_labels))
        sym_counts = Counter(all_sym_labels)
        self.sym_dist = sym_counts.most_common()
        total = sum(sym_counts.values())
        if write:
            # Writing the sorted frequency data to a readable text file
            with open(os.path.join(self.formula_level_dir, 
                                   "symbols_distribution.csv"), 'w') as file:
                file.write("S.N.,Symbol,Count,Percentage\n")
                for i, (char, count) in enumerate(self.sym_dist, start=1):
                    file.write(f"{i},{char},{count},{count/total*100:.2f}\n")
                file.write(f"*,Total,{total},100.00")

    def plot_smiles_dist(self, dataset="Pubchem", xlabel="LG Symbol Labels"):
        # Plotting the frequency distribution
        # Sort the frequency by decreasing order
        sorted_frequency = Counter(self.sym_dist).most_common()

        # Extracting characters and counts for plotting
        sorted_characters = list(map(itemgetter(0), sorted_frequency))
        sorted_counts = list(map(itemgetter(1), sorted_frequency))

        plt.figure(figsize=(15, 8))
        plt.bar(sorted_characters, sorted_counts, color='skyblue')
        plt.xlabel(xlabel)
        plt.ylabel('Frequency')
        plt.title(f'{xlabel} Frequency Distribution - {dataset}')
        plt.show()

    def store_data_items(self):
        global CURR_COUNT, MAX_COUNT
        formulas_dict_path = os.path.join(self.formula_level_dir, "formulas_dict.ptk")
        treasure_path = os.path.join(self.formula_level_dir, "treasure.csv")
        # if os.path.exists(formulas_dict_path) and os.path.exists(treasure_path):
        if os.path.exists(self.formulas_dict_sqlite_path) and os.path.exists(treasure_path):
            # print(f"Formulas metadata loaded from {self.formula_level_dir}")
            # self.formulas_dict = self.read_feature_from_disk(formulas_dict_path)
            # self.formulas_dict = sqlitedict.SqliteDict(self.formulas_dict_sqlite_path)
            # self.dTimer.qcheck("Load formula dict")
            self.treasure = pd.read_csv(treasure_path,
                                        index_col=0, dtype=DatasetItem.get_types())
            # self.treasure = pd.read_parquet(treasure_path, columns=DatasetItem.get_types())
            self.dTimer.check(f"Load formula metadata from {self.formula_level_dir}")
        else:
            print(f"Generating formulas metadata...\n")
            # self.dTimer.check("Start")
            regionTriples = self.regionTriples        
            track_dict = {}
            for triple in regionTriples: 
                if (not triple[:3] in track_dict) or (triple[:3] in track_dict and triple[3] == 1):
                    track_dict[triple[:3]] = triple
            regionTriplesFiltered = list(track_dict.values())
            
            
            class_dict = self.__dict__
            needed = ['GRAPH','balance_edges','currDoc',
                    'currPage','currRegion','data_dir','data_files',
                    'dataset','find_punc','format',
                    'inputRegionTypes','level','lg','lg_dir',
                    'node_seg', 'prune_num_NN','prune_rel','context_num_NN',
                    'prune_seg','rels_dict', 'remove_opposite_edges',
                    'symbols_dict', 'task']
            new_dict = {key:class_dict[key] for key in needed}
            
            formulas_dict = {}  
            formulas_dict_sqlite = sqlitedict.SqliteDict(self.formulas_dict_sqlite_path,
                                                          autocommit=True)
            treasure_list = []
            # treasure = pd.DataFrame(DatasetItem.get_scheme())    
            # print(f"parallel={self.feature_generation_parallel}")
            
            # process = psutil.Process()
            # self.dTimer.check(f"MIO init parallel")
            if self.feature_generation_parallel:
                # print(f"Memory storage before metadata generation = {process.memory_info().rss/(10**6)} MB")
                # regionEntries = [self.proTableList[doc][page][region] for doc, page, region, _ 
                #                  in regionTriplesFiltered]
                data_list = list(zip(repeat(new_dict), regionTriplesFiltered,
                                     repeat(self.protables_sqlite_path),
                                     repeat(self.server_mode)))
                data_sublists = [data_list[i:i+self.chunk_size] for i in range(0, len(data_list), 
                                                                          self.chunk_size)]
                # self.dTimer.check(f"end split")
                # print("Check before process formula")
                for i, data_sublist in enumerate(data_sublists):
                    # print(f"Memory storage before metadata generation chunk {i} = {process.memory_info().rss/(10**6)} MB")
                    MAX_COUNT = len(data_sublist)
                    print(f"Processing Chunk {i+1}/{math.ceil(len(data_list) / self.chunk_size)} ...")
                    formulas_data = map_reduce(process_formula, combine_dict_pair,
                                               data_sublist, cpu_count=self.num_workers)
                    CURR_COUNT.value = 0
                                            #    data_sublist, cpu_count=self.num_workers )
                    # print(f"Processed Chunk {i+1}/{math.ceil(len(data_list) / self.chunk_size)} ...")
                    for item in formulas_data.values():
                        # treasure = pd.concat([treasure,item.dataframe])
                        if item is not None:
                            treasure_list.append(item.dataframe)
                            item.treasure = None
                    # formulas_dict.update(formulas_data)
                    formulas_dict_sqlite.update(formulas_data)
                    # print(f"Chunk {i} formula dict updated")
                    gc.collect()  # Collect garbage to free memory
                    # print(f"Memory storage after metadata generation chunk {i} = {process.memory_info().rss/(10**6)} MB")
                # self.dTimer.check(f"end map reduce")
                # print("Check after process formula")
                # print(f"Memory storage after metadata generation completed = {process.memory_info().rss/(10**6)} MB")
                
            else:                       
                for idx, triple in enumerate(regionTriplesFiltered):                  
                    doc, page, region, _ = triple                
                    # self.dTimer.check(f"start process formula")
                    # formula_dict = process_formula((new_dict, triple, self.proTableList[doc][page][region]))                
                    formula_dict = process_formula((new_dict, triple,
                                                    self.protables_sqlite_path,
                                                    self.server_mode))
                    # self.dTimer.check(f"end process formula")
                    # formulas_dict = combine_dict_pair(formulas_dict,formula_dict)
                    formulas_dict_sqlite.update(formula_dict)
                    # treasure = pd.concat((treasure, list(formula_dict.values())[0].dataframe))
                    treasure_list.append(list(formula_dict.values())[0].dataframe)
                    # list(formula_dict.values())[0].dataframe = None                                
                    gc.collect()  # Collect garbage to free memory

            formulas_dict_sqlite.close()
            # Concatenate treasure DataFrames once at the end
            treasure = pd.concat(treasure_list)
            # self.formulas_dict = formulas_dict_sqlite
            
            # int_columns = {"label_class_node", "label_class_seg", "label_class_rel", 
            #                "id_item", "epoch", "predicted_value"}
            # for col in int_columns:
            #     treasure[col] = treasure[col].astype('Int64')
            # for col in set(treasure.columns) - int_columns:
            #     treasure[col] = treasure[col].astype('string')            
            # self.treasure = treasure.reset_index().drop(columns=['index'])
            self.treasure = treasure.reset_index(drop=True)
            self.dTimer.check("Formula metadata generated")
            # self.write_feature_to_disk(formulas_dict, os.path.join(self.formula_level_dir,
            #                                         "formulas_dict.ptk"))
            # self.dTimer.check("Save dict")
            self.treasure.to_csv(os.path.join(self.formula_level_dir, "treasure.csv"))
            # self.treasure.to_parquet(treasure_path, compression='gzip')
            self.dTimer.qcheck("Save treasure")
            self.dTimer.check(f"Formula metadata saved at {self.formula_level_dir}")
        
        self.symbol_class_counts = self.compute_class_counts("label_class_node", len(self.symbols))
        self.rel_class_counts = self.compute_class_counts("label_class_rel", len(self.rels))
        self.seg_class_counts = self.compute_class_counts("label_class_seg", 2)
        
        self.indexes_features_in_treasure = list(self.treasure.index)                

        print(f"Original number of instances = {len(self.indexes_features_in_treasure)}")
        # print("\n\nOversampling turned off, TURN IT BACK ON AGAIN\n\n")
        if self.task == "train":
            if self.sample_nodes_uniform:
                self.sample_nodes()
            if self.sample_edges_uniform:
                self.sample_edges()
            self.oversample_data()

    def __len__(self):
        return len(self.indexes_features_in_treasure)

    def compute_class_counts(self, column_name, num_classes):
        """
        Compute class counts for a specified column.

        Args:
            column_name (str): The name of the column for which to compute class counts.

        Returns:
            pd.Series: A pandas Series containing the class counts sorted by class.
        """
        if column_name not in self.treasure.columns:
            raise ValueError(f"Column '{column_name}' does not exist in the DataFrame.")

        # Drop NaN values, convert to integer, and compute class counts
        class_counts = self.treasure[column_name].dropna().astype(int).value_counts().sort_index()
       # Create a list of counts with indices from 0 to num_classes - 1
        counts_list = [class_counts.get(i, 0) for i in range(num_classes)]
        return counts_list

    def sample_nodes(self):
        # Function to balance a subset of the DataFrame
        def balance_subset(subset_df, sample_reduction=self.sample_reduction):
            class_counts = subset_df['label_class_node'].value_counts()
            total_samples = len(subset_df)
            num_classes = len(class_counts)
            desired_samples_per_class = int((total_samples * sample_reduction) // num_classes)

            balanced_indices = []

            for label_class, count in class_counts.items():
                class_indices = subset_df[subset_df['label_class_node'] == label_class].index
                if count > desired_samples_per_class:
                    sampled_indices = np.random.choice(class_indices, desired_samples_per_class, replace=False)
                else:
                    sampled_indices = np.random.choice(class_indices, desired_samples_per_class, replace=True)
                balanced_indices.extend(sampled_indices)

            return balanced_indices
        
        # Step 1: Filter the dataset to include only node types
        node_df = self.treasure[self.treasure['type'] == 'node']
        # edge_df = self.treasure[self.treasure['type'] == 'edge']
        # import pdb; pdb.set_trace()
        
        # Step 2: Split the DataFrame based on type_derivation
        segmentation_df = node_df[node_df['type_derivation'] == 'segmentation']
        primitive_df = node_df[node_df['type_derivation'] == 'primitive']

        # Step 3: Balance each subset
        segmentation_indices = balance_subset(segmentation_df)
        primitive_indices = balance_subset(primitive_df)

        # Step 4: Combine the indices and sort
        balanced_indices = sorted(segmentation_indices + primitive_indices)
        self.indexes_features_in_treasure = balanced_indices

    def sample_edges(self):
        # Define categories
        def categorize_edge(row):
            char_pattern = r'^[A-Za-z0-9]$'
            other_pattern = r'^SW|\+|\-$'

            if row['parent_str'] == 'Single':
                parent_cat = 'Single'
            elif re.match(char_pattern, row['parent_str']):
                parent_cat = 'Character'
            elif re.match(other_pattern, row['parent_str']):
                parent_cat = 'Others'
            else:
                parent_cat = 'Others'
                
            if row['child_str'] == 'Single':
                child_cat = 'Single'
            elif re.match(char_pattern, row['child_str']):
                child_cat = 'Character'
            elif re.match(other_pattern, row['child_str']):
                child_cat = 'Others'
            else:
                child_cat = 'Others'
                
            category = tuple(sorted([parent_cat, child_cat]))
            return f'{category[0]} - {category[1]}'

        # Function to balance a subset of the DataFrame
        def balance_subset(subset_df, sample_reduction=self.sample_reduction):
            category_counts = subset_df['category'].value_counts()
            total_samples = len(subset_df)
            num_categories = len(category_counts)
            desired_samples_per_category = int((total_samples * sample_reduction) // num_categories)
            balanced_indices = []

            for category, count in category_counts.items():
                category_indices = subset_df[subset_df['category'] == category].index
                if count > desired_samples_per_category:
                    sampled_indices = np.random.choice(category_indices, desired_samples_per_category, replace=False)
                else:
                    sampled_indices = np.random.choice(category_indices, desired_samples_per_category, replace=True)
                balanced_indices.extend(sampled_indices)

            return balanced_indices

        # Filter the dataset to include only edge types
        edge_df = self.treasure[self.treasure['type'] == 'edge']
        noseg_df = edge_df[edge_df.label_class_seg == 1]
        seg_df = edge_df[edge_df.label_class_seg == 0]

        # Apply categorization
        noseg_df.loc[:, 'category'] = noseg_df.apply(categorize_edge, axis=1)

        # Split the DataFrame based on type_derivation
        segmentation_df = noseg_df[noseg_df['type_derivation'] == 'segmentation']
        primitive_df = noseg_df[noseg_df['type_derivation'] == 'primitive']

        # Balance each subset
        segmentation_indices = balance_subset(segmentation_df)
        primitive_indices = balance_subset(primitive_df)

        # Combine and sort indices
        balanced_indices = sorted(list(seg_df.index) + segmentation_indices + primitive_indices)
        self.indexes_features_in_treasure += balanced_indices

    def oversample_data(self):
        treasure = self.treasure.iloc[self.indexes_features_in_treasure]
        padding_seg_len = 0
        padding_rel_len = 0
        if self.oversample_relation:
            positive_rel_idx = list(treasure[treasure.label_class_rel == 1].index)
            negative_rel_idx = list(treasure[treasure.label_class_rel == 0].index)
            if len(positive_rel_idx) < len(negative_rel_idx):
                # print(f"Pos rel = {len(positive_rel_idx)}")
                # print(f"Neg rel = {len(negative_rel_idx)}")
                sampling_size = round(1 * (len(negative_rel_idx) - len(positive_rel_idx)))
                padding_rel = np.random.choice(positive_rel_idx, size=sampling_size,
                                           replace=True).tolist()
                self.indexes_features_in_treasure += padding_rel
                # padding_seg_len = len(padding_seg)
                print(f"Oversampled number of positive relations = {len(padding_rel)}")
            elif len(positive_rel_idx) > len(negative_rel_idx):
                # print(f"Pos rel = {len(positive_rel_idx)}")
                # print(f"Neg rel = {len(negative_rel_idx)}")
                sampling_size = round(1 * (len(positive_rel_idx) - len(negative_rel_idx)))
                padding_rel = np.random.choice(negative_rel_idx, size=sampling_size,
                                           replace=True).tolist()
                self.indexes_features_in_treasure += padding_rel
                # padding_seg_len = len(padding_seg)
                print(f"Oversampled number of negative relations = {len(padding_rel)}")

        if self.oversample_segmentation:
            # positive_rel_idx = list(treasure[treasure.label_class_rel == 1].index)
            positive_seg_idx = list(treasure[treasure.label_class_seg == 0].index)
            negative_seg_idx = list(treasure[treasure.label_class_seg == 1].index)
            if len(positive_seg_idx) < len(negative_seg_idx):
                # print(f"Pos seg = {len(positive_seg_idx)}")
                # print(f"Neg seg = {len(negative_seg_idx)}")
                sampling_size = round(0.1 * (len(negative_seg_idx) - len(positive_seg_idx)))
                padding_seg = np.random.choice(positive_seg_idx, size=sampling_size,
                                           replace=True).tolist()            
                self.indexes_features_in_treasure += padding_seg
                # padding_seg_len = len(padding_seg)
                print(f"Oversampled number of positive segmentations = {len(padding_seg)}")

        if self.oversample_nodes:
            nodes_ids = list(treasure[treasure.type == "node"].index)
            edge_ids = list(treasure[treasure.type == "edge"].index)          
            if len(nodes_ids) < len(edge_ids):
                padding = np.random.choice(nodes_ids, 
                                           size=int(len(edge_ids) + padding_seg_len - len(nodes_ids)),
                                           replace=True).tolist()            
                self.indexes_features_in_treasure += padding   
                print(f"Oversampled number of nodes = {len(padding)}")

    def get_all_formulas_ids(self):
        return list(self.formulas_dict.keys())
        
    def get_nodes_data_from_formula(self, id_formula):
        iterator_objects = self.formulas_dict[id_formula].dict_nodes.values()
        node_instances, node_probs = [],[]
        contours = {}
        for node in iterator_objects:
            node_instances.append(node.instance_lg)
            node_probs.append(node.predicted_probabilities)
            if node.type_derivation == "primitive":
                contours[node.id] = node.contours
        return node_instances, node_probs, contours

    def get_edges_data_from_formula(self, id_formula, type_task):
        edge_tuples, edge_predicted_labels, edge_probs = [], [], []
                
        iterator_object = self.formulas_dict[id_formula].dict_edges.values()        
        edges_set = set()

        for edge in iterator_object:
            if (edge.get_type_task() == type_task or edge.get_type_task() == "both") and edge.edge_tuple not in edges_set:
                edges_set.add(edge.edge_tuple)
                edge_tuples.append(edge.edge_tuple)
                last_edge = self.get_last_edge(edge, id_formula)
                edge_predicted_labels.append(last_edge.get_predicted_value(type_task))            
                edge_probs.append(last_edge.get_predicted_probabilities(type_task))
        return edge_tuples, edge_predicted_labels, edge_probs

    def get_last_edge(self, edge, id_formula):
        if edge.predicted_segmented_partner_id == -1:
            return edge
        else:
            return self.formulas_dict[id_formula].dict_edges[edge.predicted_segmented_partner_id]

    def set_predicted_data_batch(self, ids_treasure, all_probabilities, all_predicted_labels):
        (symProb, segProb, relProb) = all_probabilities
        (node_labels_pred, seg_labels_pred, rel_labels_pred) = all_predicted_labels
        
        offset_nodes = 0
        offset_rels = 0
        offset_segs = 0
        
        formulas_with_segmentation = set()
        completed_formulas = set()
        last = ""
        for id_treasure in ids_treasure:
            id_treasure = int(id_treasure)            
            row_data = self.treasure.iloc[id_treasure]            
            completed_formulas.add(row_data.id_formula)
            last = row_data.id_formula
            if row_data.type == "node":
                dataset_item = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item]
                predicted_value = node_labels_pred[offset_nodes]                
                predicted_probabilities = symProb[offset_nodes]
                offset_nodes+=1
            elif row_data.type == "edge":                
                dataset_item = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item]            
                predicted_value = {}
                predicted_probabilities = {}
                if dataset_item.get_type_task() == "rel" or dataset_item.get_type_task() == "both":
                    predicted_value["rel"] = rel_labels_pred[offset_rels]
                    predicted_probabilities["rel"] = relProb[offset_rels]
                    offset_rels+=1
                if dataset_item.get_type_task() == "seg" or dataset_item.get_type_task() == "both":
                    predicted_value["seg"] = seg_labels_pred[offset_segs]
                    predicted_probabilities["seg"] = segProb[offset_segs]                    
                    if seg_labels_pred[offset_segs] == 0:
                        
                        node1_id, node2_id = dataset_item.edge_tuple
                        node1 = self.formulas_dict[row_data.id_formula].dict_nodes[node1_id]
                        node2 = self.formulas_dict[row_data.id_formula].dict_nodes[node2_id]
                        
                        merged_contours_ids = set(node1.contours_ids).union(set(node2.contours_ids))                        
                        if merged_contours_ids != set(node1.contours_ids):
                            node1.contours_ids = merged_contours_ids
                            node2.contours_ids = merged_contours_ids                            
                            merged_contours = node1.contours+node2.contours
                            node1.contours = merged_contours
                            node2.contours = merged_contours
                            formulas_with_segmentation.add(row_data.id_formula)
                            self.formulas_dict[row_data.id_formula].segmented_nodes_ids.append( node1_id)
                            self.formulas_dict[row_data.id_formula].segmented_nodes_ids.append( node2_id)
                            
                          
                    offset_segs+=1                                                                   
            
            # self.treasure.at[id_treasure,"predicted_value"] = predicted_value
            dataset_item.predicted_value = predicted_value
            dataset_item.predicted_probabilities = predicted_probabilities        
        temp = set()
        temp.add(last)
        return formulas_with_segmentation, completed_formulas-temp

    def get_contours_map(self, id_treasure):
        contours_object = self.get_contours_object(id_treasure)
        contours_context_object = self.get_contours_context_object(id_treasure)

        # COMENTED FOR NO GAT
        # contours_neighbors_object = self.get_contours_neighbors_object(id_treasure)

        combined_contours = combine_dict_pair(contours_object, contours_context_object)        

        # COMENTED FOR NO GAT
        # for neighboor in contours_neighbors_object:
        #     combined_contours = combine_dict_pair(combined_contours, neighboor)
        
        metadata = {
            "query": np.unique(list(contours_object.keys())).tolist(),
            "context": np.unique(list(contours_context_object.keys())).tolist(),

            # COMENTED FOR NO GAT
            # "neighbors": [np.unique(list(i.keys())).tolist() for i in contours_neighbors_object]
        }
        return metadata, combined_contours        
    
    def get_label_class(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].label_string
        elif row_data.type == "edge": 
            return self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].edge_tuple
        else:
            return None
    
    def get_label(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].label_class
        elif row_data.type == "edge": 
            return self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].get_label_classes()
        else:
            return None
 
    def get_label_string(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].label_string
        elif row_data.type == "edge": 
            return self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].label_string
        else:
            return None

    def get_contours_with_id(self, id_formula, id_item):
        return {id_item:self.formulas_dict[id_formula].dict_nodes[id_item].contours}

    def get_node_ids_edge(self, id_formula, id_item):
        return self.formulas_dict[id_formula].dict_edges[id_item].edge_tuple    

    def get_contours_node(self, row_data, segmented_version=False, predicted_segmentation=False):
        if row_data.type_derivation == "primitive":
            if predicted_segmentation:
                segmented_partner_id = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].predicted_segmented_partner_id
            else:
                segmented_partner_id = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].ground_truth_segmented_partner_id

        #             self.ground_truth_segmented_partner_id = -1
        # self.predicted_segmented_partner_id = -1
            if not segmented_version or segmented_partner_id == -1:
                return self.get_contours_with_id(row_data.id_formula, row_data.id_item)
            else:                                
                contours_ids = self.formulas_dict[row_data.id_formula].dict_nodes[segmented_partner_id].contours_ids
                contours = {}
                for prim_contour_id in contours_ids:
                    contours = combine_dict_pair(contours,self.get_contours_with_id(row_data.id_formula, prim_contour_id)) 
                return contours
        elif row_data.type_derivation == "segmentation":            
            contours_ids = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].contours_ids                        
            contours = {}
            for prim_contour_id in contours_ids:
                contours = combine_dict_pair(contours,self.get_contours_with_id(row_data.id_formula, prim_contour_id)) 
            return contours
        else:
            return None

    def get_contours_edge(self, row_data):
        is_edge_from_segmentation = row_data.type_derivation == "segmentation"        
        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].is_predicted_segmentation
    
        parent, child = self.get_node_ids_edge(row_data.id_formula, row_data.id_item)
        row_parent = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == parent) & (self.treasure.type=="node")].iloc[0]                
        contours_parent = self.get_contours_node(row_parent, segmented_version=is_edge_from_segmentation, predicted_segmentation=is_predicted_segmentation)
        row_child = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == child) & (self.treasure.type=="node")].iloc[0]                
        contours_child = self.get_contours_node(row_child, segmented_version=is_edge_from_segmentation, predicted_segmentation=is_predicted_segmentation)          
        return combine_dict_pair( contours_parent,  contours_child)
                       
    def get_contours_context_node(self, row_data):
        context_ids = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].context_ids
        
        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].is_predicted_segmentation

        contours = {}        
        for id in context_ids:
            row_data_neighbor = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == id) & (self.treasure.type=="node")].iloc[0]            
            contours = combine_dict_pair(contours, self.get_contours_node(row_data_neighbor, segmented_version=False,
                                                                           predicted_segmentation=is_predicted_segmentation))
        return contours

    def get_contours_context_edge(self, row_data):
        
        context_ids = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].context_ids

        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].is_predicted_segmentation
        
        contours = {}
        for id in context_ids:            
            row_data_neighbor = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == id) & (self.treasure.type=="node")].iloc[0]
            # contours = combine_dict_pair(contours, self.get_contours_node(row_data_neighbor, segmented_version=False))           
            contours = combine_dict_pair(contours, self.get_contours_node(row_data_neighbor, segmented_version=False, 
                                                                          predicted_segmentation=is_predicted_segmentation))
        return contours
        
    def get_contours_context_object(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.get_contours_context_node(row_data)
        elif row_data.type == "edge":            
            return self.get_contours_context_edge(row_data)
        else:
            return None
    
    def get_contours_object(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.get_contours_node(row_data)
        elif row_data.type == "edge":                        
            return self.get_contours_edge(row_data)
        else:
            return None 

    def get_contours_neighbors_object(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        if row_data.type == "node":
            return self.get_contours_neighbors_node(row_data)    
        elif row_data.type == "edge":            
            return self.get_contours_neighbors_edge(row_data)
        else:
            return None 
        
    def get_contours_neighbors_node(self, row_data):
        neighbors_ids = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].neighbors_ids  
        
        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].is_predicted_segmentation

        contours = []
        for id in neighbors_ids:
            row_data_neighbor = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == id) & (self.treasure.type=="node")].iloc[0]                        
            contours.append(self.get_contours_node(row_data_neighbor, segmented_version=row_data.type_derivation=="segmentation",
                                                   predicted_segmentation=is_predicted_segmentation))
        return contours
    
    def get_contours_neighbors_edge(self, row_data):
        contours = []        
        neighbors_ids = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].neighbors_ids

        is_predicted_segmentation = self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].is_predicted_segmentation

        for id in neighbors_ids:
            row_data_neighbor = self.treasure[(self.treasure.id_formula == row_data.id_formula) & (self.treasure.id_item == id) & (self.treasure.type=="node")].iloc[0]
            contours.append(self.get_contours_node(row_data_neighbor, segmented_version=row_data.type_derivation=="segmentation",
                                                   predicted_segmentation=is_predicted_segmentation))
        return contours

    def get_filename(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]
        try:
            if row_data.type == "node":            
                return self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item].get_filename()            
            elif row_data.type == "edge":
                return self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item].get_filename()        
            else:
                return None
        except:
            import traceback
            print(traceback.print_exc())
            print(f"type = {type(row_data.id_formula)}")
            print(f"Keys = {self.formulas_dict.keys()}")

    def get_expression_graph_from_formula(self, id_formula):
        return self.formulas_dict[id_formula].expression_graph

    def formula_has_predicted_segmentations(self, id_formula):
        asdas = self.formulas_dict[id_formula]

    def get_edge_identifier(self, id_treasure):
        row_data = self.treasure.iloc[id_treasure]                    
        return self.formulas_dict[row_data.id_formula].dict_segs[row_data.id_item].edge_tuple

    def flush_formula(self, id_formula):
        ids_to_remove = self.treasure[self.treasure.id_formula == id_formula].index
        # del self.formulas_dict[id_formula]
        self.indexes_features_in_treasure = list(set(self.indexes_features_in_treasure) - set(ids_to_remove))        

    def flush_from_ids(self, ids_remove_from_treasure):
        for id_treasure in ids_remove_from_treasure:
            row_data = self.treasure.iloc[id_treasure]
            if row_data.type_derivation == "segmentation":
                if row_data.type == "node" and row_data.id_item in self.formulas_dict[row_data.id_formula].dict_nodes:                    
                    del self.formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item]
                elif row_data.id_item in self.formulas_dict[row_data.id_formula].dict_edges:                    
                    del self.formulas_dict[row_data.id_formula].dict_edges[row_data.id_item]

    def create_objects_from_segmentation(self, formula_to_edges_map, epoch_number):
        # formula_to_edges_map = {
        #     "<formula id>":[
        #         ((<node id1>,<node id2>),<id_in_treasure>),
        #         ((<node id1>,<node id2>),<id_in_treasure>),
        #         ...
        #     ]
        #     ...
        # }
        testing_mode = self.task == "test"        
        table = pd.DataFrame(DatasetItem.get_scheme())    

        if testing_mode:
            formulas_with_segmentation = set()
        # dTimer.qcheck("Start")
        if self.feature_generation_parallel:
            # input_params = list(zip(formula_to_edges_map.items(), repeat(epoch_number)))
            # formulas_dict, tables = map_reduce(self.create_merged_queries, combine_dict_lists,
            #                                    input_params)
            # dTimer.qcheck("Parallel")
            input_params = [(formula_id, list_edges, self.formulas_dict[formula_id],
                               self.treasure, testing_mode, epoch_number) for formula_id,
                      list_edges in formula_to_edges_map.items()]
            # input_params = list(zip(formula_to_edges_map.items(), repeat(epoch_number)))
            formulas_dict, tables = map_reduce(create_merged_queries, combine_dict_lists,
                                               input_params)
            # dTimer.qcheck("Parallel")
        else:
            tables = []
            formulas_dict = {}
            # for formula_id_edges in formula_to_edges_map.items():
            #     formula_dict, table = self.create_merged_queries((formula_id_edges, epoch_number))
            #     tables.append(table[0])
            #     formulas_dict.update(formula_dict)
            for formula_id, list_edges in formula_to_edges_map.items():
                formula_dict, table = create_merged_queries((formula_id, list_edges,
                                                             self.formulas_dict[formula_id],
                                                             self.treasure, epoch_number))
                tables.append(table[0])
                formulas_dict.update(formula_dict)
            # dTimer.qcheck("Series")
        # print(dTimer)
        
        self.formulas_dict.update(formulas_dict)
        
        offset_index = max(self.treasure.index)+1
        new_tables = pd.concat(tables, ignore_index=True)
        new_tables.index = new_tables.index+offset_index
        self.treasure = pd.concat([self.treasure, new_tables])
        
        candidates_indexes_features_in_treasure = list(self.treasure[(self.treasure.epoch == 0) | 
                                                   (self.treasure.epoch == epoch_number)].index)
        if testing_mode:            
            remove_all = set(chain(*[formula_value.ids_remove_from_treasure_for_testing 
                          for formula_value in formulas_dict.values()]))
            self.indexes_features_in_treasure = list(set(candidates_indexes_features_in_treasure)\
                                                     -remove_all)
        else:            
            to_remove = self.treasure[(self.treasure.epoch != 0) & (self.treasure.epoch != epoch_number)].index
            # self.flush_from_ids(to_remove)
            self.treasure = self.treasure.drop(to_remove).reset_index().drop(columns=['index'])          
            self.indexes_features_in_treasure = list(self.treasure.index)

        if self.task == "train":
            self.sample_nodes()
            self.oversample_edges()
            self.oversample_data()
        formulas_with_segmentation = {formula_id for formula_id, value in
                                      self.formulas_dict.items() if value.hasSegmentation}
        return formulas_with_segmentation


    def compare_formula_dicts(self, dict1, dict2):
        if dict1.keys() != dict2.keys():
            return False
        for key in dict1:
            obj1 = dict1[key]
            obj2 = dict2[key]
            if not self.compare_objects(obj1, obj2):
                return False
        return True
    
    def compare_objects(self, obj1, obj2):
        if obj1.__dict__.keys() != obj2.__dict__.keys():
            return False
        for attr in obj1.__dict__:
            if attr == "expression_graph" or attr == "contours":
                continue
            if np.any(obj1.__dict__[attr] != obj2.__dict__[attr]):
                return False
        return True

    def read_feature_from_disk(self, path):
        # Implement this method to read the feature from the disk
        with open(path, "rb") as fi:
            return pickle.load(fi)
    
    def write_feature_to_disk(self, feature_dict, feature_path):
        with open(feature_path, "wb") as fo:
            pickle.dump(feature_dict, fo, protocol=pickle.HIGHEST_PROTOCOL)    

    def custom_collate_fn(self, batch):
        if self.task != "test":            
            return self.custom_collate_fn_old(batch)
        else:
            return np.stack(batch, axis=0)            

    def custom_collate_fn_old(self, batch):
        # Initialize containers for batched data
        dTimer_item = DebugTimer("collate")
        batched_feature_vectors = []
        batched_pos_enc = []
        batched_ids = []
        batched_id_formulas = []
        batched_labels = []
        batched_label_classes = []
        batched_type_features = []

        # Process each item in the batch
        for item in batch:
            batched_feature_vectors.append(item["feature_vector"])
            batched_ids.append(item["id"])
            batched_id_formulas.append(item["id_formula"])
            batched_labels.append(item["label"])
            batched_label_classes.append(item["label_class"])
            batched_type_features.append(item["type_feature"])
            batched_pos_enc.append(item["positional_encoding"])
        dTimer_item.qcheck("looop")
        # Convert lists to the appropriate formats
        batched_feature_vectors = torch.stack(batched_feature_vectors, dim=0)  # Stack tensors
        batched_pos_enc = torch.FloatTensor(np.array(batched_pos_enc))
        # The rest remain lists as they contain strings or dicts which don't need tensor conversion
        dTimer_item.qcheck("finish")
        # print(dTimer_item)
        # Return a dictionary with the same structure, but batched
        return {
            "feature_vector": batched_feature_vectors,
            "positional_encoding": batched_pos_enc,
            "id": batched_ids,
            "id_formula": batched_id_formulas,
            "label": batched_labels,
            "label_class": batched_label_classes,
            "type_feature": batched_type_features          
        }

    def create_query_features(self, ids_per_type, combined_contours, image_shape, thickness=-1):
        ### NEEDED
        # image_shape = None
        # main_contours = None
        # combined_contours = None

        # Dicionary of lists, key- neighbors of the query, value- list of
        # [neighbor itself, other prim ids which this was merged with] OR
        # [neighbor_itself] if no merge
        
        query_indices = ids_per_type["query"]
        context_indices = ids_per_type["context"]

        # COMENTED FOR NO GAT
        # neighbor_indices = ids_per_type["neighbors"]

        query_contours = []
        for idx in query_indices:
            query_contours.extend(combined_contours[idx])

        points_np = np.concatenate(query_contours)
        bbox_min = points_np.min(axis=0)[0] #added extra dimension
        bbox_max = points_np.max(axis=0)[0] #added extra dimension        
        prim_bbox = [bbox_min[1], bbox_min[0], bbox_max[1], bbox_max[0]]
        
        ### QUERY FEATURES
        query_feat, _ = create_each_feature_contours(query_indices, contours=combined_contours,
                                                       new_size=self.symbol_height, thickness=thickness)
        ### NEIGHBOR FEATURES
        # COMENTED FOR NO GAT
        # neighbors_feat = []
        # for nei_idx in neighbor_indices:
        #     neighbors_feat_i, _ = create_each_feature_contours(nei_idx, contours=combined_contours,
        #                                                    new_size=self.symbol_height, thickness=thickness)
        #     neighbors_feat.append(neighbors_feat_i)
        # neighbors_feat = torch.stack(neighbors_feat)

        ### CONTEXT FEATURES
        # K nearest primitives, plus the primitives itself
        context_feat, context_size = create_each_feature_contours(context_indices, query_indices,
                                                               combined_contours, new_size=self.symbol_height, 
                                                               thickness=thickness)
        ## POSITIONAL ENCODINGS        
        pos_enc = get_each_positional_encodings(prim_bbox, context_size, image_shape)
        

        # PADDING

        # COMENTED FOR NO GAT
        # num_neighbors = len(neighbor_indices)
        # total_neighbors = self.context_num_NN
        # total_padding = total_neighbors - num_neighbors
        # if total_padding > 0:
        #     neighbors_feat = torch.cat((neighbors_feat, torch.zeros((total_padding,
        #                                              *neighbors_feat.shape[1:]))))

        ### FINAL FEATURES
        features = torch.cat((query_feat.unsqueeze(0),
                              context_feat.unsqueeze(0)))
                              # COMENTED FOR NO GAT
                            #   neighbors_feat))
        
        return features, pos_enc

def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2**32
    np.random.seed(worker_seed)
    random.seed(worker_seed)

