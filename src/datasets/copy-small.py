import os
# import glob
# from tqdm import tqdm
import shutil
from lgap_settings import DATA_DIR

if __name__ == "__main__":
    CROHME_DIR = os.path.join(DATA_DIR, "crohme2019")

    GT_CROHME_DIR = os.path.join(CROHME_DIR, "Test2019_LG")
    GT_TRAIN_CROHME_DIR = os.path.join(CROHME_DIR, "Train", "LGs", "all")
    GT_TRAIN_CROHME_DIR_INK = os.path.join(CROHME_DIR, "Train", "INKMLs", "all")

    # GT_SMALL_CROHME_DIR = os.path.join(CROHME_DIR, "Test2019_LG_0.5")
    GT_SMALL_CROHME_DEV_DIR = os.path.join(CROHME_DIR, "Train2019_LG_small")
    GT_SMALL_CROHME_DEV_DIR_INK = os.path.join(CROHME_DIR, "Train2019_small")

    # GT_SMALL_FILE_CROHME_DIR = os.path.join(DATA_DIR, "testing_0.5_crohme.txt")
    GT_SMALL_FILE_DEV_CROHME_DIR = os.path.join(DATA_DIR, "training_small_crohme.txt")

    # for file in tqdm(glob.glob(os.path.join("crohme_contour_64_spp_avg_los_node_89", "*"))):
    #     with open(file, "r") as f:
    #         lines = f.read()
    #         # import pdb; pdb.set_trace()

    #     with open(file, "w") as f:
    #         f.writelines(lines.replace("\\\\", "\\"))

    # For the dev test set

    if not os.path.exists(GT_SMALL_CROHME_DEV_DIR):
        os.makedirs(GT_SMALL_CROHME_DEV_DIR)

    with open(GT_SMALL_FILE_DEV_CROHME_DIR, "r") as f:
        lines = f.read().split('\n')
        for line in lines:
            if line:
                shutil.copyfile(os.path.join(GT_TRAIN_CROHME_DIR, line + ".lg"),
                                os.path.join(GT_SMALL_CROHME_DEV_DIR, line + ".lg"))

    if not os.path.exists(GT_SMALL_CROHME_DEV_DIR_INK):
        os.makedirs(GT_SMALL_CROHME_DEV_DIR_INK)

    with open(GT_SMALL_FILE_DEV_CROHME_DIR, "r") as f:
        lines = f.read().split('\n')
        for line in lines:
            if line:
                shutil.copyfile(os.path.join(GT_TRAIN_CROHME_DIR_INK, line +
                                             ".inkml"),
                                os.path.join(GT_SMALL_CROHME_DEV_DIR_INK, line +
                                             ".inkml"))
