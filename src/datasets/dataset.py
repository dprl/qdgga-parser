'''
Main DataLoader for Visual Parser (Math+Chem)
Document and Pattern Recognition Lab, RIT
Creator: Ayush Kumar Shah
Version 2: Abhisek Dey
Changes: Multi-Worker Batching + Queuing through iterable stream
'''

# RZ: Add debug operations
# from src.utils.debug_fns import *
# import src.utils.debug_fns as debug_fns
# dTimer = DebugTimer("LGAP dataloader")
# debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

import joblib
from collections import defaultdict
from logging import raiseExceptions
import os
from itertools import chain
import torch
import math
import numpy as np
import cv2
import random
import os.path as path
import matplotlib.pyplot as plt
from torch.utils import data
from torch.utils.data import IterableDataset
from torch import distributed as dist
from protables.ProTables import tsv2pro, lg2pro, inkml2pro

from src.utils.utils import updateEdgeLabel, get_ccs
from src.datasets.math_symbol import RELATIONS, INFRELATIONS, CHEM_RELATIONS,\
        get_image_params, add_trace, Box2Traces, check_for_merge
from src.features.Graph_construction import  GraphConstruction
from src.features.geometric_feature_extractor import getGeofeat
from qdgga_settings import ERROR_FILES_CROHME, ERROR_FILES_INFTY
from src.image_ops.image2traces import image2strokes, get_minmax, imshow, scale_primitives
from src.image_ops.contour_utils import create_features_from_contours, create_features_from_contours_old


class MathSymbolDataset(IterableDataset):

    ################################################################
    # Built-ins (constructor, len() )
    ################################################################
    def __init__(self, dataset='', level="stk", find_punc=False, data_dir="data/train",
                 Img_list= "", lg_dir=None, graph="los", balance_edges=False,
                 symbol_height=64, image_primitive=False, traces_only=False,
                 symbol_table=None, rel_table=None, node_seg=False,
                 prune_seg=False, prune_rel=False, reduced_rel=False,
                 task='train', symContext=False, allAdj=False, symCrop=False,
                 INFmode='contour', geoFeat=False, format="tsv",
                 tsv_dir=None, dot_dir=None, inputRegionTypes=['c','S'],
                 oneRelLimit=True, lg_mode="contours", inputs="primitives", 
                 seg_num_NN=2, extract_mst=True, gpu_memory_threshold=0.0155, 
                 shuffle=False, max_feat_size=64, rank=0, num_gpus=1,
                 model_dir="outputs/run/infty_contour/",
                 remove_opposite_edges=True, last_epoch=0):

        super(MathSymbolDataset, self).__init__()

        # RZ: Removed trainMST and associated code.
        # Revmoe self.gt_tree and associated code.
        # RZ: Removed self.balance_edges=balance_edges
        # RZ: Readded self.balance_edges=balance_edges
        # RZ: Removed max_strokes attribute.
        # RZ: Removing use_gt -- this belongs in an external fn/elsewhere.
        # RZ: Removed CROHME dataset specific options.
        self.task=task
        self.dataset = dataset

        self.GRAPH=graph
        self.node_seg=node_seg # to check if seg represented by node or edges
        self.prune_seg = prune_seg # to prune the segmentation edge input space
        self.prune_rel = prune_rel # to prune the relationship edge input space
        self.reduced_rel = reduced_rel # to prune the relationship edge input space
        self.level=level
        self.find_punc = find_punc

        self.symbol_height = symbol_height
        self.image_primitive = image_primitive #if true no primitive attention
        self.traces_only = traces_only # if True no label will be returned
        self.balance_edges=balance_edges
        
        self.expr_graph={}
        self.symContext=symContext
        self.allAdj=allAdj
        self.symCrop=symCrop
        
        self.mode=INFmode
        self.geoFeat=geoFeat
        self.data_dir = data_dir
        self.format = format

        # AD Mod: Additional Parameters
        self.gpu_memory_threshold = gpu_memory_threshold
        self.shuffle = shuffle
        self.max_feat_size = max_feat_size
        self.rank = rank
        self.world_size = num_gpus

        self.symbols=[line.strip() for line in open(symbol_table)]
        
        self.inputRegionTypes = set( inputRegionTypes )

        self.currDoc = -1  #AD HACK Change to -1 if first index is 0 in dataset
        self.currPage = -1
        self.currRegion = 0
        self.currPageImg = -1  # RZ HACK: Avoding None data error from logger.

        self.Img_list = Img_list
        self.tsv = False
        self.lg = True

        self.oneRelLimit = oneRelLimit
        self.lg_mode = lg_mode
        self.inputs = inputs

        # number of nearest neighbors to consider for segmentation edge hypotheses
        self.seg_num_NN = seg_num_NN
        self.extract_mst = extract_mst

        self.input_feat_dir = os.path.join(model_dir, "input_features")
        os.makedirs(self.input_feat_dir, exist_ok=True)

        if self.format == 'tsv':
            self.tsv = True
            self.lg = False
            self.tsv_dir = tsv_dir
            self.data_files, self.proTableList, self.regionTriples, \
                    self.docs_page_sizes, self.currPageImg, self.lg \
                    = tsv2pro(self.tsv_dir, self.data_dir)
            if self.dataset == 'infty':
                self.rels=INFRELATIONS
            elif self.dataset == 'chem':
                self.rels = CHEM_RELATIONS

        elif self.format == 'lg':
            self.tsv = False
            self.lg = True
            self.lg_dir = lg_dir
            if self.dataset == 'infty' or self.dataset == 'chem':
                # self.tsv_dir = self.lg_dir
                self.data_files, self.proTableList, self.regionTriples, \
                        self.currPageImg, self.lg = lg2pro(self.lg_dir, self.data_dir,
                                                           self.Img_list,
                                                           mode=self.lg_mode,
                                                           inputs=inputs
                                                           # excludeFiles=ERROR_FILES_INFTY,
                                                           )
                if self.dataset == "chem":
                    _, self.currPageImg = cv2.threshold(self.currPageImg, 0.7, 1, cv2.THRESH_BINARY)
                    # self.currPageImg = 1. - self.currPageImg
                if self.dataset == 'infty':
                    self.rels=INFRELATIONS
                else: # Chem Data
                    self.rels=CHEM_RELATIONS
            elif dataset == 'crohme':
                # self.tsv_dir = data_dir
                self.data_files, self.proTableList, self.regionTriples, \
                        self.currPageImg, self.lg = inkml2pro(self.data_dir,
                                                              self.Img_list,
                                                              task=self.task,
                                                              excludeFiles=ERROR_FILES_CROHME,
                                                              )
                self.rels=RELATIONS
            self.docs_page_sizes = []
        else:
            raise ValueError('Dataset format should either be tsv or lg!')

        # TODO: Remove hard code
        self.merge_dict = {}
        # self.regionTriples = [(*regionTriple, 0) for regionTriple in
        #                       self.regionTriples]
        if self.format == "lg":
            self.file2triples = dict(zip(self.data_files, np.array(self.regionTriples)))

        # AKS: If empty formula regions
        if len(self.regionTriples) == 0:
            print(">> Warning: No formula regions present in:")
            print("   " + str(self.data_files))
            self.data_files = []

        # Set up class dictionaries (one entry per symbol/relation class)
        self.symbols_dict = {s:i for i, s in enumerate(self.symbols)}
        self.rels_dict = {s:i for i, s in enumerate(self.rels)}

        self.data_files = np.array(self.data_files)
        self.regionTriples = np.array(self.regionTriples)

        # AD: Mod the dataset according to rank and world size
        each_chunk_data = int(math.ceil((len(self.data_files)) / float(self.world_size)))
        each_chunk_triples = int(math.ceil((len(self.regionTriples)) / float(self.world_size)))
        start_idx_data = rank * each_chunk_data
        start_idx_triples = rank * each_chunk_triples
        end_idx_data = min(len(self.data_files), start_idx_data + each_chunk_data)
        end_idx_triples = min(len(self.regionTriples), start_idx_triples + each_chunk_triples)
        self.data_files = self.data_files[start_idx_data:end_idx_data]
        self.regionTriples = self.regionTriples[start_idx_triples:end_idx_triples]

        self.tot = len(self.regionTriples)
        self.start = 0
        self.end = len(self.regionTriples)
        # AD NOTE: The start-stop are the original START-STOP indexes
        self.start_idx_triples = start_idx_triples
        self.end_idx_triples = end_idx_triples

        self.remove_opposite_edges = remove_opposite_edges
        self.last_epoch = last_epoch
        self.curr_epoch = last_epoch

    def __len__(self): 
        regionTupleCount = len( self.regionTriples )
        if regionTupleCount > 0:
            return regionTupleCount
        else:
            return len(self.Img_list)

    def __iter__(self):
        # Train/Val/Eval
        return self.create_batch(self.start, self.end)
        
    # Let the epoch be known
    def __call__(self, epoch):
        self.epoch = epoch

    def size_fn(self, tensors):
        tensors = torch.cat(tensors)
        return tensors.nelement() * tensors.element_size()

    @staticmethod
    def memory_available():
        free = os.popen('nvidia-smi -q -d Memory |grep -A5 GPU|grep Free').read().strip().split('\n')
        memory_available = [int(f.split()[2]) for f in free]
        return memory_available

    def create_batch(self, start, end):
        import warnings
        warnings.simplefilter('ignore', category=ResourceWarning)
        tensor_memory = 0
        batch = {
            "symbols": [],
            "segments": [],
            "edgeLabels": [],
            "images": [],
            # "prim_bbox": [],
            "file_name": [],
            "pdf_name": [],
            "indexTuple": [],
            "instances": [],
            "binary_edgelabels": [],
            "graph_rel_edges": [],
            "graph_seg_edges": [],
            "graph_rel_seg_edges": [],
            "graph_los_edges": [],
            # "pruned_edges": [],
            # "num_images": [],
            "num_primitives": [],
            "num_edges": [],
            "num_features": [],
            "error": [],
            # "scaled_sizes": [],
            "image_shape": [],
            "merge_idx": [],
            "expr_graph": [],
            "contours": [],
            "pos_enc": [],
        }

        concat_keys = {"symbols", "segments", "edgeLabels", "images", "pos_enc"}
                       # "prim_bbox", "scaled_sizes"}
        list_keys = {"file_name", "pdf_name", "indexTuple", "instances",
                     "binary_edgelabels", "graph_rel_edges", "graph_seg_edges", 
                     "graph_rel_seg_edges",  "graph_los_edges", "image_shape", "merge_idx",
                     "expr_graph", "num_primitives", "num_edges", "num_features", "contours",
                      # "num_images"}
                     }
        tensor_keys = batch.keys() - concat_keys - list_keys

        return_batch = {}

        img_count = 1

        # Shuffle the indexes if set
        idxs = np.arange(start, end)
        len_idxs = len(idxs)

        if self.shuffle:
            np.random.shuffle(idxs)
        
        # print(f'START:END {(start, end)}')
        # print(f'ORDER: {idxs}')

        for idx_i, i in enumerate(idxs):
            # RZ: Returns each region as an item.
            # Construct informative (non-file) name for debugging and analysis.
            ( doc, page, region, merge_idx ) = self.regionTriples[i]

            if self.format == 'tsv':
                fileName = path.splitext(self.data_files[ doc ])[0]
                regionName =  fileName + '-P' + str(page + 1) + '-R' + str(region + 1)
                lg_path = ""
            else:
                fileName = self.dataset
                # regionName = path.splitext(self.data_files[ page ])[0]
                regionName = path.splitext(self.data_files[page - self.start_idx_triples])[0]
                lg_path = path.join(self.lg_dir, regionName + ".lg")

            # xTimer = DebugTimer("LGAP feature construction")
            # xTimer.check("Start")
            single = self.get_region_data(fileName, regionName, i, lg_path,
                                          merge_idx)
            # xTimer.check("Feature construction")
            # import pdb; pdb.set_trace()
            

            if single["error"]:
                continue
            
            # print('Got 1 formula')
            images = [single['images'], *batch['images']]
            num_edges = len(single["graph_rel_seg_edges"]) + \
                            sum([len(edge) for edge in batch["graph_rel_seg_edges"]])
            length = sum([x.shape[0] for x in images]) + num_edges + img_count

            images_memory = 3 * length * self.max_feat_size * self.max_feat_size * images[0].element_size()
            other_memory = self.size_fn([single["symbols"], *batch["symbols"]]) + \
                           self.size_fn([single["segments"], *batch["segments"]]) + \
                           self.size_fn([single["edgeLabels"], *batch["edgeLabels"]])
            tensor_memory = (images_memory + other_memory) / (1024 ** 2)

            self.free_memory = self.memory_available()[0]
            memory_ratio = tensor_memory / self.free_memory
            # print(memory_ratio)
            img_count += 1

            if memory_ratio < self.gpu_memory_threshold or idx_i == len_idxs - 1:
                for key in batch.keys():
                    batch[key].append(single[key])
                # yield {}
                
            if memory_ratio >= self.gpu_memory_threshold or idx_i == len_idxs - 1:
                if len(batch['file_name']) == 0:
                    for key in concat_keys.union(tensor_keys):
                        return_batch[key] = single[key]
                    for key in list_keys:
                        return_batch[key] = [single[key]]
                
                else:
                    # Now create the actual batch for yielding
                    for key in concat_keys:
                        return_batch[key] = torch.cat(batch[key])

                    # The tensors which don't need concatenation
                    for key in tensor_keys:
                        return_batch[key] = torch.tensor(batch[key])

                    for key in list_keys:
                        return_batch[key] = batch[key]
                    
                    # Start a new batch with the current batch
                    for key in batch.keys():
                        batch[key] = [single[key]]

                # Add the merge dict
                # NOTE: This is NOT populated each formula
                return_batch['merge_dict'] = self.merge_dict

                del single
                tensor_memory = 0
                img_count = 2

                # print(return_batch["graph_edgeRef_sorted"])
                # print(return_batch["graph_segRef"])
                # exit()
                # print('Yielding one Batch')
                # if self.rank == 0:
                #     print(f'Yeilding return BATCH: {return_batch["file_name"]}')
                yield return_batch
                               
                # print(f'Images: {return_batch["images"].size()}')
                # print(f'Len: {return_batch["len"]}')
                # exit()
                


    def showCurrentTSVRegion( self ):
        # Method to check the data object state (assuming TSV data!)
        (i, l, p, b) = self.getTSVRegion( \
                (self.currDoc, self.currPage, self.currRegion ) )
        print( "Current Region: Index list, primitive id names, bbboxes:" )
        print( (l, p, b ) )
        #show_image( i, "Region Image" )

    def getTSVRegion( self, indexTuple ):
        # Reads the region, updates current doc/page/region as a side-effect.
        # dTimer.check("Before start reading protables")
        ( doc, page, region, merge_idx ) = indexTuple
        traces = []
        
        imagePath = ""
        # If page image has changed, update.
        if doc != self.currDoc or page != self.currPage:
            if self.format == 'tsv':
                ## AKS: If formula from doc pages (TSV mode)
                imagePath = path.join( self.data_dir, 
                            path.splitext(self.data_files[doc])[0],
                            str(page + 1) + ".png" )
            elif self.format == 'lg':
                ## AKS: If formula images from dataset (LG mode)
                if self.dataset=='infty' or self.dataset == "chem":
                    imagePath = path.join(self.data_dir, 
                             path.splitext(self.data_files[page - self.start_idx_triples])[0].\
                                          replace("_line", "").replace("_pdf", "") + ".PNG")

            # Image is inverted (*every time -- earlier there was a cond.)
            # '0' argument tells OpenCV to read as grayscale image
            if imagePath:
                # print(imagePath)
                self.currPageImg = cv2.imread( imagePath, 0 )
                # TODO: Maybe move to where this is done for dataset = chem
                if not self.lg:
                    self.currPageImg = 255. - self.currPageImg

                self.currPageImg = self.currPageImg / 255.0

        # Retrieve bounding box for region, and crop the current page
        # at the region's location.
        regionEntry = self.proTableList[doc][page][region]
        hasSegmentations = regionEntry[-2]
        ( minX, minY, maxX, maxY ) = regionEntry[0][:4]
        # Overlay Test
        # for field in regionEntry[1]:
        #     c_box = field[:4]
        #     c_box = [int(c) for c in c_box]
        #     self.currPageImg = cv2.rectangle(self.currPageImg, (c_box[0], c_box[1]), (c_box[2], c_box[3]), (0,255,0), 1)
        # plt.imshow(self.currPageImg, cmap='gray')
        # plt.show()
        # exit(0)
        
        # RZ DEBUG: TSV stores coordinates in X-Y order, OpenCV / numpy
        # operations assume Y-X (i.e., row-column) order for coordinates.
        
        # import pdb; pdb.set_trace()
        
        if self.currPageImg is not None:
            regionImg = self.currPageImg[ minY : maxY + 1, minX : maxX + 1] 
        else:
            regionImg = self.currPageImg
        # Binarize Image
        _, regionImg = cv2.threshold(regionImg, 0.7, 1.0, cv2.THRESH_BINARY)
        
        # dTimer.check("Read formula image")
        # if self.dataset == "chem":
            # _, self.currPageImg = cv2.threshold(self.currPageImg, 128, 255, cv2.THRESH_BINARY)
            # _, self.currPageImg = cv2.threshold(self.currPageImg, 0.7, 1, cv2.THRESH_BINARY)
            # regionImg = 1. - regionImg

        offsetVector = np.array([ minY, minX, minY, minX ]).astype(int)
        contour_offset = np.array([minX, minY])
        
        primitiveIds = []
        classLabels = []
        bboxes = {}
        contours = {}
        symtags = []

       # RZ: Add object type filter; pass only indicated types for parsing
        objects = regionEntry[1]
        for i in range(len(objects)):
            # Get label and type, construct id.
            (oLabel, oType ) = objects[i][4:6]
            if oType not in self.inputRegionTypes:
                continue

            # Use numeric identifiers as given in the input.
            try:
                # If instance id and symtags is present in data, use them
                objId = objects[i][6]
                symtags.append(objects[i][7])
            except:
                objId = int(i) 

            #  if traces present
            if len(objects[i]) >= 9:
                if self.dataset=='infty' or self.dataset == "chem":
                    # import pdb; pdb.set_trace()
                    # contour = np.array(objects[i][8])
                    contour = objects[i][8]
                    contour = [np.array(c) - contour_offset for c in contour]
                    # import pdb; pdb.set_trace()
                    
                    # contour = contour - offsetVector[:2][::-1]
                    contours[ objId ] = contour
                    traces.append(np.concatenate(contour).reshape(1, -1, 2))
                else:
                    # For CROHME dataset,
                    traces.append([objects[i][8]])
            # The 'merge' here are label mappings (e.g, all lines to one repr.)
            primitiveIds.append( objId )
            classLabels.append( check_for_merge( oLabel ) )

            # RZ DEBUG: Boxes require row-column (Y-X) coordinates.
            ( bbminX, bbminY, bbmaxX, bbmaxY ) = objects[i][:4]
            bboxes[ objId ] = np.array( ( bbminY, bbminX, bbmaxY, bbmaxX ) )\
                    .astype(int) - offsetVector

        # Update object state.
        ( self.currDoc, self.currPage, self.currRegion, _) = indexTuple
        # dTimer.check("Read protables information")

        return ( regionImg, primitiveIds, classLabels, bboxes, symtags, traces,
                contours, hasSegmentations)


    ################################################################
    # Region data access method,  used to feed a PyTorch DataLoader 
    ################################################################
    # RZ: Adding index parameter to use TSV region loading
    # All 'ground truth'-based processing removed for simplification.
    def get_region_data(self, input_file, regionName, indexTSV = -1,
                        lg_path="", merge_idx=0, regionTriples=None):

        # dTimer.check("Start reading data")
        # TSV
        # RZ: use Doc/Page/Region triple as file representative.
        fileName = regionName

        feat_dir = os.path.join(self.input_feat_dir, self.task, input_file)
        if not os.path.exists(feat_dir):
            os.makedirs(feat_dir, exist_ok=True)

        merge_groups = defaultdict(set)
        feat_file = os.path.join(feat_dir, regionName + ".compressed")
        # feat_file = os.path.join(feat_dir, regionName + ".joblib")

        if os.path.exists(feat_file):
            # Load features from disk 
            with open(feat_file, 'rb') as fo:  
                out = joblib.load(fo)
                ## NOW UPDATE features based on changes in next epochs
                ## Only changes are related to merging of primitives

                # The merge indices might be different in the next epochs
                out["merge_idx"] = merge_idx
                # dTimer.check("Construct updated segmentation gt")
                # If first epoch, then just re-initialize
                if self.curr_epoch <= self.last_epoch:
                    # If segmeentation present in formula i.e.,
                    # multi-primitive symbols present, add gt segmentation entry
                    # Re-initialize
                    if self.inputs == "primitives_gtsymbols" and out["hasSegmentations"]:
                        seg_edges = out["graph_seg_edges"]
                        segments = out["segments"]
                        merge_edges = np.array(seg_edges)[np.where(np.array(segments) == 0)].tolist()
                        self.merge_dict[fileName] = [[], get_ccs(merge_edges)]
                    else:
                        self.merge_dict[fileName] = [[]]

                # Else, create new features by combining the primitives
                else:
                    merge_filename = self.merge_dict.get(fileName, [])
                    if merge_filename:
                        merge_edges = self.merge_dict[fileName][merge_idx]

                        # Find merge groups for each primitive in the merge_dict
                        if merge_edges:
                            # print(f"{fileName}: Merge used: idx = {merge_idx} {merge_edges}")
                            for merges in merge_edges:
                                for prim in merges:
                                    merge_groups[prim] = merge_groups[prim].union(merges - {prim})

                            # Get required inputs required for feature reconstruction
                            boxes = out["boxes"]
                            contours = out["contours"]
                            rel_seg_edges = out["graph_rel_seg_edges"].tolist()
                            los_edges = out["graph_los_edges"]
                            sorted_distances = out["sorted_distances"]
                            regionImg = out["regionImg"]
                            prim_feat_batch, prim_context_feat_batch, \
                                pair_feat_batch, pair_context_feat_batch, pos_enc, contours = \
                                create_features_from_contours(regionImg, boxes, new_size=self.symbol_height, 
                                                              contours=contours, merge_groups=merge_groups,
                                                              all_edges=rel_seg_edges, los_edges=los_edges,
                                                              k=self.seg_num_NN, sorted_distances=sorted_distances,
                                                              thickness=-1, correct_contours=False)
                            image_features = torch.cat([prim_feat_batch,
                                                        prim_context_feat_batch,
                                                        pair_feat_batch, pair_context_feat_batch])
                            # Update input features
                            out["images"] = image_features
            # dTimer.check("Load from disk")
        else:
            # print("Features construct")
            # RZ: ASSUMING TSV format data -- CROHME code removed.
            regionImg = None
            symtags = None
            traces = []

            # Get next region details (image and object properties) 
            if regionTriples is not None:
                nextIndexTuple = regionTriples[ indexTSV ]
            else:
                nextIndexTuple = self.regionTriples[ indexTSV ]

            ( regionImg, instances, symbols, boxes, symtags, traces, contours, hasSegmentations ) = \
                    self.getTSVRegion( nextIndexTuple )

            # Used for making masks and image + graphs
            if self.dataset == "infty" or (self.dataset == "chem" and self.lg_mode == "ccs"):
            # or self.dataset == 'chem':
                traces = Box2Traces(input_file, boxes, instances, mode=self.mode, 
                            in_img = regionImg )

            # if self.dataset == "chem":
            #     use_contours = True
                # traces = Box2Traces(input_file, boxes, instances, mode=self.mode, 
                #             in_img = regionImg )
                

            # Check contours (traces), collect primitve ids, 
            # outer countours and image props
            if len(traces) < 1:
                print(">> Warning: SKIPPING " + fileName + ": no traces found")
                return { 'error': True }

            instances = [ int(x) for x in instances ]
            outer_traces = [ x[0] for x in traces ] 

            ################################################################
            # Graph Construction
            ################################################################
            
            ## Old: when nodes were used to represent segments
            # Create segments ground truth if available
            # segments = []
            # if self.node_seg and symtags:
            #     segments = [0]
            #     for i in range(1, len(symbols)):
            #         if symtags[i] == symtags[i-1]:
            #             segments.append(0)
            #         else:
            #             segments.append(1)
            # dTimer.check("Construct initial segmentation gt")
            
            if self.GRAPH=='complete' and len(instances) > 25 and \
                    self.task=='test':
                self.GRAPH = 'los'

            # import pdb; pdb.set_trace()
            # Construct and store the input graph (e.g. LOS/Complete)
            ( rel_edges, edgeLabels, exprGraph, seg_ref, los_edges,
             sorted_distances )= \
                    GraphConstruction.getExprGraph( self.GRAPH,
                                                    instances, symbols, outer_traces, symtags=symtags,
                                                    lg=lg_path, level=self.level, seg=self.node_seg,
                                                    task=self.task, find_punc=self.find_punc
                                                   )
            # dTimer.check("Construct LOS graph")

            if len(rel_edges) == 0:
                rel_edges.append((0, 0))
                los_edges[0].append((0, 0))
                edgeLabels.append('NoRelation')
            
            # TODO: Check if segments, rel_edges and edgeLabels are correct for all conditions:
            # reduced_rel, prune_seg and prune_rel
            # UPDATE: no longer using reduced_rel and prune_rel
            # If segementations are represented with edges (Default)
            # Construct segmentation edges using (k) nearest neighbors from LOS edges
            if not self.node_seg: #and self.task=='train':
                if self.prune_seg:
                    seg_edges = list(chain.from_iterable([v[:self.seg_num_NN] for v in los_edges]))
                else:
                    seg_edges = list(chain.from_iterable(los_edges))

                if self.remove_opposite_edges:
                    sorted_seg_edges = np.sort(seg_edges, axis=1)
                    seg_edges = list(map(tuple, np.unique(sorted_seg_edges, axis=0)))

                segments = []
                for edge in seg_edges:
                    if edge in seg_ref:
                        segments.append(0)
                    else:
                        segments.append(1)

                # If balance = True, reduce number of segmentation edges
                if self.balance_edges and not self.prune_seg:
                    seg_edges, segments = self.balance_neg_instances(seg_edges, segments,
                                                                     len(instances), negative=1)

            # print(len(rel_edges))
            rel_edges2labels = dict(zip(rel_edges, edgeLabels))
            if self.prune_rel:
                # Tolerance of 2 since should not miss positive edges
                rel_edges = list(set(chain.from_iterable([v[:self.seg_num_NN+2] for v in
                                         los_edges])).intersection(set(rel_edges)))
                edgeLabels = list(map(lambda x:rel_edges2labels[x], rel_edges))
            
            if self.remove_opposite_edges:
                sorted_rel_edges = np.sort(rel_edges, axis=1)
                rel_edges, unique_indices = np.unique(sorted_rel_edges, axis=0,
                                                      return_index=True)
                rel_edges = list(map(tuple, rel_edges)) 
                edgeLabels = np.array(edgeLabels)[unique_indices].tolist()

            if self.task == "train" and self.balance_edges and not self.prune_rel:
            # if self.task == "train" and self.balance_edges:
                # rel_edges, edgeLabels = self.hard_neg(rel_edges, edgeLabels, len(instances), 
                #                                       negative="NoRelation", los_edges=los_edges)

                rel_edges, edgeLabels = self.balance_neg_instances(rel_edges, edgeLabels, 
                                                                   len(instances),
                                                                   negative="NoRelation")

            # print(len(rel_edges))
            
            rel_seg_edges = np.unique(np.concatenate((rel_edges, seg_edges)), axis=0)
            rel_seg_edges_actual = list(map(lambda x: (str(instances[x[0]]), 
                                                       str(instances[x[1]])), rel_seg_edges))
            # rel_seg_edges_str = list(map(tuple, rel_seg_edges.astype(str))) 
            rel_seg_edges = list(map(tuple, rel_seg_edges)) 

            # print(len(rel_seg_edges))
            # import pdb; pdb.set_trace()
            

            # Keep only the edges that goes to the model in the expression graph
            # TODO: Check if this has negative effect on test results
            ## This is only used in test postprocessingGenORLg and not in train
            # exprGraph = exprGraph.edge_subgraph(rel_seg_edges_str)
            edges_to_remove = list(set(exprGraph.edges()) -
                                   set(rel_seg_edges_actual))
            exprGraph.remove_edges_from(edges_to_remove)
            
            # Store LOS/complete graph for the file in the dataloader.
            self.expr_graph[ fileName ] = exprGraph

            # dTimer.check("Construct updated segmentation gt")
            merge_groups = defaultdict(set)
            if self.curr_epoch <= self.last_epoch:
                if self.inputs == "primitives_gtsymbols" and hasSegmentations:
                # If segmeentation present in formula i.e.,
                # multi-primitive symbols present, add gt segmentation entry
                    merge_edges = np.array(seg_edges)[np.where(np.array(segments) == 0)].tolist()
                    self.merge_dict[fileName] = [[], get_ccs(merge_edges)]
                else:
                    self.merge_dict[fileName] = [[]]
            else:
                if self.inputs == "primitives_gtsymbols" and hasSegmentations:
                    merge_filename = self.merge_dict.get(fileName, [])
                    if merge_filename:
                        merge_edges = self.merge_dict[fileName][merge_idx]
                        if merge_edges:
                            for merges in merge_edges:
                                for prim in merges:
                                    merge_groups[prim] = merge_groups[prim].union(merges - {prim})

            # Features construction
            if self.mode == "raw":
                # Old code to extract primitive features only and construct rest
                # on GPU. Supports both contour and cc mode, so works for math
                # INFTY gt data which has ccs
                # primitives, primitives_scaled, primitives_batch, prim_bbox,\
                # scaled_sizes, contours = scale_primitives(regionImg, boxes, new_size=self.symbol_height, 
                #                                 filename=fileName, contours=contours,
                #                                 lg_mode=self.lg_mode, merge_edges=[])

                # Do not merge primitive features here, done in model/network
                prim_feat_batch, prim_context_feat_batch, \
                    pair_feat_batch, pair_context_feat_batch, pos_enc, contours = \
                    create_features_from_contours(regionImg, boxes, new_size=self.symbol_height, 
                                                  contours=contours, merge_groups=merge_groups,
                                                  all_edges=rel_seg_edges, los_edges=los_edges,
                                                  k=self.seg_num_NN, sorted_distances=sorted_distances,
                                                  thickness=-1)
                # print(rel_seg_edges)
                # imshow([x for x in prim_feat_batch], ncols=4)
                # imshow([x for x in prim_context_feat_batch], ncols=4)
                # imshow([x for x in pair_feat_batch[:16]], ncols=4)
                # imshow([x for x in pair_context_feat_batch[:16]], ncols=4)
                # print(dict(zip(rel_edges, edgeLabels)))
                # print(dict(zip(seg_edges, segments)))
                # import pdb; pdb.set_trace()
                # imshow(regionImg)
                
            # No support for this currently
            # TODO: Modify simnilarly to raw mode to construct scaled primitves batch
            else:
                # RZ: Begin constructing images as numpy arrays, with 'traces'
                # copied into the image.
                ( image_size, rescale ) = \
                    get_image_params(traces, height=self.symbol_height,
                                      data=self.dataset)
                # print(image_size, rescale)
                # exit(0)

                kernel = np.ones((2,2), np.uint8)
                primitives = []
                image = np.zeros(image_size)
                for ( _, trace ) in enumerate(traces):
                    primitive = np.zeros(image_size)
                    for tr in trace:
                        add_trace(image, tr, rescale, mode=self.mode)
                        add_trace(primitive, tr, rescale, mode=self.mode)
                    if self.dataset == "crohme":
                        primitive = cv2.dilate(primitive, kernel, iterations=2)
                    primitives.append(primitive)

                if self.dataset == "crohme":
                    image = cv2.dilate(image, kernel, iterations=2)

            # Binary edge labels for relation detector
            binaryrel_labels=[]
            for rel in edgeLabels:
                binaryrel_labels.append(1) if rel != 'NoRelation' \
                        else binaryrel_labels.append(0)
            
            # Geometric features for edges
            if self.geoFeat:
                geoFeat=getGeofeat(rel_edges, outer_traces)
            
            edgeLabels = [ self.rels_dict[s] for s in edgeLabels ] \
                    if self.task=='train' or self.node_seg is False else []

            # Symbol labels for primitives
            ## Use id equal to length of symbols dict if unknown symbol 
            ## (unknown symbols are found in pipeline mode and symbols not used in test mode)
            symbols = [self.symbols_dict.get(s, self.symbols_dict["_"]) for s in symbols]
            # num_ind_masks = len(symbols_img) if self.level=='sym' and self.use_gt \
            #         else len(prim_feat_batch)

            # import pdb; pdb.set_trace()
            # import pickle
            # pickle.dump(images, open("notebooks/pickles/images_symContext.pkl",
            #                          "wb"))
            image_features = torch.cat([prim_feat_batch,
                                        prim_context_feat_batch,
                                        pair_feat_batch, pair_context_feat_batch])
            ################################################################
            # Generate output record for use in loading
            ################################################################
            out= {
                "file_name": fileName, 
                "indexTuple": ( self.currDoc, self.currPage, self.currRegion ),
                "pdf_name": input_file,
                "images": image_features,
                "pos_enc": torch.FloatTensor(pos_enc),
                "symbols": torch.LongTensor(symbols),
                "instances": torch.LongTensor(instances),
                "segments": torch.LongTensor(segments),
                "edgeLabels": torch.LongTensor(edgeLabels), # Empty if no lg passes
                "binary_edgelabels": torch.LongTensor(binaryrel_labels),
                "num_primitives": len(prim_feat_batch),
                "num_edges": len(pair_feat_batch),
                "num_features": len(image_features),
                "error": False,
                "expr_graph": exprGraph}

            # num_images and len have same values - so removing num_images
            # out["num_images"] = out["images"].shape[0]
            out["image_shape"] = regionImg.shape
            # out["formula_widths"] = image.shape[1]
            # out["widths"] = max_w
            # out["scaled_sizes"] = torch.IntTensor(scaled_sizes)
            out["merge_idx"] = merge_idx

            # Required for reconstruction when loaded from disk 
            out["contours"] = contours
            out["hasSegmentations"] = hasSegmentations
            out["boxes"] = boxes
            out["sorted_distances"] = sorted_distances
            out["regionImg"] = regionImg
            # if self.level == "sym" and self.use_gt:
            #     out[ "symLabels" ] = torch.LongTensor(symbols_labels)
            if self.GRAPH:
                # out['graph_edgeRef'] = torch.IntTensor(rel_edges) # Need to write lg's 
                # # AKS: Note that los_edges includes segmentation edges
                # # out['graph_edgeRef_sorted'] = torch.IntTensor(los_edges) 
                # out['graph_edgeRef_sorted'] =  los_edges  
                # # out['graph_segRef'] = torch.IntTensor(seg_ref) # Need to write lg's 
                # out["graph_segRef"] = torch.IntTensor(seg_edges)

                # AKS: Rename the keys to match the edges
                out['graph_rel_edges'] = torch.IntTensor(rel_edges) # Need to write lg's 
                out["graph_seg_edges"] = torch.IntTensor(seg_edges)
                out['graph_rel_seg_edges'] =  torch.IntTensor(rel_seg_edges)
                out['graph_los_edges'] = los_edges 
            # if self.allAdj:
            #     out["adjsymbols"] = torch.LongTensor(adj_symbols)    
            if self.geoFeat:
                out["geoFeat"] = torch.FloatTensor(geoFeat)

            # import pdb; pdb.set_trace()
            # dTimer.check("Add other metadata and return")
            # with open(feat_file, "wb") as fo:
            #     joblib.dump(out, fo)
            # dTimer.check("Save joblib")

            with open(feat_file, "wb") as fo:
                joblib.dump(out, fo, compress=True)
            # dTimer.check("Save joblib compressed")

        return out

    def balance_neg_instances(self, data_instances, data_labels, num_primitives,
                              negative, seed=0):
        neg_idx = np.where(np.array(data_labels) == negative)[0]
        len_neg = len(neg_idx)
        len_pos = len(data_labels) - len(neg_idx) 
        sample_size = int(round(max(len_pos, num_primitives)))
        if len_neg > len_pos and len_neg > num_primitives:
            random.seed(seed)
            sampling = random.sample(list(neg_idx), sample_size)
            new_instances = []
            new_labels = []
            for i, label in enumerate(data_labels):
                if i in sampling or label != negative:
                    new_labels.append(data_labels[i])
                    new_instances.append(data_instances[i])
            data_instances = new_instances
            data_labels = new_labels
        return data_instances, data_labels

    # def hard_neg(self, data_instances, data_labels, num_primitives,
    #               negative, los_edges):
    #     pruned_edges = set(chain.from_iterable([v[:self.seg_num_NN] for v in los_edges]))
    #     neg_idx = np.where(np.array(data_labels) == negative)[0]
    #     len_neg = len(neg_idx)
    #     len_pos = len(data_labels) - len(neg_idx) 
    #     sample_size = int(round(max(len_pos, num_primitives)))
    #     if len_neg > len_pos and len_neg > num_primitives:
    #         # random.seed(seed)
    #         sampling = random.sample(list(neg_idx), sample_size)
    #         new_instances = []
    #         new_labels = []
    #         for i, label in enumerate(data_labels):
    #             if i in sampling or label != negative:
    #                 new_labels.append(data_labels[i])
    #                 new_instances.append(data_instances[i])
    #         data_instances = new_instances
    #         data_labels = new_labels
    #     return data_instances, data_labels

def worker_init_fn(worker_id):
    worker_info = data.get_worker_info()
    dataset = worker_info.dataset
    overall_start = dataset.start
    overall_end = dataset.end
    worker_id = worker_info.id
    num_workers = worker_info.num_workers
    per_worker = int(math.ceil((overall_end - overall_start)/ float(num_workers)))
    dataset.start = overall_start + worker_id * per_worker
    dataset.end = min(overall_end, dataset.start + per_worker)

    
def collate_fn(data):
    return data[0]

