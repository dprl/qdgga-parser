import pandas as pd
from src.datasets.classes.DatasetItem import PrimNodeItem, SegNodeItem, EdgeItem, DatasetItem, Formula, NodeSegmentatioEdgeItem
from src.features.Graph_construction import  GraphConstruction
from itertools import chain
import numpy as np
import os.path as path
from collections import defaultdict
from src.datasets.math_symbol import RELATIONS, INFRELATIONS, CHEM_RELATIONS,\
        get_image_params, add_trace, Box2Traces, check_for_merge
import random
from src.image_ops.image2traces import draw_points, imshow
from src.utils.utils import updateEdgeLabel, get_ccs
from src.image_ops.contour_utils import get_knn_prims_edge
from operator import itemgetter
from collections import defaultdict 
import sqlitedict
# RZ: Add debug operations
from src.utils.debug_fns import *
import src.utils.debug_fns as debug_fns
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

def combine_dict_pair( d, e ):    
    merged_dict = dict()
    merged_dict.update(d)
    merged_dict.update(e)

    return merged_dict

def get_names_formula(self_dict, doc, page, region):
    if self_dict["format"] == 'tsv':
        fileName = path.splitext(self_dict["data_files"][ doc ])[0]
        regionName =  fileName + '-P' + str(page + 1) + '-R' + str(region + 1)
        lg_path = ""
    else:
        fileName = self_dict["dataset"]
        # regionName = path.splitext(self_dict["data_files"][ page ])[0]
        regionName = path.splitext(self_dict["data_files"][page])[0]
        lg_path = path.join(self_dict["lg_dir"], regionName + ".lg")
    return fileName, regionName, lg_path

def create_node_objects(labels, labels_string, instances_lg, boxes, contours_list, merge_groups, id_formula, los_edges, k):
    table = pd.DataFrame(DatasetItem.get_scheme())
    dict_nodes = {}
    offset=len(labels)
    for idx, i in enumerate(labels):        
        knn_idx = list(map(itemgetter(1), los_edges[idx][:k]))
        context_knn = [*[idx], *knn_idx]       
        node = PrimNodeItem(id=idx, id_formula=id_formula, label_class=i, contours=contours_list[idx],
                            label_string=labels_string[idx], instance_lg=instances_lg[idx], box=boxes[idx], neighbors_ids=knn_idx,
                            context_ids=context_knn)
        dict_nodes[node.id] = node
        table = pd.concat([table, node.get_data_frame_row()])
        if idx in merge_groups:
            node_seg = node.create_segmentation_node(merge_groups[idx], offset)            
            dict_nodes[node_seg.id] = node_seg
            node.ground_truth_segmented_partner_id = offset
            node.was_segmented = True
            offset += 1
            table = pd.concat([table, node_seg.get_data_frame_row()])
                    
    return dict_nodes, table

def create_edge_objects(rel_edges, seg_edges, rel_labels, seg_labels,rel_edges_lg, seg_edges_lg, rel_seg_edges,
                        merge_groups,id_formula, los_edges, sorted_distances,k, sym_labels): 

    seg_edges_dict = defaultdict(lambda :{"seg_label":-1,"lg_tuple_seg":-1},
                                 map(lambda i,j,k : (i,{"seg_label":j,"lg_tuple_seg":k}) ,
                                     seg_edges,seg_labels,seg_edges_lg))
    rel_edges_dict = defaultdict(lambda :{"rel_label":-1,"lg_tuple_rel":-1},
                                 map(lambda i,j,k : (i,{"rel_label":j,"lg_tuple_rel":k}) ,
                                     rel_edges,rel_labels,rel_edges_lg) )
    table = pd.DataFrame(DatasetItem.get_scheme())
    dict_edges = {}
    offset = len(rel_seg_edges)
    for idx, edge_tuple in enumerate(rel_seg_edges): 
        knn_idx, knn_idx_with_prims = get_knn_prims_edge(edge_tuple, list(edge_tuple), los_edges, sorted_distances, k)      
        edge_tuple_lg = seg_edges_dict["lg_tuple_seg"] \
                                if seg_edges_dict["lg_tuple_seg"] != -1 \
                                    else rel_edges_dict["lg_tuple_rel"]
        
        edge_tuple_str = (sym_labels[edge_tuple[0]], sym_labels[edge_tuple[1]])
        edge = EdgeItem(id=idx, id_formula=id_formula,
                        label_class_seg=seg_edges_dict[edge_tuple]["seg_label"],
                        label_class_rel=rel_edges_dict[edge_tuple]["rel_label"],
                        edge_tuple=edge_tuple, edge_tuple_lg=edge_tuple_lg,
                        neighbors_ids=knn_idx, context_ids=knn_idx_with_prims,
                        edge_tuple_str=edge_tuple_str)        
        dict_edges[edge.id] = edge
        table = pd.concat([table, edge.get_data_frame_row()])

        node_id_1, node_id_2 = edge_tuple
        if node_id_1 in merge_groups or node_id_2 in merge_groups:
            edge_duplicated = edge.create_segmentation_edge(offset)
            edge.ground_truth_segmented_partner_id = offset
            edge.was_segmented = True
            dict_edges[edge_duplicated.id] = edge_duplicated            
            offset += 1
            table = pd.concat([table, edge_duplicated.get_data_frame_row()])
                            
    return dict_edges, table

def update_edges(self_dict, rel_seg_edges, seg_ref, rel_labels, rel_edges):        
    seg_labels = []
    rel_edges2labels = dict(zip(rel_edges, rel_labels))

    seg_edges = rel_seg_edges.copy()
    rel_edges = rel_seg_edges.copy()

    for edge in seg_edges:
        if edge in seg_ref:
            seg_labels.append(0)
        else:
            seg_labels.append(1)

    # Label as 1 (CONNECTED) for absent edges in edges2labels:
    # absent since segmentation edges with "MERGE" were not present in rel_edges
    # TODO: Later, change LOS graph construction, so it inlcudes same edges
    # for both tasks, and labels for both
    rel_labels = list(map(lambda x:rel_edges2labels.get(x, "CONNECTED"), rel_edges))
    
    rel_labels = [ self_dict["rels_dict"][s] for s in rel_labels ] \
            if self_dict["task"]=='train' or self_dict["node_seg"] is False else []
    return rel_edges, seg_edges, rel_labels, seg_labels

def prune_LOS(self_dict, los_edges, instances, seg_ref, 
              exprGraph, rel_labels, rel_edges):        
    if self_dict["prune_seg"]:
        seg_edges = list(chain.from_iterable([v[:self_dict["prune_num_NN"]] for v in los_edges]))
    else:
        seg_edges = list(chain.from_iterable(los_edges))

    if self_dict["remove_opposite_edges"]:
        sorted_seg_edges = np.sort(seg_edges, axis=1)
        seg_edges = list(map(tuple, np.unique(sorted_seg_edges, axis=0)))

    seg_labels = []
    for edge in seg_edges:
        if edge in seg_ref:
            seg_labels.append(0)
        else:
            seg_labels.append(1)

    # If balance = True, reduce number of segmentation edges
    if self_dict["balance_edges"] and not self_dict["prune_seg"]:
        seg_edges, seg_labels = balance_neg_instances(seg_edges, seg_labels,
                                                            len(instances), negative=1)                
    rel_edges2labels = dict(zip(rel_edges, rel_labels))
    if self_dict["prune_rel"]:
        # Tolerance of 2 since should not miss positive edges
        # rel_edges = list(set(chain.from_iterable([v[:self_dict["prune_num_NN"]] for v in
        #                             los_edges])).intersection(set(rel_edges)))
        rel_edges = list(chain.from_iterable([v[:self_dict["prune_num_NN"]] for v in los_edges]))
        # rel_edges = list(OrderedDict.fromkeys(chain.from_iterable([v[:self_dict["prune_num_NN"]]
        #                                                            for v in los_edges])))
        # Label as 1 (CONNECTED) for absent edges in edges2labels:
        # absent since segmentation edges with "MERGE" were not present in rel_edges
        # TODO: Later, change LOS graph construction, so it inlcudes same edges
        # for both tasks, and labels for both
        rel_labels = list(map(lambda x:rel_edges2labels.get(x, "NoRelation"), rel_edges))
    
    if self_dict["remove_opposite_edges"]:
        sorted_rel_edges = np.sort(rel_edges, axis=1)
        rel_edges, unique_indices = np.unique(sorted_rel_edges, axis=0,
                                                return_index=True)
        rel_edges = list(map(tuple, rel_edges)) 
        rel_labels = np.array(rel_labels)[unique_indices].tolist()

    if self_dict["task"] == "train" and self_dict["balance_edges"] and not self_dict["prune_rel"]:
    # if self_dict["task"] == "train" and self_dict["balance_edges"]:
        # rel_edges, edgeLabels = self_dict["hard_neg"](rel_edges, edgeLabels, len(instances), 
        #                                       negative="NoRelation", los_edges=los_edges)
        rel_edges, rel_labels = balance_neg_instances(rel_edges, rel_labels, 
                                                            len(instances),
                                                            negative="NoRelation")

    # print(len(rel_edges))
    assert(rel_edges == seg_edges), "Different rel and seg edges"
    rel_seg_edges = np.unique(np.concatenate((rel_edges, seg_edges)), axis=0)
    rel_seg_edges_actual = list(map(lambda x: (str(instances[x[0]]), 
                                                str(instances[x[1]])), rel_seg_edges))
    # rel_seg_edges_str = list(map(tuple, rel_seg_edges.astype(str))) 
    rel_seg_edges = list(map(tuple, rel_seg_edges)) 

    # print(len(rel_seg_edges))
    # import pdb; pdb.set_trace()
    

    # Keep only the edges that goes to the model in the expression graph
    # TODO: Check if this has negative effect on test results
    ## This is only used in test postprocessingGenORLg and not in train
    # exprGraph = exprGraph.edge_subgraph(rel_seg_edges_str)
    edges_to_remove = list(set(exprGraph.edges()) -
                            set(rel_seg_edges_actual))
    exprGraph.remove_edges_from(edges_to_remove)
    rel_labels = [ self_dict["rels_dict"][s] for s in rel_labels ] \
            if self_dict["task"]=='train' or self_dict["node_seg"] is False else []
    return exprGraph, rel_edges, seg_edges, rel_labels, seg_labels, rel_seg_edges,

def get_region_data(self_dict, region_triple, regionName, lg_path, regionEntry):
    merge_idx = region_triple[3]
    
    fileName = regionName
    # regionImg = None
    symtags = None
    traces = []
    # dTimer = DebugTimer("Get Region Data")
                    
    # The keys in bboxes and contours represent indices of the actual primitives,
    # sorted in the LR-TD order rather than the actual primitive id in the lg files
    # To get the actual prim id, just use instances[idx], where idx are key in contours, bboxes
    ( _, instances, symbol_classes, boxes, symtags, traces, contours, hasSegmentations, shape ) = \
            getTSVRegion( self_dict, region_triple, regionEntry )    
    # import pdb; pdb.set_trace()
    # dTimer.check("Get TSV Region")
    # Check contours (traces), collect primitve ids, 
    # outer countours and image props
    if len(traces) < 1:
        print(">> Warning: SKIPPING " + fileName + ": no traces found")
        # return None, {}, {}, None, None
        return None

    instances = [ int(x) for x in instances ]
    outer_traces = [ x[0] for x in traces ] 
                            
    # # Construct and store the input graph (e.g. LOS/Complete)
    # ( rel_edges, rel_labels, exprGraph, seg_ref, los_edges,
    #     sorted_distances )= \
    #         GraphConstruction.getExprGraph( self_dict["GRAPH"],
    #                                         instances, symbols, outer_traces, symtags=symtags,
    #                                         lg=lg_path, level=self_dict["level"], seg=self_dict["node_seg"],
    #                                         task=self_dict["task"], find_punc=self_dict["find_punc"])
    # TODO: prune_graph = prune_rel for now. Combine prune_seg and prune_rel as
    # one (prune_graph) everywhere else if they are always same
    ( rel_seg_edges, rel_edges, rel_labels, exprGraph, seg_ref,
     rel_seg_edges_matrix, edge_distances_matrix )= \
            GraphConstruction.getExprGraph(self_dict["GRAPH"],
                                           instances, symbol_classes, outer_traces, symtags=symtags,
                                           lg=lg_path, level=self_dict["level"], seg=self_dict["node_seg"],
                                           task=self_dict["task"], find_punc=self_dict["find_punc"],
                                           prune_graph=self_dict["prune_rel"],
                                           prune_num_NN=self_dict["prune_num_NN"],
                                           remove_opposite_edges=self_dict["remove_opposite_edges"],)
    if exprGraph is None:
        print(">> Warning: SKIPPING " + fileName + ": input graph could not be created")
        return None

    # dTimer.check("Get Expression Graph")
    if len(rel_edges) == 0:
        rel_edges.append((0, 0))
        rel_seg_edges_matrix[0].append((0, 0))
        rel_labels.append('NoRelation')
    
    symbol_classes_int = [self_dict["symbols_dict"].get(s, self_dict["symbols_dict"]["_"])
                          for s in symbol_classes]
    

    if self_dict["GRAPH"] == "complete":
        rel_edges, seg_edges, rel_labels, seg_labels =\
            update_edges(self_dict, rel_seg_edges, seg_ref, rel_labels, rel_edges)      
    elif self_dict["GRAPH"] == "los":
        exprGraph, rel_edges, seg_edges, rel_labels, seg_labels, rel_seg_edges =\
            prune_LOS(self_dict, rel_seg_edges_matrix, instances, seg_ref, exprGraph, 
                      rel_labels, rel_edges)
    else:
        raise Exception("Graph Type is not recognized: " + self_dict["GRAPH"])
    
    # dTimer.check("Prune LOS")
    
    rel_edges_lg = list(map(lambda x: (str(instances[x[0]]), 
                                                str(instances[x[1]])), rel_edges))
    seg_edges_lg = list(map(lambda x: (str(instances[x[0]]), 
                                                str(instances[x[1]])), seg_edges))
                
    if merge_idx == 1:
        merge_dict = create_merge_dict(seg_edges, seg_labels)
    else:
        merge_dict = {}
    
    #TODO: Change pred_merged_dict to be different as gt_merged_dict
    formula_object = Formula(id_formula=regionName, expression_graph=exprGraph, shape=shape,
                            instances=instances, symbol_classes=symbol_classes,
                             symbol_classes_int=symbol_classes_int, gt_merge_dict=merge_dict, 
                            pred_merge_dict=merge_dict, contours=contours,
                            rel_edges=rel_edges, seg_edges=seg_edges,
                             rel_labels=rel_labels, seg_labels=seg_labels,
                             rel_seg_edges_matrix=rel_seg_edges_matrix,
                             edge_distances_matrix=edge_distances_matrix,
                             task=self_dict["task"])

    return formula_object

def get_region_data_old(self_dict, region_triple, regionName, lg_path, regionEntry):
    merge_idx = region_triple[3]
    
    fileName = regionName
    # regionImg = None
    symtags = None
    traces = []
    # dTimer = DebugTimer("Get Region Data")
                    
    # The keys in bboxes and contours represent indices of the actual primitives,
    # sorted in the LR-TD order rather than the actual primitive id in the lg files
    # To get the actual prim id, just use instances[idx], where idx are key in contours, bboxes
    ( _, instances, symbols, boxes, symtags, traces, contours, hasSegmentations, shape ) = \
            getTSVRegion( self_dict, region_triple, regionEntry )    
    # dTimer.check("Get TSV Region")
    # Check contours (traces), collect primitve ids, 
    # outer countours and image props
    if len(traces) < 1:
        print(">> Warning: SKIPPING " + fileName + ": no traces found")
        return None, {}, {}, None, None

    instances = [ int(x) for x in instances ]
    outer_traces = [ x[0] for x in traces ] 
                            
    # Construct and store the input graph (e.g. LOS/Complete)
    ( rel_edges, rel_labels, exprGraph, seg_ref, los_edges,
        sorted_distances )= \
            GraphConstruction.getExprGraph( self_dict["GRAPH"],
                                            instances, symbols, outer_traces, symtags=symtags,
                                            lg=lg_path, level=self_dict["level"], seg=self_dict["node_seg"],
                                            task=self_dict["task"], find_punc=self_dict["find_punc"])
    # dTimer.check("Get Expression Graph")
    if len(rel_edges) == 0:
        rel_edges.append((0, 0))
        los_edges[0].append((0, 0))
        rel_labels.append('NoRelation')
    
    symbol_class_ids = [self_dict["symbols_dict"].get(s, self_dict["symbols_dict"]["_"]) for s in symbols]
    
    
    exprGraph, rel_edges, seg_edges, rel_labels, seg_labels, rel_seg_edges =\
        prune_LOS(self_dict, los_edges, instances, seg_ref, exprGraph, rel_labels, rel_edges)      
    # dTimer.check("Prune LOS")
    
    rel_edges_lg = list(map(lambda x: (str(instances[x[0]]), 
                                                str(instances[x[1]])), rel_edges))
    seg_edges_lg = list(map(lambda x: (str(instances[x[0]]), 
                                                str(instances[x[1]])), seg_edges))
                
    if merge_idx == 1:
        merge_dict = create_merge_dict(seg_edges, seg_labels)
    else:
        merge_dict = {}
    
    # dTimer.check("Create Merge Groups")
    dict_nodes, table_nodes = create_node_objects(labels=symbol_class_ids, labels_string=symbols,
                                                  instances_lg=instances, boxes=boxes,
                                                  contours_list=contours, merge_groups=merge_dict,
                                                  id_formula=regionName, los_edges=los_edges,
                                                  k=self_dict["context_num_NN"])
    dict_edges, table_edges = create_edge_objects(rel_edges=rel_edges, seg_edges=seg_edges,
                                                  rel_labels=rel_labels, seg_labels=seg_labels,
                                                  rel_edges_lg=rel_edges_lg, seg_edges_lg=seg_edges_lg,
                                                  rel_seg_edges=rel_seg_edges, merge_groups=merge_dict,
                                                  id_formula=regionName, los_edges=los_edges, 
                                                  sorted_distances=sorted_distances,
                                                  k=self_dict["context_num_NN"], sym_labels=symbols)

    # dTimer.check("Create Node and Edge Objects")
    # import pdb; pdb.set_trace()
            
    treasure = pd.concat([table_nodes, table_edges]).reset_index(drop=True)                

    return exprGraph, dict_nodes, dict_edges, treasure, shape

def create_merge_dict(seg_edges, seg_labels):
    merge_dict = defaultdict(set)
    
    merge_edges = np.array(seg_edges)[np.where(np.array(seg_labels) == 0)].tolist()
    if len(merge_edges):
        merge_edges = get_ccs(merge_edges)            
        for merges in merge_edges:
            for prim in merges:
                merge_dict[prim] = merge_dict[prim].union(merges - {prim})
    return merge_dict

def getTSVRegion( self_dict, indexTuple, regionEntry ):
    # Reads the region, updates current doc/page/region as a side-effect.
    # dTimer.check("Before start reading protables")
    ( doc, page, region, merge_idx ) = indexTuple
    traces = []
    
    hasSegmentations = regionEntry[-2]
    ( minX, minY, maxX, maxY ) = regionEntry[0][:4]
    offsetVector = np.array([ minY, minX, minY, minX ]).astype(int)
    contour_offset = np.array([minX, minY])
    
    primitiveIds = []
    classLabels = []
    bboxes = {}
    contours = {}
    symtags = []

    # RZ: Add object type filter; pass only indicated types for parsing
    objects = regionEntry[1]

    # THe objects/primitives are sorted in LR-TD order from ProTables:
    # protables/ProTables.py: read_lg(): sort_contours()
    for i in range(len(objects)):
        # Get label and type, construct id.
        (oLabel, oType ) = objects[i][4:6]
        if oType not in self_dict["inputRegionTypes"]:
            continue

        # Use numeric identifiers as given in the input.
        try:
            # If instance id and symtags is present in data, use them
            objId = objects[i][6]
            symtags.append(objects[i][7])
        except:
            objId = int(i) 

        #  if traces present
        if len(objects[i]) >= 9:
            if self_dict["dataset"]=='infty' or self_dict["dataset"] == "chem":
                # import pdb; pdb.set_trace()
                # contour = np.array(objects[i][8])
                contour = objects[i][8]
                contour = [np.array(c) - contour_offset for c in contour]
                # import pdb; pdb.set_trace()
                
                # contour = contour - offsetVector[:2][::-1]
                contours[ i ] = contour
                traces.append(np.concatenate(contour).reshape(1, -1, 2))
            else:
                # For CROHME dataset,
                traces.append([objects[i][8]])
        # The 'merge' here are label mappings (e.g, all lines to one repr.)
        primitiveIds.append( objId )
        classLabels.append( check_for_merge( oLabel ) )

        # RZ DEBUG: Boxes require row-column (Y-X) coordinates.
        ( bbminX, bbminY, bbmaxX, bbmaxY ) = objects[i][:4]
        bboxes[ i ] = np.array( ( bbminY, bbminX, bbmaxY, bbmaxX ) )\
                .astype(int) - offsetVector

    # Update object state.
    # ( self_dict["currDoc"], self_dict["currPage"], self_dict["currRegion"], _) = indexTuple
    # dTimer.check("Read protables information")

    # The keys in bboxes and contours represent indices of the actual primitives,
    # sorted in the LR-TD order rather than the actual primitive id in the lg files
    # To get the actual prim id, just use primitiveIds[idx], where idx are key in contours, bboxes
    return ( None, primitiveIds, classLabels, bboxes, symtags, traces,
            contours, hasSegmentations, (maxY, maxX)) #appended shape

def balance_neg_instances(data_instances, data_labels, num_primitives,
                              negative, seed=0):
    neg_idx = np.where(np.array(data_labels) == negative)[0]
    len_neg = len(neg_idx)
    len_pos = len(data_labels) - len(neg_idx) 
    sample_size = int(round(max(len_pos, num_primitives)))
    if len_neg > len_pos and len_neg > num_primitives:
        random.seed(seed)
        sampling = random.sample(list(neg_idx), sample_size)
        new_instances = []
        new_labels = []
        for i, label in enumerate(data_labels):
            if i in sampling or label != negative:
                new_labels.append(data_labels[i])
                new_instances.append(data_instances[i])
        data_instances = new_instances
        data_labels = new_labels
    return data_instances, data_labels


def create_merged_queries(params):
    formula_id, list_edges, formula_dict_value, \
        treasure, testing_mode, epoch_number = params
    formulas_dict_local = {}
    remove_from_treasure_for_testing = []    
    formulas_dict_local[formula_id] = formula_dict_value

    formulas_dict_local[formula_id].hasSegmentation = False
    formulas_dict_local[formula_id].ids_remove_from_treasure_for_testing = []
    table = pd.DataFrame(DatasetItem.get_scheme())    

    just_edges = list(map(itemgetter(0), list_edges))
    merge_edges = get_ccs(just_edges)
    merge_groups = defaultdict(set)
    for merges in merge_edges:
        for prim in merges:
            merge_groups[prim] = merge_groups[prim].union(merges)
    nodes_in_formula = formula_dict_value.dict_nodes
    edges_in_formula = formula_dict_value.dict_edges

    if formula_dict_value.merge_groups_test == merge_groups and testing_mode:
        return [formulas_dict_local, [table]]
    else:
        formulas_dict_local[formula_id].merge_groups_test = merge_groups

    offset_nodes = max(nodes_in_formula.keys())+1
    offset_edges = max(edges_in_formula.keys())+1

    new_nodes = {}
    new_edges = {}

    ids_primitives_nodes = treasure[(treasure.id_formula==formula_id)&
                                     (treasure.type_derivation=="primitive")
                                     &(treasure.type=="node")]["id_item"].values

    for idx in ids_primitives_nodes:
        node = nodes_in_formula[idx]
        if idx in merge_groups:
            merges = merge_groups[idx]
            node_seg = node.create_segmentation_node(merges, offset_nodes)
            node_seg.epoch = epoch_number
            new_nodes[node_seg.id] = node_seg
            node.predicted_segmented_partner_id = offset_nodes
            node_seg.is_predicted_segmentation = True
            node.was_segmented = True
            offset_nodes += 1
            table = pd.concat([table, node_seg.get_data_frame_row()])
            if testing_mode:
                id_index_node = treasure[(treasure.id_formula==formula_id)&
                                        (treasure.id_item==node.id)
                                        &(treasure.type=="node")].index
                remove_from_treasure_for_testing.append(id_index_node.item())
                formulas_dict_local[formula_id].hasSegmentation = True
        else:
            node.predicted_segmented_partner_id = -1

    ids_primitives_edges = treasure[(treasure.id_formula==formula_id)&
                                     (treasure.type_derivation=="primitive")
                                     &(treasure.type=="edge")]["id_item"].values

    for idx in ids_primitives_edges:
        edge = formula_dict_value.dict_edges[idx]
        node_id_1, node_id_2 = edge.edge_tuple
        if node_id_1 in merge_groups or node_id_2 in merge_groups:
            edge_duplicated = edge.create_segmentation_edge(offset_edges)
            edge_duplicated.epoch = epoch_number
            edge.predicted_segmented_partner_id = offset_edges
            edge_duplicated.is_predicted_segmentation = True
            edge.was_segmented = True
            new_edges[edge_duplicated.id] = edge_duplicated            
            offset_edges += 1
            table = pd.concat([table, edge_duplicated.get_data_frame_row()])
            if testing_mode:
                id_index_edge = treasure[(treasure.id_formula==formula_id)&
                                         (treasure.id_item==edge.id)
                                        &(treasure.type=="edge")].index
                remove_from_treasure_for_testing.append(id_index_edge.item())
        else:            
            edge.predicted_segmented_partner_id = -1
    
    formulas_dict_local[formula_id].dict_nodes = combine_dict_pair(nodes_in_formula, new_nodes)
    formulas_dict_local[formula_id].dict_edges = combine_dict_pair(edges_in_formula, new_edges)

    formulas_dict_local[formula_id].ids_remove_from_treasure_for_testing = remove_from_treasure_for_testing
    if testing_mode:
        flush_from_ids(remove_from_treasure_for_testing, formulas_dict_local, treasure)
    return [formulas_dict_local, [table]]

def flush_from_ids(remove_from_treasure_for_testing, formulas_dict, treasure):
    for id_treasure in remove_from_treasure_for_testing:
        row_data = treasure.iloc[id_treasure]
        if row_data.type_derivation == "segmentation":
            if row_data.type == "node":
                del formulas_dict[row_data.id_formula].dict_nodes[row_data.id_item]
            else:
                del formulas_dict[row_data.id_formula].dict_edges[row_data.id_item]
