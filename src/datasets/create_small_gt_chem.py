import os
import shutil
import argparse
from tqdm import tqdm
CHEM_DIR = os.path.join("data", "generated-dataset", "chem")

def parse_args():
    # Ignore the first argument ('streamlit', 'run', 'script.py'), parse the rest
    parser = argparse.ArgumentParser(description='Create subset of lg files')
    parser.add_argument('--input_dir', '-i', help='Path to the folder containing input lg files',
                        default=os.path.join(CHEM_DIR, "train_pubchem_5k",
                                             "lgs", "line"))
    parser.add_argument('--output_dir', '-o', help='Path to the folder containing output lg files')
    parser.add_argument('--file_list', '-l',
                        default=os.path.join("data", "training_pubchem_5k_visual_exact_matches.txt"),
                        help='List of files to include')
    args = parser.parse_args()
    return args

if __name__ == "__main__":
    args = parse_args()

    if not args.output_dir:
        output_dir = args.input_dir + "_small"
    else:
        output_dir = args.output_dir

    if not os.path.exists(output_dir):
        os.makedirs(output_dir, exist_ok=True)

    with open(args.file_list, "r") as f:
        lines = f.read().split('\n')
        for line in tqdm(lines):
            if line:
                shutil.copyfile(os.path.join(args.input_dir, line + ".lg"),
                                os.path.join(output_dir, line + ".lg"))
    print(f"Input dir: {args.input_dir}")
    print(f"Output dir: {output_dir}")
