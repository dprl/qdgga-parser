import torch
import numpy as np
import random
import os
import torch.distributed as dist

from torch.nn import ZeroPad2d
from torch.utils.data import SequentialSampler, RandomSampler


class Partition(object):

    def __init__(self, data, index):
        self.data = data
        self.index = index
        self.task = data.task

    def __len__(self):
        return len(self.index)

    def __getitem__(self, index):
        data_idx = self.index[index]
        return self.data[data_idx]


class DataPartitioner(object):

    def __init__(self, data, size=2):
        self.data = data
        self.partitions = []
        data_len = len(data)
        indexes = [x for x in range(0, data_len)]

        self.partitions = np.array_split(indexes, size)

    def use(self, partition):
        return Partition(self.data, self.partitions[partition])


""" Partitioning dataset """


def partition_dataset(dataset):
    size = dist.get_world_size()
    partition = DataPartitioner(dataset, size)
    partition = partition.use(dist.get_rank())
    return partition, len(partition)


def seed_worker(worker_id):
    worker_seed = torch.initial_seed() % 2 ** 32
    np.random.seed(worker_seed)
    random.seed(worker_seed)


class CustomDataLoader(object):
    def __init__(self, dataset, device=0, shuffle=False,
                 gpu_memory_threshold=0.0155, max_feat_size=64):
        self.dataset = dataset
        self.dataset_size = len(dataset)
        self.device = device
        self.free_mem = self.memory_available()[self.device]
        if shuffle:
            self.sampler = RandomSampler(dataset)
        else:
            self.sampler = SequentialSampler(dataset)
        self.gpu_memory_threshold = gpu_memory_threshold
        self.max_feat_size = max_feat_size
        
    def size_fn(self, tensors):
        tensors = torch.cat(tensors)
        return tensors.nelement() * tensors.element_size()

    @staticmethod
    def memory_available():
        free = os.popen('nvidia-smi -q -d Memory |grep -A5 GPU|grep Free').read().strip().split('\n')
        memory_available = [int(f.split()[2]) for f in free]
        return memory_available

    # def __len__(self):
    #     return len(self.return_batch["widths"])

    def __iter__(self):
        max_width = 0
        max_formula_width = 0
        tensor_memory = 0

        batch = {
            "symbols": [],
            "segments": [],
            "edgeLabels": [],
            "images": [],
            "prim_bbox": [],
            "file_name": [],
            "pdf_name": [],
            "indexTuple": [],
            "instances": [],
            "binary_edgelabels": [],
            "graph_edgeRef": [],
            "graph_edgeRef_sorted": [],
            # "pruned_edges": [],
            "graph_segRef": [],
            "num_images": [],
            "len": [],
            "error": [],
            # "widths": [],
            # "formula_widths": [],
            "scaled_sizes": [],
            "image_shape": [],
        }

        concat_keys = {"symbols", "segments", "edgeLabels", "images",
                       "prim_bbox", "scaled_sizes"}
        list_keys = {"file_name", "pdf_name", "indexTuple", "instances",
                     "binary_edgelabels", "graph_edgeRef", "graph_edgeRef_sorted", 
                     "graph_segRef", "num_images", "image_shape"}
                     # "pruned_edges", 
        tensor_keys = batch.keys() - concat_keys - list_keys

        return_batch = {}

        img_count = 1
        for idx in self.sampler:
            sample = self.dataset[idx]

            # RZ: skip files with errors during data prep.
            if sample["error"]:
                continue

            # width = sample["widths"]
            # formula_width = sample["formula_widths"]
            # if width > max_width:
            #     max_width = width

            # if formula_width > max_formula_width:
            #     max_formula_width = formula_width

            # Compute the memory occupied by the tensor
            images = [sample["images"], *batch["images"]]
            num_edges = len(sample["graph_edgeRef"]) + sum([len(edge) for edge in batch["graph_edgeRef"]])

            # length = sum([x.shape[0] for x in images]) 
            length = sum([x.shape[0] for x in images]) + num_edges + img_count
            # Additional count for formula image
            # images_memory = length * images[0].shape[1] * max_width * images[0].element_size()
            # images_memory = length * images[0].shape[1] * max_formula_width * images[0].element_size()
            images_memory = 3 * length * self.max_feat_size * self.max_feat_size * images[0].element_size()
            other_memory = self.size_fn([sample["symbols"], *batch["symbols"]]) + \
                           self.size_fn([sample["segments"], *batch["segments"]]) + \
                           self.size_fn([sample["edgeLabels"], *batch["edgeLabels"]])
            tensor_memory = (images_memory + other_memory) / (1024 ** 2)

            self.free_mem = self.memory_available()[self.device]
            # print(f"Free memory = {self.free_mem}")
            # print(f"idx={idx}: Tensor memory = {tensor_memory}, length = {length}, img = {sum(x.shape[0] for x in images) + img_count}, add={img_count}")
            memory_ratio = tensor_memory / self.free_mem
            img_count += 1

            # If the added tensor fits into GPU, concatenate the data instance
            # if memory_ratio < self.gpu_memory_threshold:
            # Temp fix: Add the last sample as well although it doesn't fit
            # TODO: Add last sample as a separate batch
            if memory_ratio < self.gpu_memory_threshold or idx == self.dataset_size - 1:
                # for key in batch.keys() - ["num_images", "widths",
                #                            "formula_widths"]:
                #     batch[key].append(sample[key])
                # batch["num_images"].append(sample["images"].shape[0])
                for key in batch.keys():
                    batch[key].append(sample[key])
                # batch["widths"].append(width)
                # batch["formula_widths"].append(formula_width)

            # If the added tensor does not fit into GPU any more or end of the dataset, 
            # return the batch and start new batch if more dataset
            if memory_ratio >= self.gpu_memory_threshold or idx == self.dataset_size - 1:
                # If not a single formula fits into the GPU, add 1 formula
                if len(batch["file_name"]) == 0:
                    for key in batch.keys():
                        batch[key].append(sample[key])
                    # for key in batch.keys() - ["num_images", "widths",
                    #                            "formula_widths"]:
                    #     batch[key].append(sample[key])
                    # batch["num_images"].append(sample["images"].shape[0])
                    # batch["widths"].append(width)
                    # batch["formula_widths"].append(formula_width)
                # print(f"\n\nTensor memory = {tensor_memory}, length = {length}")
                # import pdb; pdb.set_trace()
                # print(f"\n\nImages memory = {images_memory / (1024 ** 2)}")
                # max_width = max(batch["widths"])
                # max_formula_width = max(batch["formula_widths"])

                # Paddding along the widths
                # batch["images"] = [ZeroPad2d((0, max_width - t.shape[2]))(t)
                #                    for t in batch["images"]]

                # Concatenate the tensors along the num_images dimension
                for key in concat_keys:
                    return_batch[key] = torch.cat(batch[key])

                # The tensors which don't need concatenation
                for key in tensor_keys:
                    return_batch[key] = torch.tensor(batch[key])

                for key in list_keys:
                    return_batch[key] = batch[key]

                # Start new batch with the currently added instance
                for key in batch.keys():
                    batch[key] = [sample[key]]
                # for key in batch.keys() - ["num_images", "widths",
                #                            "formula_widths"]:
                    # batch[key] = [sample[key]]
                # batch["num_images"] = [sample["images"].shape[0]]
                # batch["widths"] = [width]
                # batch["formula_widths"] = [formula_width]

                # Reset the variables for new batch
                tensor_memory = 0
                img_count = 2  # Last formula which didn't fit already added
                # max_width = width
                # max_formula_width = formula_width
                # self.return_batch = return_batch
                yield return_batch
