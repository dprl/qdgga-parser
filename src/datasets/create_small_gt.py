import os
import shutil
from lgap_settings import DATA_DIR

if __name__ == "__main__":
    INFTY_DIR = os.path.join(DATA_DIR, "InftyMCCDB-2")
    CROHME_DIR = os.path.join(DATA_DIR, "crohme2019")

    GT_DIR = os.path.join(INFTY_DIR, "test_merged_class")
    GT_TRAIN_DIR = os.path.join(INFTY_DIR, "train_merged_class")

    GT_CROHME_DIR = os.path.join(CROHME_DIR, "Test2019_LG")
    GT_TRAIN_CROHME_DIR = os.path.join(CROHME_DIR, "Train", "LGs", "all")

    GT_SMALL_DIR = os.path.join(INFTY_DIR, "test_merged_class_0.25")
    GT_SMALL_DEV_DIR = os.path.join(INFTY_DIR, "dev_merged_class_0.25")

    GT_SMALL_CROHME_DIR = os.path.join(CROHME_DIR, "Test2019_LG_0.5")
    GT_SMALL_CROHME_DEV_DIR = os.path.join(CROHME_DIR, "Dev2019_LG_0.5")

    GT_SMALL_FILE_DIR = os.path.join(DATA_DIR, "testing_0.25.txt")
    GT_SMALL_FILE_CROHME_DIR = os.path.join(DATA_DIR, "testing_0.5_crohme.txt")

    GT_SMALL_FILE_DEV_DIR = os.path.join(DATA_DIR, "dev_0.25.txt")
    GT_SMALL_FILE_DEV_CROHME_DIR = os.path.join(DATA_DIR, "dev_0.5_crohme.txt")


    if not os.path.exists(GT_SMALL_DIR):
        os.makedirs(GT_SMALL_DIR)

    with open(GT_SMALL_FILE_DIR, "r") as f:
        lines = f.read().split('\n')
        for line in lines:
            if line:
                shutil.copyfile(os.path.join(GT_DIR, line + ".lg"),
                                os.path.join(GT_SMALL_DIR, line + ".lg"))

    if not os.path.exists(GT_SMALL_CROHME_DIR):
        os.makedirs(GT_SMALL_CROHME_DIR)

    with open(GT_SMALL_FILE_CROHME_DIR, "r") as f:
        lines = f.read().split('\n')
        for line in lines:
            if line:
                shutil.copyfile(os.path.join(GT_CROHME_DIR, line + ".lg"),
                                os.path.join(GT_SMALL_CROHME_DIR, line + ".lg"))


    # For the dev test set
    if not os.path.exists(GT_SMALL_DEV_DIR):
        os.makedirs(GT_SMALL_DEV_DIR)

    with open(GT_SMALL_FILE_DEV_DIR, "r") as f:
        lines = f.read().split('\n')
        for line in lines:
            if line:
                shutil.copyfile(os.path.join(GT_TRAIN_DIR, line + ".lg"),
                                os.path.join(GT_SMALL_DEV_DIR, line + ".lg"))

    if not os.path.exists(GT_SMALL_CROHME_DEV_DIR):
        os.makedirs(GT_SMALL_CROHME_DEV_DIR)

    with open(GT_SMALL_FILE_DEV_CROHME_DIR, "r") as f:
        lines = f.read().split('\n')
        for line in lines:
            if line:
                shutil.copyfile(os.path.join(GT_TRAIN_CROHME_DIR, line + ".lg"),
                                os.path.join(GT_SMALL_CROHME_DEV_DIR, line + ".lg"))
