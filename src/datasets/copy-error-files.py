import os
import sys
import shutil
from lgap_settings import DATA_DIR, ERROR_FILES_CROHME

CROHME_DIR = os.path.join(DATA_DIR, "crohme2019")
GT_CROHME_DIR = os.path.join(CROHME_DIR, "Test2019_LG")

def copy(output_dir):
    if not os.path.exists(output_dir):
        print(f"Directory does not exist. Creating the directory: {output_dir}")
        os.makedirs(output_dir)

    count = 0
    for file in ERROR_FILES_CROHME:
        file = os.path.splitext(file)[0] + ".lg"
        if os.path.exists(os.path.join(GT_CROHME_DIR, file)):
            shutil.copyfile(os.path.join(GT_CROHME_DIR, file), 
                            os.path.join(output_dir, file))
            count += 1
            print(f"{file} copied")
        else:
            print(f">> Warning: {file} does not exist")

    print(f"Copied {count} error files to {output_dir}")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python copy-error-files.py <output_dir>")
        sys.exit()
    out_dir = sys.argv[1]
    copy(out_dir)

