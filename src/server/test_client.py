import os.path
from io import BytesIO

import aiohttp
import asyncio
import argparse
import time
import requests
from aiohttp import FormData
import zipfile


async def aiohttp1(pdf_list, tsv_list, endpoint, save_loc):
    os.makedirs(save_loc, exist_ok=True)
    async with aiohttp.ClientSession() as session:
        data = FormData()
        for pdf in pdf_list:
            data.add_field('pdf_list',
                           open(pdf, 'rb'),
                           filename=os.path.basename(pdf),
                           content_type='application/pdf')
        for tsv in tsv_list:
            data.add_field('tsv_list',
                           open(tsv, 'rb'),
                           filename=os.path.basename(tsv),
                           content_type='text/tab-separated-values')

        co_route = session.post(endpoint, data=data)
        responses = await asyncio.gather(co_route)
        zip_bytes = await responses[0].content.read()
        z = zipfile.ZipFile(BytesIO(zip_bytes))
        z.extractall(save_loc)


def sync_call(pdf_list, tsv_list, endpoint, save_loc):
    os.makedirs(save_loc, exist_ok=True)
    for pdf, tsv in zip(pdf_list, tsv_list):
        files = {'pdf_list': open(pdf,'rb'), 'tsv_list': open(tsv,'rb')}
        response = requests.post(endpoint, files=files)
        zip_bytes = response.content
        z = zipfile.ZipFile(BytesIO(zip_bytes))
        z.extractall(save_loc)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--endpoint', default="http://localhost:8000/predict-pdf?dpi=256")
    args = parser.parse_args()

    pdf_files = ['../../server_data/ACL/pdf/A00-3007.pdf',
                 '../../server_data/ACL/pdf/K15-1002.pdf',
                 '../../server_data/ACL/pdf/K15-1004.pdf',
                 '../../server_data/ACL/pdf/W07-0901.pdf',
                 '../../server_data/ACL/pdf/W07-1428.pdf']

    tsv_files = ['../../server_data/ACL/tsv/A00-3007.tsv',
                 '../../server_data/ACL/tsv/K15-1002.tsv',
                 '../../server_data/ACL/tsv/K15-1004.tsv',
                 '../../server_data/ACL/tsv/W07-0901.tsv',
                 '../../server_data/ACL/tsv/W07-1428.tsv']

    start = time.time()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(aiohttp1(pdf_files, tsv_files, args.endpoint, "../../output_async"))
    end = time.time()
    print(f"time taken to process pdfs async {str(end - start)}")

    '''start = time.time()
    sync_call(pdf_files, tsv_files, args.endpoint, "../../output_sync")
    end = time.time()
    print(f"time taken to process pdfs sync {str(end - start)}")'''