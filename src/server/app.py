import os.path
import uvicorn
from fastapi import FastAPI, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from src.server.directory_service import *
from test import parse_args, Parser
import torch.multiprocessing as mp

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

#mp.set_start_method("spawn")

@app.get("/")
def root():
    return {"message": "Welcome to LGAP recognition system"}


@app.post("/predict-pdf")
async def predict_pdf(tsv_list: List[UploadFile] = File(...), pdf_list: List[UploadFile] = File(...), dpi: int = 256):
    image_dir, tsv_dir, out_dir = prepare_temp_dirs()

    config = parse_args(
        [
            "--config", os.path.join("config", "infty.yaml"),
            "--img_dir", image_dir,
            "--tsv_dir", tsv_dir,
            "--given_sym", "1",
            "--output_dir", out_dir,
            "--model_name", "infty_contour",
            "--last_epoch", "89",
            "--dot_dir", "",
            "--visualize", "0"
        ])

    await save_pdf_list(pdf_list, image_dir, dpi)
    await save_tsv_list(tsv_list, tsv_dir)


    parser = Parser(default_config=config.default_config,
                    run_config=config.run_config, InputData=config.InputData,
                    model_name=config.model_name, model_dir=config.model_dir,
                    last_epoch=config.last_epoch)

    parser.parse(
        tsv_output_dir=config.output_dir,
        given_sym=config.given_sym, ptsv_dir=config.tsv_dir,
        pimg_dir=config.img_dir,
        dot2pdf=config.sltpdf, dot_dir=config.dot_dir,
        img_list=config.img_list, lg_dir=config.lg_dir,
        data_dir=config.data_dir)

    streaming_out = package_into_zip_streaming_response(out_dir)
    clean_up_temp_dirs(image_dir, tsv_dir, out_dir)
    return streaming_out

if __name__ == "__main__":
    uvicorn.run(app, debug=True)
