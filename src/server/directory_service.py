import os
import zipfile
from io import BytesIO
from typing import List, Tuple
from fastapi import UploadFile, Response
import uuid
from pdf2image import convert_from_bytes
from fastapi.responses import StreamingResponse
import glob
import shutil


async def save_pdf_list(file_list: List[UploadFile], abs_dir_path, dpi):
    for pdf in file_list:
        filename_no_ext = os.path.splitext(pdf.filename)[0]
        sub_dir = os.path.join(abs_dir_path, filename_no_ext)
        os.makedirs(sub_dir, exist_ok=True)
        await convert_pdf_and_save_images(pdf, sub_dir, dpi)


async def convert_pdf_and_save_images(pdf, sub_dir, dpi):
    pdf_bytes = await pdf.read()
    image_list = convert_from_bytes(pdf_bytes, dpi=dpi)
    for i, img in enumerate(image_list):
        img.save(os.path.join(sub_dir, f"{str(i+1)}.png"), fmt="png")


async def save_tsv_list(file_list: List[UploadFile], abs_dir_path):
    for f in file_list:
        await save_uploaded_file(f, abs_dir_path)


async def save_uploaded_file(tsv: UploadFile, abs_dir_path):
    tsv_bytes = await tsv.read()
    with open(os.path.join(abs_dir_path, tsv.filename), "wb") as f:
        f.write(tsv_bytes)


def prepare_temp_dirs() -> Tuple[str, str, str]:
    dir_uuid = str(uuid.uuid4())
    base_dir = os.path.join(os.sep, 'tmp', dir_uuid)
    images_dir = os.path.join(base_dir, "images")
    tsv_dir = os.path.join(base_dir, "tsv")
    out_dir = os.path.join(base_dir, "output")
    os.makedirs(images_dir, exist_ok=True)
    os.makedirs(tsv_dir, exist_ok=True)
    os.makedirs(out_dir, exist_ok=True)
    return images_dir, tsv_dir, out_dir


def package_into_zip_streaming_response(out_dir) -> StreamingResponse:
    output_tsv_list = glob.glob(os.path.join(out_dir, '*.tsv'))

    zip_io = BytesIO()
    zipped_tsv = zipfile.ZipFile(zip_io, "w")

    for tsv in output_tsv_list:
        with open(tsv, 'rb') as f:
            
            zipped_tsv.writestr(os.path.basename(tsv), data=f.read(), compress_type=zipfile.ZIP_DEFLATED)
    zipped_tsv.close()

    response = StreamingResponse(
        iter([zip_io.getvalue()]),
        media_type='application/x-zip-compressed',
        headers={
            'Content-Disposition': 'attachment;filename=parser_predictions.zip',
            'Access-Control-Expose-Headers': 'Content-Disposition'
        }
    )
    return response


def clean_up_temp_dirs(images_dir, tsv_dir, out_dir):
    shutil.rmtree(images_dir)
    shutil.rmtree(tsv_dir)
    shutil.rmtree(out_dir)
    shutil.rmtree(os.path.dirname(images_dir))
