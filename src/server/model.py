from pydantic import BaseModel
from enum import Enum
from typing import List, Optional
from datetime import datetime


class Image(BaseModel):
    base64_img: str
    dpi: int


class Strokes(BaseModel):
    points: str
    id: str


class InputType(str, Enum):
    strokes = "strokes"
    image = "image"


class Input(BaseModel):
    request_time: datetime
    input_type: InputType
    file_id: str
    annotation: Optional[str] = None
    input_image: Optional[Image] = None
    input_strokes: Optional[List[Strokes]] = None


class Output(BaseModel):
    response_time: datetime
    duration: float
    input_type: InputType
    input_file_id: str
    output_file_id: str
    annotation: Optional[str] = None
    mathml: str
    latex: str
    lg: str
    topk_symbols: List = []


class PdfOutput(BaseModel):
    mathml: str
    latex: str
    lg: str
    topk_symbols: List = []