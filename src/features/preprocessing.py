import math
import numpy as np
from .math_ops import MathOps


#MM preprocessing points from LPGA
def fixTrace(trace):
    
    trace = removeDuplicatedPoints(trace)
    trace = addMissingPoints(trace)
    trace = apply_catmull_Smoothing(trace)
    # rounding points to remove decimals
    trace = [[int(round(x)),int(round(y))] for x,y in trace]
    return np.array(trace)

def distance(points,i, j):
    x1, y1 = points[i]
    x2, y2 = points[j]

    return math.sqrt(math.pow((x1 - x2), 2) + math.pow((y1 - y2), 2))


def removeDuplicatedPoints(trace):
    trace=list(trace)

    minX, minY = np.min(trace, axis=0)
    maxX, maxY = np.max(trace, axis=0)
    
    w = maxX - minX
    h = maxY - minY
    diagonal = math.sqrt( w * w + h * h )

    i = 0
    while i < len(trace):
        j = i + 1
        while j < len(trace)-1:
            if distance(trace,i, j) > diagonal * 0.1:
                break

            if trace[i][0] == trace[j][0] and trace[i][1] == trace[j][1]:
                del trace[j]
            else:
                j += 1
        i += 1
    return np.array(trace)
        
# Add points where there are missing points (pre processing)
def addMissingPoints(trace):
    # calculate Le (average segment length) as defined in [1]
    trace=list(trace)
    Le = 0
    p=len(trace)
    
    for i in range(p - 1):
        Le += distance( trace,i, i + 1)
    Le /= p

    # the distance used to insert points...
    d = 0.95 * Le
    i = 0
    while i < p - 1:
        # search point to interpolate ...
        n = 1
        length = distance(trace, i, i + n)
        sum = 0

        while sum + length < d and i + n + 1 < p:
            n += 1
            sum += length
            length = distance(trace,i + n - 1, i + n)

        diff = d - sum

        # insert a point between i + n- 1 and i +n at distance diff
        # use linear interpolation...
        if length !=0: 
            w2 = diff / length

            if w2 < 1.0:
                xp = trace[i + n - 1][0] * (1- w2) + trace[i + n][0] * w2
                yp = trace[i + n - 1][1] * (1- w2) + trace[i + n][1] * w2
    
                #check for collision with next point...
                insert = True
    
                if i + n < p:
                    if xp == trace[i + n][0] and yp==trace[i + n][1]:
                        # weird case where a point after interpolated falls of the same
                        # coordinates as next point, don't insert it
                        insert = False
    
                if insert:             
                    trace.insert(i + n, (round(xp), round(yp)))
                
        else:
            # at the end, no point added but erase the one at the end
            #n += 1
            pass

        # now erase points from i + 1 .. f + n - 1
        toErase = n - 1
        for j in range(toErase):
            del trace[i + 1]
        i += 1
        
    return np.array(trace)
        

# smooths the curve (pre-processing)
def apply_catmull_Smoothing(trace):
    # Detect sharp points and extract them....
    sharp_points = getSharpPoints(trace)
    # Remove hooks using sharp points information
    sharp_points = removeHooks(trace,sharp_points)
    # Finally re-sample
    trace = splineResample(trace, 2, sharp_points)
    return trace

# uses the algorithm defined in [1] to find the sharp points of the trace...
# returns a list of tuples of the form (index, (x, y))  for all the sharp points
def getSharpPoints(trace):
    # the first is a sharp point
    sharpPoints = [(0, (trace[0][0],trace[0][1]))]
    p =len(trace)

    # now calculate the slope angles between each pair of consecutive points...
    alpha = []
    for i in range(p - 1):
        alpha.append(MathOps.slopeAngle(trace[i], trace[i + 1]))

    # check
    if len(alpha) <= 1:
        # very special case where the trace is one single point
        # no more sharp points than itself...
        return sharpPoints

    # now detect sharp points...
    theta = [(alpha[0] - alpha[1])]
    for k in range(1, p - 1):
        # use two different tests to detect sharp point...
        # 1) change in writing direction (as defined in [1])
        # 2) difference in writing direction angle between current point and
        #   and last sharp point higher than a threshold
        addPoint = False

        # for 1) calculate difference in writing direction between
        # current point and previous point
        if k < p - 2:
            theta.append( alpha[k] - alpha[k + 1] )

            if theta[k] != 0.0 and k > 0:
                delta = theta[k] * theta[k - 1]

                if delta <= 0.0 and theta[k - 1] != 0.0:
                    # direction at which the pen is moving has changed
                    addPoint = True

        # for 2) calculate the difference of angle between
        # current point and last sharp point
        phi = MathOps.angularDifference(alpha[sharpPoints[-1][0]], alpha[k])

        # circular angle....
        if phi > math.pi:
            phi = math.pi * 2 - phi

        if phi >= math.pi / 8:
            addPoint = True

        if addPoint:
            sharpPoints.append((k, (trace[k][0],trace[k][1])))

    # the last point is a sharp point
    sharpPoints.append((len(trace) - 1, (trace[-1][0],trace[-1][1])))

    return sharpPoints

# removes the hooks at the beginning and at the end (pre-processing)
def removeHooks(trace, sharp_points):
    #sharp_points = trace.sharp_points

    if len(sharp_points) >= 4:
        # remove hooks....
        # Check for hooks in the segment b (beginning)
        # and at the segment e (ending)

        #get the diagonal length....
        minX, minY = np.min(trace, axis=0)
        maxX, maxY = np.max(trace, axis=0)
        ld = math.sqrt(math.pow(maxX - minX, 2) + math.pow(maxY - minY, 2))

        # at beginning
        betha_b0 = MathOps.slopeAngle(sharp_points[0][1], sharp_points[1][1])
        betha_b1 = MathOps.slopeAngle(sharp_points[1][1], sharp_points[2][1])
        lseg_b = distance(trace,sharp_points[0][0], sharp_points[1][0])
        lambda_b = MathOps.angularDifference(betha_b0, betha_b1)

        # At ending
        betha_e0 =  MathOps.slopeAngle(sharp_points[-1][1], sharp_points[-2][1])
        betha_e1 = MathOps.slopeAngle(sharp_points[-2][1], sharp_points[-3][1]);
        lseg_e = distance(trace,sharp_points[-1][0], sharp_points[-2][0])
        lambda_e = MathOps.angularDifference(betha_e0, betha_e1)

        if lambda_e > math.pi / 4 and lseg_e < 0.07 * ld:
            # remove sharp point at the end...
            del sharp_points[-1]

        if lambda_b > math.pi / 4 and lseg_b < 0.07 * ld:
            # remove sharp point at the beginning
            del sharp_points[0]

    return sharp_points


# Resample the trace using splines (pre-processing)
def splineResample(trace, subDivisions, sharp_points):
    new_points = []
    # check special case: Only one sharp_point
    if len(sharp_points) == 1:
        trace = [sharp_points[0][1]]
        return trace

    for i in range(len(sharp_points)):
        # add the sharp point
        new_points.append(sharp_points[i][1])

        if i < len(sharp_points) - 1:
            innerPoints = (sharp_points[i + 1][0] - sharp_points[i][0]) * subDivisions
            tStep = 1.0 / innerPoints

        # depending on the current range...
        if i == 0 or i == len(sharp_points) - 2:
            # between first and second sharp points...
            # or between the last two sharp points...
            # use linear interpolation...
            for k in range(1, innerPoints):
                new_points.append(MathOps.lerp(sharp_points[i][1], sharp_points[i + 1][1], tStep * k))

        elif i < len(sharp_points) - 2:
            # in the middle of four sharp points... use Catmull-Rom
            for k in range(1, innerPoints):
                point = MathOps.CatmullRom(sharp_points[i - 1][1], sharp_points[i][1],
                                           sharp_points[i + 1][1], sharp_points[i + 2][1], tStep * k)
                new_points.append(point)

    # now replace
    trace = np.array(new_points)
    return trace
