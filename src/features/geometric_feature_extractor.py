import numpy as np
import math
from scipy.spatial.distance import cdist
import warnings

class GeometricFeatureExtractor:

    @staticmethod
    def findBoxDistances(pbox, cbox):
        distances = np.array([])

        # Horizontal Distances
        distances= np.append(distances,(cbox[0] - pbox[0]))
        distances = np.append(distances,(cbox[0] - pbox[2]))
        distances = np.append(distances,(cbox[2] - pbox[0]))
        distances = np.append(distances,(cbox[2] - pbox[2]))

        # Vertical Distances
        distances = np.append(distances,cbox[1] - pbox[1])
        distances = np.append(distances,cbox[1] - pbox[3])
        distances = np.append(distances,cbox[3] - pbox[1])
        distances = np.append(distances,cbox[3] - pbox[3])

        # Center Distances
        pcenter = np.array([(pbox[0]+pbox[1])/2, (pbox[1]+pbox[3])/2])
        ccenter = np.array([(cbox[0]+cbox[1])/2, (cbox[1]+cbox[3])/2])
        distances = np.append(distances,(ccenter[0] - pcenter[0]))
        distances = np.append(distances,ccenter[1] - pcenter[1])
        distances = np.append(distances,np.linalg.norm(pcenter - ccenter))

        distances[np.isnan(distances)] = 0
        distances[~np.isfinite(distances)] = 0

        return distances.tolist()

    @staticmethod
    def getBoundingBoxDistances(pbox, cbox, ratio=False, center_distance_norm=False):
        # distances normalized by avg width, avg height, avg diagonal, distance between center
        distances = np.array(GeometricFeatureExtractor.findBoxDistances(pbox, cbox))

        if ratio:
            distances = np.abs(distances)

        if center_distance_norm:
            cen_dist = distances[-1]
            distances = np.array(distances[:-1]) / cen_dist
        else:
            avg_width = ((pbox[2] - pbox[0]) + (cbox[2] - cbox[0])) / 2
            avg_height = ((pbox[3] - pbox[1]) + (cbox[3] - cbox[1])) / 2
            avg_diagonal = (np.linalg.norm(pbox[0:2] - pbox[2:]) + np.linalg.norm(cbox[0:2] - cbox[2:])) / 2

            distances[0:5] /= avg_width
            distances[5:9] /= avg_height
            distances[9:] /= avg_diagonal
        #handle special cases
        distances[np.isnan(distances)] = 0
        distances[~np.isfinite(distances)] = 0

        return list(distances)

    @staticmethod
    def getBoundingBoxOverlaps(pbox, cbox):
        # initialize an empty array
        overlaps = np.array([])

        # Horizontal overlap
        if pbox[0] < cbox[2] and pbox[2] > cbox[0]:
            avg_width = ((pbox[2] - pbox[0]) + (cbox[2] - cbox[0])) / 2
            overlaps= np.append(overlaps, ( min(pbox[2],cbox[2]) - max(pbox[0],cbox[0]) ) / avg_width)
        else:
            overlaps = np.append(overlaps,0)

        # Vertical overlap
        if pbox[1] < cbox[3] and pbox[3] > cbox[1]:
            avg_height = ((pbox[3] - pbox[1]) + (cbox[3] - cbox[1])) / 2
            overlaps = np.append(overlaps,( min(pbox[3],cbox[3]) - max(pbox[1],cbox[1]) ) / avg_height)
        else:
            overlaps = np.append(overlaps,0)

        # Overlap
        avg_area = ( ((pbox[2] - pbox[0]) * (pbox[3] - pbox[1])) + ((cbox[2] - cbox[0]) * (cbox[3] - cbox[1])) ) / 2
        overlaps = np.append(overlaps,(overlaps[0] * overlaps[1]) / avg_area)

        # handle special cases
        overlaps[np.isnan(overlaps)] = 0
        overlaps[~np.isfinite(overlaps)] = 0

        return overlaps.tolist()

    @staticmethod
    def getBoundingBoxSizes(pbox, cbox):
        # overall size is the largest dimension of the trace/symbol
        # normalized by average of the two sizes
        sizes = np.array([])

        # difference in horizontal size
        pwidth = pbox[2] - pbox[0]
        cwidth = cbox[2] - cbox[0]
        sizes= np.append( sizes,(pwidth - cwidth) / ( (pwidth + cwidth) / 2 ) )

        # difference in vertical size
        pheight = pbox[3] - pbox[1]
        cheight = cbox[3] - cbox[1]
        sizes = np.append(sizes, (pheight - cheight) / ( (pheight + cheight) / 2 ) )

        # difference in size
        psize = max(pwidth, pheight)
        csize = max(cwidth, cheight)
        sizes = np.append(sizes, (psize - csize) / ( (psize + csize) / 2) )

        # ratio in size

        # handle special cases
        sizes[np.isnan(sizes)] = 0
        sizes[~np.isfinite(sizes)] = 0

        return sizes.tolist()

    @staticmethod
    def getBoundingBoxAreas(pbox, cbox):
        # areas of bounding boxes normalized by average area
        areas = np.array([])

        parea = (pbox[2] - pbox[0]) * (pbox[3] - pbox[1])
        carea = (cbox[2] - cbox[0]) * (cbox[3] - cbox[1])
        avg_area = (parea + carea) / 2

        # Parent area
        areas=np.append(areas,parea/avg_area)

        # Child area
        areas= np.append(areas,carea/avg_area)

        # Difference in area
        areas = np.append( areas, (parea - carea)/avg_area )

        #handle special cases
        areas[np.isnan(areas)] = 0
        areas[~np.isfinite(areas)] = 0

        return areas.tolist()

    @staticmethod
    def getBoundingBoxRatios(pbox, cbox):
        ratios = np.array([])

        pwidth = pbox[2] - pbox[0]
        cwidth = cbox[2] - cbox[0]
        pheight = pbox[3] - pbox[1]
        cheight = cbox[3] - cbox[1]

        psize = max(pwidth, pheight)
        csize = max(cwidth, cheight)
        pdiag = math.sqrt(pwidth**2 + pheight**2)
        cdiag = math.sqrt(cwidth**2 + cheight**2)
        parea = (pwidth) * (pheight)
        carea = (cwidth) * (cheight)

        if pwidth > 0 and cwidth > 0:
            ratios = np.append(ratios, (pwidth / cwidth))
        else:
            ratios = np.append(ratios, 0)

        if pheight > 0 and cheight > 0:
            ratios = np.append(ratios, (pheight / cheight))
        else:
            ratios = np.append(ratios, 0)

        if psize > 0 and csize > 0:
            ratios = np.append(ratios, (psize / csize))
        else:
            ratios = np.append(ratios, 0)

        if pdiag > 0 and cdiag > 0:
            ratios = np.append(ratios, (pdiag / cdiag))
        else:
            ratios = np.append(ratios, 0)

        # Area Ratio parent/child
        if parea > 0 and carea > 0:
            ratios = np.append(ratios, (parea/carea))
        else:
            ratios = np.append(ratios, 0)

        # handle special cases
        ratios[np.isnan(ratios)] = 0
        ratios[~np.isfinite(ratios)] = 0

        return ratios.tolist()

    @staticmethod
    def getPointDistances(parent, child):
        # Normalized by average diagonal
        pointDistances = np.array([])

        plows = np.array([np.amin(np.array(parent)[:,0]), np.amin(np.array(parent)[:,0])])
        phighs = np.array([np.amax(np.array(parent)[:,0]), np.amax(np.array(parent)[:,1])])

        clows = np.array([np.amin(np.array(child)[:,0]), np.amin(np.array(child)[:,1])])
        chighs = np.array([np.amax(np.array(child)[:,0]), np.amax(np.array(child)[:,1])])

        average_diagonal = (np.linalg.norm(plows - phighs) + np.linalg.norm(clows - chighs)) / 2

        # Min Distance
        euclid_distances = cdist(parent, child, metric='euclidean').flatten()
        min_dist = np.amin(euclid_distances) / average_diagonal
        pointDistances=np.append(pointDistances,min_dist)

        # Max Distance
        max_dist = np.amax(euclid_distances) / average_diagonal
        pointDistances=np.append(pointDistances,max_dist)

        # handle special cases
        pointDistances[np.isnan(pointDistances)] = 0
        pointDistances[~np.isfinite(pointDistances)]=0

        return pointDistances.tolist()

    @staticmethod
    def getStartEndPointDistances(parent, child):
        # Normalized by avg_width, avg_height, average_diagonal
        pointDistances = np.array([])

        plows = np.array([np.amin(np.array(parent)[:,0]), np.amin(np.array(parent)[:,0])])
        phighs = np.array([np.amax(np.array(parent)[:,0]), np.amax(np.array(parent)[:,1])])

        clows = np.array([np.amin(np.array(child)[:,0]), np.amin(np.array(child)[:,1])])
        chighs = np.array([np.amax(np.array(child)[:,0]), np.amax(np.array(child)[:,1])])

        avg_width = ( (phighs[0] - plows[0]) + (chighs[0] - clows[0]) ) / 2
        avg_height = ( (phighs[1] - plows[1]) + (chighs[1] - clows[1]) ) / 2
        average_diagonal = (np.linalg.norm(plows - phighs) + np.linalg.norm(clows - chighs)) / 2

        # Distances between start points
        start_horiz = (child[0,0] - parent[0,0]) / avg_width
        start_vert = (child[0,1] - parent[0,1]) / avg_height
        start = np.linalg.norm(child[0] - parent[0]) / average_diagonal
        pointDistances= np.append( pointDistances,[start_horiz, start_vert, start])

        # Distances between end points
        end_horiz = (child[-1,0] - parent[-1,0]) / avg_width
        end_vert = (child[-1,1] - parent[-1,1]) / avg_height
        end = np.linalg.norm(child[-1] - parent[-1]) / average_diagonal
        pointDistances = np.append(pointDistances,[end_horiz, end_vert, end])

        # Distances between parent start and child end points
        startend_horiz = (child[-1,0] - parent[0,0]) / avg_width
        startend_vert = (child[-1,1] - parent[0,1]) / avg_height
        startend = np.linalg.norm(child[-1] - parent[0]) / average_diagonal
        pointDistances = np.append(pointDistances,[startend_horiz, startend_vert, startend])

        # Distances between parent end and child start points
        endstart_horiz = (child[0,0] - parent[-1,0]) / avg_width
        endstart_vert = (child[0,1] - parent[-1,1]) / avg_height
        endstart = np.linalg.norm(child[0] - parent[-1]) / average_diagonal
        pointDistances = np.append(pointDistances,[endstart_horiz, endstart_vert, endstart])

        # handle special cases
        pointDistances[np.isnan(pointDistances)] = 0
        pointDistances[~np.isfinite(pointDistances)] = 0

        return pointDistances.tolist()

    @staticmethod
    def getMassCenterDistances(parent, child):
        # Normalized by avg_width, avg_height, avg_diagonal
        massCenter_distances = np.array([])

        plows = np.array([np.amin(np.array(parent)[:,0]), np.amin(np.array(parent)[:,0])])
        phighs = np.array([np.amax(np.array(parent)[:,0]), np.amax(np.array(parent)[:,1])])

        clows = np.array([np.amin(np.array(child)[:,0]), np.amin(np.array(child)[:,1])])
        chighs = np.array([np.amax(np.array(child)[:,0]), np.amax(np.array(child)[:,1])])

        avg_width = ( (phighs[0] - plows[0]) + (chighs[0] - clows[0]) ) / 2
        avg_height = ( (phighs[1] - plows[1]) + (chighs[1] - clows[1]) ) / 2
        average_diagonal = (np.linalg.norm(plows - phighs) + np.linalg.norm(clows - chighs)) / 2

        pcenter = np.mean(parent, axis=0)
        ccenter = np.mean(child, axis=0)

        # Horizontal distance between centers
        massCenter_distances=np.append(massCenter_distances,(ccenter[0] - pcenter[0]) / avg_width)

        # Vertical distance between centers
        massCenter_distances = np.append(massCenter_distances,(ccenter[1] - pcenter[1]) / avg_height)

        # Distance between centers
        massCenter_distances = np.append(massCenter_distances, np.linalg.norm(ccenter - pcenter) / average_diagonal)

        massCenter_distances[np.isnan(massCenter_distances)] = 0
        massCenter_distances[~np.isfinite(massCenter_distances)] = 0

        return massCenter_distances.tolist()

    @staticmethod
    def getAngles(parent, child):
        angles = np.array([])

        # Angle between vectors of start points to end points
        parent_vector = [parent[-1][0] - parent[0][0], parent[-1][1] - parent[0][1]]
        child_vector = [child[-1][0] - child[0][0], child[-1][1] - child[0][1]]
        angles=np.append(angles,GeometricFeatureExtractor.angle(parent_vector,child_vector))

        # Measure of trace parallelity (the angle of the two diagonals)
        plows = np.array([np.amin(np.array(parent)[:,0]), np.amin(np.array(parent)[:,0])])
        phighs = np.array([np.amax(np.array(parent)[:,0]), np.amax(np.array(parent)[:,1])])

        clows = np.array([np.amin(np.array(child)[:,0]), np.amin(np.array(child)[:,1])])
        chighs = np.array([np.amax(np.array(child)[:,0]), np.amax(np.array(child)[:,1])])

        parent_diagonal = phighs - plows
        child_diagonal = chighs - clows

        angles = np.append(angles,GeometricFeatureExtractor.angle(parent_diagonal,child_diagonal))

        # angle between the horizontal line and the line connecting the last point of the current stroke
        # and the first point of the next stroke
        endstart_vector = [-parent[-1][0] + child[0][0], -parent[-1][1] + child[0][1]]
        horiz_line = [1,0]
        angles = np.append(angles,GeometricFeatureExtractor.angle(endstart_vector,horiz_line))

        return angles.tolist()

    ## dot  (Lei Hu)
    @staticmethod
    def dotproduct(v1,v2):
        return sum((a*b) for a,b in zip(v1,v2))

    ## module of vector (Lei Hu)
    @staticmethod
    def length(v):
        return math.sqrt(GeometricFeatureExtractor.dotproduct(v,v))

    ## angle between two vectors (Lei Hu)
    @staticmethod
    def angle(v1,v2):
        if (GeometricFeatureExtractor.length(v1) * GeometricFeatureExtractor.length(v2)):
            if GeometricFeatureExtractor.dotproduct(v1,v2)/(GeometricFeatureExtractor.length(v1) * GeometricFeatureExtractor.length(v2)) >= 1.0:
                return math.acos(1.0)
            elif GeometricFeatureExtractor.dotproduct(v1,v2)/(GeometricFeatureExtractor.length(v1) * GeometricFeatureExtractor.length(v2)) <= -1.0:
                return math.acos(-1.0)
            else:
                return math.acos(GeometricFeatureExtractor.dotproduct(v1,v2)/(GeometricFeatureExtractor.length(v1) * GeometricFeatureExtractor.length(v2)))
        else:
            return 0

    @staticmethod
    def getMisc(pbox, cbox):
        miscFeatures = np.array([])

        pcenter = np.array([(pbox[0]+pbox[1])/2, (pbox[1]+pbox[3])/2])
        ccenter = np.array([(cbox[0]+cbox[1])/2, (cbox[1]+cbox[3])/2])
        cdistance = np.linalg.norm(pcenter - ccenter)

        # Height of child / distance between centers
        miscFeatures = np.append(miscFeatures, np.array((cbox[3]-cbox[1])) / cdistance)

        miscFeatures[np.isnan(miscFeatures)] = 0
        miscFeatures[~np.isfinite(miscFeatures)] = 0

        return miscFeatures.tolist()
    
    
# two of them (center norm and bbox area is not used for CROHME - source:LPGA) 
# geofeat for crohme 53
def getGeometricFeatures(pid, cid , traces):
    geometricFeatureExtractor = GeometricFeatureExtractor
    features = []
    with warnings.catch_warnings():
        warnings.filterwarnings("ignore", message="divide by zero encountered in true_divide")
        warnings.filterwarnings("ignore", message="divide by zero encountered in double_scalars")
        warnings.filterwarnings("ignore", message="invalid value encountered in true_divide")
        warnings.filterwarnings("ignore", message="invalid value encountered in double_scalars")

        parent_composite_trace = np.array(traces[pid])
        child_composite_trace = np.array(traces[cid])

        parent_box = np.array([np.min(parent_composite_trace[:,0]), np.min(parent_composite_trace[:,1]), np.max(parent_composite_trace[:,0]), np.max(parent_composite_trace[:,1])])
        child_box = np.array([np.min(child_composite_trace[:,0]), np.min(child_composite_trace[:,1]), np.max(child_composite_trace[:,0]), np.max(child_composite_trace[:,1])])

        raw_features = geometricFeatureExtractor.getBoundingBoxDistances(parent_box, child_box, ratio=False)            
        features += raw_features
        
        raw_features = geometricFeatureExtractor.getBoundingBoxDistances(parent_box, child_box, ratio=True)
        features += raw_features

        raw_features = geometricFeatureExtractor.getBoundingBoxOverlaps(parent_box, child_box)
        features += raw_features

        raw_features = geometricFeatureExtractor.getBoundingBoxSizes(parent_box, child_box)
        features += raw_features

        raw_features = geometricFeatureExtractor.getBoundingBoxRatios(parent_box, child_box)
        features += raw_features
        
        raw_features = geometricFeatureExtractor.getPointDistances(parent_composite_trace, child_composite_trace)
        features += raw_features

        raw_features = geometricFeatureExtractor.getStartEndPointDistances(parent_composite_trace, child_composite_trace)
        features += raw_features

        raw_features = geometricFeatureExtractor.getMassCenterDistances(parent_composite_trace, child_composite_trace)
        features += raw_features

        raw_features = geometricFeatureExtractor.getAngles(parent_composite_trace, child_composite_trace)
        features += raw_features


    numpy_features = np.array(features)
    if np.isfinite(numpy_features).sum() != numpy_features.size:
        print("ALL FEATURES ARE NOT FINITE", np.isfinite(numpy_features).sum(), numpy_features.size, numpy_features)

    return features

def getGeofeat(edge_ref,traces):
    
    features=[]
    for pid,cid in edge_ref:
        feat=getGeometricFeatures(pid, cid , traces)
        features.append(feat)
    return np.array(features)






