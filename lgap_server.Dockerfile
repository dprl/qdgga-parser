# docker build -f lgap_server.Dockerfile . -t dprl/lgap:latest
# docker push dprl/lgap:latest

FROM dprl/lgap_base:latest

COPY bin bin
COPY data data
COPY test.py test.py
COPY src src
COPY train.py train.py
COPY requirements_new.txt requirements_new.txt
COPY server.py server.py
COPY config config

COPY lgap_settings.py lgap_settings.py
COPY lgap_base.Dockerfile lgap_base.Dockerfile
COPY lgap_server.Dockerfile lgap_server.Dockerfile
COPY Makefile Makefile

RUN make

ENV PYTHONPATH="/workspace/:/workspace/protables:/workspace/lgeval:"
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN apt-get update -y
RUN apt-get update && apt-get install libenchant-2-dev ffmpeg libsm6 libxext6 -y


EXPOSE 8007

CMD ["./bin/start-server"]
