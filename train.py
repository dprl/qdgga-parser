#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  5 01:25:02 2021

@author: Ayush Kumar Shah (as1211@rit.edu)
"""

# RZ: Add debug operations
from src.utils.debug_fns import *
import src.utils.debug_fns as debug_fns
dTimer = DebugTimer("LGAP dataloader")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

# AD: Addition
import traceback
import pprint 
import torch
import random
import wandb
import numpy as np
import os
import os.path as path
import argparse
from operator import itemgetter
import torch.distributed as dist
import torch.multiprocessing as mp
from torch.utils.data import DataLoader, DistributedSampler
from src.utils.utils import get_config, save_config, update_config, get_checkpoint
from src.utils.log import Logger
from src.models.model import Trainer, separate_outputs
from src.utils.common_utils import get_type
from collections import defaultdict
import pandas as pd
from multiprocessing import Manager
import itertools
from src.datasets.classes.DatasetItem import DatasetItem
from src.datasets.dataset_simple_refactored import MathSymbolDataset, seed_worker
from src.datasets.CleanedDataset import CleanDataset
from src.datasets.dataset_utilities import create_merge_dict
from tqdm import tqdm

pp = pprint.PrettyPrinter(indent=4)

def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    raise TypeError

def generate_data_recurrent(loader, predicted_segmentation_edges, epoch):

    formulas_segmentation = pd.unique(predicted_segmentation_edges.id_formula)
    data_all = None
    print(f"Formulas with segmentation: {len(formulas_segmentation)}")
    

    if loader.dataset.formulas_dict is None:
        formulas_dict_path = os.path.join(loader.dataset.formula_level_dir, "formulas_dict.ptk")
        treasure_path = os.path.join(loader.dataset.formula_level_dir, "treasure.csv")                            
        loader.dataset.formulas_dict = loader.dataset.read_feature_from_disk(formulas_dict_path)            
        loader.dataset.treasure = pd.read_csv(treasure_path,
                                    index_col=0, dtype=DatasetItem.get_types())

    for id_formula in formulas_segmentation:
        
        
        formula_object = loader.dataset.formulas_dict[id_formula]
        
        indexes_edges_segmentation = predicted_segmentation_edges[predicted_segmentation_edges.id_formula == id_formula].id_item
        nodes_segmented_ids = np.unique(np.array(formula_object.seg_edges)[indexes_edges_segmentation])
        num_nodes_segmented = len(nodes_segmented_ids)
        if num_nodes_segmented:
            #TODO: This is done in DatasetItem.py, this should be one function call to reuse code
            #nodes
            id_formula_array = np.repeat(id_formula, num_nodes_segmented)
            type_array = np.repeat("node",num_nodes_segmented)
            data_source_array = np.repeat("predicted",num_nodes_segmented)
            epoch_array = np.repeat((epoch+1), num_nodes_segmented)        
            data_nodes_predicted = np.stack((id_formula_array, nodes_segmented_ids, type_array,
                                    data_source_array, epoch_array), axis=1)
            if data_all is not None:
                data_all = np.concatenate((data_all, data_nodes_predicted))
            else:
                data_all = data_nodes_predicted
            
            #edges        
            segmented_edges = set(itertools.chain(*list(itemgetter(*nodes_segmented_ids)(formula_object.neighbors))))
            
            rel_edges_idx = list(enumerate(formula_object.rel_edges))
            ids = list(map(itemgetter(0), list(filter(lambda x: x[1] in
                                                                segmented_edges,
                                                                rel_edges_idx))))
            num_predicted_edges = len(ids)
            if num_predicted_edges:
                id_formula_array = np.repeat(id_formula, num_predicted_edges)
                type_array = np.repeat("edge", num_predicted_edges)
                data_source_array = np.repeat("predicted", num_predicted_edges)
                epoch_array = np.repeat( (epoch+1) , num_predicted_edges)
                data_edges_predicted = np.stack((id_formula_array, ids, type_array, 
                                        data_source_array, epoch_array), axis=1)
                data_all = np.concatenate((data_all, data_edges_predicted))

                predicted_merge_dict = create_merge_dict(seg_edges=list(segmented_edges),seg_labels=np.zeros(len(segmented_edges)))
                formula_object.predicted_merge_dict = predicted_merge_dict
    return data_all
class TrainParser(object):
    def __init__(self, opts):
        self.opts = opts
        self.validation = self.opts.init.validation

        self.run_dir = path.join(self.opts.run_dir, self.opts.model_name)
        if not path.isdir(self.run_dir):
            os.makedirs(self.run_dir, exist_ok=True)

        # Create model
        self.opts.init.update(get_checkpoint(self.run_dir,
                                             self.opts.init.last_epoch,
                                             'train'))
        self.opts.data.last_epoch = self.opts.init.last_epoch

    def run_train(self, rank, world_size, dataset, dataset_val,
                  shuffle=True, seed=0, visualize_loss=False):
        # print(f"GPU {rank}: Number of expressions in training set = {num_files}")
        # print(f"GPU {rank}: Number of features in training set = {len(partition)}")
        # print(f"shuffle={opts.init.shuffle}")
        # setup(rank, world_size, self.opts.init.port)
        set_seed(seed)

        dataset_clean = CleanDataset(dataset_base=dataset)
        # sampler = DistributedSampler(dataset_clean, num_replicas=world_size,
        #                              rank=rank, shuffle=self.opts.init.shuffle)

        g = torch.Generator()
        g.manual_seed(self.opts.init.seed)

        loader = DataLoader(dataset_clean, batch_size=self.opts.init.batch_size,
                            num_workers=self.opts.init.num_workers,
                            # pin_memory=True, sampler=sampler,
                            shuffle=self.opts.init.shuffle,
                            collate_fn=dataset.custom_collate_fn,
                            worker_init_fn=seed_worker, generator=g)
        # TODO: Add support for validation dataset
        # if dataset_val:
        #     loader_val = DataLoader(dataset_val, batch_size=None, num_workers=self.opts.init.num_workers,
        #                             worker_init_fn=worker_init_fn, collate_fn=collate_fn)
        # else:
        #     loader_val = None

        # Set seed for reproducibility
        self.trainer = Trainer(self.opts.init, self.opts.learn, self.opts.net,
                               rank, world_size, dataset.symbol_class_counts,
                               dataset.rel_class_counts, dataset.seg_class_counts)

        # Create logger
        logger = Logger(self.run_dir, rank, epoch=self.opts.init.last_epoch,
                        name=self.opts.model_name, visualize_loss=visualize_loss)
        logger.add_loss_log(self.trainer.get_loss, self.opts.print_step, self.opts.window_size)
        logger.add_save_log(self.trainer.save, self.opts.save_step, seed)
        # Create validation logger
        if self.validation:
            val_logger = Logger(self.run_dir, rank, epoch=self.opts.init.last_epoch,
                                name=self.opts.model_name, mode="val",
                                visualize_loss=visualize_loss)
            val_logger.add_loss_log(self.trainer.get_val_loss, self.opts.print_step, self.opts.window_size)
            val_logger.add_save_log(self.trainer.save, self.opts.save_step, seed)

        # Initialize wandb visualization of loss
        if rank == 0 and visualize_loss:
            _ = wandb.init(project="qdgga-v2", 
                             name=f"{self.opts.model_name}",
                             config={**self.opts.learn, **self.opts.net},
                             # resume=True,
                             )
        # print(f"[GPU {rank}] Training on {world_size} GPUs")
        dTimer.check(f"[Rank {rank}]: Initialize training")
        # Training loop
        for epoch in range(self.opts.init.last_epoch, self.opts.learn.num_epochs):
            # sampler.set_epoch(epoch)
            # print(f"[GPU {rank}] Epoch {epoch}: Total number of instances = {len(dataset_clean)}")
            # local_samples = sampler.num_samples  # This is the number of samples assigned to this GPU
            # print(f"[GPU {rank}] Epoch {epoch}: Local instances = {local_samples} / {len(dataset_clean)} (Total)")

            #pandas of id of edge and id formula
            predicted_segmentation_edges = None
            # Batch loop
            for data in logger(loader):
                segProb = self.trainer.train(data)
                seg_labels_pred = list(segProb.argmax(1)) if segProb is not None  else []
                
                # For recurrent mode, collect segmentation edges
                if self.opts.init.recurrent:
                    keys_to_include = ['id', 'id_formula', 'type_feature']
                    subset_dict = dict(zip(keys_to_include, map(data.get, keys_to_include)))
                    df = pd.DataFrame(data=subset_dict)
                    df = df[df.type_feature == "edge"].assign(predicted_segmentation=seg_labels_pred)
                    merge_edges = df[df.predicted_segmentation == 0][["id","id_formula"]]
                    if predicted_segmentation_edges is not None:
                        predicted_segmentation_edges = pd.concat((predicted_segmentation_edges, merge_edges))
                    else:
                        predicted_segmentation_edges = merge_edges
               
            if self.opts.init.recurrent:
                data_source_column = np.repeat("predicted",len(predicted_segmentation_edges))
                type_column = np.repeat("edge",len(predicted_segmentation_edges))
                epoch_column = np.repeat((epoch+1),len(predicted_segmentation_edges))

                predicted_segmentation_edges = predicted_segmentation_edges.assign(data_source=data_source_column).assign(type=type_column).assign(epoch=epoch_column)
                predicted_segmentation_edges.rename(columns={"id":"id_item"},inplace=True)
                
                data_all = generate_data_recurrent(loader, predicted_segmentation_edges, epoch)
                        
                if data_all is not None:
                    dataframe = pd.DataFrame(data_all).astype({0:'str', 1:'int', 2:'str',
                                                    3:'str', 4:'int'})
                    dataframe.columns = ["id_formula","id_item","type","data_source","epoch"]                
                    #filter existing pandas
                    loader.dataset.treasure = loader.dataset.treasure[loader.dataset.treasure['epoch'] == 0]
                    loader.dataset.treasure = pd.concat((loader.dataset.treasure, dataframe))
                    loader.dataset.treasure = loader.dataset.treasure.reset_index().drop(columns=['index'])
                    
                    dataset.write_feature_to_disk(loader.dataset.formulas_dict, os.path.join(loader.dataset.formula_level_dir,
                                                            "formulas_dict.ptk"))
                    loader.dataset.indexes_features_in_treasure = loader.dataset.treasure.index
                    loader.dataset.treasure.to_csv(os.path.join(loader.dataset.formula_level_dir, "treasure.csv"))
                
            loader.dataset.curr_epoch += 1
            self.trainer.scheduler.step()
            logger.save_weights()
            # if rank == 0:
            #     dTimer.check(f"[GPU {rank}]: Training epoch {epoch} time")
            dTimer.check(f"[GPU {rank}]: Training epoch {epoch} time")
        # cleanup()

    # @profile()
    def train(self):
        validation = self.validation
        save_config(self.opts, path.join(self.run_dir, 'train' + '_options.yaml'))

        # Get files from img list
        files = []
        if os.path.exists(self.opts.data.Img_list):
            for filename in open(self.opts.data.Img_list):
                #TODO: Assumes only LG files for training; adapt to tsv as well
                if self.opts.data.dataset == "infty" or self.opts.data.dataset == "chem":
                    files.append(path.join(filename.strip() + '.lg'))
                elif self.opts.data.dataset == "crohme":
                    files.append(path.join(filename.strip() + '.inkml'))

        if validation:
            random.seed(self.opts.init.seed)
            random.shuffle(files)
            ratio = 0.8
            idx = int(ratio * len(files))
            train_files = files[:idx]
            val_files = files[idx:]

        manager = Manager()
        shared_lock = manager.RLock()

        if validation:
            # Generate input data, and logger.
            self.opts.data.Img_list = train_files
            
            dataset = MathSymbolDataset(**self.opts.data)
            # print(f"Number of expressions in training set = {dataset.tot}")

            self.opts.data.Img_list = val_files
            dataset_val = MathSymbolDataset(**self.opts.data)
            # print(f"Number of expressions in validation set = {dataset_val.tot}")

        else:
            # Generate input data, and logger.

            # process = psutil.Process()
            # print(f"Memory storage before dataset = {process.memory_info().rss/(10**6)} MB")
            
            self.opts.data.Img_list = files
            dataset = MathSymbolDataset(**self.opts.data, shuffle=self.opts.init.shuffle, 
                                        model_dir=self.run_dir, shared_lock=shared_lock)
            print(f"Total Number of expressions in training set = {len(dataset.data_files)}")
            print(f"Total Number of features in training set = {len(dataset)}")
            # dTimer.check("Dataset initialization: Read lg files into proTables")
            dataset_val = None

        if self.opts.init.num_gpu == "single":
            n_gpu = 1
        else:
            n_gpu = torch.cuda.device_count()
        
        # If single GPU, don't use DDP
        if n_gpu == 1:
            print('Running WITHOUT DP!!!!')
            self.run_train(rank=0, world_size=1, dataset=dataset,
                           dataset_val=dataset_val, seed=self.opts.init.seed,
                            visualize_loss=self.opts.init.visualize_loss)

        else:
            print('Running WITH DP!!!')
            # processes = []
            # mp.set_start_method("spawn", force=True)
            # for rank in range(n_gpu):
            #     p = mp.Process(target=init_process, args=(rank, n_gpu, dataset,
            #                                               dataset_val,
            #                                               self.opts.init.shuffle, self.opts.init.seed,
            #                                               self.opts.init.visualize_loss,
            #                                               self.opts.init.port,
            #                                               self.run_train))
            #     p.start()
            #     processes.append(p)

            # for p in processes:
            #     p.join()
            # mp.spawn(self.run_train, args=(n_gpu, dataset, dataset_val,
            #                                self.opts.init.shuffle,
            #                                self.opts.init.seed,
            #                                self.opts.init.visualize_loss), nprocs=n_gpu, join=True)

            self.run_train(rank=0, world_size=n_gpu, dataset=dataset,
                           dataset_val=dataset_val, seed=self.opts.init.seed,
                            visualize_loss=self.opts.init.visualize_loss)


def parse_args():
    parser = argparse.ArgumentParser(
        description="LGAP - Expression Parsing: Training"
    )
    parser._action_groups.pop()
    required = parser.add_argument_group('Required Args')
    optional = parser.add_argument_group('Optional Args')

    required.add_argument(
        "-c",
        "--config",
        required=True,
        type=str,
        help="default configuration to be used - inkml or crohme",
    )
    required.add_argument(
        "-r",
        "--run_config",
        type=str, required=True,
        default="config/run.yaml",
        help="run configuration to be used",
    )
    required.add_argument(
        "-m",
        "--model_name", required=True,
        type=str,
        help="The name of model to be used",
    )
    required.add_argument(
        "-o",
        "--output_dir",
        type=str, required=True,
        help="Directory containing the output trained weights",
    )
    required.add_argument(
        "-op",
        "--optional",
        type=int, default=0,
        help="Use optional args to override YAML configs",
    )

    #-----------EXTRA OPTIONAL ARGS------------
    #--------OVERRIDES .YAML CONFIGS!----------

    optional.add_argument(
        "-dd", "--data_dir",
        type=str,
        help="Directory containing input images: .inkml or png",
    )
    # RZ: Adding tsv dir arg
    optional.add_argument(
        "-t", "--tsv_dir",
        type=str,
        help="Directory containing TSV input files",
    )
    optional.add_argument(
        "-g", "--given_sym",
        type=int,
        help="Flag to use given symbols or not"
    )
    # RZ -- adding *input* image directory.
    # optional.add_argument(
    #     "-ii", "--img_dir",
    #     type=str,
    #     help="Input page image path (use with TSV input)",
    # )
    optional.add_argument(
        "-i", "--img_list",
        type=str,
        help="The file containing list of images to be used for INFTY for training",
    )
    optional.add_argument(
        "-l", "--lg_dir",
        type=str,
        help="The directory containing lg inputs or ground truths",
    )
    optional.add_argument(
        "-le", "--last_epoch",
        type=str,
        help="Last epoch to use"
    )
    optional.add_argument(
        "-n", "--num_gpu",
        type=str,
        help="Number of GPUs to use: `single` or `multi`",
    )
    optional.add_argument(
        "-v", "--validation",
        type=int,
        help="Flag to use validation set or not"
    )
    optional.add_argument(
        "-s", "--seed",
        type=int,
        help="Seed for randomness"
    )
    optional.add_argument(
        "-vis", "--visualize_loss",
        type=int,
        help="Flag for enabling weights and biases logging for loss\
        visualization"
    )
    optional.add_argument(
        "-sh", "--shuffle",
        type=int,
        help="Flag for shuffling training data per epoch"
    )
    optional.add_argument(
        "-w", "--workers",
        type=int,
        help="Number of workers to be used for dataloader"
    )
    optional.add_argument(
        "-cs", "--chunk_size",
        type=int,
        help="Number of formulas processed at once for feature generation"
    )
    optional.add_argument(
        "-bs", "--batch_size",
        type=int,
        help="Batch size for features to be used for dataloader"
    )

    optional.add_argument(
        "-p", "--feat_parallel",
        type=int,
        help="Parallel flag for feat generation"
    )

    optional.add_argument(
        "--port",
        type=str,
        help="Port number for distributed training"
    )

    optional.add_argument(
        "-rec", "--recurrent",
        type=int,
        help="Flag for recurrent training (on/off)"
    )

    args = parser.parse_args()
    # Get default and run configs from .yaml files
    config = get_config(args.config)
    run_opts = get_config(args.run_config)[args.model_name]['train']
    opts = update_config(config, run_opts)
    opts.run_dir = args.output_dir
    opts.model_name = args.model_name

    # Override with optional args if specified
    if args.optional:
        if args.data_dir is not None:
            opts.data.data_dir = args.data_dir
        
        if args.tsv_dir is not None:
            if opts.data.format == 'tsv':
                opts.data.tsv_dir = args.tsv_dir
            else:
                raise ValueError('TSV Directory must be specified\
                when format is set as tsv OR SET correct config in\
                run config file - \"format\"')
        elif args.lg_dir is not None:
            if opts.data.format == 'lg':
                opts.data.lg_dir = args.lg_dir
            else:
                raise ValueError('LG Directory must be specified\
                when format is set as lg OR SET correct config in\
                run config file - \"format\"')
        
        if args.img_list is not None:
            opts.data.Img_list = args.img_list
        
        if args.last_epoch is not None:
            opts.init.last_epoch = args.last_epoch
        
        if args.num_gpu is not None:
            opts.init.num_gpu = args.num_gpu
        
        if args.validation is not None:
            opts.init.validation = args.validation

        if args.seed is not None:
            opts.init.seed = args.seed

        if args.visualize_loss is not None:
            opts.init.visualize_loss = args.visualize_loss

        if args.shuffle is not None:
            opts.init.shuffle = bool(args.shuffle)

        if args.workers is not None:
            opts.init.num_workers = args.workers
            opts.data.num_workers = args.workers

        if args.batch_size is not None:
            opts.init.batch_size = args.batch_size

        if args.feat_parallel is not None:
            opts.data.feat_parallel = args.feat_parallel

        if args.port is not None:
            opts.init.port = args.port

        if args.recurrent is not None:
            opts.init.recurrent = bool(args.recurrent)

        if args.chunk_size is not None:
            opts.data.chunk_size = args.chunk_size

    return opts

def init_process(rank, size, dataset, dataset_val, shuffle, seed,
                 visualize_loss, port, fn, backend='gloo'):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = port
    dist.init_process_group(backend, rank=rank, world_size=size)
    fn(rank, size, dataset, dataset_val, shuffle, seed, visualize_loss)

def setup(rank, world_size, port):
    """Initialize process group for distributed training."""
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = port
    dist.init_process_group("nccl", rank=rank, world_size=world_size)
    torch.cuda.set_device(rank)

def cleanup():
    """Destroy distributed process group."""
    dist.destroy_process_group()


def set_seed(seed):
    np.random.seed(seed)
    random.seed(seed)

    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    torch.backends.cudnn.enabled = True
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True 

if __name__ == "__main__":
    opts = parse_args()
    trainparser = TrainParser(opts)
    try:
        trainparser.train()
    except:
        print(traceback.print_exc())
        cleanup()

