#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  5 01:25:02 2021

@author: as1211
"""

import pickle
from tqdm import tqdm
import torch
import random
import numpy as np
import os
import os.path as path
import argparse
import torch.distributed as dist
import torch.multiprocessing as mp
from src.utils.utils import get_config, save_config, update_config, get_checkpoint
from src.datasets.dataset import MathSymbolDataset
from src.datasets.dataloader import partition_dataset
from qdgga_settings import SYM_WEIGHTS_DIR, SEG_WEIGHTS_DIR, REL_WEIGHTS_DIR
from sklearn.utils import class_weight

class TrainParser(object):
    def __init__(self, default_config=None, run_config=None, 
                 model_name=None, output_dir=None, last_epoch=None,
                 validation=False):
        self.PHASE = "train"
        self.model_name = model_name
        # Load options
        default_opts = get_config(default_config)
        run_opts = get_config(run_config)[model_name][self.PHASE]
        self.opts = update_config(default_opts, run_opts)
        self.opts_val = None
        self.validation = validation
        if output_dir:
            self.opts.run_dir = output_dir
        self.run_dir = path.join(self.opts.run_dir, self.model_name)
        if not path.isdir(self.run_dir):
            os.makedirs(self.run_dir)

        # Create model
        if last_epoch:
            self.opts.init.last_epoch = last_epoch
        self.opts.init.update(get_checkpoint(self.run_dir,
                                             self.opts.init.last_epoch,
                                             self.PHASE))

    def run_train(self, rank, world_size, dataset, dataset_val=None, seed=0):
        # Set seed for reproducibility
        set_seed(seed)

        print(f"Getting class weights on gpu {rank}.")

        if world_size >= 2:
            partition, _ = partition_dataset(dataset)
            if dataset_val:
                partition_val, _ = partition_dataset(dataset_val)
        else:
            partition = dataset
            if dataset_val:
                partition_val = dataset_val

        sym = torch.LongTensor()
        seg = torch.LongTensor()
        rel = torch.LongTensor()
        for data in tqdm(partition):
            sym = torch.cat((sym, data["symbols"]))
            seg = torch.cat((seg, data["segments"]))
            rel = torch.cat((rel, data["edgeLabels"]))

        # pickle.dump(symbols.numpy(), 
        #             open("config/class_weights/sym.pkl", "wb"))
        # pickle.dump(segments.numpy(),
        #             open("config/class_weights/seg.pkl", "wb"))
        # pickle.dump(relations.numpy(),
        #             open("config/class_weights/rel.pkl", "wb"))
        # sym = pickle.load(open("config/class_weights/sym.pkl", "rb"))
        # seg = pickle.load(open("config/class_weights/seg.pkl", "rb"))
        # rel = pickle.load(open("config/class_weights/rel.pkl", "rb"))

  
        sym_weights = class_weight.compute_class_weight('balanced',
                                                        classes=np.array(range(self.opts.net.num_node_classes)),
                                                        y=sym)
        sym_weights = torch.tensor(sym_weights, dtype=torch.float)
        seg_weights = class_weight.compute_class_weight('balanced',
                                                        classes=np.array(range(2)),
                                                        y=seg)
        seg_weights = torch.tensor(seg_weights, dtype=torch.float)
        rel_weights = class_weight.compute_class_weight('balanced',
                                                        classes=np.array(range(self.opts.net.num_edge_classes)),
                                                        y=rel)
        rel_weights = torch.tensor(rel_weights, dtype=torch.float)

        pickle.dump(sym_weights, open(SYM_WEIGHTS_DIR, "wb"))
        pickle.dump(seg_weights, open(SEG_WEIGHTS_DIR, "wb"))
        pickle.dump(rel_weights, open(REL_WEIGHTS_DIR, "wb"))


    def train(self, ptsv_dir="", pimg_dir="", 
              lg_dir="", img_list="", data_dir="", num_gpu="multi",
              seed=0):
              
        validation = self.validation

        if data_dir: self.opts.data.data_dir = data_dir
        # if lg_dir: self.opts.data.lg_dir = lg_dir
        # AKS: Modify for new structure
        if img_list: self.opts.data.Img_list = img_list
        if not pimg_dir: pimg_dir = self.opts.data.data_dir
        if not ptsv_dir and lg_dir: 
            self.opts.data.lg_dir = lg_dir

        save_config(self.opts, path.join(self.run_dir, self.PHASE + "_options.yaml"))

        # Get files from img list
        files = []
        if os.path.exists(self.opts.data.Img_list):
            for filename in open(self.opts.data.Img_list):
                files.append(path.join(filename.strip() + '.lg'))

        if validation:
            random.seed(seed)
            random.shuffle(files)
            ratio = 0.8
            idx = int(ratio * len(files))
            train_files = files[:idx]
            val_files = files[idx:]

        if validation:
            # Generate input data, and logger.
            self.opts.data.Img_list = train_files
            dataset = MathSymbolDataset(**self.opts.data, tsv_dir=ptsv_dir,
                                        img_dir=pimg_dir)
            print(f"Number of expressions in training set = {len(dataset)}")

            self.opts.data.Img_list = val_files
            dataset_val = MathSymbolDataset(**self.opts.data, tsv_dir=ptsv_dir,
                                        img_dir=pimg_dir)
            print(f"Number of expressions in validation set = {len(dataset_val)}")

        else:
            # Generate input data, and logger.
            self.opts.data.Img_list = files
            dataset = MathSymbolDataset(**self.opts.data, tsv_dir=ptsv_dir,
                                        img_dir=pimg_dir)
            print(f"Number of expressions in training set = {len(dataset)}")
            dataset_val = None


        if num_gpu == "multi":
            n_gpu = torch.cuda.device_count()
        elif num_gpu == "single":
            n_gpu = 1

        # If single GPU, don't use DDP
        if n_gpu == 1:
            self.run_train(rank=0, world_size=1, dataset=dataset,
                           dataset_val=dataset_val, seed=seed)

        else:
            processes = []
            mp.set_start_method("spawn")
            for rank in range(n_gpu):
                p = mp.Process(target=init_process, args=(rank, n_gpu, dataset,
                                                          dataset_val, seed, self.run_train))
                p.start()
                processes.append(p)

            for p in processes:
                p.join()


def parse_args():
    parser = argparse.ArgumentParser(
        description="QDGGA - Expression Parsing: Training"
    )
    parser.add_argument(
        "-c",
        "--config",
        required=True,
        type=str,
        help="default configuration to be used - inkml or crohme",
    )
    parser.add_argument(
        "-dd",
        "--data_dir",
        type=str,
        help="Directory containing input CROHME data i.e. inkml files",
    )
    # RZ: Adding tsv dir arg
    parser.add_argument(
        "-t",
        "--tsv_dir",
        default="",
        type=str,
        help="Directory containing TSV input files",
    )
    parser.add_argument(
        "-g",
        "--given_sym",
        type=int,
        default=0,
        help="Flag to use given symbols or not"
    )
    # RZ -- adding *input* image directory.
    parser.add_argument(
        "-ii",
        "--img_dir",
        type=str,
        default="",
        help="Input page image path (use with TSV input)",
    )
    parser.add_argument(
        "-o",
        "--output_dir",
        type=str,
        help="Directory containing the output trained weights",
    )
    parser.add_argument(
        "-i",
        "--img_list",
        default="",
        type=str,
        help="The file containing list of images to be used for INFTY for trainig",
    )
    parser.add_argument(
        "-l",
        "--lg_dir",
        type=str,
        help="The directory containing lg inputs or ground truths",
    )
    parser.add_argument(
        "-le",
        "--last_epoch",
        type=str,
        help="Last epoch to use"
    )
    parser.add_argument(
        "-m",
        "--model_name",
        type=str,
        help="The name of model to be used",
    )
    parser.add_argument(
        "-r",
        "--run_config",
        type=str,
        default="config/run.yaml",
        help="run configuration to be used",
    )
    parser.add_argument(
        "-n", "--num_gpu",
        type=str, default='multi',
        help="Number of GPUs to use: `single` or `multi`",
    )
    parser.add_argument(
        "-v", "--validation",
        type=int, default=0,
        help="Flag to use validation set or not"
    )
    parser.add_argument(
        "-s", "--seed",
        type=int, default=0,
        help="Seed for randomness"
    )

    args = parser.parse_args()
    config = get_config(args.config)
    if args.model_name:
        config.model_name = args.model_name

    config.data_dir = args.data_dir
    config.output_dir = args.output_dir
    config.img_list = args.img_list

    # config.preprocess_contours = bool(args.preprocess_contours)
    config.last_epoch = args.last_epoch
    config.run_config = args.run_config

    config.lg_dir = args.lg_dir

    # RZ: addition for TSV input
    config.tsv_dir = args.tsv_dir
    config.img_dir = args.img_dir

    config.num_gpu = args.num_gpu
    config.validation = args.validation

    config.seed = args.seed

    return config


def init_process(rank, size, dataset, dataset_val, seed, fn, backend='gloo'):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '29500'
    dist.init_process_group(backend, rank=rank, world_size=size)
    fn(rank, size, dataset, dataset_val, seed)

def set_seed(seed):
    np.random.seed(seed)
    random.seed(seed)

    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)

    torch.backends.cudnn.enabled = True
    torch.backends.cudnn.benchmark = False
    torch.backends.cudnn.deterministic = True 

if __name__ == "__main__":
    config = parse_args()
    print(config)
    trainparser = TrainParser(default_config=config.default_config, 
                              run_config=config.run_config,
                              model_name=config.model_name,
                              output_dir=config.output_dir,
                              last_epoch=config.last_epoch,
                              validation=config.validation)
    trainparser.train(img_list=config.img_list, ptsv_dir=config.tsv_dir,
                      lg_dir=config.lg_dir, pimg_dir=config.img_dir,
                      data_dir=config.data_dir, num_gpu=config.num_gpu,
                      seed=config.seed)

