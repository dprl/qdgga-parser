################################################################
# Makefile for LGAP Parser
#
# A.K. Shah, Nov. 30, 2021
#################################################################

# Weight file names (will change)
# INFTY weights
LGAPIW="net_89.pt"
# CROHME weights
LGAPCW="net_79.pt"

.PHONY: tests

# default: build all, stop on failure
all:
	./bin/install
	./bin/install-merge-symbol-classes
	./bin/install-modify-punc-edges
	./bin/install-revert-punc-edges
	./bin/install-create-small-gt
	./bin/install-copy-error-files

# 'make force' - do not stop on failure
force:
	./bin/install force

##################################
# Docker container
###################################
# Usage: 
#  1. Login to dockerhub (docker login -u 'dprl')
#  2. To make 'staging' container, issue:
#
#        make docker
#        make docker-push
#
#  3. To make custom tag for a container, issue:
#
#        make tag=tag_name docker-tag
#        make tag=tag_name docker-push-tag

docker: docker-base docker-server

docker-base:
	docker build -f lgap_base.Dockerfile . -t dprl/lgap_base:latest

docker-base-push: docker-base
	docker push dprl/lgap_base:latest

docker-base-tag:
	# USAGE: make tag=tag_label docker-tag 
	# Will create chemscraper container with label 'tag_label'
	docker build -f lgap_base.Dockerfile . -t dprl/lgap_base:${tag}

docker-base-push-tag: 
	# USAGE: make tag=tag_label docker-push-tag 
	# Pushes the named container.
	docker push dprl/lgap_base:${tag}

docker-server:
	docker build -f lgap_server.Dockerfile . -t dprl/lgap:latest

docker-server-push: docker-server
	docker push dprl/lgap:latest

docker-server-tag:
	# USAGE: make tag=tag_label docker-tag 
	# Will create chemscraper container with label 'tag_label'
	docker build -f lgap_server.Dockerfile . -t dprl/lgap:${tag}

docker-server-push-tag: 
	# USAGE: make tag=tag_label docker-push-tag 
	# Pushes the named container.
	docker push dprl/lgap:${tag}

install-data: install-infty-data install-crohme-data

modify-punc-edges:
	./bin/modify-punc-edges

merge-symbol-classes:
	./bin/merge-symbol-classes

create-small-gt:
	./bin/create-small-gt

install-infty-data:
	./bin/install-infty-data 
	./bin/merge-symbol-classes
	./bin/modify-punc-edges

install-crohme-data:
	./bin/install-crohme-data

# 'make test-inf' - run test on INFTY test set
test-inf:
	./run-parser --config ./config/infty.yaml 

test-acl:
	./run-parser \
	--config config/infty.yaml \
    --img_dir data/ACL/images \
    --tsv_dir data/ACL/tsv \
    --given_sym 1 \
    --model_name infty_contour \
    --output_dir outputs/ACL \
    --last_epoch 89 \
    --dot_dir outputs/ACL/dot \
    --visualize 1

test-acl-1gpu:
	./run-parser \
        --config config/infty.yaml \
      --img_dir data/ACL/images \
      --tsv_dir data/ACL/tsv \
      --given_sym 1 \
      --model_name infty_contour \
      --output_dir outputs/ACL1gpu \
      --last_epoch 89 \
      --dot_dir outputs/ACL/dot \
      --visualize 1 \
	--num_gpu single

test-inf-1gpu:
	./run-parser --config ./config/infty.yaml --num_gpu single

# 'make test-inf-small' - run test on small INFTY test set
test-inf-small:
	./run-parser --config ./config/infty.yaml --img_list data/testing_small.txt

test-inf-small-1gpu:
	./run-parser --config ./config/infty.yaml --img_list data/testing_small.txt --num_gpu single

# 'make test-crohme' - run test on INFTY test set
test-crohme:
	./run-parser --config ./config/crohme.yaml 

# 'make test-inf-small' - run test on small INFTY test set
# test-crohme-small:
# 	./run-parser --config ./config/crohme.yaml --data_dir data/test2019_small

# 'make train-inf' - run train on INFTY train set
train-inf:
	./train-parser --config ./config/infty.yaml 

# Train from beginning (0 epoch)
train-inf-0:
	./train-parser --config ./config/infty.yaml --last_epoch -1

# 'make train-inf-small' - train train on small INFTY train set
train-inf-small:
	./train-parser --config ./config/infty.yaml --img_list data/training_small.txt

train-inf-small-new-single:
	CUDA_VISIBLE_DEVICES=0 ./train-parser --config config/defaultINFTY.yaml -m infty_posenc_bb_32x31_detSP -o outputs/run_test -r config/run.yaml -op 1 -le -1 -i data/training_small.txt -n multi -sh 0

# Train from beginning (0 epoch)
train-inf-small-0:
	./train-parser --config ./config/infty.yaml --img_list data/training_small.txt --last_epoch -1

# 'make train-crohme' - train train on INFTY train set
train-crohme:
	./train-parser --config ./config/crohme.yaml 

# Train from beginning (0 epoch)
train-crohme-0:
	./train-parser --config ./config/crohme.yaml --last_epoch -1

train-chem-pubchem-single:
	CUDA_VISIBLE_DEVICES=0 ./train-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -o outputs/run_pubchem_3416 -r config/run.yaml -op 1 -le -1 

train-chem-small-single-noshuffle:
	CUDA_VISIBLE_DEVICES=0 ./train-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -o outputs/run_small -r config/run.yaml -op 1 -le -1 -l data/generated-dataset/chem/train/lgs/line -dd data/generated-dataset/chem/train/images/ -i data/training_chem.txt -sh 0

train-chem-small-single:
	CUDA_VISIBLE_DEVICES=0 ./train-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -o outputs/run_small -r config/run.yaml -op 1 -le -1 -l data/generated-dataset/chem/train/lgs/line -dd data/generated-dataset/chem/train/images/ -i data/training_chem.txt

train-chem-small-single-vis:
	CUDA_VISIBLE_DEVICES=0 ./train-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -o outputs/run_small -r config/run.yaml -op 1 -le -1 -l data/generated-dataset/chem/train/lgs/line -dd data/generated-dataset/chem/train/images/ -i data/training_chem.txt -vis 1

train-chem-small:
	CUDA_VISIBLE_DEVICES=0 ./train-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -o outputs/run_small -r config/run.yaml -op 1 -le -1 -l data/generated-dataset/chem/train/lgs/line -dd data/generated-dataset/chem/train/images/ -i data/training_chem.txt

test-chem-small-single:
	CUDA_VISIBLE_DEVICES=0 ./run-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -md outputs/run_small -o outputs/run_small -r config/run.yaml -op 1 -le 19 -lo 1 -w 4 -l data/generated-dataset/chem/test/lgs/line -dd data/generated-dataset/chem/test/images -i data/testing_chem.txt -rec 0

test-chem-small-single-train:
	CUDA_VISIBLE_DEVICES=0 ./run-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -md outputs/run_pubchem_3416 -o outputs/run_small -r config/run.yaml -op 1 -le 19 -lo 1 -w 4 -l data/generated-dataset/chem/train/lgs/line -dd data/generated-dataset/chem/train/images/ -i data/training_chem.txt -rec 0

test-chem-small:
	./run-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1 -md outputs/run_small -o outputs/run_small -r config/run.yaml -op 1 -le 99 -lo 1 -rec 0

test-chem-clef-small:
	CUDA_VISIBLE_DEVICES=0 ./run-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -md outputs/run_pubchem_3416 -o outputs/run_pubchem_3416_clef_small -r config/run.yaml -op 1 -le 19 -lo 1 -w 4 -l data/generated-dataset/chem/test_clef_992/lgs/line -dd data/generated-dataset/chem/test_clef_992/images -i data/testing_clef_small.txt -rec 0

test-chem-clef:
	CUDA_VISIBLE_DEVICES=0 ./run-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -md outputs/run_pubchem_3416 -o outputs/run_pubchem_3416_clef_992 -r config/run.yaml -op 1 -le 19 -lo 1 -w 4 -l data/generated-dataset/chem/test_clef_992/lgs/line -dd data/generated-dataset/chem/test_clef_992/images -i data/testing_clef.txt -rec 0

test-chem-indigo-small:
	CUDA_VISIBLE_DEVICES=0 ./run-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -md outputs/run_pubchem_3416 -o outputs/run_pubchem_3416_indigo_small -r config/run.yaml -op 1 -le 19 -lo 1 -w 4 -l data/generated-dataset/chem/test_indigo_5704/lgs/line -dd data/generated-dataset/chem/test_indigo_5704/images -i data/testing_indigo_small_100.txt -rec 0

test-chem-indigo:
	CUDA_VISIBLE_DEVICES=0 ./run-parser --config config/defaultINFTY.yaml -m chem_line_SP_recurrent_6NN_28x28_32x7_prune_rel_dropout_0.1_resnet_k3_s1_p_same_simple_dataloader_wd0_nogat_nomerge_linefixed_oversample_relseg_pubchem -md outputs/run_pubchem_3416 -o outputs/run_pubchem_3416_indigo_5704 -r config/run.yaml -op 1 -le 19 -lo 1 -w 4 -l data/generated-dataset/chem/test_indigo_5704/lgs/line -dd data/generated-dataset/chem/test_indigo_5704/images -i data/testing_indigo.txt -rec 0

# 'make train-inf-small' - train train on small INFTY train set
# train-crohme-small:
# 	./train-parser --config ./config/crohme.yaml --data_dir data/train_small

clean: clean-out
	rm -f  ./outputs/run/infty_contour/${LGAPIW}
	rm -f  ./outputs/run/3branchAtt_REL_mode2_4CH/${LGAPCW}
	rm -f  ./run-parser
	rm -f  ./train-parser
	rm -r -f  ./lgeval
	rm -r -f ./protables
	rm -f ./run-server

# 'make clean-out' remove only output data from running examples
clean-out:
	rm -fr ./outputs/run/*.tsv

# 'make conda-remove' - remove mathseer conda environment
conda-remove:
	conda env remove -n lgap

# 'make rebuild' - cleans and reinstalls pipeline
rebuild: conda-remove clean all

tests:
	pytest -vv tests/*

test_edmonds:
	pytest -s tests/test_branchings_custom.py
