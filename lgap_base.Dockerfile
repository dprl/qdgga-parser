# this is the base docker image for the scanssd image. it exist because it takes a really long time to build
# rebuilding it every time lgap is rebuild would waste our gitlab ci/cd minutes
#

# docker build -f lgap_base.Dockerfile . -t dprl/lgap_base:latest
# docker push dprl/lgap_base:latest

FROM continuumio/miniconda3

RUN apt-get update
RUN apt install build-essential -y --no-install-recommends
RUN apt-get install libenchant-2-dev ffmpeg libsm6 libxext6 -y

COPY requirements_new.txt requirements_new.txt
COPY bin/install_container bin/install_container


RUN ./bin/install_container

