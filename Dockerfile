FROM dprl/lgap-base:latest

COPY . .
RUN make
ENV PYTHONPATH="/workspace/:/workspace/protables:/workspace/lgeval:"
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN apt-get update
RUN apt-get install libenchant-dev -y

CMD ["./bin/start-server-docker"]
