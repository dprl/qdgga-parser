# LGAP

Line-of-sight with Graph Attention Parser (LGAP) for Math Formulas

**Authors:** Ayush Kumar Shah, Bryan Amador, Abhisek Dey, Matt Langsenkamp, Richard Zanibbi
([Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html), Rochester Institute of Technology, USA)

## Version 1.0 - Second Release, June 2024

Note that LGAP evolved from Query-Driven Global Graph Attention (QD-GGA) parser,
which was developed by
Madhshad Madhavi, Leilei Sun, and Richard Zanibbi.

**System Description and Citation.** A description of the system, which you should
cite if you use LGAP in your own work, is available:

    @InProceedings{10.1007/978-3-031-41734-4_25,
    author="Shah, Ayush Kumar and Zanibbi, Richard",
    editor="Fink, Gernot A. and Jain, Rajiv and Kise, Koichi and Zanibbi, Richard",
    title="Line-of-Sight with Graph Attention Parser (LGAP) for Math Formulas",
    booktitle="Document Analysis and Recognition - ICDAR 2023",
    year="2023",
    publisher="Springer Nature Switzerland",
    address="Cham",
    pages="401--419",
    isbn="978-3-031-41734-4"
    }

Version 0.1 - First Release, August 2021

## Datasets

Download both the `INFTY` and `CROHME` datasets using the command:

```zsh
make install-data
```

**i. CROHME dataset - handwritten formula strokes**

<!-- Download the dataset from here [CROHME 2019](http://tc11.cvc.uab.es/datasets/ICDAR2019-CROHME-TDF_1). -->

<!-- Unzip the train and test sets and place the files under `data/` to make --> 
<!-- CROHME 2019 dataset available for use. -->

For downloading the CROHME dataset only,

```zsh
make install-crohme-data
```

<details>

<summary>
Dataset description
</summary>

We use CROHME 2019 dataset which contains handwritten mathematical expressions. 
The ground truth files are provided in InkML format (an XML tag-based 
representation) and Label Graph files (LG files), which have a simpler 
CSV-based representation.

The train set has 9993 math expressions and the test set contains 1199 formulas.
</details>

<!-- TODO: Add model weights to dprl server and install script -->
<!-- Download the saved model weight [net_79.pt](https://drive.google.com/file/d/1Mhf5SAciEGLMjrhFgv8e_zpLqJ_fEgDQ/view?usp=sharing) -->
<!-- for the model `3branchAtt_REL_mode2_4CH` --> 
<!-- into the folder `outputs/run/3branchAtt_REL_mode2_4CH/`. -->

**ii. Infty dataset - typeset formula images**

<!-- Download the dataset from here [InftyMCCDB-2 dataset](https://zenodo.org/record/3483048#%23.XaCwmOdKjVo) and unzip -->
<!-- `IMG.zip`, `LG.zip`, and `LG_test.zip` into `./data/infty/`. -->

For downloading the INFTY dataset only,

```zsh
make install-infty-data
```

<details>

<summary>
Dataset description
</summary>

We use InftyMCCDB-2 dataset, which is a modified version of InftyCDB-2. It contains
mathematical expressions from scanned article pages.

The train set has 12551 typeset formula images and the test set contains 6830 
images. The two sets
have approximately the same distribution of symbol classes and relation classes. 
The expressions range in size from a single symbol to more than 75 symbols,
with an average of 7.33 symbols per expression.

</details>

## Installation

**Requirements**

1. Python
2. Conda (e.g., from Anaconda) and pip for Python environments
3. wget unix utility
4. libenchant-dev: needed for dictionary-based word filtering, available through linux package managers (e.g., `apt`) -- Ubuntu 1.6.0-11.3build1 amd64 used in our work to date. 

The parser has been tested primarily on Linux systems (specifically, Ubuntu 20.04 LTS).

Once you have these requirements installed, from the top directory of the project, 
you run the installation script by issuing:

```zsh
$ make
```

This will install all modules, conda environment (lgap), download model weights, and create bash scripts 
named `run-parser` and `train-parser`, which
can be used for testing and training the parser respectively (see instructions below).

To force the installation to continue after encountering errors, issue:

```zsh
$ make force
```

### Removal and Reinstallation
To remove all installed data and tools, issue:

```zsh
$ make clean
```
This is particularly useful if you want to rebuild the system from scratch ('cleanly').

To rebuild the entire system, including the conda environment, issue:

```zsh
$ make rebuild
```

### run-parser (test parser program)

After the program has been installed, the script `run-parser` is created. 
You can run this without arguments to see the command line
arguments and options. Examples and details are provided below.

### train-parser (train parser program)

After the program has been installed, the script `train-parser` is created. 
You can run this without arguments to see the command line
arguments and options. Examples and details are provided below.

### Installing Manually

If you experience problems using the installation script, or would prefer to install manually, 
here are some more detailed instructions for installation. 

<details>
<summary>
1. Conda installation
</summary>

From the root directory, issue the commands:

```zsh
$ conda create -n lgap python=3.6.9
$ conda activate lgap
(lgap) $ pip install -r requirements.txt
```
</details>

<details>
<summary>
2. Download Neural Network Weights
</summary>

The installation script automatically downloads the network
weights needed from the dprl@RIT, and then move the files to the appropriate directories.

**If you encounter problems**, the installation script (`install`) may be read and modified using any standard text editor. 

</details>

## Getting Started: Testing and training the parser (INFTY and CROHME data)

**Please Note:** The required training and testing dataset needs to be
downloaded and be available in the directory `./data/` as instructed above.

### Testing the parser

**Example Execution.**  If you were able to create the `run-parser` program 
using the `install` script, to run (test) the parser, you can issue:

*INFTY Dataset*

```zsh
$ make test-inf
```

*CROHME Dataset*
```zsh
$ make test-crohme
```

In the `Makefile` you can see commands for other (smaller) examples as well.

**Using run-parser.** `make test-inf` is equivalent to issuing the following:

*INFTY Dataset*
```zsh
$ ./run-parser --config config/infty.yaml
```

For specific model,
```zsh
$ ./run-parser --config config/infty.yaml --model_name model_name --last_epoch <value>
```

while `make test-crohme` is equivalent to:

*CROHME Dataset*
```zsh
$ ./run-parser --config config/crohme.yaml
```

For specific model,
```zsh
$ ./run-parser --config config/infty.yaml --model_name model_name --last_epoch <value>
```

which passes the corresponding dataset and writes tsv results to the `outputs/run`
directory by default.

#### Summary of Command-Line Arguments

* (Required) `config:` default configuration to be used - inkml.yaml or
    crohme.yaml

**Optional Parameters**

* `output_dir:` set the output directory for tsv output

* `data_dir:` set the directory containing input inkml files (CROHME)

* `model_dir:` set the directory containing the saved model weights

* `img_list:` set the path to the file containing list of images to be used for INFTY for trainig

* `lg_dir:` set the directory containing lg inputs or ground truths
 
* `given_sym:` set the flag to use given symbols or not
 
* `batch_size:` set the batch size for ScanSSD (formula detection). 

* `model_name:` parser network to use

* `last_epoch:` epoch number to use to defined LGAP network model weights

* `num_gpu:` Specify whether to use single or multiple GPUs

* `seed:` set the seed to be used for testing 

### Evaluating the test results

After running the test scripts, we use LgEval to evaluate the results. To get
the summary metrics, perform the following commands

```zsh
$ conda activate lgap
$ python src/utils/tsv2Lg.py outputs/run/model_name/model-out.tsv outputs/run/model_name/lgoutputs_model_name
$ evaluate outputs/run/model_name/lgoutputs_model_name data/InftyMCCDB-2/test_merged_class
```
> Note: Replace model_name by the name of model whose results you want to evaluate

### Training the parser

If you want to use the feature to log the losses during training to `weights and biases framework`,
first create an account using this [SignUp Link](https://wandb.ai/login).

Then run the following commands to login in your system:
```zsh
$ conda activate lgap
$ wandb login
```

Go to [Settings page](https://wandb.ai/settings) and copy the API key to the
shell.


**Example Execution.**  If you were able to create the `train-parser` program 
using the `install` script, to train the parser, you can issue:

*INFTY Dataset*

```zsh
$ make train-inf-0
```

For continuing training from last saved weight:

```zsh
$ make train-inf
```

*CROHME Dataset*
```zsh
$ make train-crohme-0
```

For continuing training from last saved weight:

```zsh
$ make train-crohme
```

In the `Makefile` you can see commands for other (smaller) examples as well.

**Using train-parser.** `make train-inf-0` is equivalent to issuing the following:

To enable logging during training, set the flag `--visualize_loss 1` or `-vis 1` with any of
the training commands below

*INFTY Dataset*
```zsh
$ ./train-parser --config config/infty.yaml --last_epoch 0
```
while `make train-crohme-0` is equivalent to:

*CROHME Dataset*
```zsh
$ ./train-parser --config config/crohme.yaml --last_epoch 0
```

**Using train-parser.** `make train-inf` is equivalent to issuing the following:

*INFTY Dataset*
```zsh
$ ./train-parser --config config/infty.yaml
```
while `make train-crohme` is equivalent to:

*CROHME Dataset*
```zsh
$ ./train-parser --config config/crohme.yaml
```

The training script takes the corresponding input dataset and saves network weights
to `outputs/run/MODEL_NAME/`
directory by default. Here, 
- MODEL_NAME = 3branchAtt_REL_mode2_4CH for CROHME dataset and 
- MODEL_NAME = infty_contour for INFTY dataset.

#### Summary of Command-Line Arguments

* (Required) `config:` default configuration to be used - inkml.yaml or
    crohme.yaml

**Optional Parameters**

* `output_dir:` set the output directory for model weights output

* `data_dir:` set the directory containing input inkml files (CROHME)
 
* `img_list:` set the path to the file containing list of images to be used for INFTY for trainig

* `lg_dir:` set the directory containing lg inputs or ground truths
 
* `given_sym:` set the flag to use given symbols or not
 
* `batch_size:` set the batch size for ScanSSD (formula detection). 

* `model_name:` parser network to use

* `last_epoch:` epoch number to use to defined LGAP network model weights

* `num_gpu:` Specify whether to use single or multiple GPUs

* `seed:` set the seed to be used for training

* `visualize_loss:` set the flag to log losses during training to weights and biases





## Contributors

The LGAP parser was developed by the [Document and Pattern Recognition Lab (dprl)](https://www.cs.rit.edu/~dprl/index.html) 
at the Rochester Institute of Technology (USA), for the [MathSeer](https://www.cs.rit.edu/~dprl/mathseer) project.

### Pipeline Update and Release Contributors

* **LGAP:** Ayush Kumar Shah
* **Documentation:** Auyush Kumar Shah, Matt Langsenkamp, and Richard Zanibbi

First LGAP release was made in August 2021.

### Additional Contributors 

The following people made contributions between 2016 and 2020 while they were members of the dprl lab.

* **LGAP:** Wei Zhong, Kenny Davila, Michael Condon, Mahshad Mahdavi, Alex Keller

## Support

This material is based upon work supported by the National Science Foundation (USA) under Grant Nos. IIS-1016815, IIS-1717997, and 2019897 (MMLI), and the Alfred P. Sloan Foundation under Grant No. G-2017-9827.

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the Alfred P. Sloan Foundation.

<!-- By default, the script looks for the test dataset containing the inkml files --> 
<!-- in the directory `data/test2019/`. However, this can be modified using the argument -->
<!-- `--data_dir` or `-dd`. -->

<!-- ```python -->
<!-- python test.py --config config/crohme.yaml -->
<!-- ``` -->

<!-- If you want to over-ride the default configurations, use the following script --> 
<!-- and change the argument's values as required. -->

<!-- ```python -->
<!-- python test.py \ -->
<!-- --config config/crohme.yaml \ -->
<!-- --data_dir data/test2019 \ -->
<!-- --use_gt 0 \ -->
<!-- --output_dir outputs/run/ \ -->
<!-- --model_dir outputs/run/ \ -->
<!-- --model_name 3branchAtt_REL_mode2_4CH \ -->
<!-- --last_epoch 79 \ -->
<!-- --given_sym 0 -->
<!-- ``` -->

<!-- Using short arguments: -->

<!-- ```python -->
<!-- python test.py \ -->
<!-- -c config/crohme.yaml -->
<!-- -dd data/test2019 \ -->
<!-- -u 0 \ -->
<!-- -o outputs/run/ \ -->
<!-- -md outputs/run/ \ -->
<!-- -m 3branchAtt_REL_mode2_4CH \ -->
<!-- -le 79 \ -->
<!-- -g 0 -->
<!-- ``` -->

<!-- ### Infty dataset - typeset formula images -->

<!-- By default, the script looks for the test dataset containing the typeset formula images --> 
<!-- in the directory `data/infty/IMG/` and the corresponding lg files in the --> 
<!-- directory `data/infty/LG_test`. However, this can be modified using the argument -->
<!-- `--data_dir` or `-dd` and `--lg_dir` or `-l` respectively. -->

<!-- The file `data/infty/Testing.txt` contains a list of typeset images, a subset of files -->
<!-- contained in the data directory, for which the predictions are to be generated. Both the -->
<!-- content and location of the file -->
<!-- can be modified as needed using the argument `--img_list` or `-i` -->

<!-- ```python -->
<!-- python test.py --config config/infty.yaml -->
<!-- ``` -->

<!-- If you want to over-ride the default configurations, use the following script --> 
<!-- and change the argument's values as required. -->

<!-- ```python -->
<!-- python test.py \ -->
<!-- --config config/infty.yaml \ -->
<!-- --data_dir data/infty/IMG \ -->
<!-- --lg_dir data/infty/LG_test_corrected \ -->
<!-- --use_gt 1 \ -->
<!-- --output_dir outputs/run/ \ -->
<!-- --img_list data/infty/Testing.txt \ -->
<!-- --model_dir outputs/run/ \ -->
<!-- --model_name infty_contour \ -->
<!-- --last_epoch 89 \ -->
<!-- --given_sym 0 -->
<!-- ``` -->

<!-- Using short arguments: -->

<!-- ```python -->
<!-- python test.py \ -->
<!-- -c config/infty.yaml \ -->
<!-- -dd data/infty/IMG \ -->
<!-- -l data/infty/LG_test_corrected \ -->
<!-- -u 1 \ -->
<!-- -o outputs/run/ \ -->
<!-- -i data/infty/Testing.txt \ -->
<!-- -md outputs/run/ \ -->
<!-- -m infty_contour \ -->
<!-- -le 89 \ -->
<!-- -g 0 -->
<!-- ``` -->

<!-- ## Running the training -->

<!-- Make sure you are inside the `mathseer` conda environment. -->

<!-- ```zsh -->
<!-- conda activate mathseer -->
<!-- ``` -->

<!-- ### CROHME dataset - handwritten formula strokes -->

<!-- By default, the training script looks for the dataset containing the inkml files --> 
<!-- in the directory `data/train/`. -->

<!-- ```python -->
<!-- python train.py --config config/crohme.yaml -->
<!-- ``` -->

<!-- If you want to over-ride the default configurations, use the following script --> 
<!-- and change the argument's values as required. -->

<!-- ```python -->
<!-- python train.py \ -->
<!-- --config config/crohme.yaml \ -->
<!-- --data_dir data/train \ -->
<!-- --output_dir outputs/run/ \ -->
<!-- --model_name 3branchAtt_REL_mode2_4CH \ -->
<!-- --last_epoch 79 -->
<!-- ``` -->

<!-- Using short arguments: -->

<!-- ```python -->
<!-- python train.py \ -->
<!-- -c config/crohme.yaml \ -->
<!-- -dd data/train \ -->
<!-- -o outputs/run/ \ -->
<!-- -m 3branchAtt_REL_mode2_4CH \ -->
<!-- -le 79 -->
<!-- ``` -->

<!-- ### Infty dataset - typeset formula images -->

<!-- By default, the training script looks for the train dataset containing the typeset formula images --> 
<!-- in the directory `data/infty/IMG/` and the corresponding lg files in the --> 
<!-- directory `data/infty/LG`. However, this can be modified using the argument -->
<!-- `--data_dir` or `-dd` and `--lg_dir` or `-l` respectively. -->

<!-- The file `data/infty/Training.txt` contains a list of typeset images, a subset of files -->
<!-- contained in the data directory, which will be used for training. Both the -->
<!-- content and location of the file -->
<!-- can be modified as needed using the argument `--img_list` or `-i` -->

<!-- ```python -->
<!-- python train.py --config config/infty.yaml -->
<!-- ``` -->

<!-- If you want to over-ride the default configurations, use the following script --> 
<!-- and change the argument's values as required. -->

<!-- ```python -->
<!-- python train.py \ -->
<!-- --config config/infty.yaml \ -->
<!-- --data_dir data/infty/IMG \ -->
<!-- --lg_dir data/infty/LG \ -->
<!-- --output_dir outputs/run/ \ -->
<!-- --img_list data/infty/Training.txt \ -->
<!-- --model_name infty_contour \ -->
<!-- --last_epoch 89 -->
<!-- ``` -->

<!-- Using short arguments: -->

<!-- ```python -->
<!-- python train.py \ -->
<!-- -c config/infty.yaml \ -->
<!-- -dd data/infty/IMG \ -->
<!-- -l data/infty/LG \ -->
<!-- -o outputs/run/ \ -->
<!-- -i data/infty/Training.txt \ -->
<!-- -m infty_contour \ -->
<!-- -le 89 -->
<!-- ``` -->

